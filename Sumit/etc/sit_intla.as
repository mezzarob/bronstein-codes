-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
#include "sumit"

macro {
       I==MachineInteger;
       A==PrimitiveArray;
}

IntegerLinearAlgebra(Z:IntegerCategory, M:MatrixCategory Z): with {
      determinant: M -> Partial Z;
      hadamard: M -> Z;
} == add {
    
      determinant(m:M): Partial Z == {
         import from Z, I, A I, A A I;
         import from  ModulopGaussElimination, ChineseRemaindering Z;
	 import from Boolean, LazyHalfWordSizePrimes;

         B:= 2*hadamard(m);
         N:Z := 1;
         det:Z :=0;
         firststep:=true;
         n := numberOfRows m;
         assert(n=numberOfColumns m);

         mp: A A I := new n;
         i:I := 0;
         while i<n repeat { mp.i := new n; i:= next i;}
	 nbtour:I :=0;
	-- main parallelizable loop
         for p in allPrimes while N<B repeat {
	   nbtour:=nbtour+1;
           i:I :=1;

	   -- 1. reduction mod p
           while i<=n repeat {
	      j:I := 1;
	      while j<=n repeat {
               	mp.(i-1).(j-1) := m(i,j) mod p;
                j:= next j;}
              i:= next i;
	   }

	   -- 2. determinant mod p
           d :=determinant!(mp,n,p);
           if d > (p quo 2) then d := d - p;

	   -- 3. combination step  of pairs [d, p]
           if firststep then {
             det:=d::Z;
             firststep:=false;
           }		
           else
             det := combine(N,p)(det,d);

	   N:=N*p::Z;
         }
        N>=B => [det];
        failed;
      }

      hadamard(m:M): Z == {
         import from I;
         n:=numberOfRows m;

         assert(square? m);
         
         rn:Z := rNorm(m,1,n);
         cn:Z := cNorm(m,1,n);
         i:I := 2;
         while i<=n repeat {
            rn:=rn*rNorm(m,i,n);
            cn:=cn*cNorm(m,i,n);
            i:=next i;
         }

         (exact?, hb):=nthRoot(min(rn,cn),2);
         exact? => hb;
         next hb;
      }

      local rNorm(m:M,i:I,n:I):Z == {
         import from I;
         Nm:Z := m(i,1)^2;
         j:I := 2;
         while j<=n repeat {
            Nm:=Nm + m(i,j)^2;
            j:=j+1;}
         Nm;
      }

      local cNorm(m:M,i:I,n:I):Z == {
         import from I;
         Nm:Z := m(1,i)^2;
         j:I := 2;
         while j<=n repeat {
          Nm:=Nm + m(j,i)^2;
          j:=j+1;}
         Nm;
      }
}

