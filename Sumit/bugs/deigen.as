-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
#include "sumit"
#include "aldorio"
#include "stdmac"

import from Symbol;

macro PZX == Product ZX;

local getop():ZXD == {
	import from Z;
	x:ZX := monom;
	D:ZXD := monom;
-- 	L := x^2*(x^4-2::ZX)*D^5-2*x*(3*x^4-2::ZX)*D^4
-- 		+(-x^8+16*x^4-4::ZX)*D^3-x^3*(x^4-2::ZX)*D^2+4*x^6*D
-- 		+(x^5*(x^4-10::ZX))::ZXD;
	L := x^3*(x-1)^3*D^6-3*x^2*(2*x-1)*(x-1)^2*D^5
		+6*x*(x-1)*(3*x^2-3*x+1)*D^4
		+(-x^8+2*x^7-2*x^5+x^4-24*x^3+36*x^2-24*x+6::ZX)*D^3
		+3*x^4*(x-1)^2*D^2-6*x^4*(x-1)*D
		+x^4*(x^5-3*x^4+3*x^3-x^2+6::ZX)::ZXD;
	L;
}

main():() == {
	import from Q, ZX, V ZX, V QXD, PZX,
		LinearOrdinaryDifferentialEigenring(Z,Q,coerce,ZX,ZXD,QX,QXD);
	L := getop();
	TIMESTART;
	-- (d, ignore, num) := matrixEigenring L;
	-- TIME("matrix eigenring at: ");
	-- stdout << "d = " << d << endnl;
	-- stdout << "num = " << num << endnl;
	(den, num) := eigenring L;
	TIME("eigenring at: ");
	stdout << "den = " << den << endnl;
	stdout << "num = " << num << endnl;
}

main();


