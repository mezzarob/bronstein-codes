-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ compfault.as ------------------------
--
-- Either  axiomxl -q2 compfault.as
--     or  axiomxl -q1 -fc compfault.as
-- causes the compiler to seg-fault
--

#include "sumit"

macro {
	Z	== Integer;
	FR	== FractionalRoot;
	NP	== NewtonPolygon(R, RX, RXD);
	F	== FractionBy(R, lc, false$Boolean);
	FX	== DenseUnivariatePolynomial F;
}

CompilerFault(R: Join(GcdDomain, RationalRootRing),
	RX: UnivariatePolynomialCategory R,
	RXD:LinearOrdinaryDifferentialOperatorCategory RX): with {
		analyze: (RXD, RX) -> List FR Z;
} == add {
	import from Boolean;

	analyze(L:RXD, a:RX):List FR Z == {
		import from NP;
		analyzeRatQ(newtonPolygon(L, a), leadingCoefficient a);
	}

	local analyzeRatQ(N:NP, lc:R):List FR Z == {
		import from Boolean, Z, F, RX, FX;
		import from MonogenicAlgebra2(R, RX, F, FX);
		import from NewtonPolygonEquations(R, RX, RXD, F, FX);
		a := shift((-coefficient(center N, 0))::F, -1);
		rationalExponents(N, (q:RX):F +-> (map(coerce)(q))(a));
	}
}

