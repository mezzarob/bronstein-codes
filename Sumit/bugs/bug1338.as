-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------------- oddBInt.as -------------------------
--
-- The function BIntIsOdd in the runtime is poorly implemented:
-- it does an allocation for the BInt 2 and a full euclidean division
-- as shown in the C code for foo below.
-- Instead, it should test the lsb without any allocation
-- as shown in the C code for bar.
-- GMPInteger from libaldor does the right thing,
-- but AldorInteger reroutes odd? to BIntIsOdd.
--

#include "axllib"

import from Machine;

foo?(n:BInt):Bool == odd? n;

-- static FiBool
-- CF1_foo_QMARK_(FiEnv e1, FiBInt P0_n)
-- {
--         return fiBIntNE(fiBIntMod(P0_n, fiBIntNew(2L)), fiBInt0());
-- }


bar?(n:BInt):Bool == { import from SInt; bit(n, 0); }

-- static FiBool
-- CF2_bar_QMARK_(FiEnv e1, FiBInt P0_n)
-- {
--         FiBool T0;
--         fiBINT_BIT(T0, FiBool, P0_n, 0L);
--         return T0;
-- }


