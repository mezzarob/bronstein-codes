-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
----------------------------- symb.as ------------------------------
--
-- YOU NEED SALLI 0.1.12a IN ORDER TO REPRODUCE THIS BUG
--   (available from http://www.inria.fr/cafe/Manuel.Bronstein/salli.html
--
-- Yet another optimizer bug:
--
-- % axiomxl -q1 -ginterp symb.as
-- foo
--
-- % axiomxl -q2 -ginterp symb.as
-- Program fault (segmentation violation).
--

#include "salli"

macro {
	Z	== MachineInteger;
	H	== HashTable(String, String);
	SIZE	== 16384;
}

Symbol: Join(PrimitiveType, OutputType) with { -:  String -> % } == add {
	Rep == String;

	local stable:H		== { import from Z, String; table SIZE }
	local enter(s:String):%	== { stable.s := s; per s }
	(s:%) = (t:%):Boolean	== { import from Pointer; address s = address t}
	(p:TextWriter) << (s:%):TextWriter	== { import from Rep;p << rep s}
	local address(s:%):Pointer		== s pretend Pointer;

	-(s:String):% == {
		import from H, Partial String;
		failed?(u := find(stable, s)) => enter copy s;
		per retract u;
	}

}

import from String, Symbol, TextWriter, Character;

s := - "foo";
stdout << s << newline;

