-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------- optbug.as -------------------------------
--
-- YOU NEED SALLI 0.1.12c IN ORDER TO REPRODUCE THIS BUG
--   (available from http://www.inria.fr/cafe/Manuel.Bronstein/salli.html)
--
-- Here is the C-code generated for = at -q2:
-- static FiWord
-- CF5__EQ_(FiEnv e1, FiWord P0_a, FiWord P1_b)
-- {
--         FiWord T0_bb, T1_aa;
--         PFmt8 l1;
--         l1 = (PFmt8) fiEnvLevel(e1);
--         T1_aa = fiCCall1(FiWord, l1->X3_first, P0_a);
--         T0_bb = fiCCall1(FiWord, l1->X3_first, P1_b);
--         return fiCCall2(FiWord, l1->X5__EQ_, T1_aa, T0_bb);
-- }
--
-- And here is the C-code generated for = at -q3:
-- static FiWord
-- CF5__EQ_(FiEnv e1, FiWord P0_a, FiWord P1_b)
-- {
--         FiBool T2;
--         FiWord T0_bb, T1_aa;
--         PFmt8 l1;
--         l1 = (PFmt8) fiEnvLevel(e1);
--         T1_aa = fiCCall1(FiWord, l1->X3_first, P0_a);
--         T0_bb = fiCCall1(FiWord, l1->X3_first, P1_b);
--         fiBINT_EQ(T2, FiBool, (FiBInt) T1_aa, (FiBInt) T0_bb);
--         return (FiWord) T2;
-- }
--
-- THE CALL TO fiBINT_EQ ON OBJECTS OF TYPE 'R' WITH A CAST TO (FiBInt)
-- CAUSES EITHER SEGMENTATION FAULTS OR WRONG RESULTS, DEPENDING ON YOUR LUCK
--

#include "salli"

macro Z	== Integer;

Foo(R:PrimitiveType): PrimitiveType with { foo: R -> % } == add {
	macro Rep == List R;

	import from Rep;

	foo(x:R):% == per [x];

	(a:%) = (b:%):Boolean == {
		import from R;
		aa := first rep a;
		bb := first rep b;
		aa = bb;
	}
}

