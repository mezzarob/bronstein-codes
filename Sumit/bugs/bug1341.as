-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-------------------------------- optbug.as -----------------------------
--
-- Yet another optimized bug, somewhere in sit_dmp1 or sit_dmp0
--
-- % aldor -ginterp optbug.as
-- Here I was ... 1
-- Program fault (segmentation violation).#2 (Error) Program fault (seg...)
--
-- % aldor -ginterp -dDEBUG optbug.as
-- Here I was ... 1
-- Here I was ... 2
-- Here I was ... 3
-- x^5 + 5*x^4*y + ...
--

#include "algebra"
#include "aldorio"

import from String, Symbol;

macro V == OrderedVariableTuple ( -"x", -"y", -"z");

import from MachineInteger, Integer, V;

MI == MachineInteger;
I == Integer;

macro E == MachineIntegerDegreeLexicographicalExponent(V);
macro P == DistributedMultivariatePolynomial1(Integer, V, E);

x: P := variable(1)$V :: P;
y: P := variable(2)$V :: P;
z: P := variable(3)$V :: P;

stdout << "Here I was ... 1" << newline;

p: P := (1+x+y+z)^(5::MI);

stdout << "Here I was ... 2" << newline;

p1 := p+1;

stdout << "Here I was ... 3" << newline;

stdout << p1 << newline;
