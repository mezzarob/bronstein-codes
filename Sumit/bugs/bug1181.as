-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-------------------------------- pcpc.as -----------------------------
--
-- The following must be separated into 2 files in order to reproduce
-- the bug. Split the bottom part as pcpcbug.as, then:
-- % axiomxl pcpc.as
-- % axiomxl pcpcbug.as
-- "pcpcbug.as", line 7: f(T:Bar, x:T):Integer == bar x;
--                       .............................^
-- [L7 C30] #1 (Error) Argument 1 of `bar' did not match any possible parameter
--                     type.
--     The rejected type is T.
--     Expected type %.
--
-- The bug disappears if Integer is not extended,
-- or if 'foo$%' is replaced by 'foo' in the default definition of #
--

#include "axllib"

define Foo: Category == Finite with { foo: Integer };

define Bar: Category == Foo with {
		bar: % -> Integer;
		default #:Integer == foo$%;
}

define Baz: Category == with { baz: % -> % };

extend Integer:Baz == add { baz(x:%):% == x };

-------------------------------- pcpcbug.as -----------------------------
#include "axllib"

#library lib "pcpc.ao"
import from lib;

f(T:Bar, x:T):Integer == bar x;

