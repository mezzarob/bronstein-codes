-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------- nilbug.as ----------------------------------
--
-- THIS MIGHT BE CONNECTED TO BUG892 (still open)
-- % axiomxl -fx nilbug.as
-- % nilbug
-- Looking in NilRecord((SingleInteger, String)) for coerce with code 493023431
-- Export not found
--
-- IN 1.1.13p1 THIS BECOMES BUG1231 (still open)
-- % axiomxl -m2 bug1108.as
-- "bug1108.as", line 17: } == add {
--                        .....^
-- [L17 C6] #3 (Error) The domain is missing some exports.
--         Missing coerce: Record(T) -> %
-- 
-- "bug1108.as", line 24: 
--         coerce(r:Record T):%    == per(r pretend Pointer);
-- ...............^...........^
-- [L24 C16] #2 (Error) Have determined 1 possible types for the expression.
--         Meaning 1: (r: Record(T)) -> %
--   The context requires an expression of type (r: Record(T)) -> %.
-- [L24 C28] #1 (Error) No meaning for identifier `%'.
--

#include "axllib"

NilRecord(T:Tuple Type): with {
	coerce: Record T -> %;
	nil: %;
	nil?: % -> Boolean;
	record: % -> Record T;
} == add {
	macro Rep == Pointer;

	import from Rep;

	nil?(r:%):Boolean	== nil? rep r;
	nil:%			== per(nil$Pointer);
	coerce(r:Record T):%	== per(r pretend Pointer);
	record(r:%):Record T	== r pretend Record T;
}

macro {
	REC == Record(a:SingleInteger, b:String);
	NREC == NilRecord(a:SingleInteger, b:String);
}

import from SingleInteger, String, REC, NREC;
r1:REC := [5, "hello"];
n1 := r1::NREC;
