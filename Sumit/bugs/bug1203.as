-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
----------------------------- loop.as ----------------------------------
--
-- To scan for this bug in C files, do
--      grep generDone *.c | grep fiClosMake | grep -v R0
--      grep generStepper *.c | grep fiClosMake | grep -v R1
--	grep generValue *.c | grep fiClosMake | grep -v R2
--
-- You need axiomxl 1.1.11e and sumit 0.1.11e to reproduce this problem.
-- Sumit 0.1.11e is available from
--         http://www.inria.fr/cafe/Manuel.Bronstein/sumit.html
-- 
-- IN THIS CODE (AND IN SEVERAL OTHER EXAMPLES) A FOR LOOP WITH
-- CONSTANT SINGLEINTEGER ARGUMENTS IS NOT INLINED. THIS IS THE
-- CODE GENERATED FOR THE LOOP (*) BELOW:
-- L19:    fiEnvEnsure(l4->X4_generator->env);
--         T25 = fiEnvPush(fiNew(struct Fmt20 ), l4->X4_generator->env);
--         T40 = fiNew(struct Fmt17 );
--         T40->X0_open_QMARK_ = (FiWord) 0;
--         T40->X1_low = (FiWord) ((FiSInt) T34 + 2);
--         T40->X2_high = (FiWord) T124;
--         T40->X3_step = (FiWord) 1;
--         ((PFmt20) fiEnvLevel0(T25))->X0_s = (FiWord) T40;
--         ((PFmt20) fiEnvLevel0(T25))->X4 = 0;
--         fiEnvEnsure(T25);
--         T30 = fiEnvPush(fiNil, T25);
--         T19 = fiClosMake(T30, C26_generDone_QMARK_);
--         T20 = fiClosMake(T30, C27_generStepper);
--         T21 = fiClosMake(T30, C28_generValue);
-- L14:    fiCCall0(void, T20);
--         if ((FiBool) fiCCall0(FiWord, T19)) goto L15;
--         T18_i = fiCCall0(FiWord, T21);
--         T119 = (FiSInt) T18_i - 1;
--         T31 = fiCCall2(FiWord, l1->X21__STAR_, P1_c, ((FiWord*) T1_cq)[
--                 T119]);
--         ((FiWord*) T36)[T119] = T31;
--         goto L14;
-- L15: 
--
-- WHAT IS HARD TO UNDERSTAND, IS WHY THE ABOVE CODE CAUSED A FACTOR 8
-- IN AN EXECUTABLE WHEN COMPARED TO A WHILE-LOOP.
-- IF THE CODE IS MODIFIED OR SIMPLIFIED, THE LOOP BECOMES OPTIMIZED!!!

#include "sumit"

#if SUMIT0112
macro {
	SingleInteger == MachineInteger;
	SumitRing == Ring;
	retract == machine;
	extreeSymbol == extree;
	copy(x) == x;
}
import from Boolean, String;
#endif

macro {
	I == SingleInteger;
	Z == Integer;
}

LoopBug(R:SumitRing):SumitRing == add {
	macro ARR	== PrimitiveArray R;
	macro Rep	== Record(siz:I, deg:I, koeffs:ARR);

	import from ARR, Rep, R, I;

	0:%				== { import from Z; monomial(0, 0); }
	1:%				== { import from Z; monomial(1, 0); }
	local size(p:%):I		== rep(p).siz;
	local degr(p:%):I		== rep(p).deg;
	local coeffs(p:%):ARR		== rep(p).koeffs;
	local setDegree!(p:%, n:I):I	== { rep(p).deg := n; }
	extree(p:%):ExpressionTree	== extreeSymbol "?";
	(p:%) + (q:%):%			== p;
	(p:%) - (q:%):%			== q;
	(port:TextWriter) << (p:%):TextWriter   == port;
	(p:%) ^ (n:Z):% == p;
	coerce(n:Z):% == monomial(n::R, 0);
	coerce(n:I):% == n::Z::%;
	characteristic:Z == characteristic$R;
	(p:%) * (q:%):% == p;

	local newDegree!(p:%, olddeg:I, newdeg:I, c:ARR):% == {
		d := {
			olddeg = newdeg => {
				zero? c next newdeg => computeDegree(newdeg, c);
				newdeg;
			}
			max(olddeg, newdeg);
		}
		setDegree!(p, d);
		p;
	}

	add!(p:%, q:%):% == {
		zero? q => p; zero? p => copy q;
		one? p => q + 1;
		d := min(n := degr p, m := degr q);
		cp := coeffs p; cq := coeffs q;
		for i in 1..next d repeat cp.i := cp.i + cq.i;
		m >= (sz := size p) => {
			cp := resize!(cp, sz, next m);
			for i in d+2..next m repeat cp.i := cq.i;
			per [next m, m, cp];
		}
		for i in d+2..next m repeat cp.i := cq.i;
		newDegree!(p, n, m, cp);
	}

	add!(p:%, c:R, q:%):% == {
		one? c => add!(p, q);
		zero? q or zero? c => p;
		zero? p => times!(c, copy q);
		one? p => add!(times!(c, copy q), 1);
		d := min(n := degr p, m := degr q);
		cp := coeffs p; cq := coeffs q;
		-- THIS LOOP IS NOT INLINED (*)
		for i in 1..next d repeat cp.i := cp.i + c * cq.i;
		m >= (sz := size p) => {
			cp := resize!(cp, sz, next m);
			-- THIS LOOP IS NOT INLINED
			for i in d+2..next m repeat cp.i := c * cq.i;
			per [next m, m, cp];
		}
		-- THIS LOOP IS NOT INLINED
		for i in d+2..next m repeat cp.i := c * cq.i;
		newDegree!(p, n, m, cp);
	}

	add!(p:%, c:R, n:Z):% == {
		zero? c => p;
		zero? p => monomial(c, n);
		one? p => add!(monomial(c, n), 1);
		cp := coeffs p;
		nn:I := retract n;
		m := degr p;
		nn >= (sz := size p) => {
			cp := resize!(cp, sz, next nn);
			for i in m+2..nn repeat cp.i := 0;
			cp next nn := c;
			per [next nn, nn, cp];
		}
		for i in m+2..nn repeat cp.i := 0;
		cp next nn := cp(next nn) + c;
		newDegree!(p, m, nn, cp);
	}

	monomial(c:R, n:Z):% == 0;
	-(p:%):% == p;
	(x:%) = (y:%):Boolean == true;

	local computeDegree(n:I, c:ARR):I == {
		for i in next n..1 by -1 repeat ~zero?(c.i) => return(i-1);
		0;
	}

	(c:R) * (p:%):% == p;
	times!(c:R, p:%):% == p;
}

