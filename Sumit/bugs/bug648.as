-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------------- apply.as -------------------------
--
-- bar1 compiles, but bar2 does not:
--
-- % asharp -v -M2 apply.as
-- A# version 0.35.0 for SPARC [=] 
-- "apply.as", line 20:         bar2(f:Z -> Z, x:Foo Z):Foo Z == f x;
--                      ...........................................^
-- [L20 C44] (Error) Argument 1 of `f' did not match with any possible parameter type.
--     The rejected type is Foo(SingleInteger).
--     Expected type SingleInteger.
--

#include "axllib.as"

Foo(S:BasicType):BasicType with {
	foo:   S -> %;
	apply: (S -> S, %) -> %;
} == S add {
	macro Rep == S;
	foo(s:S):% == per s;
	apply(f:S->S,x:%):% == per f rep x;
}

macro Z == SingleInteger;

Bar: with {
	bar1: (Z -> Z, Foo Z) -> Foo Z;
	bar2: (Z -> Z, Foo Z) -> Foo Z;
} == add {
	bar1(f:Z -> Z, x:Foo Z):Foo Z == apply(f, x);
	bar2(f:Z -> Z, x:Foo Z):Foo Z == f x;
}
