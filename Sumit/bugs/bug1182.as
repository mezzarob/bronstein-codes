-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------------- bug1182.as ------------------------------
--
-- The array access s := s + a.i in sum() is not inlined
-- when the apply() function in PArray is broken into 2 lines.
-- An fiCCall2 to _elt is generated instead.
--

#include "axllib"

macro A == PArray;
macro Z == SingleInteger;

PArray(T:BasicType): FiniteLinearAggregate T with {} == add {
	import from Machine;
	Rep == Arr;

	-- THIS VERSION DOES NOT GET INLINED IN CLIENTS
	local elt:(Arr, SInt) -> T	== get T;
	apply(x:%, n:Z):T		== elt(rep x, n::SInt);

	-- THIS VERSION GETS INLINED OK
	-- apply(x:%, n:Z):T		== get(T)(rep x, n::SInt);

	set!(x:%, n:Z, y:T):T		== set!(T)(rep x, n::SInt, y);
	new(n:Z):%			== per(array(Z)(0, n::SInt));
	empty():%			== (nil$Pointer) pretend %;
	empty?(a:%):Boolean		== nil?(a pretend Pointer)$Pointer;
	bracket(g:Generator T):%	== empty();
	bracket(t:Tuple T):%		== empty();
	#(x:%):Z			== 0;
	(x:%) = (y:%):Boolean		== false;
	sample:%			== empty();
	generator(x:%):Generator T	== generate {};
	(p:TextWriter) << (x:%):TextWriter == p;
}


-- sum the first n elements of a (in a stupid way, this is to check the C code)
sum(a:A Z, n:Z):Z == {
	s := 0;
	i := 1;
	maxindex := n;
	while i <= maxindex repeat {
		s := s + a.i;
		i := next i;
	}
	s;
}

