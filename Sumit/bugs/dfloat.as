-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
#include "salli"
#include "sallio"

macro {
	Z == MachineInteger;
	DFLOVAL == 2.718281828;
}

Main():() == {
	import from PrimitiveMemoryBlock, MachineInteger, DoubleFloat;
	import from Machine, Boolean, Byte;
    
	mem:PrimitiveMemoryBlock := new(50);
	df:DoubleFloat := 0.0;
    
	x:DoubleFloat := DFLOVAL;
	stdout << "x = " << x << newline;
	mx := x::DFlo;
	(s, e, m1, m2) := dissemble mx;
	S := s::Boolean;
	E := e::Z;
	M1 := (m1 pretend SInt)::Z;
	M2 := (m2 pretend SInt)::Z;
	stdout << "s = " << S << newline;
	stdout << "e = " << E << newline;
	stdout << "m1 = " << M1 << newline;
	stdout << "m2 = " << M2 << newline;
	mem::BinaryWriter << x << flush;
	stdout << "mem = ";
	for i in 0..24 repeat stdout << mem.i << " ";
	stdout << newline;

	my := assemble(S::Bool, E::SInt, (M1::SInt) pretend Word,
					(M2::SInt) pretend Word);
	y := my::DoubleFloat;
	df := << mem::BinaryReader;

	stdout << "y = " << y << " and should be " << x << newline;
	stdout << "df = " << df << " and should be " << x << newline;
	stdout << "df = x is " << (df = x)@Boolean << newline;
}

Main();

