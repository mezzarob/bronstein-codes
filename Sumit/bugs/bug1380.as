-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------- optbug.as --------------------------------
--
-- % aldor -fx -lalgebra -laldor optbug.as
-- % optbug
-- Segmentation fault
-- The bug disappears if algebra/src/mat/modular/sit_modpoge.as
-- is recompiled at -q1 and linked with optbug
-- It reappears always when sit_modpoge is compiled at -q3 and higher
-- It appears occasionally with other matrices when compiled -q2
-- But sit_modpoge is critical C-like code that must be optimized
-- down to machine integer operations

#include "algebra"
#include "aldorio"

macro {
        Z == Integer;
	F == SmallPrimeField 3;
        FX == DenseUnivariatePolynomial(F, -"x");
	M == DenseMatrix;
}


main():() == {
	import from Symbol, MachineInteger, F, FX;
	import from LinearAlgebra(FX, M FX);

	m:M FX := zero(6,6);
	m(1,2) := 1; m(1,5) := -1;
	m(2,3) := -1; m(2,6) := -1;
	m(4,5) := 1;
	m(5,6) := -1;
	r := rank m;
	stdout << "rank(m) = " << r << newline;
}

main();

