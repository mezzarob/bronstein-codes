-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------ aldor 1.0.2 optimization bug ------------------------
--
-- works ok (eq? = true) if sit_dup.as is compiled at -q2
-- works bad (eq? = false) if sit_dup.as is compiled at -q3
--

#include "algebra"
#include "aldorio"

macro {
	Z == Integer;
	Zx == DenseUnivariatePolynomial(Z, -"x");
}

import from Symbol;

local optbug():() == {
	import from Z;
	x:Zx := monom;
	p:Zx := 1;
	for i in 1..5@Z repeat p := p * (x - i::Zx)^i;
	stdout << "p := " << p << newline;
	g := x^10-40*x^9+715*x^8-7518*x^7+51471*x^6-239628*x^5+767837*x^4_
		-1670990*x^3+2361800*x^2-1956000*x+720000::Zx;
	stdout << "g := " << g << newline;
	q := x^5-15*x^4+85*x^3-225*x^2+274*x-120::Zx;
	stdout << "q := " << q << newline;
	stdout << "p - g q = " << p - g * q << newline;
	eq? := equal?(p, q, g, 10);
	stdout << "eq? = " << eq? << newline;
}

optbug();
