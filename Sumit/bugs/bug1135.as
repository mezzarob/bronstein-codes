-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------- badq3.as --------------------------
--
-- To reproduce this bug, you need:
-- sumit, which I'm sending you as a tar file
-- to separate this file into lodfo.as and inline.as
--
-- darwin3{bronstei} 176: axiomxl -q2 lodfo.as
-- darwin3{bronstei} 177: axiomxl -q2 inline.as 
-- darwin3{bronstei} 178: axiomxl -q3 inline.as
-- #1 (Fatal Error) Looking for `function' with code `1048257851'.  Export not found.
-- #1 (Warning) Removing file `inline.ao'.
-- 
------------------------- lodfo.as --------------------------
#include "sumit"

OrdinaryDifferenceOperatorCategory(R:CommutativeRing): Category ==
	UnivariateSkewPolynomialCategory R with { apply: (%, R) -> R }

macro DUSP == DenseUnivariateSkewPolynomial(R, sigma, function 0, avar);

OrdinaryDifferenceOperator(R:CommutativeRing, sigma: Automorphism R,
	avar:String): OrdinaryDifferenceOperatorCategory R == {
		import from Derivation R;

		DUSP add {
			macro Rep == DUSP;
			import from Rep;

			apply(p:%, r:R):R == apply(rep p, 1, r);
		}
}

OrdinaryDifferenceOperator(R:CommutativeRing, P:UnivariatePolynomialCategory R,
	avar:String): OrdinaryDifferenceOperatorCategory P == {
		import from R, Automorphism P;

		local shift(p:P, n:Integer):P == p(monom + n::P);

		OrdinaryDifferenceOperator(P, morphism shift, avar);
}

------------------------- inline.as --------------------------
#include "sumit"

#library bar "lodfo.ao"
import from bar;
inline from bar;

macro {
	Z	== Integer;
	Zx	== DenseUnivariatePolynomial Z;
	Zxe	== OrdinaryDifferenceOperator(Z, Zx, "E");
}

BadInlining: with { foo: Zxe -> Z } == add { foo(R:Zxe):Z == 1; }
