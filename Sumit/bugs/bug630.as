-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
--------------------------- minisup.as -----------------------------
-- produces an "Export not found" at runtime:
--
-- vinci.inf.ethz.ch{bronstei} 188: minisup
-- Looking in UP(Integer : <category>, x==<value> : String) for monomial
-- with code 436577962
-- Export not found

#include "axllib.as"

macro Z == Integer;
macro OutPort == TextWriter;

UPolyCat(F:Ring):Category == Ring with {
	monomial: (F, Z) -> %;
	degree  : % -> Z;
	leadingCoefficient: % -> F;
	reductum: % -> %;
	coerce  : F -> %;
	*       : (F, %) -> %;
}

SUP(F:Ring): UPolyCat F with {
	apply: (OutPort, %, String) -> OutPort;
} == add {
	local Term: BasicType with {
		term: (F, Z) -> %;    -- term(c, n) creates the monomial c x^n
		coef: % -> F;         -- coef(c x^n) returns c
		expt: % -> Z;         -- expt(c x^n) returns n
	} == add {
		macro Rep == Record(coef:F, expt:Z);
		import from Rep;

		term(c:F, n:Z):% == per [c, n];
		coef(t:%):F      == rep(t).coef;
		expt(t:%):Z      == rep(t).expt;
		sample:%         == term(1, 1);
		(p:OutPort) << (x:%):OutPort == p;  -- never used
		(u:%) = (v:%):Boolean == coef u = coef v and expt u = expt v;
	}

	Rep == List Term;   -- sorted, highest term first
	import from {Term, Rep, F, Z};

	0:%                       == per empty();
	1:%                       == per list term(1, 0);
	degree(p:%):Z             == {zero? p => 0; expt first rep p}
	coerce(n:Integer):%	  == n::F::%;
	coerce(n:SingleInteger):% == n::F::%;
	(p:%)^(n:Integer):%	  == p;
	leadingCoefficient(p:%):F == {zero? p => 0; coef first rep p}
	reductum(p:%):%           == {zero? p => 0; per rest rep p}
	coerce(c:F):%             == monomial(c, 0);
	(x:%) = (y:%):Boolean     == rep x = rep y;
	-(p:%):%                  == per map(cterm(-1), rep p);
	(c:F) * (p:%):%           == per map(cterm c, rep p);
	cterm(c:F):(Term->Term)   == (t:Term):Term +-> term(c * coef t,expt t);

	(p:OutPort) << (x:%):OutPort == {
		import from String;
		apply(p, x, "?");
	}

	monomial(c:F, n:Z):% == {
		n < 0 => error("monomial: exponent should be nonnegative!");
		zero? c => 0;
		per list term(c, n);
	}

	(x:%) + (y:%):% == {
		zero? x => y; zero? y => x;
		nx := degree x; ny := degree y;
		nx > ny => per cons(first rep x, rep(reductum x + y));
		nx < ny => per cons(first rep y, rep(x + reductum y));
		z := reductum x + reductum y;
		zero?(c := leadingCoefficient x + leadingCoefficient y) => z;
		per cons(term(c, nx), rep z);
	}

	applyTerm(p:OutPort, c:F, n:Z, v:String):OutPort == {
		if c ~= 1 then
			if zero? n then p := p << c;
					else p := p << "\left("<< c <<"\right)";
		if zero? n and c = 1 then p := p<< c;
		if n > 0 then
			{p := p << v; if n > 1 then p := p << "^{" << n << "}";}
		p;
	}

	apply(p:OutPort, x:%, v:String):OutPort == {
		zero? x => p << (0$F);
		while x ~= 0 repeat {
			p := applyTerm(p, leadingCoefficient x, degree x, v);
			x := reductum x;
			if (x ~= 0) then p := p << "+";
		}
		p;
	}

	coefficient(p:%, n:Z):F == {
		for t in rep p repeat { n = expt t => return coef t }
		0
	}

	(x:%) * (y:%):% == {
		zero? x => 0;
		a := leadingCoefficient x; n := degree x;
		reductum(x) * y + per [term(a*coef t, n+expt t) for t in rep y];
	}

}

UP(F:Ring, x:String): UPolyCat F == SUP F add {
		Rep == SUP F;
		import from Rep;

		(p:OutPort) << (q:%):OutPort == apply(p, rep q, x);
}

-- THIS SEEMS TO BE CAUSING TROUBLE
apply(F:Ring, l:List String): UPolyCat F == {
        empty? l => error "apply: expect at least one indeterminate";
        empty? rest l => UP(F, first l);
        error "apply: can only handle univariate polys for now";
}

main():Integer == {
        macro alpha == "\alpha";

        import from {SingleInteger, Z, String,  List String};
        -- import from UP(Z, alpha);  -- THIS ONE IS OK
	import from Z[alpha];        -- THIS ONE ISN'T

        a := monomial(1, 1);
        q := p := a + 1;
	default i:SingleInteger;
        for i in 1..5 repeat {
		print << "$$" << q << "$$" << newline;
                q := p * q;
        }
        1;
}

main()

