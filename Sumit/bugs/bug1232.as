-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------------- optloop.as ----------------------------------
--
-- This is a simpler instance of bug1203 for which sumit is not required.
-- You do need salli 1.1.12d for the 1.1.12p6 compiler however:
--         http://www.inria.fr/cafe/Manuel.Bronstein/salli.html
--
-- This bug is the simplest example I have where the inlining of
-- basic machine-integer operations is suddently turned off.
-- Any modification to this file turns in on again.
-- The problem occurs in the << function below, see marked statements
-- and their corresponding C-code.
--

#include "salli"

macro {
	A  == PrimitiveArray;
	Z  == MachineInteger;
}

Foo(T:Type): LinearStructureType T with {
	#: % -> Z;
	if T has OutputType then OutputType;
} == add {
	Rep == Record(ncnst:Z,cval:Partial T,elts:Z,szarr:Z,arr:A T,fun:()->T);

	firstIndex:Z		== 0;
	set!(s:%, n:Z, t:T):T	== t;
	free!(s:%):()		== ();
	#(s:%):Z		== { import from Rep; rep(s).elts; }
	local func(s:%):() -> T	== { import from Rep; rep(s).fun; }
	local data(s:%):A T	== { import from Rep; rep(s).arr; }
	local size(s:%):Z	== { import from Rep; rep(s).szarr; }
	local constFrom(s:%):Z	== { import from Rep; rep(s).ncnst; }
	local constVal(s:%):Partial T	== { import from Rep; rep(s).cval; }
	local constant(s:%):(Z, Partial T)	== (constFrom s, constVal s);

	bracket(g:Generator T):%== {
		import from A T, Partial T, Rep;
		per [-1, failed, 0, 0, empty, ():T +-> next! g];
	}

	local resize!(s:%, k:Z):() == {
		import from Rep, A T;
		if (sz := size s) <= k then {
			a := data s;
			m := max(next k, sz + sz);
			a := resize!(a, sz, m);
			rep(s).arr := a;
			rep(s).szarr := m;
		}
	}

	apply(s:%, n:Z):T == {
		import from Rep, A T;
		assert(n >= 0);
		f := func s;
		(m := constFrom s) >= 0 and n > m => s.m;
		resize!(s, n);
		a := data s;
		itm := #s;
		rep(s).elts := max(itm, next n);
		for j in itm..n repeat a.j := f();
		a.n;
	}


	if T has OutputType then {
-- IN-LINING SEEMS TURNED-OFF INSIDE THIS FUNCTION
-- ALTHOUGH COMMENTING ANY LINE OUT TURNS IT ON AGAIN
		(p:TextWriter) << (s:%):TextWriter == {
			import from String, Boolean, T, Partial T;
-- THIS MACHINE-INT COMPARISON IS NOT INLINED:
-- if ((FiBool) fiCCall2(FiWord, l4->X10__GT_, T0_n, (FiWord) 0L)) goto L0;
		       	if (n := #s) > 0 then {
				p := p << s.0;
-- THIS MACHINE-INT FOR-LOOP IS NOT INLINED:
-- T130 = fiCCall1(FiWord, l4->X12_prev, T0_n);
-- T7 = fiCCall2(FiWord, l4->X13__DOT__DOT_, (FiWord) 1L, T130);
-- etc
				for i in 1..prev n repeat {
					p := p << "," << s.i;
				}
			}
			(m, u) := constant s;
-- THIS MACHINE-INT COMPARISON IS NOT INLINED:
-- if ((FiBool) fiCCall2(FiWord, l4->X9__LT_, T3, (FiWord) 0L)) goto L3;
			m < 0 => p << "...]";
-- THIS MACHINE-INT COMPARISON IS NOT INLINED:
-- if ((FiBool) fiCCall2(FiWord, l4->X11__LT__EQ_, T0_n, T3)) goto L5;
			if n <= m then p := p << "...";
			p;
		}
	}

	if T has PrimitiveType then {
		equal?(s:%, t:%, n:Z):Boolean == false;
	}
}

