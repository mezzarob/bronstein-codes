-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------- optbug.as --------------------------
--
-- YOU NEED minisumit TO REPRODUCE THIS BUG
--
-- % axiomxl -q2 -lminisumit -fx optbug.as
-- % optbug
-- p = 5
-- Segmentation fault
--
-- % axiomxl -q1 -lminisumit -fx optbug.as
-- % optbug
-- p = 5
-- p = 7
-- p = 11
-- p = 13
-- p = 17

#include "axllib"

#library minisumit "libminisumit.al"
import from minisumit;
inline from minisumit;

macro Z == SingleInteger;

Foo(L:ListCategory Z): with { foo: L -> List Z; } == add {
	foo(f:L):List Z == {
		import from Z, SmallPrimes;
		zero?(d := #f) => empty();
		d = 1 => [first f quo next first f];
		p:Z := 5;
		r := bar(f, p);
		while empty? r repeat {
			-- SEG FAULTS AT -q2 HAPPENS AT THE NEXT LINE
			p := nextPrime p;
			r := bar(f, p);
		}
		r;
	}

	local bar(f:L, p:Z):List Z == {
		print << "p = " << p << newline;
		p > 15 => [0];
		empty();
	}
}			

boom():() == {
	import from Z, List Z, Foo List Z;
	foo [1, 0, 0, 0, -1];
}

boom();
