-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
--------------------------------- opteqbug.as -------------------------------------
--
-- WE FOUND 94 OCCURENCES OF THIS BUG INSIDE libalgebra!
--
-- THE CODE GENERATED AT -Q2 SHOWS THAT THE CALL TO =$% IS TRANSFORMED TO fiBINT_EQ
-- aldor -q2 -fc opteqbug.as
-- static FiWord
-- CF4_foo(FiEnv e1, FiWord P0_x)
-- {
--         FiBool T1;
--         FiWord T0;
--         PFmt8 l1;
--         l1 = (PFmt8) fiEnvLevel(e1);
--         T0 = fiCCall1(FiWord, l1->X1_bar, P0_x);
--         fiBINT_EQ(T1, FiBool, (FiBInt) T0, (FiBInt) P0_x);
--         return (FiWord) T1;
-- }
-- 
-- THE CODE GENERATED AT -Q1 IS OK:
-- aldor -q1 -fc opteqbug.as
-- static FiWord
-- CF4_foo(FiEnv e1, FiWord P0_x)
-- {
--         FiWord T0;
--         PFmt8 l1;
--         l1 = (PFmt8) fiEnvLevel(e1);
--         T0 = fiCCall1(FiWord, l1->X1_bar, P0_x);
--         return fiCCall2(FiWord, l1->X2__EQ_, T0, P0_x);
-- }
--
--
-- THE BUG DOES NOT OCCUR WITH #include "aldor" (SMALLER LIBRARY)
#include "algebra"

define Foo(R:PrimitiveType):Category == PrimitiveType with {
	foo: % -> Boolean;
	bar: % -> %;
	default foo(x:%):Boolean == bar(x) = x;
}

