-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------- optbug.as -----------------------------------
--
-- Yet another optimization bug that occurs when specialize! becomes inlined
--
-- aldor -q1 -fx -lalgebra -laldor optbug.as
-- optbug
-- Calling rank()...T
--
-- aldor -q2 -fx -lalgebra -laldor optbug.as
-- optbug
-- Segmentation fault
--
-- This bug also illustrates a second compile-time seg fault:
-- aldor -q1 -wdebug optbug.as
-- Program fault (segmentation violation).
--

#include "algebra"
#include "aldorio"

macro {
	I == MachineInteger;
	Z == Integer;
	M == DenseMatrix;
	A == PrimitiveArray;
}

Special(R:Join(CommutativeRing, Specializable), M:MatrixCategory R): with {
	rankLowerBound: M -> Partial Cross(Boolean, I);
} == add {
	rankLowerBound(m:M):Partial Cross(Boolean, I) == {
		import from Boolean, I, A I, A A I, LazyHalfWordSizePrimes;
		(r, c) := dimensions m;
		mp:A A I := new r;
		for i in 0..prev r repeat mp.i := new c;
		for i in 1..2 repeat {
			p := randomPrime();
			~failed?(u := rankLowerBound(m, mp, p)) => return u;
		}
		failed;
	}

	local rankLowerBound(m:M, mp:A A I, p:I):Partial Cross(Boolean, I) == {
		import from I, ModulopGaussElimination;
		(r, c) := dimensions m;
		maxrank := min(r, c);
		for i in 1..2 repeat {
			specialize!(mp, m, p) => {
				rk := rank!(mp, r, c, p);
				return [(rk = maxrank, rk)];
			}
		}
		failed;
	}

	local specialize!(mp:A A I, m:M, p:I):Boolean == {
		macro F == SmallPrimeField0 p;
		import from I, A I, A A I, F, Partial F, PartialFunction(R, F);
		(r, c) := dimensions m;
		sigma := partialMapping(specialization(F)$R);
		for i in 1..r repeat {
			rowi := mp(prev i);
			for j in 1..c repeat {
				failed?(u := sigma m(i, j)) => return false;
				rowi(prev j) := machine retract u;
			}
		}
		true;
	}
}

local rank():Boolean == {
	import from I, Z, M Z, Partial Cross(Boolean, I);
	import from Special(Z, M Z);
	m:M Z := zero(4, 4);
	m(1, 2) := -1;
	m(1, 4) := 1;
	m(3, 4) := 1;
	m(4, 4) := -1;
	u := rankLowerBound m;
	failed? u => false;
	(b?, r) := retract u;
	r = 2 and ~b?;
}

stdout << "Calling rank()..." << rank() << newline;
stdout << endnl;

