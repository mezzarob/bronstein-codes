-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
#include "sumit"
#include "aldorio"
#include "stdmac"

import from Symbol;

macro PZX == List Cross(ZX, Z);

local getop():ZXE == {
	import from Z;
	n:ZX := monom;
	E:ZXE := monom;
	L := (4*n+3::ZX)*E^4+(-4*n-5::ZX)*E^3+E^2+(4*n+5::ZX)*(n+1)*E
		-(n*(4*n+7::ZX)*(n+1))::ZXE;
-- the next one has a trivial eigenring but its universal denominator is
--   x^3*(x+1)*(x-3)^6*(x-1)^11*(x-2)^12
--	L := -n^6*(n-1)*(n-2::ZX)*(n-3::ZX)*(n+2::ZX)*E^3
--		+(n-3::ZX)*(n^6-2*n^5+n^4+n^3-2*n^2-n+1)*(n-1)^2*(n+1)^2*E^2
--		-n^2*(n-2::ZX)*(n^5-4*n^4+n^3+10*n^2-12*n+3::ZX)*(n+1)^2*E
--		+(n^2*(n-2::ZX)*(n-3::ZX)*(n-1)^2*(n+1)^2)::ZXE;
--	L;
}

main():() == {
	import from Q, ZX, ZXE, V ZX, V QXE, LinearOrdinaryFirstOrderSystem ZX,
		UnivariatePolynomialAlgebraTools(ZX, ZXE),
		LinearOrdinaryRecurrenceRationalSolutions(Z,Q,coerce,ZX,ZXE,QX),
		LinearOrdinaryRecurrenceEigenring(Z,Q,coerce,ZX,ZXE,QX,QXE),
		LinearOrdinaryRecurrenceSystemRationalSolutions(Z,Q,coerce,_
								ZX,ZXE,QX);
	L := getop();
	TIMESTART;
	(d, ignore, num) := matrixEigenring L;
	TIME("matrix eigenring at: ");
	stdout << "d = " << d << endnl;
	stdout << "num = " << num << endnl;
	(den, dvec, num) := eigenring L;
	TIME("eigenring at: ");
	stdout << "den = " << factorialExpansion den << endnl;
	stdout << "dvec = " << dvec << endnl;
	stdout << "num = " << num << endnl;
#endif
}

main();


