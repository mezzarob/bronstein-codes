-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_system1.as ------------------------------
-- Copyright (c) Manuel Bronstein 2000
-- Copyright (c) INRIA 2000, Version 0.1.12
-- Logiciel Sum^it (c) INRIA 2000, dans sa version 0.1.12
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I == MachineInteger;
	Z == Integer;
	M == DenseMatrix;
	V == Vector;
	RR == (R pretend GcdDomain);
	Q == Fraction RR;
	MOR == Automorphism;
	SYS == LinearOrdinaryFirstOrderSystem;
}

#if ALDOC
\thistype{LinearOrdinaryFirstOrderSystem}
\History{Manuel Bronstein}{26/9/2000}{created}
\Usage{ import from \this~R}
\Params{
{\em R} &\alexttype{CommutativeRing}{} & The coefficient ring of the operators\\
}
\Descr{\this~R provides linear ordinary
systems of first order with coefficients in R.}
\begin{exports}
\alexp{dimension}:& \% $\to$ \alexttype{MachineInteger}{} & Number of unknowns\\
\alexp{matrix}:
& \% $\to$ (\alexttype{Vector}{} R, \alexttype{DenseMatrix}{} R) &
Matrix representation\\
\alexp{system}: & \alexttype{DenseMatrix}{} R $\to$ \% & Create a system\\
            & (R, \alexttype{DenseMatrix}{} R) $\to$ \% &\\
            & (\alexttype{Vector}{} R, \alexttype{DenseMatrix}{} R) $\to$ \% &\\
\end{exports}
\begin{exports}[if \emph{R} has \alexttype{GcdDomain}{} then]
\alexp{changeVariable}:
& (\%, M Q, \alexttype{Automorphism}{} Q, Q) $\to$ Q &
Change of variable\\
\alexp{makeNonSingular}:
&(\%, \alexttype{Automorphism}{} R) $\to$ (B, \alexttype{Vector}{} R, M R, \%) &
Make non singular\\
\alexp{system}: & M Q $\to$ \% & Create a system\\
\end{exports}
\begin{alwhere}
B &==& \alexttype{Boolean}{}\\
M &==& \alexttype{DenseMatrix}{}\\
\end{alwhere}
\begin{exports}[if \emph{R} has \alexttype{Field}{} then]
\alexp{changeVariable}:
& (\%, \alexttype{DenseMatrix}{} R, \alexttype{Automorphism}{} R, R) $\to$ R &
Change of variable\\
\end{exports}
\begin{alwhere}
Q &==& \alexttype{Fraction}{} R\\
\end{alwhere}
\begin{exports}[if R has \alexttype{GcdDomain}{} then]
\alexp{pCurvature}: & (\%, \alexttype{Automorphism}{} R) $\to$ \% & p-curvature\\
                    & (\%, \alexttype{Derivation}{} R) $\to$ \% & \\
\end{exports}
#endif

LinearOrdinaryFirstOrderSystem(R:CommutativeRing): with {
	if R has GcdDomain then {
		changeVariable: (%,   Q, Automorphism Q, Q -> Q) -> %;
		-- TEMPORARY: 1.0.0 COMPILER BUG IF OVERLOADED!
		-- changeVariable: (%, Q,Q, Automorphism Q, Q -> Q) -> %;
		changeVariableHyper: (%, Q,Q, Automorphism Q, Q -> Q) -> %;
		changeVariable: (%, M Q, Automorphism Q, Q -> Q) -> %;
	}
	if R has Field then {
		changeVariable: (%,   R, Automorphism R, R -> R) -> %;
		-- TEMPORARY: 1.0.0 COMPILER BUG IF OVERLOADED!
		-- changeVariable: (%, R,R, Automorphism R, R -> R) -> %;
		changeVariableHyper: (%, R,R, Automorphism R, R -> R) -> %;
		changeVariable: (%, M R, Automorphism R, R -> R) -> %;
	}
#if ALDOC
\alpage{changeVariable}
\Usage{\name($L,T,\sigma,\delta$)}
\Signatures{
\name:
& (\%, \alexttype{DenseMatrix}{} R, \alexttype{Automorphism}{} R, R $\to$ R)
$\to$ \%\\
\name:
& (\%, \alexttype{DenseMatrix}{} Q, \alexttype{Automorphism}{} Q, Q $\to$ Q)
$\to$ \%\\
}
\begin{alwhere}
Q &==& \alexttype{Fraction}{} R\\
\end{alwhere}
\Params{
{\em L} & \% & A system\\
{\em T} & \alexttype{DenseMatrix}{} R & An invertible matrix\\
        & \alexttype{DenseMatrix}{} \alexttype{Fraction}{} R & \\
$\sigma$ & \alexttype{Automorphism}{} R & The automorphism to use\\
         & \alexttype{Automorphism}{} \alexttype{Fraction}{} R & \\
$\delta$ & R $\to$ R & The $\sigma$-derivation to use\\
         & \alexttype{Fraction}{} R $\to$ \alexttype{Fraction}{} R & \\
}
\Retval{Returns a new system $\theta Z = M Z$ equivalent to
the original system $\theta Y = A Y$ under the change of variable 
$Y = T Z$.}
#endif
	if R has IntegralDomain then {
		commonDenominator: % -> (R, M R);
	}
	dimension: % -> I;
#if ALDOC
\alpage{dimension}
\Usage{\name~L}
\Signature{\%}{\alexttype{MachineInteger}{}}
\Params{ {\em L} & \% & A system\\ }
\Retval{Returns the number of unknowns of $L$.}
#endif
	if R has GcdDomain then {
		makeNonSingular: (%, Automorphism R) -> (Boolean, V R, M R, %);
#if ALDOC
\alpage{makeNonSingular}
\Usage{(nonSing?, v, A, $L'$) := \name(L, $\sigma$)}
\Signature{(\%, \alexttype{Automorphism}{} R)}
{(\alexttype{Boolean}{}, \alexttype{Vector}{} R,
\alexttype{DenseMatrix}{} R, \%)}
\Params{
{\em L} & \% & A difference system\\
$\sigma$ & \alexttype{Automorphism}{} R &
The difference automorphism for \emph{L}\\
}
\Descr{Returns a flag \emph{nonSing?}, a vector $[v_1,\dots, v_n]$,
a matrix \emph{A} and a system $L'$
of the form $\sigma Z = B Z$ where \emph{B} is nonsingular,
such that the solutions of \emph{L} are
$$
Y = \pmatrix{
v_1^{-1} & & \cr & \ddots & \cr & & v_n^{-1}
} A Z
$$
where \emph{Z} ranges over the solutions of $L'$. In \emph{nonSing?}
is \true, then \emph{L} already had a nonsingular matrix, each $v_i$ is
\emph{1} and \emph{A} is the identity matrix.}
\Remarks{\name{} is only applicable to (generalized) difference systems,
not to differential systems.}
#endif
	}
	matrix: % -> (V R, M R);
#if ALDOC
\alpage{matrix}
\Usage{\name~L}
\Signature{\%}{(\alexttype{Vector}{} R, \alexttype{DenseMatrix}{} R)}
\Params{ {\em L} & \% & A system\\ }
\Retval{Returns $([a_1,\dots,a_m], A)$ such that $L$ is the system
$$
\pmatrix{
a_1 & & \cr & \ddots & \cr & & a_m
}
\theta Y = A Y
$$
for some pseudo--linear operator $\theta$.}
#endif
	if R has IntegralDomain and R has FiniteCharacteristic then {
		pCurvature: (%, Automorphism R) -> %;
		pCurvature: (%, Derivation R) -> %;
#if ALDOC
\alpage{pCurvature}
\Usage{\name(L,$\sigma$)\\ \name(L,$\partial$)}
\Signatures{
\name: & (\%, \alexttype{Automorphism}{} R) $\to$ \%\\
\name: & (\%, \alexttype{Derivation}{} R) $\to$ \%\\
}
\Params{
{\em L} & \% & A system\\
$\sigma$ & \alexttype{Automorphism}{} R & a morphism\\
$\partial$ & \alexttype{Derivation}{} R & a derivation\\
}
\Retval{Returns the p-curvature of \emph{L} viewed either as a
difference system with morphism $\sigma$ or a differential system with derivation
$\partial$.}
\alseealso{\alfunc{StandardUnivariateSkewPolynomialCategory}{pCurvature}}
#endif
	}
	system: M R -> %;
	system: (R, M R) -> %;
	system: (V R, M R) -> %;
	if R has GcdDomain then {
		system: M Q -> %;
	}
#if ALDOC
\alpage{system}
\Usage{\name~A\\ \name~B\\ \name(a, A)\\ \name($[a_1,\dots,a_m]$, A)}
\Params{
{\em a}, $a_i$ & R & Coefficients\\
{\em A} & \alexttype{DenseMatrix}{} R & A square matrix\\
{\em B} & \alexttype{DenseMatrix}{} \alexttype{Fraction}{} R & A square matrix\\
}
\Retval{Return respectively the systems $\theta Y = A Y$,
$\theta Y = B Y$, $a \theta Y = A Y$ and
$$
\pmatrix{
a_1 & & \cr & \ddots & \cr & & a_m
}
\theta Y = A Y
$$
}
#endif
} == add {
	Rep == Record(coeff:V R, mat:M R);
	import from Rep;
 
	-- TEMPORARY: WOULD LIKE TO CACHE THE TESTS
	-- local intdom?:Boolean	== R has IntegralDomain;
	-- local gcd?:Boolean		== R has GcdDomain;
	-- local field?:Boolean		== R has Field;
	local coef(s:%):V R		== rep(s).coeff;
	local mat(s:%):M R		== rep(s).mat;
	matrix(s:%):(V R, M R)		== explode rep s;
	local system0(a:V R, m:M R):%	== { assert(square? m); per [a, m]; }
	dimension(s:%):I		== { import from M R; numberOfRows mat s; }
	system(m:M R):%			== { import from R; system(1, m); }

	system(a:V R, m:M R):% == {
		-- TEMPORARY: WOULD LIKE TO CACHE THE TESTS
		-- field? => fieldsystem(a, m);
		-- gcd? => gcdsystem(a, m);
		-- intdom? => intdomsystem(a, m);
		R has Field => fieldsystem(a, m);
		R has GcdDomain => gcdsystem(a, m);
		R has IntegralDomain => intdomsystem(a, m);
		system0(a, m);
	}

	system(a:R, m:M R):% == {
		import from V R;
		system(new(numberOfRows m, a), m);
	}

	if R has Field then {
		local fieldsystem(a:V R, A:M R):% == {
			import from I, R;
			(r, c) := dimensions A;
			B := copy A;
			b := copy a;
			for i in 1..r | ~zero?(q := a.i) and ~one?(q) repeat {
				b.i := 1;
				q1 := inv q;
				for j in 1..c repeat B(i,j) := times!(B(i,j), q1);
			}
			system0(b, B);
		}

		-- computes a * inv(d) * b, trashing a
		local AinvB!(a:M R, d:V R, b:M R):% == {
			import from I, R;
			(n, c) := dimensions a;
			assert(c = #d);
			for i in 1..n repeat for j in 1..c repeat
				a(i, j) := a(i, j) / d.j;
			system(a * b);
		}

		-- Y = t Z, t scalar
		changeVariable(s:%, t:R, sigma:MOR R, delta: R -> R):% == {
			TRACE("system1::changeVariable:t = ", t);
			assert(~zero? t);
			t1 := inv sigma t;
			-- TEMPORARY: 1.0.0 COMPILER BUG IF OVERLOADED!
			-- changeVariable(s, t1 * t, t1 * delta t, sigma, delta);
			changeVariableHyper(s, t1 * t, t1 * delta t, sigma, delta);
		}

		-- Y = t Z, t hyperexponential, f = t/s(t), g = d(t)/s(t)
		-- TEMPORARY: 1.0.0 COMPILER BUG IF OVERLOADED!
		-- changeVariable(s:%, f:R, g:R, sigma:MOR R, delta: R -> R):% == {
		changeVariableHyper(s:%, f:R, g:R, sigma:MOR R, delta: R -> R):% == {
			import from I, V R, LinearAlgebra(R, M R);
			TRACE("system1::changeVariable:f = ", f);
			TRACE("system1::changeVariable:g = ", g);
			(d, a) := matrix s;
			n := #d;
			dd := diagonal [f * inv(d.i) for i in 1..n];
			system(dd * a - diagonal(g, n));
		}

		-- Y = t Z, t matrix
		changeVariable(s:%, t:M R, sigma:MOR R, delta: R -> R):% == {
			import from I, R, V R, LinearAlgebra(R, M R);
			TRACE("system1::changeVariable:t = ", t);
			assert(square? t);
			n := numberOfRows t;
			(d, a) := matrix s;
			assert(n = numberOfColumns a);
			dd := [inv(d.i) for i in 1..n];
			(t1, dt1) := inverse(map(function sigma) t);
			system(t1 * diagonal(dd) * a * t - t1 * map(delta)(t));
		}
	}

	if R has GcdDomain then {
		local gcdsystem(a:V R, A:M R):% == {
			import from I, R, List R;
			(r, c) := dimensions A;
			B := copy A;
			b := copy a;
			for i in 1..r | ~zero?(q := a.i) and ~unit?(q) repeat {
				(g, l) := gcdquo cons(q, [generator row(A, i)]);
				if ~unit?(g) then {
					b.i := first l;
					for j in 1..c for p in rest l repeat
						B(i,j) := p;
				}
			}
			system0(b, B);
		}

		makeNonSingular(s:%, sigma: MOR R):(Boolean, V R, M R, %) == {
			import from I, Array I, Z, R, LinearAlgebra(R, M R);
			(d, a) := matrix s;
			assert(~zero? a);
			n := numberOfRows a;
			(rank?, r) := rankLowerBound a;
			r = n => (true, new(n, 1), one n, s);
			(ar, ac) := maxInvertibleSubmatrix a;
			r := #ar;
			assert(r > 0); assert(#ac = r);
			r = n => (true, new(n, 1), one n, s);
			all:Array I := [i for i in 1..n];
			sigma1 := inv sigma;
			s1a := map!(function sigma1)(a(all, ac));
			s1d:V R := map(function sigma1)(d);
			minor := a(ar, ac);
			(tminor1, v) := inverse transpose minor;
			b := transpose(tminor1) * a(ar, all);
			LL := {
				R has Field => AinvB!(b, s1d, s1a);
				invAinvB(v, b, s1d, s1a);
			}
			(false, s1d, s1a, LL);
		}

		local invAinvB(v:V R, a:M R, d:V R, b:M R):% == {
			import from I, R, Q, M Q;
			import from MatrixCategoryOverFraction(RR,M RR,Q,M Q);
			(r, n) := dimensions a;
			aa:M Q := zero(r, n);
			for i in 1..r repeat for j in 1..n repeat
				aa(i, j) := a(i, j) / (v.i * d.j);
			system(aa * makeRational b);
		}

		-- Y = t Z, t scalar
		changeVariable(s:%, t:Q, sigma:MOR Q, delta: Q -> Q):% == {
			assert(~zero? t);
			t1 := inv sigma t;
			-- TEMPORARY: 1.0.0 COMPILER BUG IF OVERLOADED!
			-- changeVariable(s, t1 * t, t1 * delta t, sigma, delta);
			changeVariableHyper(s, t1 * t, t1 * delta t, sigma, delta);
		}

		-- Y = t Z, t hyperexponential, f = t/s(t), g = d(t)/s(t)
		-- TEMPORARY: 1.0.0 COMPILER BUG IF OVERLOADED!
		-- changeVariable(s:%, f:Q, g:Q, sigma:MOR Q, delta: Q -> Q):% == {
		changeVariableHyper(s:%, f:Q, g:Q, sigma:MOR Q, delta: Q -> Q):% == {
			import from I, V Q, M Q, LinearAlgebra(Q, M Q);
			import from MatrixCategoryOverFraction(RR,M RR,Q,M Q);
			(d, a) := matrix s;
			n := #d;
			dd := diagonal [f * inv((d.i)::Q) for i in 1..n];
			system(dd * makeRational(a) - diagonal(g, n));
		}

		-- Y = t Z, t matrix
		changeVariable(s:%, t:M Q, sigma:MOR Q, delta: Q -> Q):% == {
			import from I, Q, V Q, LinearAlgebra(Q, M Q);
			import from MatrixCategoryOverFraction(RR,M RR,Q,M Q);
			assert(square? t);
			n := numberOfRows t;
			(d, A) := matrix s;
			assert(n = numberOfColumns A);
			d1 := diagonal [inv(f::Q) for f in d];
			a := d1 * makeRational A;
			(t1, dt1) := inverse(map(function sigma) t);
			system(t1 * a * t - t1 * map(delta) t);
		}

		system(m:M Q):% == {
			import from MatrixCategoryOverFraction(RR,M RR,Q,M Q);
			system0 makeRowIntegral m;
		}
	}

	if R has IntegralDomain then {
		local intdomsystem(a:V R, A:M R):% == {
			import from I, R;
			(r, c) := dimensions A;
			B := copy A;
			b := copy a;
			for i in 1..r | ~zero?(q := a.i) and ~unit?(q) repeat {
				v := copy row(A, i);
				if exactQuotient?!(v, q) then {
					b.i := 1;
					for j in 1..c repeat B(i,j) := v.j;
				}
			}
			system0(b, B);
		}

		local exactQuotient?!(v:V R, q:R):Boolean == {
			import from I, Partial R;
			for i in 1..#v repeat {
				failed?(u := exactQuotient(v.i, q)) => return false;
				v.i := retract u;
			}
			true;
		}

		local commonMultiple(v:V R):R == {
			import from List R;
			-- TEMPORARY: would like to cache the test
			-- gcd? => lcm [generator v];
			R has GcdDomain => lcm [generator v];
			p:R := 1;
			for q in v repeat p := times!(p, q);
			p;
		}

		commonDenominator(s:%):(R, M R) == {
			import from I, V R;
			(v, A) := matrix s;
			d := commonMultiple v;
			n := dimension s;
			B := copy A;
			for i in 1..n repeat {
				q := quotient(d, v.i);
				if ~one? q then for j in 1..n repeat
					B(i,j) := times!(B(i,j), q);
			}
			(d, B);
		}

		if R has FiniteCharacteristic then {
			pCurvature(s:%, sigma:Automorphism R):% == {
				import from I, Z, R;
				(d, A) := commonDenominator s;
				p := machine(characteristic$R);
				q := d;
				B := copy A;
				f := function sigma;
				for i in 1..prev p repeat {
					q := f q;
					d := q * d;
					B := map!(f)(B);
					A := B * A;
				}
				system(d, A);
			}

			pCurvature(s:%, D:Derivation R):% == {
				import from I, Z, R;
				(d, A) := commonDenominator s;
				p := machine(characteristic$R);
				B := -A;
				f := function D;
				for i in 1..prev p repeat {
					Bn := copy B;
					B := times!(d, map!(f)(B));
					B := minus!(B, A * Bn);
					B := add!(B, (-i)::Z * f d, Bn);
				}
				system(d^p, B);
			}
		}
	}
}

LinearOrdinaryFirstOrderSystem2(R:CommutativeRing, S:CommutativeRing): with {
	map: (R -> S) -> SYS R -> SYS S;
} == add {
	map(f:R -> S):SYS R -> SYS S == {
		import from V R, V S;
		fm := map(f)$MatrixCategory2(R, M R, S, M S);
		(L:SYS R):SYS S +-> {
			(v, m) := matrix L;
			system([f(r) for r in v], fm m);
		}
	}
}

