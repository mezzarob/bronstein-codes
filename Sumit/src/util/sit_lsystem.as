-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_lsystem.as ------------------------------
-- Copyright (c) Manuel Bronstein 2004
-- Copyright (c) INRIA 2004, Version 1.0.3
-- Logiciel Sum^it (c) INRIA 2004, dans sa version 1.0.3
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I == MachineInteger;
	Z == Integer;
	M == DenseMatrix;
	V == Vector;
	RR == (R pretend GcdDomain);
	Q == Fraction RR;
	SYS == LinearOrdinarySystem;
	UP == SparseUnivariatePolynomial;
}

#if ALDOC
\thistype{LinearOrdinarySystem}
\History{Manuel Bronstein}{20/8/2004}{created}
\Usage{ import from \this~R}
\Params{
{\em R} &\alexttype{CommutativeRing}{} & The coefficient ring of the operators\\
}
\Descr{\this~R provides linear ordinary
systems of arbitrary order with coefficients in R.}
\begin{exports}
\end{exports}
#endif

LinearOrdinarySystem(R:CommutativeRing): with {
	matrix: % -> (V R, Z, UP M R);
#if ALDOC
\alpage{matrix}
\Usage{\name~L}
\Signature{\%}{(\alexttype{Vector}{} R, \alexttype{Integer}{},
\alexttype{SparseUnivariatePolynomial}{} \alexttype{DenseMatrix}{} R)}
\Params{ {\em L} & \% & A system\\ }
\Retval{Returns $([a_1,\dots,a_m], n, P)$ such that \emph{L} is the system
$$
\pmatrix{
a_1 & & \cr & \ddots & \cr & & a_m
}
\theta^n Y = P(\theta) Y
$$
for some pseudo--linear operator $\theta$.}
#endif
	order: % -> Z;
#if ALDOC
\alpage{order}
\Usage{\name~L}
\Signature{\%}{\alexttype{Integer}{}}
\Params{ {\em L} & \% & A system\\ }
\Retval{Returns \emph{n} such that the \emph{L} is of the form
$\theta^n Y = P(\theta) Y$
for some pseudo--linear operator $\theta$.}
#endif
	size: % -> I;
#if ALDOC
\alpage{size}
\Usage{\name~L}
\Signature{\%}{\alexttype{MachineInteger}{}}
\Params{ {\em L} & \% & A system\\ }
\Retval{Returns the number of unknowns of $L$.}
#endif
	system: (Z, UP M R) -> %;
	system: (R, Z, UP M R) -> %;
	system: (V R, Z, UP M R) -> %;
	if R has GcdDomain then {
		system: (Z, UP M Q) -> %;
	}
#if ALDOC
\alpage{system}
\Usage{\name(n, P)\\ \name(n, Q)\\ \name(a, n, P)\\
\name($[a_1,\dots,a_m]$, n, P)}
\Params{
{\em a}, $a_i$ & R & Coefficients\\
{\em P} & \alexttype{SparseUnivariatePolynomial}{} \alexttype{DenseMatrix}{} R &
A matrix polynomial\\
{\em Q} & \alexttype{SparseUnivariatePolynomial}{} \alexttype{DenseMatrix}{}
\alexttype{Fraction}{} R & A matrix polynomial\\
}
\Retval{Return respectively the systems $\theta&^n Y = P(\theta) Y$,
$\theta^n Y = Q(\theta) Y$, $a \theta^n Y = P(\theta) Y$ and
$$
\pmatrix{
a_1 & & \cr & \ddots & \cr & & a_m
}
\theta^n Y = P(\theta) Y
$$
}
\Remarks{The matrix coefficients of \emph{P} or \emph{Q} must all have
the same square dimensions.}
#endif
} == add {
	Rep == Record(coeff:V R, ord:Z, mat:UP M R);
	import from Rep;

	local coef(s:%):V R		== rep(s).coeff;
	local mat(s:%):UP M R		== rep(s).mat;
	matrix(s:%):(V R, Z, UP M R)	== explode rep s;
	order(s:%):Z			== rep(s).ord;
	system(n:Z, m:UP M R):%		== { import from R; system(1, n, m); }

	size(s:%):I == {
		import from M R, UP M R;
		numberOfRows leadingCoefficient mat s;
	}

	system(a:V R, n:Z, m:UP M R):% == {
		assert(~zero? m); assert(n > degree m);
		per [a,n,m];
	}

	system(a:R, n:Z, m:UP M R):% == {
		import from V R, M R;
		system(new(numberOfRows leadingCoefficient m, a), n, m);
	}

	if R has GcdDomain then {
		system(n:Z, m:UP M Q):% == {
			import from I, UP R, M Q;
			import from MatrixCategoryOverFraction(RR,M RR,Q,M Q);
			nu := numberOfRows leadingCoefficient m;
			q:V UP R := zero nu;
			p:UP M R := 0;
			for trm in m repeat {
				(b, e) := trm;
				(v, a) := makeRowIntegral b;	-- a = v b
				p := add!(p, a, e);
				for i in 1..nu repeat q.i := add!(q.i, v.i, e);
			}
			system(n, p);	-- TEMPORARY
		}
	}
}

LinearOrdinarySystem2(R:CommutativeRing, S:CommutativeRing): with {
	map: (R -> S) -> SYS R -> SYS S;
} == add {
	map(f:R -> S):SYS R -> SYS S == {
		import from V R, V S, MonogenicAlgebra2(M R,UP M R,M S,UP M S);
		fp := map mapmat f;
		(L:SYS R):SYS S +-> {
			(v, n, m) := matrix L;
			system([f(r) for r in v], n, fp m);
		}
	}

	local mapmat(f:R -> S)(m:M R):M S == {
		import from I;
		(r, c) := dimensions m;
		ms:M S := zero(r, c);
		for i in 1..r repeat for j in 1..c repeat ms(i, j) := f m(i, j);
		ms;
	}
}

