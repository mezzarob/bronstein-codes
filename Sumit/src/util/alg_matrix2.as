-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------   alg_matrix2.as   -----------------------

-- Moved to ALGEBRA 1.0.3

#include "algebra"

macro {
	I == MachineInteger;
	V == Vector;
}

#if ALDOC
\thistype{MatrixCategory2}
\History{Manuel Bronstein}{8/9/2004}{created}
\Usage{import from \this(R, MR, S, MS)}
\Params{
\emph{R,S} & \altype{ExpressionType} & Coefficient domains\\
           & \altype{ArithmeticType} &\\
{\em MR} & \altype{MatrixCategory} R & a matrix type over R\\
{\em MS} & \altype{MatrixCategory} S & a matrix type over S\\
}
\Descr{\this(R,MR,S,MS) provides tools for lifting maps $R \to S$ to
maps $MR \to MS$.}
\begin{exports}
\alexp{map}: & (R $\to$ S) $\to$ MR $\to$ MS & Lift a mapping\\
\end{exports}
#endif

MatrixCategory2(R:Join(ExpressionType, ArithmeticType), MR:MatrixCategory R,
	S:Join(ExpressionType, ArithmeticType), MS:MatrixCategory S): with {
		map: (R -> S) -> MR -> MS;
#if ALDOC
\alpage{map}
\Usage{\name~f\\\name(f)(m)}
\Signature{(R $\to$ S) $\to$ MR}{MS}
\Params{
{\em f} & R $\to$ S & A map\\
{\em m} & MR & A matrix with entries in \emph{R}\\
}
\Descr{\name(f)(m) returns a matrix whose $\sth{(i,j)}$ entry is $f(m_{ij})$,
while \name(f) returns the mapping $m \to f(m)$.}
#endif
} == add {
	map(f:R -> S)(m:MR):MS == {
		import from MachineInteger;
		(r, c) := dimensions m;
		mm:MS := zero(r, c);
		for i in 1..r repeat for j in 1..r repeat mm(i,j) := f m(i, j);
		mm;
	}
}
