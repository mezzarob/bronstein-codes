-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_matpow.as ------------------------------
-- Copyright (c) Manuel Bronstein 2003
-- Copyright (c) INRIA 2003, Version 1.0.2
-- Logiciel Sum^it (c) INRIA 2003, dans sa version 1.0.2
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I   == MachineInteger;
	Z   == Integer;
	ARR == PrimitiveArray;
	V   == Vector;
	PP  == DenseHomogeneousPowerProduct;
	DHP == DenseHomogeneousPolynomial;
}

MatrixSymmetricPower(R:Join(ArithmeticType, ExpressionType),
			M: MatrixCategory R): with {
	extendPowerMatrices: (V M, I) -> V M;
	powerMatrices: (M, I) -> V M;
	symmetricPower: (M, I, Z -> R) -> M;
	symmetricPower: (M, I, V M, Z -> R) -> M;
	if R has Ring then {
		symmetricPower: (M, I) -> M;
		symmetricPower: (M, I, V M) -> M;
	}
} == add {
	if R has Ring then {
		symmetricPower(A:M, n:I):M == symmetricPower(A, n, coerce$R);

		symmetricPower(A:M, n:I, An:V M):M ==
			symmetricPower(A, n, An, coerce$R);
	}

	symmetricPower(A:M, n:I, inj:Z -> R):M == {
		assert(n > 0); assert(square? A);
		one? n => A;
		symmetricPower(A, n, numberOfRows A, powerMatrices(A, n), inj);
	}

	symmetricPower(A:M, n:I, An:V M, inj:Z -> R):M == {
		assert(n > 0); assert(square? A);
		assert(numberOfRows(A) = #An);
		assert(empty?(An) or (n = numberOfRows(An.1)));
		one? n or zero?(m := numberOfRows A) => A;
		symmetricPower(A, n, m, An, inj);
	}

	-- dimension of the n-th symmetric power of a space of dimension m
	local dimension(n:I, m:I):I == {
		import from Z;
		machine binomial(prev(m::Z + n::Z), prev(m::Z));
	}

	local symmetricPower(A:M, n:I, m:I, An:V M, inj:Z -> R):M == {
		import from Z;
		assert(n > 1); assert(m > 0); assert(square? A);
		assert(m = numberOfRows A); assert(m = #An);
		assert(n = numberOfRows(An.1));
		N := dimension(n, m);
		B := zero(N, N);
		for i in 1..N repeat fillRow!(B, N, n, m, i, An, inj);
		B;
	}

	-- fills row r of B
	local fillRow!(B:M, N:I, n:I, m:I, r:I, pow:V M, inj:Z -> R):() == {
		import from Z, PP(n,m), Pointer, List Array I;
		p := lookup(r::Z);
		-- each Pi is in fact a hom poly of degree degree(p, i)
		P:ARR Pointer := new(m, nil);
		for i in 1..m repeat {
			di := degree(p, i);
			if di > 0 then {
				zeroRow?(pow.i, 1) => return;
				P(prev i) := power(pow.i, di, m, inj);
			}
		}
		-- need to compute the product of all the non nil Pi's
		prod:DHP(R, n, m) := 0;
		for pair in paths(P, m, exponent p, 1) repeat {
			(exponents, coeff) := pair;
			prod := add!(prod, coeff, sum(exponents, n, m));
		}
		for j in 1..N repeat B(r, j) := coefficient(prod, j);
	}

	local sum(l:List Array I, n:I, m:I):PP(n, m) == {
		s:Array I := new(m, 0);
		for a in l repeat {
			assert(m = #a);
			for i in 0..prev m repeat s.i := s.i + a.i;
		}
		assert(n = sum s);
		monomial s;
	}

	local sum(a:Array I):I == {
		s := 0;
		for x in a repeat s := s + x;
		s;
	}

	local paths(P:ARR Pointer, m:I, p:Array I, frum:I):
		Generator Cross(List Array I, R) == {
			import from List Array I, R;
			assert(0 < frum);
			frum > m => generate { yield (empty, 1); }
			zero?(p prev frum) => paths(P, m, p, next frum);
			paths0(P, m, p, frum, p prev frum);
	}

	local paths0(P:ARR Pointer, m:I, p:Array I, frum:I, d:I):
		Generator Cross(List Array I, R) == generate {
			import from Pointer, Z, R, List Array I, R;
			import from PP(d, m), DHP(R, d, m);
			assert(0 < frum); assert(frum <= m); assert(d > 0);
			assert(~nil?(P prev frum));
			N := dimension(d, m);
			q := P(prev frum) pretend DHP(R, d, m);	-- whopee!
			for pair in paths(P, m, p, next frum) repeat {
				(exponents, coeff) := pair;
				for i in 1..N repeat {
					c := coefficient(q, i);
					if ~zero?(c) then yield_
						(cons(exponent lookup(i::Z),
							exponents), c * coeff);
				}
			}
	}

	local zeroRow?(A:M, r:I):Boolean == {
		import from R;
		for j in 1..numberOfColumns A repeat {
			~zero? A(r, j) => return false;
		}
		true;
	}

	-- return (\sum_j A(1,j) Y_j)^d 
	local power(A:M, d:I, m:I, inj:Z -> R):Pointer ==
		((power(M, inj)$DHP(R, d, m))(A)) pretend Pointer;

	-- returns [A1,A2,...,Ar] where r = number of rows of A
	-- and row(j) of Ai contains the j-th powers of the i-th row of A
	powerMatrices(A:M, n:I):V M ==
		[powers(A, i, n) for i in 1..numberOfRows A];

	-- returns a matrix whose rows are the powers of the r-th row of A
	local powers(A:M, r:I, n:I):M == {
		import from R;
		assert(r <= numberOfRows A);
		c := numberOfColumns A;
		B := zero(n, c);
		for j in 1..c repeat B(1, j) := A(r, j);
		for i in 2..n repeat for j in 1..c repeat
			B(i, j) := B(1, j) * B(prev i, j);
		B;
	}

	-- extend each Am.i up to the n-th powers of each first row
	extendPowerMatrices(Am:V M, n:I):V M == {
		import from M;
		empty? Am or n <= (m := numberOfRows(Am.1)) => Am;
		[extendMatrix(A, m, n) for A in Am];
	}

	local extendMatrix(A:M, m:I, n:I):M == {
		import from R;
		assert(m = numberOfRows A); assert(m < n);
		c := numberOfColumns A;
		B := zero(n, c);
		setMatrix!(B, 1, 1, A);
		for i in next(m)..n repeat for j in 1..c repeat
			B(i, j) := B(1, j) * B(prev i, j);
		B;
	}
}
