-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------------- sit_version.as ----------------------------------
--
-- This file provides utilities for storing version info in libraries
--
-- Copyright (c) Manuel Bronstein 2001
-- Copyright (c) INRIA 1998, Version 29-10-98
-- Logiciel Sum^it (c) INRIA 1999, dans sa version 0.1.12
-----------------------------------------------------------------------------

#include "sumit"

macro {
	Z == MachineInteger;
	TX == TextWriter;
	VER == "Sum^it Library Version 1.0.2 - Septembers 20, 2004";
	CPR == _
	"Sum^it - Copyright (c) 1994-2004, ETH Zurich, INRIA and M.Bronstein";
}

#if ALDOC
\thistype{SumitLibraryInformation}
\History{Manuel Bronstein}{24/9/2001}{created}
\Usage{import from \this}
\Descr{\this~provides version information about the \sumit{} library.}
\begin{exports}
\category{\alexttype{VersionInformationType}{}}\\
\alexp{banner}: & (\alexttype{TextWriter}{}, \alexttype{String}{}) $\to$
\alexttype{TextWriter}{} & write a \sumit{} banner\\
\end{exports}
#endif

SumitLibraryInformation: VersionInformationType with {
	banner: (TX, String) -> TX;
#if ALDOC
\alpage{banner}
\Usage{\name(p, s)}
\Signature{(\alexttype{TextWriter}{}, \alexttype{String}{})}
{\alexttype{TextWriter}{}}
\Params{
{\em p} & \alexttype{TextWriter}{} & The port to write to\\
{\em s} & \alexttype{String}{} & A name to center in the banner\\
}
\Descr{Writes the default \sumit~banner to the port p.}
\Remarks{The string s is automatically centered in the banner. In order
for the banner to look ok, it must have a maximum of 33 characters.}
\begin{alex}
\begin{ttyout}
banner(stdout, "B E R N I N A");
\end{ttyout}
writes the banner
\begin{verbatim}
                                                          /\
      -----  it                                          /| \
      \                   B E R N I N A                 / |  \
       \                                               |   \  \
       /                 A Sum^it server               |    |  \
      /                                                /     \  \
      -----                                           /      |   \
                                                     /       /    \


\end{verbatim}
to {\tt stdout}.
\end{alex}
#endif
} == add {
	name:String		== "sumit";
	version:String		== VER;
	credits:List String	== cons(CPR, credits$AlgebraLibraryInformation);
	major:Z			== 1;
	minor:Z			== 0;
	patch:Z			== 2;

	-- title is automatically centered in a 33-character field
	banner(p:TX, title:String):TX == {
		macro NL == newline;
		import from Z, Character, WriterManipulator;
		n := max(0, 33 - #title);	-- total # of spaces to add
		n2 := n quo 2;
		left := new(n2, space);
		right := new(n - n2, space);
		p << NL;
p << "                                                          /\    " << NL;
p << "      -----  it                                          /| \    " << NL;
p << "      \         "  << left << title << right << "       / |  \    " << NL;
p << "       \                                               |   \  \   " << NL;
p << "       /                 A Sum^it server               |    |  \  " << NL;
p << "      /                                                /     \  \ " << NL;
p << "      -----                                           /      |   \" << NL;
p << "                   Use ^D to terminate session       /       /    \"<< NL;
		p << NL << endnl;
	}

}

