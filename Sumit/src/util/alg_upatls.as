-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
----------------------------- alg_upatls.as --------------------------------
-- Copyright (c) Manuel Bronstein 2004
-- Copyright (c) INRIA 2004, Version 1.0.2
-- Logiciel Sum^it (c) INRIA 2004, dans sa version 1.0.2
-----------------------------------------------------------------------------

-- SHOULD MOVE TO ALGEBRA 1.0.3

#include "algebra"

macro {
	I == MachineInteger;
	Z == Integer;
	V == Vector;
	M == DenseMatrix;
}

UnivariatePolynomialAlgebraTools(R:IntegralDomain,
				RX:UnivariatePolynomialAlgebra R): with {
	coefficient: (M RX, Z) -> M R;
	degree: M RX -> Z;
	trailingDegree: M RX -> Z;
	degrees: M RX -> (Z, Z);
	remainderMatrix: (RX, RX -> RX) -> M R;
	transform: (M RX, RX -> (R, RX)) -> (V R, M RX);
	if R has GcdDomain then {
		content: M RX -> V R;
		primitive: M RX -> (V R, M RX);
		primitive!: M RX -> (V R, M RX);
		primitivePart: M RX -> M RX;
		primitivePart!: M RX -> M RX;
	}
} == add {
	degree(L:M RX):Z		== { (mn, mx) := degrees L; mx; }
	trailingDegree(L:M RX):Z	== { (mn, mx) := degrees L; mn; }

	-- given f s.t. f(p) = (r, q) where q = r g(p)
	-- returns [v, B] s.t. B = diag(v) g(A)
	transform(A:M RX, f:RX -> (R, RX)):(V R, M RX) == {
		import from I;
		(r, c) := dimensions A;
		B := zero(r, c);
		v:V R := [transform!(B, i, A, f) for i in 1..r];
		(v, B);
	}

	-- fills row i of B
	local transform!(B:M RX, i:I, A:M RX, f:RX -> (R, RX)):R == {
		import from R, RX;
		l:List R := empty;
		c := numberOfColumns A;
		assert(c = numberOfColumns B);
		for j in 1..c repeat {
			(r, q) := f(A(i,j));
			l := cons(r, l);
			B(i,j) := q;
		}
		l := reverse! l;
		r := {
			R has GcdDomain => lcm l;
			product l;
		}
		for j in 1..c for s in l repeat
			B(i, j) := times!(quotient(r, s), B(i, j));
		r;
	}

	local product(l:List R):R == {
		p:R := 1;
		for r in l repeat p := times!(p, r);
		p;
	}

	coefficient(L:M RX, n:Z):M R == {
		import from I, RX;
		(r, c) := dimensions L;
		m:M R := zero(r, c);
		for i in 1..r repeat for j in 1..c repeat
			m(i, j) := coefficient(L(i, j), n);
		m;
	}

	-- returns (trailing degree, leading degree)
	degrees(L:M RX):(Z, Z) == {
		import from I, RX;
		mx:Z := 0;
		mn:Z := -1;
		(r, c) := dimensions L;
		for i in 1..r repeat for j in 1..c | ~zero? L(i,j) repeat {
			d := degree L(i,j);
			if d > mx then mx := d;
			if mn < 0 or d < mn then mn := d;
		}
		(mn, mx);
	}

	remainderMatrix(p:RX, rrembyp:RX -> RX):M R == {
		import from I, Z, R;
		assert(~zero? p);
		n :=  machine degree p;
		s := zero(n, n);
		zero? n => s;
		r := monomial(prev(n)::Z);
		for i in 1..n repeat {
			r := rrembyp(monom * r);
			for trm in r repeat {
				(c, e) := trm;
				s(i, next machine e) := c;
			}
		}
		s;
	}

	if R has GcdDomain then {
		content(m:M RX):V R		== content!(copy m, false);
		primitive(m:M RX):(V R, M RX)	== primitive! copy m;
		primitivePart(m:M RX):M RX	== primitivePart! copy m;
		primitivePart!(m:M RX):M RX	== { content!(m, true); m; }

		primitive!(m:M RX):(V R, M RX) == {
			c := content!(m, true);
			(c, m);
		}

		-- makes m primitive along the way
		local content!(m:M RX, modify?:Boolean):V R == {
			import from I, R, RX;
			(r, c) := dimensions m;
			v:V R := new(r, 1);
			zero? c => v;
			for i in 1..r repeat  {
				ct := content m(i, 1);
				for j in 2..c while ~unit?(ct) repeat
					ct := gcd(ct, content m(i, j));
				v.i := ct;
				if modify? and ~zero?(ct) and ~unit?(ct) then {
					f := quotientBy ct;
					g:RX -> RX := map f;
					for j in 1..c repeat m(i,j) := g m(i,j);
				}
			}
			v;
		}
	}
}
