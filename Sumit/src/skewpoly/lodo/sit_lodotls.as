-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_lodotls.as ------------------------------
--
-- Fraction-free tools for linear ordinary differential operators
--
-- Copyright (c) Manuel Bronstein 1995-2001
-- Copyright (c) INRIA 2000, Version 0.1.12
-- Logiciel Sum^it (c) INRIA 2000, dans sa version 0.1.12
-- Copyright Swiss Federal Polytechnic Institute Zurich, 1995-1997
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I == MachineInteger;
	Z == Integer;
	V == Vector;
	M == DenseMatrix;
	F == Fraction R;
	LF == LinearOrdinaryDifferentialOperator F;
}

#if ALDOC
\thistype{LinearOrdinaryDifferentialOperatorTools}
\History{Manuel Bronstein}{10/6/97}{created}
\Usage{ import from \this(R, L) }
\Params{
{\em R} & \alexttype{IntegralDomain}{} & The coefficient ring\\
{\em L} & \altype{LinearOrdinaryDifferentialOperatorCategory} R& The operators\\
}
\Descr{\this(R, L) implements some useful manipulations on the
elements of \emph{L}.}
\begin{exports}
\alexp{compose}:
& (R, R) $\to$ L $\to$ (L, R) & Compose operator with $D + a/b$\\
\alexp{logarithmicDerivative}:
& \alexttype{Product}{} R $\to$ (R, R) & Logarithmic derivative\\
\alexp{twist}:
& \alexttype{Product}{} R $\to$ L $\to$ (R, L) & Change of variable\\
\alexp{unimodularize}: & L $\to$ (R, L, R, R) & Make operator unimodular\\
\end{exports}
#endif

LinearOrdinaryDifferentialOperatorTools(R:IntegralDomain,
	L:LinearOrdinaryDifferentialOperatorCategory R): with {
		compose: (R, R) -> L -> (R, L);
#if ALDOC
\alpage{compose}
\Usage{\name(a, b)\\ \name(a, b)(p)}
\Signature{(R, R) $\to$ L}{(R, L)}
\Params{
{\em a} & R & An element of the coefficient ring\\
{\em b} & R & A nonzero element of the coefficient ring\\
{\em p} & L & A differential operator\\
}
\Retval{\name(a,b)(p) returns $(\alpha, {\overline p})$ such that
$$
{\overline p} = \alpha p\paren{\partial + \frac ab} =
\alpha \sum_{i=0}^n a_i \paren{\partial + \frac ab}^i
$$
where $p = \sum_{i=0}^n a_i \partial^i$,
and $\alpha \in R\setminus\{0\}$ is chosen so that
the result has coefficients in $R$.
\name(a,b) returns the map $p \to \name(a,b)(p)$.
}
#endif
		-- returns (a, b) s.t. d'/d = a/b
		logarithmicDerivative: Product R -> (R, R);
#if ALDOC
\alpage{logarithmicDerivative}
\Usage{\name~q}
\Signature{\alexttype{Product}{} R}{(R, R)}
\Params{ {\em q} & \alexttype{Product}{} R & A product of coefficients\\ }
\Retval{Returns $(a, b)$ such that $q'/q = a/b$.}
#endif
		twist: Product R -> L -> (R, L);
#if ALDOC
\alpage{twist}
\Usage{\name~q\\ \name(q)(p)}
\Signature{\alexttype{Product}{} R $\to$ L}{(R, L)}
\Params{
{\em q} & \alexttype{Product}{} R & A product of coefficients\\
{\em p} & L & A differential operator\\
}
\Retval{\name(q)(p) returns $(\alpha, {\overline p})$ such that
$\deg({\overline p}) = \deg(p)$ and
$$
{\overline p} = \alpha p\paren{\partial - \frac {q'}q} =
\alpha \sum_{i=0}^n a_i \paren{\partial - \frac {q'}q}^i
$$
where $p = \sum_{i=0}^n a_i \partial^i$,
and $\alpha \in R\setminus\{0\}$ is chosen so that
the result has coefficients in $R$.
We have
$$
{\overline p}(y) = \alpha q p\paren{\frac yq}
$$
for any $y$ in any differential extension of $R$,
and in particular,
$$
{\overline p}(y) = 0 \iff p\paren{\frac yq} = 0\,.
$$
\name~q returns the map $p \to \name(q)(p)$.
}
#endif
	unimodularize: L -> (R, L, R, R);
#if ALDOC
\alpage{unimodularize}
\Usage{($\alpha$, q, a, b) := \name~p}
\Signature{L}{(R, L, R, R)}
\Params{ {\em p} & L & A differential operator\\ }
\Retval{\name~p returns $(\alpha, q, a, b)$ such that
the coefficient of $\partial^{n-1}$ in $q$ is $0$ and
$$
q = \alpha p\paren{\partial + \frac ab} =
\alpha \sum_{i=0}^n a_i \paren{\partial + \frac ab}
$$
where $p = \sum_{i=0}^n a_i \partial^i$.
$\alpha \in R\setminus\{0\}$ is chosen so that
the result has coefficients in $R$.
}
\Remarks{If $q(e^{\int u}) = 0$ for $r(x,u) = 0$, then
$p(e^{\int u}) = 0$ for $r(x,u - a/b) = 0$.}
#endif
} == add {
	-- TEMPO: WOULD LIKE TO CACHE THE TEST
	-- local gcd?:Boolean == R has GcdDomain;

	-- returns (alpha, U, a, b) where U = alpha * the unimodular transform of op
	-- and (a, b) is the shift for Riccati polynomials (c(y) --> c(y - a/b))
	-- a = 0 iff op is already unimodular, in which case U = op
	unimodularize(op:L):(R, L, R, R) == {
		import from Z;
		zero? op or zero?(d := degree op) => (0, op, 0, 1);
		a := - coefficient(op, prev d);
		b := d * leadingCoefficient op;
		(alpha, U) := compose(a, b)(op);
		(alpha, U, a, b);
	}

	-- Returns (alpha, op') such that op' = alpha sum_i a_i (D - d'/d)^i
	-- has coefficients in R, where where op = sum_i a_i D^i
	-- We have op(z/d) = d^{-1} alpha^{-1} op'(z), so
	-- op(z/d) = 0  iff  op'(z) = 0, and
	-- op(z/d) = g  iff  op'(z) = d alpha g
	twist(d:Product R):L -> (R, L) == {
		import from R;
		(a, b) := logarithmicDerivative d;
		compose(-a, b);
	}

	-- returns (a, b) s.t. d'/d = a/b
	logarithmicDerivative(d:Product R):(R, R) == {
		import from R, Derivation R;
		dstar:R := 1;
		for term in d repeat {
			(p, n) := term;
			dstar := times!(dstar, p);
		}
		D: Derivation R := derivation$L;
		a:R := 0;
		for term in d repeat {
			(p, n) := term;
			a := add!(a, n * quotient(dstar, p) * D p);
		}
		(a, dstar);
	}

	if R has GcdDomain then {
		local gcdcompose(a:R, b:R):L -> (R, L) == {
			import from Z, Partial R, L;
			TRACE("lodotls::gcdcompose: a = ", a);
			TRACE("lodotls::gcdcompose: b = ", b);
			(g, A, B) := gcdquo(a, b);
			TRACE("lodotls::gcdcompose: g = ", g);
			TRACE("lodotls::gcdcompose: A = ", A);
			TRACE("lodotls::gcdcompose: B = ", B);
			~failed?(u := reciprocal B) => {
				TRACE("lodotls::compose: B^{-1} = ", retract u);
				q := monom + (A * retract u)::L;
				(op:L):(R, L) +-> (1, compose(op, q));
			}
			(op:L):(R, L) +-> {
				(opbar, n) := compose0(op, A, B);
				assert(n >= 0);
				(B^n, opbar);
			}
		}
	}

	-- returns (d, opbar) such that opbar = d sum_i a_i (D + a/b)^i
	-- has coefficients in R, where where op = sum_i a_i D^i
	compose(a:R, b:R):L -> (R, L) == {
		import from Z, Partial R, L;
		TRACE("lodotls::compose: a = ", a);
		TRACE("lodotls::compose: b = ", b);
		assert(~zero? b);
		zero? a => (op:L):(R, L) +-> (1, op);
		-- TEMPO: WOULD LIKE TO CACHE THE TEST
		-- gcd? => gcdcompose(a, b);
		R has GcdDomain => gcdcompose(a, b);
		~failed?(u := exactQuotient(a, b)) => {
			q := monom + retract(u)::L;
			(op:L):(R, L) +-> (1, compose(op, q));
		}
		(op:L):(R, L) +-> {
			(opbar, n) := compose0(op, a, b);
			assert(n >= 0);
			(b^n, opbar);
		}
	}

	macro {
		Rb == FractionBy(R, b, false);
		Lb == LinearOrdinaryDifferentialOperator(Rb, D);
	}

	local compose0(op:L, a:R, b:R):(L, Z) == {
		import from Rb;
		TRACE("lodotls::compose0: op = ", op);
		TRACE("lodotls::compose0: a = ", a);
		TRACE("lodotls::compose0: b = ", b);
		(opbar, n) := compose0(op, a, b, lift(derivation$L));
		TRACE("lodotls::compose0: opbar = ", opbar);
		TRACE("lodotls::compose0: n = ", n);
		n < 0 => (b^(-n) * opbar, 0);
		(opbar, n);
	}

	-- all simplifications have failed, a/b is not in R
	-- returns (opbar, n) such that opbar = b^n L(D + a/b) has coeffs in R
	local compose0(op:L, a:R, b:R, D:Derivation Rb):(L, Z) == {
		import from MachineInteger, Z, Rb, Lb;
		assert(~unit? b);
		TRACE("lodotls::compose0: op = ", op);
		TRACE("lodotls::compose0: a = ", a);
		TRACE("lodotls::compose0: b = ", b);
		TRACE("lodotls::compose0: D = ", "derivation");
		outside := next degree op;
		pq:Lb := term(1, outside);		-- to allocate space
		TRACE("lodotls::compose0: pq = ", pq);
		-- TEMPO: WEIRD 1.0.0 BUG WITH :: (RUNTIME SEG FAULT, EVEN @-q1)
		-- q:Lb := monom + shift(a::Rb, -1)::Lb;	-- q = D + a/b
		q:Lb := monom + term(shift(a::Rb, -1), 0);	-- q = D + a/b
		TRACE("lodotls::compose0: q = ", q);
		qn:Lb := 1;
		d:Z := 0;
		TRACE("lodotls::compose0: d = ", d);
		for term in terms op while ~zero?(qn) repeat {	-- low to high
			(c, n) := term;
			TRACE("lodotls::compose0: c = ", c);
			TRACE("lodotls::compose0: n = ", n);
			-- TEMPORARY: BAD OPTIMIZATION BUG (WAS 1203/1232)
			-- for i in 1..machine(n-d) repeat qn := q * qn;
			i:MachineInteger := 1; while i <= machine(n-d) repeat {
								qn := q * qn;
								i := next i;
			}
			TRACE("lodotls::compose0: qn = ", qn);
			pq := add!(pq, c::Rb, qn);
			TRACE("lodotls::compose0: pq = ", pq);
			d := n;
			TRACE("lodotls::compose0: d = ", d);
		}
		pq := add!(pq, -1, outside);	-- remove extra term
		ans:L := 0;			-- pq is the true composition
		zero? pq => (ans, 0);
		m:Z := order leadingCoefficient pq;
		for bterm in reductum pq repeat {
			(bc, n) := bterm;
			n := order bc;
			if n < m then m := n;
		}
		m := -m;
		for bterm in pq repeat {	-- compute b^m pq
			(bc, n) := bterm;
			bc := shift(bc, m);
			assert(one? denominator bc);
			ans := add!(ans, numerator bc, n);
		}
		(ans, m);
	}
}

