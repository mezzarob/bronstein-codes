-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_lodo.as ------------------------------
-- Copyright (c) Manuel Bronstein 1994
-- Copyright (c) INRIA 1999, Version 0.1.12
-- Logiciel Sum^it (c) INRIA 1999, dans sa version 0.1.12
-- Copyright (c) Swiss Federal Polytechnic Institute Zurich, 1994-97
-----------------------------------------------------------------------------

#include "sumit"

#if ALDOC
\thistype{LinearOrdinaryDifferentialOperator}
\History{Manuel Bronstein}{21/12/94}{created}
\Usage{
import from \this~S;\\
import from \this(S, x);\\
import from \this(R, D);\\
import from \this(R, D, x);
}
\Params{
{\em R} & \alexttype{Ring}{} & The coefficient ring of the operators\\
{\em S}&\alexttype{DifferentialRing}{} & The coefficient ring of the operators\\
{\em D} & \alexttype{Derivation}{} R & The derivation to use for the action\\
{\em x} & \alexttype{Symbol}{} & The variable name (optional)\\
}
\Descr{\this(R, D, x) implements linear ordinary differential
operators with coefficients in R. If the derivation D is not specified,
then R must be a \alexttype{DifferentialRing}{}. It is possible to override the
default derivation on R simply by passing another one as parameter.}
\begin{exports}
\category{\altype{LinearOrdinaryDifferentialOperatorCategory} R}\\
\end{exports}
#endif

macro {
	I == MachineInteger;
	RG == (R pretend GcdDomain);
	RID== (R pretend IntegralDomain);
	Rx == DenseUnivariatePolynomial(R, avar);
	F  == Fraction RG;
	LF == LinearOrdinaryDifferentialOperator(F, lift D);
	M  == DenseMatrix;
	TOOLS == UnivariateSkewPolynomialCategoryTools;
	TOOLSQ== UnivariateSkewPolynomialCategoryOverFraction;
}

LinearOrdinaryDifferentialOperator(R:Ring, D: Derivation R,
	avar:Symbol == new()): LinearOrdinaryDifferentialOperatorCategory R ==
	UnivariateSkewPolynomial(R, Rx, 1$Automorphism(R), function D) add {
		apply(p:%, r:R):R	== apply(p, 0, r);
		derivation:Derivation R	== D;

		if R has GcdDomain then {
			remainderMatrix(L:%):(Vector R, M R) == {
				import from R;
				unit? leadingCoefficient L => monicRemMatrix L;
				fracRemMatrix L;
			}

			local monicRemMatrix(L:%):(Vector R, M R) == {
				import from Integer, R, TOOLS(RID, %);
				(new(machine degree L, 1),
							monicRemainderMatrix L);
			}

			local fracRemMatrix(L:%):(Vector R, M R) == {
				import from F, TOOLSQ(RG, %, F, LF);
				fractionalRemainderMatrix L;
			}

			if R has FiniteCharacteristic then {
				pCurvature(L:%):(R, %) == {
					import from R;
					unit? leadingCoefficient L => monicPCurv L;
					fracPCurvature L;
				}

				local monicPCurv(L:%):(R, %) == {
					import from TOOLS(RID, %);
					(1, monicPCurvature L);
				}

				local fracPCurvature(L:%):(R, %) == {
					import from F, TOOLSQ(RG, %, F, LF);
					fractionalPCurvature L;
				}
			}
		}
}

LinearOrdinaryDifferentialOperator(R:DifferentialRing,
	avar:Symbol == new()): LinearOrdinaryDifferentialOperatorCategory R ==
		-- TEMPORARY: WORKAROUND FOR BUG1167
		-- LinearOrdinaryDifferentialOperator(R, derivation$R, avar);
		LinearOrdinaryDifferentialOperator(R, derivation$R, avar) add;


#if ALDORTEST
---------------------- test lodo.as --------------------------
#include "sumit"
#include "aldortest"

macro {
	Z == Integer;
	Zx == DenseUnivariatePolynomial(Z, -"x");
	Fd == LinearOrdinaryDifferentialOperator(Zx, -"D");
}

degree():Boolean == {
	import from Symbol, Z, Zx, Fd;
	x:Zx := monom;
	dx:Fd := monom;
	p := x^2 * dx^2 - x * dx + 1;
	degree p = 2 and leadingCoefficient p = x*x
			and zero?(x + leadingCoefficient reductum p);
}

exactQuotient():Boolean == {
	import from Symbol, Z, Zx, Fd, Partial Fd;

	x:Zx := monom;
	dx:Fd := monom;
	p := dx * dx - x::Fd;			-- p = D^2 - x  (Airy)
	p2 := p * p;
	ap2 := adjoint p2;			-- p and p^2 are self-adjoint
	ql := leftExactQuotient(ap2, p);	-- must be p
	qr := rightExactQuotient(p2, p);	-- must be p
	~(failed? ql) and ~(failed? qr) and retract(ql) = p and retract(qr) = p;
}

stdout << "Testing sit__lodo..." << endnl;
aldorTest("degree", degree);
aldorTest("exactQuotient", exactQuotient);
stdout << endnl;
#endif

