-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
----------------------------- sit_sskpolc.as --------------------------------
-- Copyright (c) Manuel Bronstein 2004
-- Copyright (c) INRIA 2004, Version 1.0.2
-- Logiciel Sum^it (c) INRIA 2004, dans sa version 1.0.2
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I == MachineInteger;
	Z == Integer;
	-- TEMPORARY: WOULD LIKE TO AVOID pretend WHEN THE COMPILER ALLOWS IT
	-- SYS == LinearOrdinaryFirstOrderSystem R;
	SYS == LinearOrdinaryFirstOrderSystem(R pretend CommutativeRing);
	V == Vector;
	M == DenseMatrix;
	RID == R pretend IntegralDomain;
}

#if ALDOC
\thistype{StandardUnivariateSkewPolynomialCategory}
\History{Manuel Bronstein}{6/09/2004}{created}
\Usage{\this~R: Category}
\Params{ {\em R} & \alexttype{Ring}{} & The coefficient ring\\ }
\Descr{\this~is the category of univariate skew polynomials with coefficients
in \emph{R} and such that either $\sigma$ is the identity on \emph{R}
or $\delta$ is the zero map on \emph{R}.}
\begin{exports}
\category{\alexttype{StandardUnivariateSkewPolynomialCategory}{} R}\\
\end{exports}
\begin{exports}[if R has \alexttype{GcdDomain}{} then]
\alexp{eigenringSystem}:
& \% $\to$ \alexttype{DenseMatrix}{} \% & Eigenring system\\
\end{exports}
\begin{exports}
[if R has \alexttype{FiniteCharacteristic}{} and R has \alexttype{GcdDomain}{} then]
\alexp{pCurvature}: & \% $\to$ (R, \%) & p-curvature\\
\end{exports}
#endif

StandardUnivariateSkewPolynomialCategory(R:Ring): Category ==
	UnivariateSkewPolynomialCategory R with {
		differential?: Boolean;
		recurrence?: Boolean;
	if R has GcdDomain then {
		eigenringSystem: % -> M %;
#if ALDOC
\alpage{eigenringSystem}
\Usage{\name~p}
\Signature{\%}{\alexttype{DenseMatrix}{} \%}
\Params{ \emph{p} & \% & A nonzero skew polynomial\\ }
\Retval{Returns a matrix \emph{M} of skew polynomials such that
a skew polynomial $q = \sum_{i=0}^{\deg(p)-1} q_i x^i$ is in the
eigenring of \emph{p} if and only if the vector of coefficients
$(q_0,\dots,q_{\deg(p)-1})$ is a solution of the system given by \emph{M}.}
#endif
	}
	if R has FiniteCharacteristic and R has GcdDomain then {
		pCurvature: % -> (R, %);
#if ALDOC
\alpage{pCurvature}
\Usage{\name~p}
\Signature{\%}{(R, \%)}
\Params{ {\em p} & \% & A skew--polynomial\\ }
\Retval{Returns $(r, q)$ such that the p-curvature of \emph{p}
is $r^{-1} q$.}
\alseealso{\alfunc{LinearOrdinaryFirstOrderSystem}{pCurvature}}
#endif
        }
 
	if R has GcdDomain then {
		remainderMatrix: % -> (V R, M R);
	}
	twists: % -> Array %;
	default {
		if R has GcdDomain then {
			eigenringSystem(L:%):M % == {
				import from I, Z, R, Array %, V R, M R,
					UnivariatePolynomialAlgebraTools(RID,%);
				assert(~zero? L);
				zero?(n :=  machine degree L) => zero(0, 0);
				one? n => zero(1, 1);
				(d, m) := remainderMatrix L;
				n2 := 2 * n;
				s:M % := zero(n, n2);
				t:M % := zero(n2, n);
				a := twists L;
				for i in 1..n repeat {
					s(i,i) := d.i :: %;
					t(i,i) := a.0;
					for j in 1..n repeat {
						s(i,j+n) := m(j, i)::%;
						t(j+i,i) := a.j;
					}
				}
				primitivePart!(s * t);
			}
		}
	}   -- end from default definitions
}
