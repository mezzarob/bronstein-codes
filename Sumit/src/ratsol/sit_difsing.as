-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-------------------------------- sit_difsing.as ------------------------------
--
-- Singularities of linear ordinary differential equations
-- with polynomial coefficients
--
-- Copyright (c) Manuel Bronstein 1998-2001
-- Copyright (c) INRIA 2000, Version 0.1.12
-- Logiciel Sum^it (c) INRIA 2000, dans sa version 0.1.12
-----------------------------------------------------------------------------

#include "sumit"

macro {
	Z	== Integer;
	Q	== Fraction Z;
	QR	== FractionalRoot Z;
	NP	== NewtonPolygon(R, RX, RXD);
	QmZ	== SetsOfRationalsModuloZ;
}

-- represents classes of equivalence of rational numbers modulo Z,
-- where the representative of each class is its smallest element
SetsOfRationalsModuloZ: ExpressionType with {
	+: (%, %) -> %;
	+: (Q, %) -> %;
	*: (Z, %) -> %;
	empty: %;
	empty?: % -> Boolean;
	generator: % -> Generator Q;
	min: % -> (Partial Z, Partial Q);
	moduloZ: List QR -> %;
} == add {
	Rep == Set Q;
	import from Rep;

	empty:%				== per empty;
	empty?(s:%):Boolean		== empty? rep s;
	generator(s:%):Generator Q	== generator rep s;
	(s:%) = (t:%):Boolean		== rep(s) = rep(t);

	moduloZ(g:List QR):% == {
		import from Q, QR;
		l:% := empty;
		for q in g repeat {
			(a, b) := value q;
			l := (a/b) + l;
		}
		l;
	}

	extree(s:%):ExpressionTree == {
		import from Q, List ExpressionTree;
		ExpressionTreeList [extree x for x in s];
	}

	-- if t contains an element < q and congruent to q modulo Z, then return t
	-- otherwise return union(t,q) minus {elts of t congruent to q modulo Z}
	(q:Q) + (t:%):% == {
		import from Z;
		TRACE("QmodZ::*: q = ", q);
		TRACE("QmodZ::*: t = ", t);
		del:Set Q := empty;
		for s in t repeat {
			n := s - q;
			if one? denominator n then {
				if n > 0 then del := insert(s, del); else return t;
			}
		}
		TRACE("QmodZ::*: adding ", q);
		TRACE("QmodZ::*: removing ", del);
		per insert(q, rep(t) - del);
	}

	-- for s,t nonempty, return the classes modZ for { a+b | a in s & b in t }
	(s:%) + (t:%):% == {
		import from Q;
		TRACE("QmodZ::+: s = ", s);
		TRACE("QmodZ::+: t = ", t);
		l:% := empty;
		for r in s repeat for q in t repeat l := (r + q) + l;
		TRACE("QmodZ::+: l = ", l);
		l;
	}

	-- creates the set of classes modulo Z for { a1 + ... + am | ai in s }
	(m:Z) * (s:%):% == {
		assert(m > 0);
		TRACE("QmodZ::*: m = ", m);
		TRACE("QmodZ::*: s = ", s);
		one? m or empty? s => s;
		t := (m quo 2) * s;
		TRACE("QmodZ::*: t = ", t);
		t := t + t;
		TRACE("QmodZ::*: t = ", t);
		even? m => t;
		s + t;
	}

	-- return the min integer and min rational in s
	min(s:%):(Partial Z, Partial Q) == {
		import from Z, Q;
		mint:Partial Z := failed;
		mrat:Partial Q := failed;
		for r in s repeat {
			if failed? mrat or r < retract mrat then mrat := [r];
			if one? denominator r then {
				n := numerator r;
				if failed? mint or n < retract mint then
								mint := [n];
			}
		}
		(mint, mrat);
	}
}

PlaneSingularity(R:GcdDomain, RX: UnivariatePolynomialCategory R,
		RXD: LinearOrdinaryDifferentialOperatorCategory RX): with {
		allRationalExponents?: % -> Boolean;
		center: % -> RX;	-- divides the center of the polygon
		polygon: % -> NP;
		denominator: (%, RX) -> Partial Product RX;
		-- homogeneous bound:
		degreeBound: (%, m:Z == 1) -> (Partial Z, Partial Q);
		finite: (RX, Boolean, NP, List QR) -> %;
		infinity: (NP, List QR) -> %;
		irreducible?: % -> Boolean;
		minIntegerExponent: (%, m:Z == 1) -> Partial Z;
		numberOfIntegerExponents: % -> Z;
		numberOfRationalExponents: % -> Z;
		integerExponents: % -> List Z;
		rationalExponents: % -> List QR;
} == add {
	-- ctr		== center of the place
	-- irr?		== true when center is known to be irreducible
	-- npol		== newton polygon there (ctr | center(npol))
	-- nrat		== number of rational exponents (per root of the center)
	-- nint		== number of integer exponents (per root of the center)
	-- ratexp	== rational exponents
	Rep == Record(ctr:RX,irr?:Boolean,npol:NP,nrat:Z,nint:Z,ratexp:List QR);

	center(s:%):RX			== { import from Rep; rep(s).ctr; }
	polygon(s:%):NP			== { import from Rep; rep(s).npol; }
	rationalExponents(s:%):List QR	== { import from Rep; rep(s).ratexp; }
	numberOfRationalExponents(s:%):Z== { import from Rep; rep(s).nrat; }
	numberOfIntegerExponents(s:%):Z	== { import from Rep; rep(s).nint; }
	irreducible?(s:%):Boolean	== { import from Rep; rep(s).irr?; }

	integerExponents(s:%):List Z == {
		import from QR, List QR;
		[integralValue r for r in rationalExponents s | integral? r];
	}

	finite(a:RX, irred?:Boolean, N:NP, l:List QR):% == {
		import from Rep;
		(nq, nz) := sumOfMultiplicities l;
		per [a, irred?, N, nq, nz, l];
	}

	infinity(N:NP, l:List QR):% == {
		import from Rep, Boolean;
		(nq, nz) := sumOfMultiplicities l;
		per [center N, true, N, nq, nz, l];
	}

	-- return (nq, nz) which are the number of rational/integer exponents
	local sumOfMultiplicities(l:List QR):(Z, Z) == {
		import from QR;
		nq:Z := 0;
		nz:Z := 0;
		for x in l repeat {
			nq := add!(nq, m := multiplicity x);
			if integral? x then nz := add!(nz, m);
		}
		(nq, nz);
	}

	allRationalExponents?(s:%):Boolean == {
		import from Z, RX, NP;
		m := numberOfRationalExponents s;
		newton := polygon s;
		m = degree newton;
	}

	(p:TextWriter) << (s:%):TextWriter == {
		import from RX, String;
		p << "PlaneSingularity(" << center s << ")";
	}

	-- denrhs = denominator of rhs, 0 if homogeneous equation,
	-- in which case can return failed when there is no nonzero rat sol.
	-- returns a "denominator" d(x) in the following extended sense:
	--  any rational solution must be of the form p(x) / d(x) for a
	--  poly p(x), but d(x) can itself be a fraction (i.e. negative powers)
	denominator(s:%, denrhs:RX):Partial Product RX == {
		import from Z, Partial Z, NP, Product RX;
		import from BalancedFactorization(R, RX);
		assert(finite? polygon s);
		(u, ignore) := minExponent1 s;	-- homogeneous bound
		p := center s;
		zero? denrhs => {		-- homogeneous equation
			failed? u => failed;
			[term(p, - retract u)];
		}
		-- here we are solving an inhomogeneous equation
		b := { failed? u => 0; - retract u }
		zero?(degree denrhs) or zero? degree(g := gcd(p, denrhs)) => {
			b > 0 => [term(p, b)];
			[1];
		}
		mu := orderDrop polygon s;
		den:Product RX := 1;
		for trm in balance(g, denrhs) repeat {
			(a, n) := trm;
			b := max(b, n + mu);
			if b > 0 then den := times!(den, a, b);
		}
		[den];
	}

	-- if s is finite, returns an upper bound on the exponent with which the
	-- center of s can appear in the denominator of a rational solution
	-- of the m-th symmetric power
	-- if s is infinite, returns an upper bound on deg(num)-deg(den) for
	-- rational solutions of the m-th symmetric power
	-- in both cases, return failed if no integer exponent,
	-- which implies no nonzero rational solutions for the m-th sympow
	-- when m > 1, should only be called when all the exponents are rational
	--
	-- the second value is an upper bound on the rational exponent with with
	-- the center of s can appear or on deg(num)-deg(den) as a rational
	-- or failed if there is no rational exponent
	degreeBound(s:%, m:Z):(Partial Z, Partial Q) == {
		import from Q;
		assert(m > 0); assert(one?(m) or allRationalExponents? s);
		(uint, urat) := minExponent(s, m);
		failed? urat => { assert(failed? uint); (uint, urat) }
		q := - retract urat;
		failed? uint => (uint, [q]);
		([- retract uint], [q]);
	}

	-- returns failed if no integer exponent
	-- when m > 1, should only be called when all the exponents are rational
	minIntegerExponent(s:%, m:Z):Partial Z == {
		(uint, urat) := minExponent(s, m);
		uint;
	}

	-- returns failed if no integer or rational exponent
	-- when m > 1, should only be called when all the exponents are rational
	local minExponent(s:%, m:Z):(Partial Z, Partial Q) == {
		import from Q, QmZ, QR, List QR;
		assert(m > 0);
		one? m => minExponent1 s;
		assert(allRationalExponents? s);
		min(m * moduloZ rationalExponents s);
	}

	-- returns failed if no integer or rational exponent
	local minExponent1(s:%):(Partial Z, Partial Q) == {
		import from Z, Q, QR, List QR;
		mint:Partial Z := failed;
		mrat:Partial Q := failed;
		for q in rationalExponents s repeat {
			(num, den) := value q;
			qq := num / den;
			if failed? mrat or qq < retract mrat then mrat := [qq];
			if one?(denominator qq) and
				(failed? mint or numerator(qq) < retract mint)
					then mint := [numerator qq];
		}
		(mint, mrat);
	}
}

