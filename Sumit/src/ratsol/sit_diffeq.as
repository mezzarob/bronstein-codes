-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-------------------------------- sit_diffeq.as ------------------------------
--
-- Linear ordinary differential equations with polynomial coefficients
--
-- Copyright (c) Manuel Bronstein 1998-2001
-- Copyright (c) INRIA 2000, Version 0.1.12
-- Logiciel Sum^it (c) INRIA 2000, dans sa version 0.1.12
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I	== MachineInteger;
	Z	== Integer;
	Q	== Fraction Z;
	QR	== FractionalRoot Z;
	QmZ	== SetsOfRationalsModuloZ;
	M	== DenseMatrix;
	NP	== NewtonPolygon(R, RX, RXD);
	SING	== PlaneSingularity(R, RX, RXD);
	EX	== DenseUnivariatePolynomial E;
	RXY	== DenseUnivariatePolynomial RX;
	Rx	== Fraction RX;
	Rlc	== FractionBy(R, lc, false);
	RlcX	== DenseUnivariatePolynomial Rlc;
	FXX	== DenseUnivariateTaylorSeries F;
}

#if ALDOC
\thistype{LinearHolonomicDifferentialEquation}
\History{Manuel Bronstein}{14/1/98}{created}
\Usage{import from \this(R, F, $\iota$, RX, RXD)}
\Params{
{\em R} & \alexttype{GcdDomain}{} & Coefficient domain\\
        & \alexttype{RationalRootRing}{} & \\
        & \alexttype{CharacteristicZero}{} &\\
\emph{F} & \alexttype{Field}{} & A field containing R\\
$\iota$ & $R \to F$ & Injection from R into F\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
{\em RXD} & \altype{LinearOrdinaryDifferentialOperatorCategory} RX &
Differential operators\\
}
\Descr{\this(R, F, $\iota$, RX, RXD) provides a type that analyzes the local
singularities of operators in \emph{RXD} and stores the local information for
eventual reuse.}
\begin{exports}
{\tt allRationalExponents?}:
& \% $\to$ \alexttype{Boolean}{} & check local exponents\\
{\tt allRegular?}: & \% $\to$ \alexttype{Boolean}{} & check singularities\\
{\tt analyze!}:
& (\%, RX) $\to$ \alexttype{List}{} SING & singularity analysis\\
{\tt analyze!}:
& (\%, Product RX) $\to$ \alexttype{List}{} SING & singularity analysis\\
{\tt analyzedSingularities}:
& \% $\to$ \alexttype{List}{} SING & analyzed singularities\\
{\tt coerce}: & RXD $\to$ \% & Conversion\\
{\tt finiteSingularities!}:
& \% $\to$ \alexttype{List}{} SING & affine singularities\\
{\tt operator}: & \% $\to$ RXD & Underlying operator\\
{\tt order}: & \% $\to$ \alexttype{Integer}{} & Order\\
{\tt singularAtInfinity?}:
& \% $\to$ \alexttype{Boolean}{} & check singularity at $x = \infty$\\
{\tt singularityAtInfinity!}: & \% $\to$ SING & singularity at $x = \infty$\\
\end{exports}
\begin{alwhere}
SING &==& {\tt PlaneSingularity}(R, RX, RXD)\\
\end{alwhere}
#endif

LinearHolonomicDifferentialEquation(
	R: Join(GcdDomain, RationalRootRing, CharacteristicZero),
	F: Field, inj:R -> F,
	RX: UnivariatePolynomialCategory R,
	RXD:LinearOrdinaryDifferentialOperatorCategory RX):ExpressionType with {
		allRationalExponents?: % -> Boolean;
		allRegular?: % -> Boolean;
		analyze!: (%, RX) -> List SING;
		analyze!: (%, Product RX) -> List SING;
		analyzedSingularities: % -> List SING;
		cannotHaveExponentialSolution?: % -> Boolean;
		coerce: RXD -> %;
		finiteSingularities!: % -> List SING;
		irregularAtInfinity?: % -> Boolean;
		localSolutions: % -> (R, M FXX);
		operator: % -> RXD;
		order: % -> Z;
		ordinaryPoint: % -> R;
		singularAtInfinity?: % -> Boolean;
		singularityAtInfinity!: % -> SING;
		unimodular?: % -> Boolean;
		unimodularize: % -> (RX, %, Rx);
} == add {
	-- op       == underlying operator
	-- finsing  == list of finite singularities
	-- todo     == singularities still to analyze
	-- infsing  == singularity at infinity (could be an ordinary point)
	-- ord      == ordinary point, or failed if not yet computed
	-- fundsols == fundamental solution matrix at x=ord or [] if not yet
	Rep == Record(op:RXD, finsing:List SING, todo:RX, infsing:Partial SING,
			ord:Partial R, fundsols:M FXX);

	local factor?:Boolean		== R has FactorizationRing;
	operator(eq:%):RXD		== { import from Rep; rep(eq).op; }
	local unanalyzed(eq:%):RX	== { import from Rep; rep(eq).todo; }
	local inf(eq:%):Partial SING	== { import from Rep; rep(eq).infsing; }
	order(eq:%):Z			== {import from RXD;degree operator eq;}
	extree(eq:%):ExpressionTree	== { import from RXD; extree operator eq; }

	-- returns (a, LL, s) where LL = a * the unimodular transform of L
	-- and s is the shift for the curves (c(y) becomes c(y - s))
	-- s = 0 iff L is already unimodular, in which case LL = L
	unimodularize(eq:%):(RX, %, Rx) == {
		import from LinearOrdinaryDifferentialOperatorTools(RX, RXD);
		unimodular? eq => (1, eq, 0);
		(alpha, U, a, b) := unimodularize operator eq;
		assert(~zero? a); assert(~zero? b);
		(alpha, U::%, a / b);
	}

	unimodular?(eq:%):Boolean == {
		import from Z, QR, RX, RXY, Derivation RX, Rx, RXD;
		L := operator eq;
		assert(~zero? L);
		assert(degree L > 0);
		f := coefficient(L, prev degree L) / leadingCoefficient L;
		zero?(a := numerator f) => true;
		d := denominator f;
		(dd := degree d) ~= next(degree a) => false;
		D: Derivation RX := derivation$RXD;
		dprime := D d;
		degree(gcd(d, dprime)) > 0 => false;
		phi := map(coerce)$MonogenicAlgebra2(R, RX, RX, RXY);
		r := resultant(phi d, phi(a) - monom$RX * phi(dprime));
		s:Z := 0;
		for rt in integerRoots r repeat s := s + multiplicity rt;
		s = dd;
	}

	ordinaryPoint(eq:%):R == {
		import from Rep, Partial R, RX, RXD;
		if failed?(rep(eq).ord) then rep(eq).ord :=
			[ordinaryPoint(leadingCoefficient operator eq)::R];
		retract(rep(eq).ord);
	}

	localSolutions(eq:%):(R, M FXX) == {
		import from Rep, I, M FXX,
			LinearOrdinaryDifferentialOperatorSeriesSolutions(R,_
							 F, inj, RX, RXD, FXX);

		a := ordinaryPoint eq;
		if zero? numberOfRows(m := rep(eq).fundsols) then {
			m := fundamentalMatrix(operator eq, a);
			rep(eq).fundsols := m;
		}
		(a, m);
	}

	analyzedSingularities(eq:%):List SING == {
		import from Rep;
		rep(eq).finsing;
	}

	local analyzedAtInfinity?(eq:%):Boolean	== {
		import from Partial SING;
		~failed?(inf eq);
	}

	allRegular?(eq:%):Boolean == {
		import from NP;
		all?(eq, (s:SING):Boolean +-> regular? polygon s);
	}

	allRationalExponents?(eq:%):Boolean == {
		import from RXD, SING;
		all?(eq, allRationalExponents?);
	}

	(eq1:%) = (eq2:%):Boolean == {
		import from RXD;
		operator eq1 = operator eq2;
	}

	-- true means no exponential solution for sure (necessary cond. failed)
	-- false means don't know
	cannotHaveExponentialSolution?(eq:%):Boolean == {
		import from Z, Partial Z, RX, SING, List SING, QmZ;
		sinf := singularityAtInfinity! eq;
		~allRationalExponents? sinf => false;
		l := moduloZ rationalExponents sinf;
		for s in finiteSingularities! eq repeat {
			~allRationalExponents? s => return false;
			l := l + degree(center s) * moduloZ(rationalExponents s);
		}
		(uz, uq) := min l;
		failed?(uz) or retract(uz) > 0;
	}

	local all?(eq:%, test?:SING -> Boolean):Boolean == {
		import from List SING;
		for s in finiteSingularities! eq repeat ~test?(s) =>
								return false;
		test? singularityAtInfinity! eq;
	}

	singularAtInfinity?(eq:%):Boolean == {
		import from NP, SING;
		singular? polygon singularityAtInfinity! eq;
	}

	irregularAtInfinity?(eq:%):Boolean == {
		import from NP, SING;
		irregular? polygon singularityAtInfinity! eq;
	}

	coerce(L:RXD):%	== {
		import from Rep, I, Z, Partial R, RX;
		import from M FXX, List SING, Partial SING;
		per [L, empty, squareFreePart leadingCoefficient L,
			failed, failed, zero(0, 0)];
	}

	-- does not analyze at infinity
	finiteSingularities!(eq:%):List SING == {
		import from Z, RX;
		TRACE("holonom::finiteSingularities!:eq = ", eq);
		zero? degree(p := unanalyzed eq) => analyzedSingularities eq;
		TRACE("holonom::finiteSingularities!:unanalyzed part = ", p);
		analyze0!(eq, p, p);
	}

	-- only analyzes at the roots of q, everywhere if q = 0
	analyze!(eq:%, q:RX):List SING == {
		import from Boolean, Z;
		zero? q => finiteSingularities! eq;
		zero? degree(p:=unanalyzed eq) or zero? degree(q:=gcd(q,p)) =>
						analyzedSingularities eq;
		analyze0!(eq, p, q);
	}

	-- p = unanalyzed part, q = part to analyze
	-- returns all the analyzed singularities, including previous ones
	local analyze0!(eq:%, p:RX, q:RX):List SING == {
		import from Boolean, Z, RXD, Product RX, Partial RX, List RX;
		import from BalancedFactorization(R, RX);
		assert(~zero? q); assert(degree(q) > 0);
		assert(~failed?(exactQuotient(p, q)));
		f:List SING := empty;
		L := operator eq;
		for trm in balance(q, [nonZeroCoefficients L]) repeat {
			(a, ignore) := trm;
			f := append!(f, analyze(L, a));
			p := quotient(p, a);
		}
		setFinite!(eq, append!(analyzedSingularities eq, f), p);
		analyzedSingularities eq;
	}

	-- does not analyze at infinity
	-- returns all the analyzed singularities, including previous ones
	analyze!(eq:%, places:Product RX):List SING == {
		import from Z, RX, RXD, Product RX;
		import from BalancedFactorization(R, RX);
		zero? degree(p := unanalyzed eq) => empty;
		coeffs:List RX := [nonZeroCoefficients(L := operator eq)];
		f:List SING := empty;
		for trm in places repeat {
			(a, ignore) := trm;
			a := gcd(a, p);
			if degree(a) > 0 then {
				for trm0 in balance(a, coeffs) repeat {
					(b, ignore) := trm0;
					f := append!(f, analyze(L, b));
					p := quotient(p, b);
				}
			}
		}
		setFinite!(eq, append!(analyzedSingularities eq, f), p);
		analyzedSingularities eq;
	}

	singularityAtInfinity!(eq:%):SING == {
		import from Rep, Boolean, Partial SING, NP, RX;
		import from NewtonPolygonEquations(R, RX, RXD, R, RX);
		analyzedAtInfinity? eq => retract inf eq;
		N := newtonPolygon operator eq;
		l := rationalExponents(N, leadingCoefficient);
		s := infinity(N, l);
		rep(eq).infsing := [s];
		s;
	}

	local setFinite!(eq:%, f:List SING, p:RX):% == {
		import from Rep;
		rep(eq).finsing := f;
		rep(eq).todo := p;
		eq;
	}

	local analyze(L:RXD, a:RX):List SING == {
		import from Z, R, NP;
		TRACE("holonom::analyze:L = ", L);
		TRACE("holonom::analyze:a = ", a);
		N := newtonPolygon(L, a);
		inv? := unit?(lc := leadingCoefficient a);
		one?(d := degree a) => {
			inv? => [analyzeRat(N, a)];
			[analyzeRatQ(N, a, lc)];
		}
		factor? => analyze(L, N, a, inv?);
		inv? => [analyzeAlg(N, a)];
		[analyzeAlgQ(N, a, lc)];
	}

	if R has FactorizationRing then {
		local analyze(L:RXD, N:NP, a:RX, inv?:Boolean):List SING == {
			import from Z, R, Product RX;
			(ignore, prod) := factor a;
			l:List SING := empty;
			for trm in prod repeat {
				(p, n) := trm;
				lc := leadingCoefficient p;
				pinv? := inv? or unit? lc;
				s := {
					one?(d := degree p) => {
						pinv? => analyzeRat(N, p);
						analyzeRatQ(N, p, lc);
					}
					pinv? => analyzeAlgIrr(N, p);
					analyzeAlgIrrQ(N, p, lc);
				}
				l := cons(s, l);
			}
			l;
		}
	}

	local analyzeRat(N:NP, p:RX):SING == {
		import from Boolean, Z, R, Partial RX;
		import from NewtonPolygonEquations(R, RX, RXD, R, RX);
		assert(~failed? exactQuotient(center N, p));
		assert(unit? leadingCoefficient p);
		assert(one? degree p);
		TRACE("holonom::analyzeRat:N = ", N);
		TRACE("holonom::analyzeRat:p = ", p);
		l := {
			zero?(p0 := coefficient(p, 0)) =>
				rationalExponents(N,
						(q:RX):R +-> coefficient(q, 0));
			a := quotient(-p0, leadingCoefficient p);
			rationalExponents(N, (q:RX):R +-> q a);
		}
		finite(p, true, N, l);
	}

	local analyzeRatQ(N:NP, p:RX, lc:R):SING == {
		import from Boolean, Z, Rlc, RlcX, Partial RX;
		import from MonogenicAlgebra2(R, RX, Rlc, RlcX);
		import from NewtonPolygonEquations(R, RX, RXD, Rlc, RlcX);
		assert(~failed? exactQuotient(center N, p));
		assert(~unit? lc);
		a := shift((-coefficient(p, 0))::Rlc, -1);
		l := rationalExponents(N, (q:RX):Rlc +-> (map(coerce)(q))(a));
		finite(p, true, N, l);
	}

	local analyzeAlg(N:NP, p:RX):SING == {
		macro E == UnivariatePolynomialModSqfr(R, RX, p);
		import from R, E, NewtonPolygonEquations(R, RX, RXD, E, EX);
		assert(p = center N);
		assert(unit? leadingCoefficient p);
		l := rationalExponents(N, (q:RX):E +-> reduce q);
		finite(p, false, N, l);
	}

	local analyzeAlgQ(N:NP, p:RX, lc:R):SING == {
		import from Boolean, Rlc, MonogenicAlgebra2(R, RX, Rlc, RlcX);
		assert(~unit? lc);
		analyzeAlgQ0(N, lc, p, map(coerce)(p));
	}

	local analyzeAlgQ0(N:NP, lc:R, p:RX, pp:RlcX):SING == {
		macro E == UnivariatePolynomialModSqfr(Rlc, RlcX, pp);
		import from Rlc, E, MonogenicAlgebra2(R, RX, Rlc, RlcX);
		import from NewtonPolygonEquations(R, RX, RXD, E, EX);
		assert(p = center N);
		assert(~unit? lc);
		l := rationalExponents(N, (q:RX):E +-> reduce(map(coerce)(q)));
		finite(p, false, N, l);
	}

	local analyzeAlgIrr(N:NP, p:RX):SING == {
		macro E == SimpleAlgebraicExtension(R, RX, p);
		import from Boolean, R, E, Partial RX;
		import from NewtonPolygonEquations(R, RX, RXD, E, EX);
		assert(~failed? exactQuotient(center N, p));
		assert(unit? leadingCoefficient p);
		l := rationalExponents(N, (q:RX):E +-> reduce q);
		finite(p, true, N, l);
	}

	local analyzeAlgIrrQ(N:NP, p:RX, lc:R):SING == {
		import from Boolean, Rlc, Partial RX;
		import from MonogenicAlgebra2(R, RX, Rlc, RlcX);
		assert(~failed? exactQuotient(center N, p));
		assert(~unit? lc);
		analyzeAlgIrrQ0(N, lc, p, map(coerce)(p));
	}

	local analyzeAlgIrrQ0(N:NP, lc:R, p:RX, pp:RlcX):SING == {
		macro E == SimpleAlgebraicExtension(Rlc, RlcX, pp);
		import from Rlc, E, MonogenicAlgebra2(R, RX, Rlc, RlcX);
		import from NewtonPolygonEquations(R, RX, RXD, E, EX);
		assert(~unit? lc);
		l := rationalExponents(N, (q:RX):E +-> reduce(map(coerce)(q)));
		finite(p, true, N, l);
	}
}

