-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------- sit_diffrat.as --------------------------
--
-- Rational solutions of linear ordinary differential equations
--
-- Copyright (c) Manuel Bronstein 1995-2001
-- Copyright (c) INRIA 2000, Version 0.1.12
-- Logiciel Sum^it (c) INRIA 2000, dans sa version 0.1.12
-- Copyright Swiss Federal Polytechnic Institute Zurich, 1995-1997
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I	== MachineInteger;
	Z	== Integer;
	Q	== Fraction Z;
	Rx	== Fraction RX;
	RxD	== LinearOrdinaryDifferentialOperator Rx;
	V	== Vector;
	PRX	== Product RX;
	PFX	== Partial FX;
	SING	== PlaneSingularity(R, RX, RXD);
	EQ	== LinearHolonomicDifferentialEquation(R, F, inj, RX, RXD);
	PEQ	== Partial EQ;
}

#if ALDOC
\thistype{LinearOrdinaryDifferentialOperatorRationalSolutions}
\History{Manuel Bronstein}{30/6/95}{created}
\History{Manuel Bronstein}{14/12/98}{added parametric right--hand side}
\Usage{import from \this(R, F, $\iota$, RX, RXD, FX)}
\Params{
{\em R} & \alexttype{GcdDomain}{} & The coefficient ring\\
        & \alexttype{RationalRootRing}{} &\\
        & \alexttype{CharacteristicZero}{} &\\
{\em F} & \alexttype{Field}{} & A field containing R\\
        & \alexttype{RationalRootRing}{} &\\
$\iota$ & $R \to F$ & Injection\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
{\em RXD} & \altype{LinearOrdinaryDifferentialOperatorCategory} RX &
Differential operators\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
}
\Descr{\this(R, F, $\iota$, RX, RXD, FX) provides a rational solver
for linear ordinary differential operators with polynomial coefficients.}
\begin{exports}
\alexp{kernel}:
& EQ $\to$ (\alexttype{Product}{} RX, V FX) & Rational solutions\\
& (EQ, RX) $\to$ (\alexttype{Product}{} RX, V FX) & \\
& RXD $\to$ (\alexttype{Product}{} RX, V FX) & \\
& (RXD, RX) $\to$ (\alexttype{Product}{} RX, V FX) & \\
\alexp{particularSolution}:
& (RXD, Rx) $\to$ (\alexttype{Product}{} RX, P FX) &
A rational solution\\
\alexp{solve}:
& (RXD, Rx) $\to$ (\alexttype{Product}{} RX, V FX, P FX) &
Rational solutions\\
\end{exports}
\begin{alwhere}
EQ &==& \altype{LinearHolonomicDifferentialEquation}(R, F, $\iota$, RX, RXD)\\
Rx &==& \alexttype{Fraction}{} RX\\
V &==& \alexttype{Vector}{}\\
P &==& \alexttype{Partial}{}\\
\end{alwhere}
#endif

LinearOrdinaryDifferentialOperatorRationalSolutions(_
	R: Join(GcdDomain, RationalRootRing, CharacteristicZero),
	F: Join(Field, RationalRootRing), inj: R -> F,
	RX: UnivariatePolynomialCategory R,
	RXD:LinearOrdinaryDifferentialOperatorCategory RX,
	FX: UnivariatePolynomialCategory F): with {
		degree: PRX -> Z;
		kernel: EQ -> (PRX, V FX);
		kernel: RXD -> (PRX, V FX);
		kernel: (RXD, RX) -> (PRX, V FX);
		kernel: (EQ, RX) -> (PRX, V FX);
#if ALDOC
\alpage{kernel}
\Usage{\name~L\\ \name(L, p)}
\Signatures{
\name: & EQ $\to$ (\alexttype{Product}{} RX, \alexttype{Vector}{} FX)\\
\name: & (EQ, RX) $\to$ (\alexttype{Product}{} RX, \alexttype{Vector}{} FX)\\
\name: & RXD $\to$ (\alexttype{Product}{} RX, \alexttype{Vector}{} FX)\\
\name: & (RXD, RX) $\to$ (\alexttype{Product}{} RX, \alexttype{Vector}{} FX)\\
}
\begin{alwhere}
EQ &==& \altype{LinearHolonomicDifferentialEquation}(R, F, $\iota$, RX, RXD)\\
\end{alwhere}
\Params{
{\em L} & EQ & A linear ordinary differential operator\\
        & RXD & \\
\emph{p} & RX & Allowed poles\\
}
\Retval{\name(L) returns $(d, [p_1,\dots,p_m])$
such that $\{p_1/d,\dots,p_m/d\}$ is
a basis for all the rational solutions of $Ly = 0$.
If a nonzero second argument \emph{p} is present, then only the solutions
having their poles among the roots of \emph{p} (and at infinity) are
computed.}
\Remarks{\emph{d} is a ``universal denominator'' in the sense that any
rational solution must be of the form $p/d$ for a polynomial \emph{p}.
However, \emph{d} is allowed to have negative exponents, hence it can
be a fraction itself rather than a polynomial.
Use {\tt expandFraction} from \alexttype{Product}{} to expand it properly.}
#endif
		particularSolution: (RXD, Rx) -> (PRX, PFX);
#if ALDOC
\alpage{particularSolution}
\Usage{\name(L, g)}
\Signature{(RXD, \alexttype{Fraction}{} RX)}
{(\alexttype{Product}{} RX, \alexttype{Partial}{} FX)}
\Params{
{\em L} & RXD & A linear ordinary differential operator\\
{\em g} & \alexttype{Fraction}{} RX & A fraction\\
}
\Retval{Returns (d, \failed) if $L y = g$ has no rational
solution, in which case {\em d} is a universal denominator for such solutions.
Otherwise, it returns $(d,p)$ such that $p/d$ is a particular solution.}
\Remarks{\emph{d} is a ``universal denominator'' in the sense that any
rational solution must be of the form $p/d$ for a polynomial \emph{p}.
However, \emph{d} is allowed to have negative exponents, hence it can
be a fraction itself rather than a polynomial.
Use {\tt expandFraction} from \alexttype{Product}{} to expand it properly.}
\alseealso{\alexp{solve}}
#endif
		solve: (RXD, Rx) -> (PRX, V FX, PFX);
#if ALDOC
\alpage{solve}
\Usage{\name(L, g)}
\Signature{(RXD, \alexttype{Fraction}{} RX)}
{(\alexttype{Product}{} RX, \alexttype{Vector}{} FX, \alexttype{Partial}{} FX)}
\Params{
{\em L} & RXD & A linear ordinary differential operator\\
{\em g} & \alexttype{Fraction}{} RX & A fraction\\
}
\Retval{Returns $(d, [p_1,\dots,p_s], \alpha)$ where $p_1/d,\dots,p_s/d$ is
a basis for all the rational solutions of $Ly = 0$,
and $(d, \alpha)$ is the result that \alexp{particularSolution} would return
on the same input.}
\Remarks{\emph{d} is a ``denominator'' in the sense that any
rational solution must be of the form $p/d$ for a polynomial \emph{p}.
However, \emph{d} is allowed to have negative exponents, hence it can
be a fraction itself rather than a polynomial.
Use {\tt expandFraction} from \alexttype{Product}{} to expand it properly.}
\alseealso{\alexp{kernel},\alexp{particularSolution}}
#endif
		symmetricKernel: (EQ, Z) -> (RXD, PRX, V FX);
		symmetricKernel: (RXD, Z) -> (RXD, PRX, V FX);
		denominator:
			(EQ,PEQ,Z) -> (Partial PRX,List SING,List Z,List Q,PEQ);
		degreeBound:     (EQ, PEQ, Z) -> (Partial Z, Partial Q, PEQ);
} == add {
	kernel(L:EQ):(PRX, V FX)	== kernel(L, 0$RX);
	kernel(L:RXD):(PRX, V FX)	== kernel(L, 0$RX);
	degree(p:PRX):Z			== log(Z, degree$RX)(p);

	-- first arg returned is either 0 or n-th sym pow if computed
	symmetricKernel(L:RXD, n:Z):(RXD, PRX, V FX) == {
		import from EQ;
		assert(n > 0);
		TRACE("symmetricKernel:L = ", L);
		symmetricKernel(primitivePart(L)::EQ, n);
	}

	-- first arg returned is either 0 or n-th sym pow if computed
	-- the operator of L should be primitive at this point
	symmetricKernel(L:EQ, n:Z):(RXD, PRX, V FX) == {
		import from Partial Z, PRX, Partial PRX, RXD, Partial EQ, FX,
		   LinearOrdinaryDifferentialOperatorTools(RX, RXD),
		   LinearOrdinaryOperatorPolynomialSolutions(R,F,inj,RX,RXD,FX);
		assert(n > 0);
		TRACE("symmetricKernel:L = ", L);
		one? n => {
			(den, num) := kernel L;
			(operator L, den, num);
		}
		one?(order L) => {
			(den, num) := kernel L;
			(0, den^n, [p^n for p in num]);
		}
		(u, ignore, Ln) := degreeBound(L, failed, n);
		failed? u => (opOr0 Ln, 1, empty);
		(uden, ignore1, ignore2, ignore3, Ln) := denominator(L, Ln, n);
		failed? uden => (opOr0 Ln, 1, empty);
		d := retract uden;
		(bound := retract(u) + degree(d)) < 0 => (opOr0 Ln, d, empty);
		if failed? Ln then Ln := [symmetricPower(operator L, n)::EQ];
		(alpha, LL) := twist(d)(opn := operator retract Ln);
		(opn, d, kernel(LL, bound));
	}

	local opOr0(L:Partial EQ):RXD == {
		import from EQ;
		failed? L => 0;
		operator retract L;
	}

	-- returns an upper bound on deg(num)-deg(den) for
	-- rational solutions of the m-th symmetric power
	-- return failed if no integer exponent,
	-- as well as a rational upper bound on that degree
	-- which implies no nonzero rational solutions for the m-th sympow
	degreeBound(L:EQ, Ln:PEQ, n:Z):(Partial Z, Partial Q, PEQ) == {
		import from SING, RXD;
		allRationalExponents?(s := singularityAtInfinity! L) => {
			(dint, drat) := degreeBound(s, n);
			(dint, drat, Ln);
		}
		if failed? Ln then Ln := [symmetricPower(operator L, n)::EQ];
		(dint, drat) := degreeBound singularityAtInfinity! retract Ln;
		(dint, drat, Ln);
	}

	-- returns a "denominator" d(x) in the following extended sense:
	-- any rational solution of the n-th symmetric power of L must be of
	-- the form p(x) / d(x) for a poly p(x), but d(x) can itself be a
	-- fraction (i.e. negative powers)
	-- also returns the list of finite singularities of L together with
	-- corresponding lists of integer and rational exponent bounds there
	-- as well the n-th symmetric power if it has been computed
	-- returns (failed, empty, empty, empty, Ln) if provably no rat sol
	denominator(L:EQ, Ln:PEQ, n:Z):_
		(Partial PRX, List SING, List Z, List Q, PEQ) == {
		import from Partial Z, Partial Q, RXD, SING;
		assert(n > 1);
		den:PRX := 1;
		ls:List SING := empty;
		lz:List Z := empty;
		lq:List Q := empty;
		for s in finiteSingularities! L repeat {
			p := center s;
			if allRationalExponents? s then {
				ls := cons(s, ls);
				(uint, urat) := degreeBound(s, n);
				failed? uint =>
					return(failed, empty, empty, empty, Ln);
				lz := cons(u := retract uint, lz);
				lq := cons(retract urat, lq);
				if ~zero?(u) then den := times!(den, p, u);
			}
			else {
				if failed? Ln then
					Ln:= [symmetricPower(operator L,n)::EQ];
				for t in analyze!(retract Ln, p) repeat {
					ls := cons(t, ls);
					(uint, urat) := degreeBound t;
					failed? uint => return(failed, empty,_
							 empty, empty, Ln);
					lz := cons(u := retract uint, lz);
					lq := cons(retract urat, lq);
					if ~zero?(u) then den:= times!(den,p,u);
				}
			}
		}
		([den], ls, lz, lq, Ln);
	}

	local denom(L:RXD, g:Rx):PRX == {
		import from Boolean, EQ, Partial PRX;
		assert(~zero? g);
		retract denom(L::EQ, 0$RX, denominator g);
	}

	kernel(L:RXD, poles:RX):(PRX, V FX) == {
		import from EQ;
		kernel(primitivePart(L)::EQ, poles);
	}

	-- poles = allowed poles of the solution, 0 if all poles allowed
	-- the operator of L should be primitive at this point
	kernel(L:EQ, poles:RX):(PRX, V FX) == {
		import from Rx, Z, Partial Z, Partial PRX, SING,
		   LinearOrdinaryDifferentialOperatorTools(RX, RXD),
		   LinearOrdinaryOperatorPolynomialSolutions(R,F,inj,RX,RXD,FX);
		-- TRACE("diffrat::kernel: L = ", L);
		TIMESTART;
		(u, ignore):= degreeBound singularityAtInfinity! L;
		failed? u => (1, empty);
		TIME("diffrat::kernel: degree bound at ");
		failed?(ud := denom(L, poles, 0)) => (1, empty);
		TIME("diffrat::kernel: denominator at ");
		d := retract ud;
		TRACE("diffrat::kernel: d = ", d);
		(bound := retract(u) + degree(d)) < 0 => (d, empty);
		(alpha, LL) := twist(d)(operator L);
		TIME("diffrat::kernel: operator twisted at ");
		TRACE("diffrat::kernel: alpha = ", alpha);
		-- TRACE("diffrat::kernel: LL = ", LL);
		(d, kernel(LL, bound));
	}

	particularSolution(L:RXD, g:Rx):(PRX, PFX) == {
		import from Z, RX, PRX, Rx, FX, PFX,
		   LinearOrdinaryDifferentialOperatorTools(RX, RXD),
		   LinearOrdinaryOperatorPolynomialSolutions(R,F,inj,RX,RXD,FX);
		TRACE("diffrat::particularSolution: L = ", L);
		TRACE("diffrat::particularSolution: g = ", g);
		zero? g => (1, [0]);
		(den, L) := primitive L;
		TRACE("diffrat::particularSolution: den = ", den);
		TRACE("diffrat::particularSolution: pp(L) = ", L);
		g := g / (den::Rx);
		TRACE("diffrat::particularSolution: g = ", g);
		one?(d := denom(L, g)) => {
			zero? degree denominator g =>
				(1, particularSolution(L, Rx2FX g));
			(1, failed);
		}
		TRACE("diffrat::particularSolution: d = ", d);
		(alpha, LL) := twist(d)(L);
		TRACE("diffrat::particularSolution: alpha = ", alpha);
		TRACE("diffrat::particularSolution: LL = ", LL);
		(nd, dd) := expandFraction d;
		g := alpha * (nd / dd) * g;
		zero? degree denominator g =>
					(d, particularSolution(LL, Rx2FX g));
		(d, failed);
	}

	solve(L:RXD, g:Rx):(PRX, V FX, PFX) == {
		import from Z, RX, PRX, Rx, FX, PFX,
		   LinearOrdinaryDifferentialOperatorTools(RX, RXD),
		   LinearOrdinaryOperatorPolynomialSolutions(R,F,inj,RX,RXD,FX);
		TRACE("diffrat::solve: L = ", L);
		TRACE("diffrat::solve: g = ", g);
		zero? g => {
			(dn, num) := kernel(L)$%;
			(dn, num, [0]);
		}
		(den, L) := primitive L;
		TRACE("diffrat::solve: den = ", den);
		TRACE("diffrat::solve: pp(L) = ", L);
		g := g / (den::Rx);
		TRACE("diffrat::solve: g = ", g);
		one?(d := denom(L, g)) => {
			zero? degree denominator g => {
				(hom, part) := solve(L, Rx2FX g);
				(1, hom, part);
			}
			(1, kernel L, failed);
		}
		TRACE("diffrat::solve: d = ", d);
		(alpha, LL) := twist(d)(L);
		TRACE("diffrat::solve: alpha = ", alpha);
		TRACE("diffrat::solve: LL = ", LL);
		(nd, dd) := expandFraction d;
		g := alpha * (nd / dd) * g;
		zero? degree denominator g => {
			(hom, part) := solve(LL, Rx2FX g);
			(d, hom, part);
		}
		(d, kernel LL, failed);
	}

	-- poles = allowed poles of the solution, 0 if all poles allowed
	-- denrhs = denominator of rhs, 0 if homogeneous equation,
	-- in which case can return failed when there is non nonzero rat sol.
	-- returns a "denominator" d(x) in the following extended sense:
	--  any rational solution must be of the form p(x) / d(x) for a
	--  poly p(x), but d(x) can itself be a fraction (i.e. negative powers)
	local denom(L:EQ, poles:RX, denrhs:RX):Partial PRX == {
		import from Boolean, Z, List RX, Partial RX, RXD, PRX;
		import from BalancedFactorization(R, RX);
		-- TRACE("diffrat::denom:L = ", L);
		TRACE("diffrat::denom:poles = ", poles);
		TRACE("diffrat::denom:denrhs = ", denrhs);
		denrhs := gcd(denrhs, poles);
		d := singDenom(L, poles, denrhs);
		TRACE("diffrat::denom:singular d = ", d);
		zero? denrhs or zero? degree denrhs => d;
		-- here we are solving an inhomogeneous equation
		den := { failed? d => 1; retract d }
		p := leadingCoefficient operator L;
		coeffs := [nonZeroCoefficients operator L];
		for trm in balance(denrhs, coeffs) repeat {
			(a, n) := trm;
			if failed? exactQuotient(p, a) then {
				e := n - order L;
				if e > 0 then den := times!(den, a, e);
			}
		}
		TRACE("diffrat::denom:total den = ", den);
		[den];
	}

	-- return the part of the denominator of solutions of L y = rhs
	-- that corresponds to the singularities of L
	-- poles = allowed poles of the solution, 0 if all poles allowed
	-- denrhs = denominator of rhs, 0 if homogeneous equation,
	-- in which case can return failed when there is non nonzero rat sol.
	local singDenom(L:EQ, poles:RX, denrhs:RX):Partial PRX == {
		import from Boolean, Partial RX, SING, List SING, PRX;
		den:PRX := 1;
		for s in analyze!(L, poles) repeat {
			p := center s;
			TRACE("diffrat::singDenom:p = ", p);
			if ~failed?(exactQuotient(poles, p)) then {
				d := denominator(s, denrhs);
				failed? d => return failed;
				den := times!(den, retract d);
			}
			TRACE("diffrat::singDenom:den = ", den);
		}
		[den];
	}

	local Rx2FX(f:Rx):FX == {
		import from Z, F, RX;
		assert(zero? degree denominator f);
		inv(inj leadingCoefficient denominator f) * R2FX numerator f;
	}
		
	local R2FX(p:RX):FX == {
		import from MonogenicAlgebra2(R, RX, F, FX);
		map(inj)(p);
	}
}

#if ALDORTEST
---------------------- test sit_diffrat.as --------------------------------
#include "sumit"
#include "aldortest"

macro {
	I == MachineInteger;
	Z == Integer;
	ZX == DenseUnivariatePolynomial Z;
	Q == Fraction Z;
	QX == DenseUnivariatePolynomial Q;
	Qx == Fraction QX;
	ZXD == LinearOrdinaryDifferentialOperator ZX;
	DIM == 5;
}

local polys():Boolean == {
	import from I,Q,LinearOrdinaryDifferentialOperatorRationalSolutions(Z,_
							Q, coerce, ZX, ZXD, QX);
	x:ZX := monom;
	dx:ZXD := monom;
	op := dx^DIM;
	polys0 kernel op;
}

local polys0(d:Product ZX, nums:Vector QX):Boolean == {
	import from I, Z, QX;
	n := #nums;
	n ~= DIM or ~one? d => false;
	vd:Vector Z := zero n;
	for i in 1..n repeat vd(next machine degree(nums.i)) := 1;
	for i in 1..n repeat zero?(vd.i) => return false;
	true;
}

local solve3():Boolean == {
	import from Z,Q,LinearOrdinaryDifferentialOperatorRationalSolutions(Z,_
							Q, coerce, ZX, ZXD, QX);
	x:ZX := monom;
	dx:ZXD := monom;
	op := (x^9+x^3)*dx^3 + 18*x^8*dx^2 - 90*x*dx-30*(11*x^6-(3@Z)::ZX)::ZXD;
	solve30 kernel op;
}

local solve30(d:Product ZX, nums:Vector QX):Boolean == {
	import from I, Q, Qx, MonogenicAlgebra2(Z, ZX, Q, QX);
	(n := #nums) ~= 1 => false;
	(nd, dd) := expandFraction d;
	f := nums(1) * map(coerce)(dd) / map(coerce)(nd);
	xx:QX := monom;
	zero? differentiate(((xx^6+1)/xx) * f);
}

stdout << "Testing rational solutions of Q[x,d/dx]..." << newline;
aldorTest("D^k", polys);
aldorTest("3rd order", solve3);
stdout << newline;
#endif
