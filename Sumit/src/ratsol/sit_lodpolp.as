-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_lodpolp.as ------------------------------
-- Copyright (c) Manuel Bronstein 2004
-- Copyright (c) INRIA 2004, Version 1.0.2
-- Logiciel Sum^it (c) INRIA 2004, dans sa version 1.0.2
-----------------------------------------------------------------------------

#include "sumit"

macro {
	B == Boolean;
	I == MachineInteger;
	Z == Integer;
	V == Vector;
	M == DenseMatrix;
	FR == FractionalRoot;
	RXE == LinearOrdinaryRecurrence(R, RX);
	PFX == Partial FX;
}

PolynomialBasisTools(F:Field, FX:UnivariatePolynomialCategory F): with {
	-- returns a monic triangular basis of the F-space spanned by v in FX
	-- sorted from higher to lower degree
	triangulate!: V FX -> V FX;
} == add {
	local higher(p:FX, q:FX):B == {
		import from Z;
		assert(~zero? p); assert(~zero? q);
		degree p > degree q;
	}

	triangulate!(v:V FX):V FX == {
		import from I, F, FX;
		a:Array FX := [p for p in v];
		a := sort!(a, higher);		-- sorted high to low degree
		n := #a;
		for i in 1..n repeat {
			p := a(prev i);
			a := triangulate!(a, p, i, prev n);
			v.i := inv(leadingCoefficient p) * p;
		}
		v;
	}

	local triangulate!(a:Array FX, p:FX, m:I, n:I):Array FX == {
		import from Z, F;
		assert(~zero? p);
		assert(m > 0); assert(m <= #a); assert(next(n) = #a);
		(c, d) := leadingTerm p;
		for i in m..n while degree(a.i) = d repeat {
			a.i := p - (c / leadingCoefficient(a.i)) * a.i;
			assert(degree(a.i) < d);
		}
		sort!(a, higher);
	}
}

#if ALDOC
\thistype{LinearOrdinaryOperatorModularPolynomialSolutions}
\History{Manuel Bronstein}{30/3/2004}{created}
\Usage{import from \this(R, F, $\iota$, RX, RXY, FX, FXY)}
\Params{
{\em R} & \alexttype{IntegerCategory}{} & A ring of integers\\
{\em F} & \alexttype{Field}{} & A field containing R\\
$\iota$ & $R \to F$ & Injection from R into F\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
{\em RXY}
& \altype{StandardUnivariateSkewPolynomialCategory} RX & Operators over RX\\
{\em FX} & \alexttype{MonogenicAlgebra}{} F & Polynomials over F\\
         & \alexttype{IntegralDomain}{} &\\
}
\Descr{\this(R, F, $\iota$, RX, RXY, FX) provides a modular
polynomial solver for linear ordinary differential or recurrence operators
with polynomial coefficients.}
\begin{exports}
\end{exports}
\begin{alwhere}
\end{alwhere}
#endif

LinearOrdinaryOperatorModularPolynomialSolutions(
	R:Join(IntegerCategory, RationalRootRing),
	F:Field, inj: R -> F,
	RX: UnivariatePolynomialCategory R,
	RXY:StandardUnivariateSkewPolynomialCategory RX,
	FX: Join(IntegralDomain, MonogenicAlgebra F),
	FXY:UnivariateSkewPolynomialCategory FX): with {
		kernel: RXY -> Partial V FX;
} == add {
	-- returns a new prime that does not divide a leading coeff of r
	-- goes in descending order
	local getPrime(p:I, r:RXE):I == {
		import from R, WordSizePrimes;
		p := { p < 0 => maxPrime; previousPrime p; }
		while (~zero? p) and bad?(p::R, r) repeat p := previousPrime p;
		p;
	}

	local RXY2FXY(L:RXY):FXY == {
		import from MonogenicAlgebra2(R, RX, F, FX);
		import from MonogenicAlgebra2(RX, RXY, FX, FXY);
		map(map inj)(L);
	}

	local VRX2VFX(v:V RX):V FX == {
		import from MonogenicAlgebra2(R, RX, F, FX);
		f := map inj;
		[f p for p in v];
	}

	local bad?(p:R, r:RXE):B == {
		import from R, Partial R, RX;
		for trm in r repeat {
			(poly, n) := trm;
			~failed? exactQuotient(leadingCoefficient poly, p) =>
				return true;
		}
		false;
	}

	local combine(big:V RX, modulus:R, small:V RX, p:I):(V RX, B) == {
		import from I, ChineseRemaindering R;
		stable?:B := true;
		v:V RX := zero(n := #big);
		assert(n > 0); assert(n = #small);
		CRT := combine(modulus, p)$R;
		for i in 1..n repeat {
			(q, stable?) := combine(big.i, small.i, CRT, stable?);
			v.i := q;
		}
		(v, stable?);
	}

	local combine(big:RX, small:RX, CRT:(R, I) -> R, stable?:B):(RX, B) == {
		import from I, Z, R;
		assert(~zero? big); assert(~zero? small);
		d := degree big;
		assert(d = degree small);
		q:RX := 0;
		for i in d..0 by -1 repeat {
			cb := coefficient(big, i);
			cs := machine coefficient(small, i);
			if ~(zero? cb and zero? cs) then {
				c := CRT(cb, cs);
				if stable? and c ~= cb then stable? := false;
				q := add!(q, c, i);
			}
		}
		(q, stable?);
	}

	local basis?(basis:V RX, L:RXY):B == {
		import from RX;
		for p in basis repeat {
			~zero?(L p) => return false;
		}
		true;
	}

	local basis?(basis:V FX, L:FXY):B == {
		import from FX;
		for p in basis repeat {
			~zero?(L p) => return false;
		}
		true;
	}

	kernel(L:RXY):Partial V FX == {
		import from I, Z, FR Z, RX, RXE, V RX, V FX, Partial V RX;
		import from LinearOrdinaryOperatorToRecurrence(R, RX, RXY, RXE);
		TIMESTART;
		assert(~zero? L);
		(c, L) := primitive L;
		TIME("lodopolp::kernel: pp(L) at ");
		n := machine degree L;
		assert(n > 0);
		(r, e) := recurrence L;
		TIME("lodopolp::kernel: recurrence at ");
		(poly, m) := trailingTerm r;
		bd := m + e;		-- degree of trailing term of true rec.
		bound := bd;
		degset:List Z := empty;
		if bd > 0 then degset := cons(bd, degset);
		for rt in integerRoots poly repeat {
			b := bd + integralValue rt;
			if b > 0 then degset := cons(b, degset);
			if b > bound then bound := b;
		}
		TIME("lodopolp::kernel: possible degree set at ");
		TRACE("lodopolp::kernel: possible degree set = ", degset);
		zero?(dim := #degset) => [empty];
		minprime := machine(bound + degree(r) - m);  -- N+A+B in ABP'95
		modulus:R := 0;
		LL:FXY := 0;
		p := getPrime(-1, r);
		while p > minprime repeat {
			assert(dim > 0);
			TRACE("lodopolp::kernel: trying prime ", p);
			u := tryprime(r, e, n, degset, bound, dim,
							 p, SmallPrimeField p);
			TIME("lodopolp::kernel: modular kernel at ");
			if ~failed? u then {
				ndim := #(sol := retract u);
				TRACE("lodopolp::kernel: sol = ", sol);
				assert(ndim <= dim);
				if zero? modulus or ndim < dim then {
					-- start anew with this prime p
					zero?(dim := ndim) => return [empty];
					modulus := p::R;
					basis := sol;
				}
				else {	-- combine previous sol with this p
					(basis, stable?) :=
						combine(basis, modulus, sol, p);
					TIME("lodopolp::kernel: CRT at ");
					modulus := (p::R) * modulus;
					stable? and basis?(basis, L) =>
							return [VRX2VFX basis];
					uu := ratrecon(basis, modulus);
					TIME("lodopolp::kernel: ratrecon at ");
					if ~failed? uu then {
						qbasis := retract uu;
						if zero? LL then LL:= RXY2FXY L;
						-- TEMPO: SHOULD CHECK STABILITY
						basis?(qbasis, LL) =>
								return [qbasis];
					}
				}
				TRACE("lodopolp::kernel: dim = ", dim);
				TRACE("lodopolp::kernel: modulus = ", modulus);
				TRACE("lodopolp::kernel: basis = ", basis);
			}
			p := getPrime(p, r);
		}
		failed;
	}

	-- lifts from Fp to R using balanced representation
	local balance(p:I): I->R == {
		pover2 := shift(p, -1);			-- floor(p/2) = (p-1)/2
		(n:I):R +-> {
			if n > pover2 then n := n - p;
			n::R;
		}
	}

	local tryprime(r:RXE, e:Z, n:I, degset:List Z, bound:Z, maxdim:I,
			p:I, Fp:SmallPrimeFieldCategory):Partial V RX == {
		macro FpX == DenseUnivariatePolynomial Fp;
		macro FpXE == LinearOrdinaryRecurrence(Fp, FpX);
		local idFp(f:Fp):Fp == f;
		import from FpX, V FpX;
		import from PolynomialBasisTools(Fp, FpX);
		import from MonogenicAlgebra2(R, RX, Fp, FpX);
		import from MonogenicAlgebra2(RX, RXE, FpX, FpXE);
		import from LinearOrdinaryOperatorPolynomialSolutions(Fp, Fp,
							idFp, FpX, FpXE, FpX);
		local R2Fp(r:R):Fp == integer(r)::Fp;
		recp := map(map(R2Fp))(r);
		v := kernel(recp, e, n, bound);
		#v > maxdim => failed;
		v := triangulate! v;
		for q in v repeat {
			~member?(degree q, degset) => return failed;
		}
		pover2 := shift(p, -1);			-- floor(p/2) = (p-1)/2
		balp := balance p;
		local Fp2R(f:Fp):R == balp machine f;
		FpX2RX := map(Fp2R)$MonogenicAlgebra2(Fp, FpX, R, RX);
		[[FpX2RX q for q in v]$V(RX)];
	}

	local ratrecon(basis:V RX, modulus:R):Partial V FX == {
		import from I, Partial FX, MonogenicAlgebra2(R, RX, F, FX);
		f := rationalReconstruction modulus;
		v:V FX := zero(n := #basis);
		for i in 1..n repeat {
			failed?(u := ratrecon(basis.i, f)) => return failed;
			v.i := retract u;
		}
		[v];
	}

	local ratrecon(p:RX, f:R -> Partial Cross(R, R)):Partial FX == {
		import from F, Partial Cross(R, R);
		q:FX := 0;
		for trm in p repeat {
			(c, n) := trm;
			failed?(u := f c) => return failed;
			(num, den) := retract u;
			q := add!(q, inj(num) / inj(den), n);
		}
		[q];
	}
}
