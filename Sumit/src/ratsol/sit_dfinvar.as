-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_dfinvar.as ------------------------------
-- Copyright (c) Manuel Bronstein 2003
-- Copyright (c) INRIA 2003, Version 1.0.2
-- Logiciel Sum^it (c) INRIA 2003, dans sa version 1.0.2
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I   == MachineInteger;
	Z   == Integer;
	Q   == Fraction Z;
	V   == Vector;
	ARR == Array;
	M   == DenseMatrix;
	PRX == Product RX;
	Rx  == Fraction RX;
	Fx  == Fraction FX;
	NEWTON== NewtonPolygon(R, RX, RXD);
	SING== PlaneSingularity(R, RX, RXD);
	EQ  == LinearHolonomicDifferentialEquation(R, F, inj, RX, RXD);
	PEQ == Partial EQ;
	SYS == LinearOrdinaryFirstOrderSystem;
	POWPROD== DenseHomogeneousPowerProduct(n, m);
	POLY== UnivariatePolynomialCategory;
	FXX == DenseUnivariateTaylorSeries F;
	RATSOL == LinearOrdinaryDifferentialOperatorRationalSolutions(R,_
							F, inj, RX, RXD, FX);
}

#if ALDOC
\thistype{LinearOrdinaryDifferentialOperatorInvariants}
\History{Manuel Bronstein}{9/7/2003}{created}
\Usage{import from \this(R, F, $\iota$, RX, RXD, FX)}
\Params{
{\em R} & \alexttype{GcdDomain}{} & The coefficient ring\\
        & \alexttype{RationalRootRing}{} & \\
        & \alexttype{CharacteristicZero}{} &\\
{\em F} & \alexttype{Field}{} & A field containing R\\
        & \alexttype{RationalRootRing}{} & \\
$\iota$ & $R \to F$ & Injection from R into F\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
\emph{RXD} & \altype{LinearOrdinaryDifferentialOperatorCategory} RX &
Differential operators\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
}
\Descr{\this(R, F, $\iota$, RX, RXD, FX) provides a solver for the
rational solutions of symmetric powers of
linear ordinary differential operators with polynomial coefficients.}
\begin{exports}
\alexp{symmetricKernel}:
& (RXD, Z) $\to$ ($\Pi$ RX, V FX) & Rational solutions of symmetric power\\
& (EQ, Z) $\to$ ($\Pi$ RX, V FX) & \\
\end{exports}
\begin{alwhere}
$\Pi$ &==& \alexttype{Product}{}\\
EQ &==& \altype{LinearHolonomicDifferentialEquation}(R, RX, RXD)\\
\end{alwhere}
#endif

LinearOrdinaryDifferentialOperatorInvariants(
	R:   Join(GcdDomain, RationalRootRing, CharacteristicZero),
	F:   Join(Field, RationalRootRing), inj: R -> F,
	RX:  UnivariatePolynomialCategory R,
	RXD: LinearOrdinaryDifferentialOperatorCategory RX,
	FX:  UnivariatePolynomialCategory F): with {
		candidateInvariants: (RXD, Z) -> (PRX, V FX, M F);
		candidateInvariants: (EQ,  Z) -> (PRX, V FX, M F);
		canonicalImage: % -> M Fx;
		degree: % -> I;
		dimension: % -> I;
		eval: (S:Ring, F -> S, V S) -> % -> V S;
		invariants: (RXD, Z) -> %;
		invariants: (EQ,  Z) -> %;
		order: % -> I;
		polynomial: % -> M F;
		riccati: (FxY:POLY Fx) -> % -> V FxY;
		symmetricKernel: (RXD, Z) -> (PRX, V FX);
		symmetricKernel: (EQ,  Z) -> (PRX, V FX);
		symmetricKernel: % -> M Fx;
#if ALDOC
\alpage{symmetricKernel}
\Usage{\name(L, n)}
\Signatures{
\name: & (RXD, \alexttype{Integer}{})
$\to$ (\alexttype{Product}{} RX, \alexttype{Vector}{} FX)\\
\name: & (EQ, \alexttype{Integer}{})
$\to$ (\alexttype{Product}{} RX, \alexttype{Vector}{} FX)\\
}
\begin{alwhere}
EQ &==& \altype{LinearHolonomicDifferentialEquation}(R, RX, RXD)\\
\end{alwhere}
\Params{
{\em L} & EQ & A linear ordinary differential operator\\
        & RXD & \\
\emph{n} & \alexttype{Integer}{} & A positive integer\\
}
\Retval{\name(L) returns $(d, [p_1,\dots,p_m])$
such that $\{p_1/d,\dots,p_m/d\}$ is
a basis for all the rational solutions of $\sympow{L}{n}y = 0$,
where $\sympow{L}{n}$ is the $\sth{n}$ symmetric power of \emph{L}.}
\alseealso{\alfunc{LinearOrdinaryDifferentialOperatorRationalSolutions}{kernel}}
#endif
		value: % -> (PRX, V FX);
} == add {
	-- the columns of .pol are the coefficients of the invariants as a polys
	-- those of .val are the coefficients of their canonical images as polys
	Rep == Record(deg:I, ord:I, symden:PRX, symnum:V FX, pol:M F, val:M Fx);

	local Z2FXX(n:Z):FXX	== { import from F; n::F::FXX; }
	invariants(L:EQ, n:Z):%	== invariants(L, n, true);
	polynomial(i:%):M F	== { import from Rep; rep(i).pol; }
	degree(i:%):I		== { import from Rep; rep(i).deg; }
	order(i:%):I		== { import from Rep; rep(i).ord; }
	canonicalImage(i:%):M Fx== { import from Rep; rep(i).val; }
	dimension(i:%):I	== { import from Rep, V FX; #(rep(i).symnum); }
	local Rx2Fx(f:Rx):Fx	== RX2FX(numerator f) / RX2FX(denominator f);

	symmetricKernel(i:%):M Fx ==
		patch!(Fx, copy canonicalImage i, degree i, order i, true);

	value(i:%):(PRX, V FX) == {
		import from Rep;
		(rep(i).symden, rep(i).symnum);
	}

	symmetricKernel(L:RXD, n:Z):(PRX, V FX) == {
		import from EQ;
		assert(n > 0);
		symmetricKernel(primitivePart(L)::EQ, n);
	}

	candidateInvariants(L:RXD, n:Z):(PRX, V FX, M F) == {
		import from EQ;
		assert(n > 0);
		candidateInvariants(primitivePart(L)::EQ, n);
	}

	invariants(L:RXD, n:Z):% == {
		import from EQ;
		assert(n > 0);
		invariants(primitivePart(L)::EQ, n);
	}

	symmetricKernel(L:EQ, n:Z):(PRX, V FX) == {
		import from I, Array I, FX, LinearAlgebra(F, M F);
		assert(n > 0);
		one? n or order(L) <= 2 => {
			(Ln, den, num) := symmetricKernel(L, n)$RATSOL;
			(den, num);
		}
		invar := invariants(L, n, false);
		(den, num) := value invar;
		zero?(dim := dimension invar) => (den, num);
		-- look for linear deps among the values of the invariants
		zero?(d := next machine maxdeg num) => (den, empty);
		m:M F := zero(d, dim);
		for j in 1..dim repeat {
			p := num.j;
			for trm in p repeat {
				(c, e) := trm;
				m(next machine e, j) := c;
			}
		}
		(den, [num.i for i in span m]);
	}

	-- returns -1 if v is all 0
	local maxdeg(v:V FX):Z == {
		import from FX;
		d:Z := -1;
		for p in v repeat
			if ~zero? p and (dp := degree p) > d then d := dp;
		d;
	}

	local RX2FX(p:RX):FX == {
		import from MonogenicAlgebra2(R, RX, F, FX);
		map(inj)(p);
	}

	local MF2MFx(m:M F):M Fx == {
		import from I, FX, Fx;
		(r, c) := dimensions m;
		mx:M Fx := zero(r, c);
		for i in 1..r repeat for j in 1..c repeat
			mx(i,j) := m(i,j)::FX::Fx;
		mx;
	}

	-- look for invariants with 0-value iff zVal? is true
	local invariants(L:EQ, n:Z, zVal?:Boolean):% == {
		import from I, V FX, Fx, V Fx, M F, M FX, M Fx, RXD, SYS FX;
		import from LinearOrdinaryFirstOrderSystem2(RX, FX);
		import from MatrixCategoryOverFraction(FX, M FX, Fx, M Fx);
		TIMESTART;
		one?(nn := machine n) => invar1 L;
		m := machine order L;
		(den, num, mat, candvals) := candidates(L, nn, m, zVal?, false);
		empty? num => noinv(nn, m, den);
		-- verify now which solutions really satisfy the n-th power
		Ln := map(RX2FX)(symmetricPowerSystem(operator L, n));
		(a, A) := matrix Ln;
		b := makeRational diagonal a;
		B := makeRational A;
		zr := b * map(differentiate)(candvals) - B * candvals;
		TIME("invariants: verification vector at ");
		zero? zr => normalize(nn, m, den, num,_
				patch!(F, mat, nn, m, false),_
				patch!(Fx, candvals, nn, m, false));
		zero? numberOfColumns(kern := constantKernel zr) =>
							noinv(nn, m, den);
		normalize(nn, m, den, dot(num, kern),
				patch!(F, mat * kern, nn, m, false),
				patch!(Fx, candvals, nn, m, false));
	}

	local constantKernel(m:M Fx):M F == {
		assert(~zero? m);
		-- TEMPORARY!!!
		never;
	}

	-- creates invariants of degree 1
	local invar1(L:EQ):% == {
		import from I, Z, PRX, Rx, Fx, V FX, V Fx;
		(den, num) := kernel(L)$RATSOL;
		m := machine order L;
		empty? num => noinv(1, m, den);
		(dnum, dden) := expandFraction den;
		d := Rx2Fx(dnum / dden);
		vals:M Fx := zero(m, dim := #num);
		for j in 1..dim repeat vals(1, j) := (num.j)::Fx / d;
		for i in 2..m repeat for j in 1..dim repeat
			vals(i, j) := differentiate vals(prev i, j);
		normalize(1, m, den, num, ident(m, #num), vals);
	}

	-- creates an empty invariant space
	local noinv(n:I, m:I, den:PRX):% == {
		import from V FX, M F, M Fx;
		normalize(n, m, den, empty, zero(0,0), zero(0,0));
	}

	-- makes the leading coefficient of the numerator of the value 1
	-- if possible, normalize the leading coeff of the polynomial otherwise
	local normalize(n:I, m:I, den:PRX, num:V FX, pol:M F, val:M Fx):% == {
		import from Rep, F, FX, Fx;
		(r, c) := dimensions pol;
		assert(c = #num);
		for j in 1..c repeat {
			lc := leadingCoefficient(num.j);
			for i in 1..r while zero? lc repeat lc := pol(i, j);
			assert(~zero? lc);
			invlc := inv lc;
			invlcx := invlc::FX::Fx;
			num.j := invlc * num.j;
			for i in 1..r repeat {
				pol(i, j) := invlc * pol(i, j);
				val(i, j) := invlcx * val(i, j);
			}
		}
		per [n, m, den, num, pol, val];
	}

	candidateInvariants(L:EQ, n:Z):(PRX, V FX, M F) == {
		import from I, V FX;
		m := machine order L;
		one?(nn := machine n) => {
			(den, num) := kernel(L)$RATSOL;
			(den, num, ident(m, #num));
		}
		(den, num, mat, candvals) := candidates(L, nn, m, true, true);
		empty? num => (den, num, mat);
		(den, num, patch!(F, mat, nn, m, false));
	}

	local candidates(L:EQ, n:I, m:I, zVal?:Boolean, heuristic?:Boolean):
		(PRX, V FX, M F, M Fx) == {
		import from Z, Rx, Fx, POWPROD;
		TIMESTART;
		Y1n := machine index(monomial(1)$POWPROD);
		(b, qb, dd, den, num, mat, Wn, lsing, lz, lq) :=
					firstRowCandidates(L, n, Y1n, zVal?);
		TIME("candidates: first row candidates at ");
		empty? num => (den, num, mat, zero(0, 0));
		nosol? := zero? dd;
		d := { nosol? => 1; Rx2Fx dd; }
		assert(~zero? d);
		a := inj ordinaryPoint L;
		dim := #num;
		N := machine(#$POWPROD);
		candvals:M Fx := zero(N, dim);
		for j in 1..dim repeat candvals(Y1n, j) := (num.j)::Fx / d;
		for i in 1..N | i ~= Y1n repeat { -- fill rows of degree 0 in Ym
			p := lookup(i::Z);
			if refine?(n, m, p) then {
				(newdim, num, mat, candvals) :=
					refine(L, Wn, i, weight(p)::Z, b, qb,
						num, lsing, lz, lq, mat,
						candvals, a, nosol?);
				TIME("candidates: next refinement at ");
				zero? newdim or (heuristic? and newdim = dim) =>
					return (den, num, mat, candvals);
				dim := newdim;
			}
		}
		TIME("candidates: refinement done at ");
		heuristic? => (den, num, mat, candvals);
		invn := inv(n::Z::Fx);
		for i in 1..prev m repeat {	-- fills derivatives of Yi^m
			Yin := monomial(i)$POWPROD;	-- Yi^n
			iYin := machine index Yin;
			index := machine index incdec(Yin, next i, i);
			for j in 1..dim repeat
				candvals(index, j) :=
					invn * differentiate candvals(iYin, j);
		}
		TIME("candidates: pure derivatives filled at ");
		vL := monicVectorize(operator L, m);
		for deg in 1..n repeat {	-- fill remaming rows
			for i in 2..N repeat {	-- fill rows of degree deg in Ym
				p := lookup(i::Z);
				if degree(p, m) = deg then
					fill!(candvals, i, deg, n, m, p, vL);
			}
		}
		TIME("candidates: others rows filled at ");
		(den, num, mat, candvals);
	}

	-- checks whether the row corresponding to p must be computed w/series
	local refine?(n:I, m:I, p:POWPROD):Boolean == {
		zero? degree(p, m) => {
			i:I := 1;
			while zero? degree(p, i) repeat i := next i;
			assert(i < m);
			degree(p, i) ~= prev n or ~one?(degree(p, next i));
		}
		false;
	}

	-- fills row r of A, which corresponds to the monomial y
	local fill!(A:M Fx, r:I, e:I, n:I, m:I, y:POWPROD, a:V Fx):() == {
		import from Z, Fx;
		assert(e = degree(y, m)); assert(e > 0);
		e1 := prev(e)::Z;
		m1 := prev m;
		c := numberOfColumns A;
		z := incdec(y, m1, m);
		kk := k := machine index z;
		for j in 1..c repeat A(r, j) := differentiate A(k, j);
		for i in 1..prev m1 | (d := degree(y, i)) > 0 repeat {
			k := machine index incdec(z, next i, i);
			ei := d::Z;
			for j in 1..c repeat
				A(r, j) := A(r, j) - ei * A(k, j);
		}
		if e1 > 0 then {
			for i in 1..m1 repeat {
				k := machine index incdec(z, i, m);
				alpha := a.i;		-- coeff of D^{i-1} in L
				for j in 1..c repeat
					A(r, j) := A(r,j) + e1 * alpha * A(k,j);
			}
			alpha := a.m;
			for j in 1..c repeat
				A(r, j) := A(r, j) + e1 * alpha * A(kk, j);
		}
		if (d := degree(y, m1)) > 0 then {
			alpha := inv(next(d)::Z::Fx);
			for j in 1..c repeat A(r, j) := alpha * A(r, j);
		}
	}

	local monicVectorize(L:RXD, m:I):V Fx == {
		import from Z, Fx;
		assert(~zero? L); assert(m = machine degree L);
		v := zero m;
		c1 := inv(RX2FX(leadingCoefficient L)::Fx);
		for trm in L repeat {
			(c, e) := trm;
			ee := machine e;
			if ee < m then v(next ee) := RX2FX(c) * c1;
		}
		v;
	}

	-- Wn is the symmetric power matrix of a fundamental sol matrix of L
	-- r is the row of Wn to use for refining, w its weight
	-- b1 is the degree bound on sol for the 1st row
	-- qb1 is the rational degree bound on sol for the 1st row
	-- ls is the list of finite singularities of L, and lz and lq are the
	-- corresponding lists of integer and rational exponent bounds there
	-- cand are the candidate invariants so far
	-- candvals are the corresponding candidate values
	-- a is the ordinary point for the expansions
	-- if nosol? is true, then the symmetric power has no nonzero
	-- rational solution, there could still be invariants with value 0
	local refine(L:EQ, Wn:M FXX, r:I, w:Z, b1:Partial Z, qb1:Partial Q,
		num1:V FX, ls:List SING, lz:List Z, lq:List Q, cand:M F,
		candvals:M Fx, a:F, nosol?:Boolean):(I, V FX, M F,  M Fx) == {
		import from Fx, RATSOL;
		import from UnivariateTaylorSeriesCategory2Poly(F, FXX, FX);
		den:PRX := { nosol? => 1; denom(w, ls, lz, lq); }
		(dnum, dden) := expandFraction den;
		dd:Rx := dnum / dden;
		degbound := {
			nosol? => -1;
			degreeBound(L, retract b1, retract qb1, w) + degree den;
		}
		(num, mat) := candidates(dot(row(Wn,r), cand), dd, degbound, a);
		zero?(dim := #num) => (0, empty, zero(0, 0), zero(0, 0));
		assert(dim = numberOfColumns mat);
		cand := cand * mat;
		candvals := candvals * MF2MFx mat;
		df := Rx2Fx dd;
		for j in 1..dim repeat candvals(r, j) := (num.j)::Fx / df;
		(dim, dot(num1, mat), cand, candvals);
	}

	-- given upper int and rat bounds b1 and qb1 for the 1st row, compute
	-- a bound for deg(num)-deg(den) for a row with weight w
	-- that bound is given by
	--              b1 + w * max(slope of algebraic newton polygon)
	--                           in the unramified case
	--             qb1 + w * max(slope of algebraic newton polygon)
	--                           in the ramified case
	-- that formula can be deduced (with some work) from Hoeij & Weil 1997
	-- in the regular singular case, the only slope of the algebraic
	-- newton polygon is -1 (differential slope 0) and the formula
	-- becomes the usual b1 - w
	local degreeBound(L:EQ, b1:Z, qb1:Q, w:Z):Z == {
		import from NEWTON, SING;
		assert(w >= 0); assert(b1::Q <= qb1);
		np := polygon singularityAtInfinity! L;
		a := numerator(q := maximalSlope np);
		assert(irregular? np or a = -1);
		ramified? np => {
			q := qb1 + w * q;
			one?(b := denominator q) => a;
			(a - (a mod b)) quo b;
		}
		assert(one? denominator q);	-- since unramified here
		b1 + w * a;
	}

	-- given upper int and rat bounds lz and lq for exponents at which
	-- the centers of ls can appear in the denom of solutions of the 1st
	-- row, compute a universal denominator for a row with weight w
	-- the bound on the exponent of each center is given by
	--             b_Z + w * max(slope of algebraic newton polygon)
	--                           in the unramified case
	--             b_Q + w * max(slope of algebraic newton polygon)
	--                           in the ramified case
	-- that formula can be deduced (with some work) from Hoeij & Weil 1997
	-- in the regular singular case, the only slope of the algebraic
	-- newton polygon is 1 (differential slope 0) and the formula
	-- for the exponent becomes the usual b_Z + w
	local denom(w:Z, ls:List SING, lz:List Z, lq:List Q):PRX=={
		assert(w >= 0);
		import from Q, NEWTON, SING;
		den:PRX := 1;
		for s in ls for bz in lz for bq in lq repeat {
			assert(bz::Q <= bq);
			p := center s;
			np := polygon s;
			a := numerator(q := maximalSlope np);
			assert(irregular? np or one? a);
			if ramified? np then {
				q := bq + w * q;
				one?(b := denominator q) => a;
				den := times!(den, p, (a - (a mod b)) quo b);
			}
			assert(one? denominator q);	-- since unramified here
			den := times!(den, p, bz + w * a);
		}
		den;
	}

	-- look for invariants from row Y1^n, with 0-value iff zVal? is true
	-- r = index of the row corresponding to Y1^m
	-- returns (b, q, d, den, [p1,...,pk], M, Wrn, ls, lz, lq)
	-- where M has k columns such that the candidate invariants are
	--          [X1^n,...,Xm^n] * Delta * column(k,j) = pj(x)/d(x)
	-- where Delta is the diagonal matrix of multinomial coefficients
	-- b is the degree bound on the solutions (-b = valuation bound)
	-- q is their rational degree bound (-q = valuation bound)
	-- ls is the list of finite singularities of L, and lz and lq are the
	-- corresponding lists of integer and rational exponent bounds there
	-- if d = 0, then no solutions, ignore, Wrn and l. Otherwise,
	--   d is the expansion of den, which is factored
	--   Wrn is the matrix symmetric power of the fund sols at the ord point
	local firstRowCandidates(L:EQ, n:I, r:I, zVal?:Boolean):(Partial Z,_
		Partial Q,Rx,PRX,V FX,M F,M FXX,List SING, List Z, List Q) == {
		import from Z, Rx, Partial PRX, Partial EQ, RATSOL;
		import from MatrixSymmetricPower(FXX, M FXX);
		TIMESTART;
		assert(n > 1);
		(bound, qbound, Ln) := degreeBound(L, failed, nz := n::Z);
		TIME("firstRowCandidates: degree bound at ");
		lsing:List SING := empty;
		lz:List Z := empty; lq:List Q := empty;
		nosol? := failed? bound;
		nosol? and ~zVal? =>
			(bound, qbound, 0, 1, empty,
					zero(0, 0), zero(0, 0), lsing, lz, lq);
		uden:Partial PRX := failed;
		if ~nosol? then {
			(uden, lsing, lz, lq, Ln) := denominator(L, failed, nz);
			nosol? := failed? uden;
		}
		TIME("firstRowCandidates: denominator at ");
		den := { nosol? => 1; retract uden; }
		nosol? and ~zVal? => (failed, qbound, 0, 1, empty, zero(0, 0),_
						zero(0, 0), lsing, lz, lq);
		(dnum, dden) := expandFraction den;
		dd := dnum / dden;
		(a, W) := localSolutions L;
		Wn := symmetricPower(W, machine n, Z2FXX);
		TIME("firstRowCandidates: symmetric power matrix at ");
		degbound := { failed? bound => -1; degree(den) + retract bound }
		(num, mat) := candidates(row(Wn, r), dd, degbound, inj a);
		TIME("firstRowCandidates: candidates at ");
		(bound, qbound, { nosol? => 0; dd }, den, num, mat,
							Wn, lsing, lz, lq);
	}

	-- multiplies A in place by the diagonal of multinomials or its inverse
	local patch!(S:Field, A:M S, n:I, m:I, inv?:Boolean):M S == {
		macro PP == DenseHomogeneousPowerProduct(n, m);
		import from Z, R, S, PP;
		if (dim := numberOfColumns A) > 0 then {
			N := machine(#$PP);
			for i in 1..N repeat {
				c := multinomial lookup(i::Z);
				if ~one? c then {
					cc := c::S;
					if inv? then cc := inv cc;
					for j in 1..dim repeat
						A(i, j) := cc * A(i, j);
				}
			}
		}
		A;
	}

	-- n = #variables (order of L), m = #sols
	local ident(n:I, m:I):M F == {
		import from F;
		assert(n <= m);
		id := zero(n, m);
		for i in 1..m repeat id(i, i) := 1;
		id;
	}

	local candidates(invars:V FXX, d:Rx, bound:Z, a:F):(V FX, M F) == {
		import from FX, Fraction FX, FXX;
		import from MonogenicAlgebra2(R, RX, F, FX);
		import from UnivariateTaylorSeriesCategory2Poly(F, FXX, FX);
		if bound < -1 then bound := -1;
		if ~one? d then {
			den := map(inj)(numerator d) / map(inj)(denominator d);
			(nu, sden) := expandFraction(a)(den);
			assert(zero? nu);	-- since a is an ordinary point
			invars := sden * invars;
		}
		(num, kern) := polynomials(invars, bound);
		([translate(p, a) for p in num], kern);
	}

	local dot(v:V FXX, m:M F):V FXX == {
		import from FXX;
		[dot(w)(v) for w in columns m];
	}

	local dot(v:V FX, m:M F):V FX == {
		import from I;
		[dot(v, m, j) for j in 1..numberOfColumns m];
	}

	local dot(v:V FX, m:M F, col:I):FX == {
		n := #v;
		assert(n = numberOfRows m);
		s:FX := 0;
		for i in 1..n repeat s := add!(s, m(i, col), v.i);
		s;
	}

	riccati(FxY:POLY Fx)(i:%):V FxY == {
		import from I;
		zero? dimension i => empty;
		riccati(FxY)(canonicalImage i, degree i, order i);
	}

	local riccati(FxY:POLY Fx)(val:M Fx, n:I, m:I):V FxY == {
		import from Z, POWPROD, Fx, FxY;
		a:V FxY := zero m;
		a.1 := monom;
		a.2 := -1;
		ev := eval(FxY, V FxY, a);
		(N, dim) := dimensions val;
		assert(dim > 0);
		p:V FxY := zero dim;
		for i in 1..N repeat {
			q := ev lookup(i::Z);		-- q = Z(y,-1,0,...,0)
			for j in 1..dim repeat p.j := add!(p.j, val(i, j), q);
		}
		p;
	}

	eval(S:Ring, sigma:F -> S, a:V S)(i:%):V S == {
		import from I;
		zero? dimension i => empty;
		eval(S, sigma, a)(polynomial i, degree i, order i);
	}

	local eval(S:Ring, sigma:F->S, a:V S)(pol:M F, n:I, m:I):V S == {
		import from Z, POWPROD, S;
		ev := eval(S, V S, a);
		(N, dim) := dimensions pol;
		assert(dim > 0);
		p:V S := zero dim;
		for i in 1..N repeat {
			q := ev lookup(i::Z);		-- q = Z(a)
			for j in 1..dim repeat
				p.j := add!(p.j, sigma(pol(i, j)) * q);
		}
		p;
	}
}
