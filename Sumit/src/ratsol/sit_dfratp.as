-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-------------------------------- sit_dfratp.as ------------------------------
--
-- Rational solutions of linear ordinary differential equations (finite fields)
--
-- Copyright (c) Manuel Bronstein 2002
-- Copyright (c) INRIA 2002, Version 1.0.2
-- Logiciel Sum^it (c) INRIA 2002, dans sa version 1.0.2
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I	== MachineInteger;
	Z	== Integer;
	V	== Vector;
	M	== DenseMatrix;
}

#if ALDOC
\thistype{LinearOrdinaryDifferentialOperatorModularRationalSolutions}
\History{Manuel Bronstein}{19/9/2002}{created}
\Usage{import from \this(F, FX, FXD)}
\Params{
{\em F} & \alexttype{FiniteField}{} & A finite field\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
{\em FXD} & \altype{LinearOrdinaryDifferentialOperatorCategory} FX &
Differential operators\\
}
\Descr{\this(F, FX, FXD) provides a rational solver
for linear ordinary differential operators with polynomial coefficients
over a finite field.}
\begin{exports}
\alexp{kernel}: & FXD $\to$ \alexttype{Vector}{} FX & Rational solutions\\
\alexp{matrix}: & FXD $\to$ \alexttype{DenseMatrix}{} FX & Matrix of operator\\
\end{exports}
#endif

LinearOrdinaryDifferentialOperatorModularRationalSolutions(_
	F: FiniteField,
	FX: UnivariatePolynomialCategory F,
	FXD:LinearOrdinaryDifferentialOperatorCategory FX): with {
		kernel: FXD -> V FX;
#if ALDOC
\alpage{kernel}
\Usage{\name~L}
\Signature{FXD}{\alexttype{Vector}{} FX}
\Params{ {\em L} & FXD & A linear ordinary differential operator\\ }
\Retval{Returns a basis $(p_1,\dots,p_m)$ over $F(x^p)$
for all the solutions in $F(x)$ of $Ly = 0$.}
#endif
		matrix: FXD -> M FX;
#if ALDOC
\alpage{matrix}
\Usage{\name~L}
\Signature{FXD}{\alexttype{DenseMatrix}{} FX}
\Params{ {\em L} & FXD & A linear ordinary differential operator\\ }
\Retval{Returns the matrix of \emph{L} as an $F(x^p)$-linear
map from $F(x)$ into $F(x)$ (seen as a vector space of dimension \emph{p}
over $F(x^p)$).}
#endif
} == add {
	local charac:Z	== characteristic$F;
	local icharac:I	== machine(charac)$Z;
	local xp:FX	== monomial charac;

	kernel(L:FXD):V FX == {
		import from I, FX, M FX, LinearAlgebra(FX, M FX);
		TRACE("dfratp::kernel: L = ", L);
		TIMESTART;
		m := matrix L;
		TIME("dfratp::kernel: matrix computed at ");
		TRACE("dfratp::kernel: m = ", m);
		k := kernel m;
		TIME("dfratp::kernel: kernel computed at ");
		TRACE("dfratp::kernel: k = ", k);
		[poly(k, j) for j in 1..numberOfColumns k];
	}

	matrix(L:FXD):M FX == {
		import from I, Z;
		m := zero(icharac, icharac);
		for j in 1..icharac repeat
			vectorize!(m, j, L(monomial(prev(j)::Z)$FX));
		m;
	}

	-- stores in column col of m the coords of q in the basis 1,..,x^(p-1)
	-- those coordinates are actually in F[x^p], represented as FX (X = x^p)
	local vectorize!(m:M FX, col:I, q:FX):() == {
		import from Z;
		for trm in q repeat {
			(c, n) := trm;			-- c x^n = c (x^p)^t x^r
			(t, r) := divide(n, charac);	-- where n = t p + r
			m(next machine r, col) := term(c, t);	-- = c (x^p)^t
		}
	}

	-- inverse op of vectorize: returns the dot product of
	-- the j-th column of m times (1,x,...,x^{p-1}) evaluated at X = x^p
	local poly(m:M FX, col:I):FX == {
		import from Z, F;
		q:FX := 0;
		for i in 1..icharac repeat
			q := add!(q, 1, prev(i)::Z, m(i,col)(xp));
		q;
	}
}
