-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_semiinv.as ------------------------------
-- Copyright (c) Manuel Bronstein 2003
-- Copyright (c) INRIA 2003, Version 1.0.2
-- Logiciel Sum^it (c) INRIA 2003, dans sa version 1.0.2
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I   == MachineInteger;
	Z   == Integer;
	Q   == Fraction Z;
	V   == Vector;
	M   == DenseMatrix;
	PRX == Product RX;
	PFX == Product FX;
	FXD == LinearOrdinaryDifferentialOperator FX;
	Fx  == Fraction FX;
	SING== PlaneSingularity(R, RX, RXD);
	EQ  == LinearHolonomicDifferentialEquation(R, F, inj, RX, RXD);
	REGSOL == LinearOrdinaryDifferentialOperatorRegularSolutions;
	EXPSOL == LinearOrdinaryDifferentialOperatorExponentialSolutions;
	INVAR == LinearOrdinaryDifferentialOperatorInvariants(F,F,id,FX,FXD,FX);
	POLY== UnivariatePolynomialCategory;
}

#if ALDOC
\thistype{LinearOrdinaryDifferentialOperatorSemiInvariants}
\History{Manuel Bronstein}{28/7/2003}{created}
\Usage{import from \this(R, F, $\iota$, RX, RXD, FX)}
\Params{
{\em R} & \alexttype{FactorizationRing}{} & The coefficient ring\\
        & \alexttype{CharacteristicZero}{} &\\
{\em F} & \alexttype{Field}{} & A field containing R\\
        & \alexttype{FactorizationRing}{} & \\
        & \alexttype{CharacteristicZero}{} &\\
$\iota$ & $R \to F$ & Injection from R into F\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
\emph{RXD} & \altype{LinearOrdinaryDifferentialOperatorCategory} RX &
Differential operators\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
}
\Descr{\this(R, F, $\iota$, RX, RXD, FX) provides a solver for the
exponential solutions of symmetric powers of
linear ordinary differential operators with polynomial coefficients.}
\begin{exports}
\end{exports}
\begin{alwhere}
EQ &==& \altype{LinearHolonomicDifferentialEquation}(R, RX, RXD)\\
\end{alwhere}
#endif

LinearOrdinaryDifferentialOperatorSemiInvariants(
	R:   Join(FactorizationRing, CharacteristicZero),
	F:   Join(Field, FactorizationRing, CharacteristicZero), inj: R -> F,
	RX:  UnivariatePolynomialCategory R,
	RXD: LinearOrdinaryDifferentialOperatorCategory RX,
	FX:  UnivariatePolynomialCategory F): with {
		canonicalImage: % -> M Fx;
		degree: % -> I;
		dimension: % -> I;
		exponential: % -> Fx;
		polynomial: % -> M F;
		order: % -> I;
		riccati: (FxY:POLY Fx) -> % -> V FxY;
		semiInvariants: (RXD, Z) -> List %;
		semiInvariants: (EQ,  Z) -> List %;
		symmetricKernel: % -> M Fx;
		value: % -> (PFX, V FX);
} == add {
	local id(f:F):F == f;

	Rep == Record(exp:Fx, inv:INVAR);

	local invar(i:%):INVAR	== { import from Rep; rep(i).inv; }
	exponential(i:%):Fx	== { import from Rep; rep(i).exp; }
	degree(i:%):I		== { import from INVAR; degree invar i; }
	dimension(i:%):I	== { import from INVAR; dimension invar i; }
	order(i:%):I		== { import from INVAR; order invar i; }
	polynomial(i:%):M F	== { import from INVAR; polynomial invar i; }
	value(i:%):(PFX, V FX)	== { import from INVAR; value invar i; }
	local trivcand():Generator Fx	== generate { import from Fx; yield 0; }

	canonicalImage(i:%):M Fx == {
		import from INVAR;
		canonicalImage invar i;
	}

	symmetricKernel(i:%):M Fx == {
		import from INVAR;
		symmetricKernel invar i;
	}

	riccati(FxY:POLY Fx):% -> V FxY == {
		f := riccati(FxY)$INVAR;
		(i:%):V FxY +-> {
			import from Z, Fx, FxY;
			v := f invar i;
			zero?(u := exponential i) => v;
			u := u / (degree(i)::Z::Fx);
			[translate(p, u) for p in v];
		}
	}

	semiInvariants(L:RXD, n:Z):List % == {
		import from EQ;
		assert(n > 0);
		semiInvariants(primitivePart(L)::EQ, n);
	}

	semiInvariants(L:EQ, n:Z):List % == {
		import from I, List SING;
		assert(n > 0);
		l:List % := empty;
		f := exp2invar(operator L, n);
		for u in candidates(L, n, finiteSingularities! L) repeat {
			iv := f u;
			if dimension(iv) > 0 then l := cons(iv, l);
		}
		l;
	}

	local candidates(L:EQ, n:Z, l:List SING):Generator Fx == {
		import from SING;
		empty? l => {
			irregularAtInfinity? L => nonRatCandidates(L, n, l);
			trivcand();
		}
		allRationalExponents?(s := first l) => candidates(L,n,s,rest l);
		nonRatCandidates(L, n, l);
	}

	local nonRatCandidates(L:EQ,n:Z,l:List SING):Generator Fx == generate {
		import from RXD, Fx, EXPSOL(R, F, inj, RX, RXD, FX);
		Ln := symmetricPower(operator L, n)::EQ;
		for cand in candidates(Ln, l) repeat {
			(u, v, ignore) := cand;
			yield(u+v);
		}
	}

	local candidates(L:EQ,n:Z,s:SING,l:List SING):Generator Fx == generate {
		import from Q, Set Q, RX, FX, Fx;
		import from MonogenicAlgebra2(R, RX, F, FX),
			LinearOrdinaryDifferentialOperatorRadicalInvariants(R,_
							F, inj, RX, RXD, FX);
		assert(allRationalExponents? s);
		(hasInt?, lexp) := candidates(L, n, 0, true, s);
		p := center s;
		pp := { empty? lexp => 0; map(inj)(p); }
		dp := { empty? lexp => 0; differentiate pp; }
		for u in candidates(L, n, l) repeat {
			if hasInt? then yield u;
			for r in lexp repeat {
				num := numerator r;
				den := denominator r;
				yield (u + ((num * dp) / (den * pp)));
			}
		}
	}

	-- TEMPO: DOES NOT REUSE THE SYMMETRIC POWER IF IT WAS COMPUTED
	local exp2invar(L:RXD, n:Z):Fx -> % == {
		m := machine degree L;
		(u:Fx):% +-> {
			import from Rep, F, FX;
			import from REGSOL(R, F, inj, RX, RXD, FX);
			TIMESTART;
			(alpha, LL) := twist(L, inv(n::F)::FX * u);
			TIME("exp2invar: operator twisted at ");
			iv := invariants(LL, n);
			TIME("exp2invar: invariants at ");
			per [u, iv];
		}
	}
}
