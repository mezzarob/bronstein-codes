-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------- sit_diffexp.as --------------------------
--
-- Exponential solutions of linear ordinary differential equations
--
-- Copyright (c) Manuel Bronstein 2001
-- Copyright (c) INRIA 2001, Version 0.1.13
-- Logiciel Sum^it (c) INRIA 2001, dans sa version 0.1.13
-----------------------------------------------------------------------------

#include "sumit"

macro {
	Z	== Integer;
	Q	== Fraction Z;
	FR	== FractionalRoot;
	Fx	== Fraction FX;
	FXD	== LinearOrdinaryDifferentialOperator FX;
	FxD	== LinearOrdinaryDifferentialOperator Fx;
	V	== Vector;
	PFX	== Product FX;
	SING	== PlaneSingularity(R, RX, RXD);
	EQ	== LinearHolonomicDifferentialEquation(R, F, inj, RX, RXD);
	EQF	== LinearHolonomicDifferentialEquation(F, F, fident, FX, FXD);
	NEWTON	== NewtonPolygon(R, RX, RXD);
	POINT	== Cross(Z, Z, RX);
	SLOPE	== Cross(Q, List POINT);
}

LODOExponentialSolutionsTools1(
	R: Join(FactorizationRing, CharacteristicZero),
	F: Join(Field, FactorizationRing, CharacteristicZero), inj: R -> F,
	RX: UnivariatePolynomialCategory R,
	RXD:LinearOrdinaryDifferentialOperatorCategory RX,
	FX: UnivariatePolynomialCategory F): with {
		expAlg: (RXD, RX, Z) -> Generator Fx;
		expRat: (RXD, R, Z) -> Generator Fx;
		infiniteExpParts: (RXD, NEWTON, Z) -> Generator Fx;
		-- TEMPO: SEE OVERLOADING BUG BELOW
		-- infiniteExpParts: (RXD, Z) -> Generator Fx;
		infiniteExpParts1: (RXD, Z) -> Generator Fx;
} == add {
	-- returns only exponential parts for 0 <= slope < smax
	-- or for all slopes if smax < 0
	infiniteExpParts(L:RXD, N:NEWTON, smax:Z):Generator Fx == generate {
		import from Z, Q, List SLOPE, List POINT, RX;
		import from NewtonPolygonEquations(R, RX, RXD, R, RX);
		import from LODOExponentialSolutionsTools2(R,F,inj,RX,RXD,FX);
		TRACE("diffexptls1::infiniteExpParts: L = ", L);
		TRACE("diffexptls1::infiniteExpParts: N = ", "*");
		TRACE("diffexptls1::infiniteExpParts: smax = ", smax);
		assert(irregular? N);
		for pair in slopes N repeat {
			(sl, seg) := pair;
			if one?(denominator sl) then {
				sp := numerator sl;
				if sp >= 0 and (smax < 0 or sp < smax) then {
					(a, b, ignore) := first seg;
					mu := a * sp - b;
					for r in characteristicRoots(N, sl,_
						leadingCoefficient) repeat _
						for e in infiniteExpParts(L,_
							sp,mu,r) repeat yield e;
				}
			}
		}
	}

	-- returns only exponential parts for 0 <= slope < smax
	-- TEMPORARY: OVERLOADING FNCS RETURN Generator = ALDOR1.0 OPTIMIZER BUG
	-- infiniteExpParts(L:RXD, smax:Z):Generator Fx == {
	infiniteExpParts1(L:RXD, smax:Z):Generator Fx == {
		import from NEWTON;
		TRACE("diffexptls1::infiniteExpParts: L = ", L);
		TRACE("diffexptls1::infiniteExpParts: smax = ", smax);
		assert(smax > 0);
		irregular?(N := newtonPolygon L) => infiniteExpParts(L,N,smax);
		generate {};
	}

	-- returns only exponential parts for 1 < slope < smax
	expAlg(L:RXD, p:RX, smax:Z):Generator Fx == generate {
		import from Z, Q, List SLOPE, List POINT, NEWTON;
		import from MonogenicAlgebra2(R, RX, F, FX),
			LODOExponentialSolutionsTools2(R,F,inj,RX,RXD,FX);
		irregular?(N := newtonPolygon(L, p)) => {
			pp := map(inj)(p);
			for pair in slopes N repeat {
				(sl, seg) := pair;
				if one?(denominator sl) then {
					slp := numerator sl;
					if slp > 1 and slp < smax then {
						(a, b, ignore) := first seg;
						mu := a * slp - b;
						for u in expAlg(L,N,slp,mu,pp) _
							repeat yield u;
					}
				}
			}
		}
	}

	-- returns only exponential parts for 1 < slope < smax
	expRat(L:RXD, p0:R, smax:Z):Generator Fx == generate {
		import from Z, Q, List SLOPE, List POINT, RX, NEWTON;
		import from LODOExponentialSolutionsTools2(R,F,inj,RX,RXD,FX);
		TRACE("diffexptls1::expRat: L = ", L);
		TRACE("diffexptls1::expRat: p0 = ", p0);
		TRACE("diffexptls1::expRat: smax = ", smax);
		p := monom$RX - p0::RX;
		irregular?(N := newtonPolygon(L, p)) => {
			for pair in slopes N repeat {
				(sl, seg) := pair;
				if one?(denominator sl) then {
					slp := numerator sl;
					if slp > 1 and slp < smax then {
						(a, b, ignore) := first seg;
						mu := a * slp - b;
						for u in expRat(L,N,slp,mu,p0) _
							repeat yield u;
					}
				}
			}
		}
	}
}

LODOExponentialSolutionsTools2(
	R: Join(FactorizationRing, CharacteristicZero),
	F: Join(Field, FactorizationRing, CharacteristicZero), inj: R -> F,
	RX: UnivariatePolynomialCategory R,
	RXD:LinearOrdinaryDifferentialOperatorCategory RX,
	FX: UnivariatePolynomialCategory F): with {
		expAlg: (RXD, NEWTON, Z, Z, FX) -> Generator Fx;
		expRat: (RXD, NEWTON, Z, Z, R) -> Generator Fx;
		-- TEMPO: SEE OVERLOADING BUG BELOW
		-- expRat: (RXD, F, FX, Z, Z, Z, F) -> Generator Fx;
		expRat1: (RXD, F, FX, Z, Z, Z, F) -> Generator Fx;
		infiniteExpParts: (RXD, Z, Z, FR R) -> Generator Fx;
} == add {
	local id(f:F):F == f;

	-- sl is an algebraic slope >= 0 and c is a root of the char. eq. for sl
	infiniteExpParts(L:RXD, sl:Z, mu:Z, c:FR R):Generator Fx == generate {
		import from F, FX, Fx,
			NewtonPolygonEquations(R, RX, RXD, F, FX),
			LODOExponentialSolutionsTools1(F, F, id, FX, FXD, FX),
			LinearOrdinaryDifferentialOperatorRegularSolutions(R,_
							F, inj, RX, RXD, FX);
		assert(sl >= 0);
		TRACE("diffexptls2::infiniteExpPart: L = ", L);
		TRACE("diffexptls2::infiniteExpPart: sl = ", sl);
		TRACE("diffexptls2::infiniteExpPart: mu = ", mu);
		TRACE("diffexptls2::infiniteExpPart: c = ", c);
		(a, b) := value c;
		f := inj(a) / inj(b);
		multiplicity(c) > 1 => {
			u := term(f, sl)::Fx;
			yield u;
			sl > 0 => {
				(alpha, LL) := twist(L, u);
				-- TEMPORARY: ALDOR 1.0.0 BAD OVERLOADING BUG
				-- for v in infiniteExpParts(LL, sl) repeat
				for v in infiniteExpParts1(LL, sl) repeat
					yield(u+v);
			}
		}
		-- TEMPORARY: ALDOR 1.0.0 BAD OVERLOADING BUG
		-- yield exponentialPart(L, sl, mu, f, inj)::Fx;
		yield exponentialPartAtInfinity(L, sl, mu, f, inj)::Fx;
	}

	-- sl is an algebraic slope > 1 centered above the irreducible p
	expAlg(L:RXD, N:NEWTON, sl:Z, mu:Z, p:FX):Generator Fx == generate {
		macro E == SimpleAlgebraicExtension(F, FX, p);
		macro EX == DenseUnivariatePolynomial E;
		import from Q, Fx, FR E, EX;
		import from NewtonPolygonEquations(R, RX, RXD, E, EX),
			LODOExponentialSolutionsTools1(F, F, id, FX, FXD, FX),
			LinearOrdinaryDifferentialOperatorRegularSolutions(R,_
							F, inj, RX, RXD, FX);
		TRACE("diffexptls2::expAlg: L = ", L);
		TRACE("diffexptls2::expAlg: sl = ", sl);
		TRACE("diffexptls2::expAlg: mu = ", mu);
		TRACE("diffexptls2::expAlg: p = ", p);
		assert(sl > 1);
		pinj := map(inj)$MonogenicAlgebra2(R, RX, F, FX);
		ppinj := map(coerce)$MonogenicAlgebra2(F, FX, E, EX);
		pp := ppinj p;
		-- TEMPORARY: THE FOLLOWING CAUSES A COMPILE-TIME SEG FAULT
		-- alpha:E := monom;
		alpha:E := reduce(monom$FX);
		assert(zero? pp alpha);
		h := quotient(pp, monom$EX - alpha::EX);	-- p / x-alpha
		halpha := h alpha;
		TRACE("diffexptls2::expAlg: p/(x-alpha) = ", h);
		hsl := h^sl;
		TRACE("diffexptls2::expAlg: h^sl = ", hsl);
		psl := p^sl;
		TRACE("diffexptls2::expAlg: p^sl = ", psl);
		trpol := trace(EX);
		for r in characteristicRoots(N,sl::Q,(q:RX):E +-> reduce pinj q)
			repeat {
			(a, b) := value r;
			f := a / b;
			TRACE("diffexptls2::expAlg: f = ", f);
			TRACE("diffexptls2::expAlg: multiplicity = ",_
								multiplicity r);
			multiplicity(r) > 1 => {
				u := trpol(f * hsl) / psl;
				yield u;
				sl > 2 => {
					(ignore, LL) := twist(L, u);
					for v in expAlg(LL, p, sl) repeat
						yield(u + v);
				}
			}
			num := exponentialPart(L, alpha, sl, mu, f,
							(s:R):E +-> inj(s)::E);
			TRACE("diffexptls2::expAlg: num = ", num);
			yield(trpol(num * hsl) / psl);
		}
	}

	expRat(L:RXD, N:NEWTON, sl:Z, mu:Z, p0:R):Generator Fx == generate {
		import from Z, Q, R, F, RX, FX, Fx, FR R;
		import from NewtonPolygonEquations(R, RX, RXD, R, RX);
		TRACE("diffexptls2::expRat: L = ", L);
		TRACE("diffexptls2::expRat: N = ", "*");
		TRACE("diffexptls2::expRat: sl = ", sl);
		TRACE("diffexptls2::expRat: mu = ", mu);
		TRACE("diffexptls2::expRat: p0 = ", p0);
		gen := {
			zero? p0 => characteristicRoots(N, sl::Q,
						(q:RX):R +-> coefficient(q, 0));
			characteristicRoots(N, sl::Q, (q:RX):R +-> q p0);
		}
		pp0 := inj p0;
		pp := monom - pp0::FX;
		psl := pp^sl;
		for r in gen repeat {
			(a, b) := value r;
			m := multiplicity r;
			f := inj(a) / inj(b);
			-- TEMPO: SEE OVERLOADING BUG BELOW
			-- for e in expRat(L,pp0,psl,sl,mu,m,f) repeat yield e;
			for e in expRat1(L,pp0,psl,sl,mu,m,f) repeat yield e;
		}
	}

	-- TEMPORARY: OVERLOADING FNCS RETURN Generator = ALDOR1.0 OPTIMIZER BUG
	-- expRat(L:RXD,p0:F,psl:FX,sl:Z,mu:Z,mult:Z,c:F):Generator Fx==generate {
	expRat1(L:RXD,p0:F,psl:FX,sl:Z,mu:Z,mult:Z,c:F):Generator Fx==generate {
		import from Fx, NewtonPolygonEquations(R, RX, RXD, F, FX),
			LODOExponentialSolutionsTools1(F, F, id, FX, FXD, FX),
			LinearOrdinaryDifferentialOperatorRegularSolutions(R,_
							F, inj, RX, RXD, FX);
		TRACE("diffexptls2::expRat: L = ", L);
		TRACE("diffexptls2::expRat: p0 = ", p0);
		TRACE("diffexptls2::expRat: psl = ", psl);
		TRACE("diffexptls2::expRat: sl = ", sl);
		TRACE("diffexptls2::expRat: mu = ", mu);
		TRACE("diffexptls2::expRat: mult = ", mult);
		TRACE("diffexptls2::expRat: c = ", c);
		assert(sl > 1);
		mult > 1 => {
			u := c::FX / psl;
			yield u;
			sl > 2 => {
				(ignore, LL) := twist(L, u);
				for v in expRat(LL, p0, sl) repeat yield(u+v);
			}
		}
		yield exponentialPart(L, p0, sl, mu, c, inj) / psl;
	}
}


#if ALDOC
\thistype{LinearOrdinaryDifferentialOperatorExponentialSolutions}
\History{Manuel Bronstein}{3/8/2001}{created}
\Usage{import from \this(R, F, $\iota$, RX, RXE, FX)}
\Params{
{\em R} & \alexttype{FactorizationRing}{} & The coefficient ring\\
        & \alexttype{CharacteristicZero}{} &\\
{\em F} & \alexttype{Field}{} & A field containing R\\
        & \alexttype{FactorizationRing}{} &\\
        & \alexttype{CharacteristicZero}{} &\\
$\iota$ & $R \to F$ & Injection\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
{\em RXD} & \altype{LinearOrdinaryDifferentialOperatorCategory} RX &
Differential operators\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
}
\Descr{\this(R, F, $\iota$, RX, RXD, FX) provides an exponential solver
for linear ordinary differential operators with polynomial coefficients.}
\begin{exports}
\alexp{exponentialSolutions}: & EQ $\to$ EXPSOLS & Exponential solutions\\
                              & RXD $\to$ EXPSOLS & \\
\end{exports}
\begin{alwhere}
EQ &==& \altype{LinearHolonomicDifferentialEquation}(R, F, $\iota$, RX, RXD)\\
EXPSOLS &==&
\alexttype{List}{} \albuiltin{Cross}(\alexttype{Fraction}{} FX,
\alexttype{Product}{} FX, \alexttype{Vector}{} FX)\\
\end{alwhere}
#endif

LinearOrdinaryDifferentialOperatorExponentialSolutions(_
	R: Join(FactorizationRing, CharacteristicZero),
	F: Join(Field, FactorizationRing, CharacteristicZero), inj: R -> F,
	RX: UnivariatePolynomialCategory R,
	RXD:LinearOrdinaryDifferentialOperatorCategory RX,
	FX: UnivariatePolynomialCategory F): with {
		candidates: (EQ, List SING) -> Generator Cross(Fx, Fx, FXD);
		exponentialSolutions: EQ -> List Cross(Fx, PFX, V FX);
		exponentialSolutions: RXD -> List Cross(Fx, PFX, V FX);
		exponentialSolutions: (EQ,  RX) -> List Cross(Fx, PFX, V FX);
		exponentialSolutions: (RXD, RX) -> List Cross(Fx, PFX, V FX);
#if ALDOC
\alpage{exponentialSolutions}
\Usage{\name~L}
\Signatures{
\name: & RXD $\to$ \alexttype{List}{} \albuiltin{Cross}(\alexttype{Fraction}{}
FX, \alexttype{Product}{} FX, \alexttype{Vector}{} FX)\\
\name: & EQ $\to$ \alexttype{List}{} \albuiltin{Cross}(\alexttype{Fraction}{}
FX, \alexttype{Product}{} FX, \alexttype{Vector}{} FX)\\
}
\begin{alwhere}
EQ &==& \altype{LinearHolonomicDifferentialEquation}(R, F, $\iota$, RX, RXD)\\
\end{alwhere}
\Params{
\emph{L} & EQ & A linear ordinary differential operator\\
         & RXD &\\
}
\Retval{Returns all the solutions $y$ of $Ly = 0$ such that $y'/y$ is
a rational function.
The solutions are returned as a list of tuples
$(u, d, [v_1,\dots,v_m])$ that correspond
to the subspace of exponential solutions spanned by
$$
\frac{v_1}d e^{\int u},\dots,\frac{v_m}d e^{\int u}
$$
where the $v_j$ are linearly independent.
The set of all the exponential solutions is then
the union of those subspaces.
}
#endif
} == add {
	local fident(f:F):F == f;

	exponentialSolutions(L:RXD):List Cross(Fx, PFX, V FX) == {
		import from EQ, RX;
		exponentialSolutions(primitivePart(L)::EQ, 0);
	}

	exponentialSolutions(L:RXD, sing:RX):List Cross(Fx, PFX, V FX) == {
		import from EQ;
		exponentialSolutions(primitivePart(L)::EQ, sing);
	}

	exponentialSolutions(L:EQ):List Cross(Fx, PFX, V FX) ==
		exponentialSolutions(L, 0$RX);

	-- the operator of L should be primitive at this point
	-- returns a list of tuples (u, d, [v1,...,vk])
	-- that describe the subspace  p1^e1...ps^es \sum_i ci vi/d
	-- only look for sols whose singularities are in sing if sing <> 0
	exponentialSolutions(L:EQ, sing:RX):List Cross(Fx, PFX, V FX) == {
		import from Boolean, SING, List SING, FX, V FX, Fx,
			LinearOrdinaryDifferentialOperatorTools(FX, FXD),
			LinearOrdinaryDifferentialOperatorRationalSolutions(F,_
							F, fident, FX, FXD, FX);
		TRACE("diffexp::exponentialSolutions: L = ", L);
		TRACE("diffexp::exponentialSolutions: sing = ", sing);
		TIMESTART;
		-- only test necessary cond. if sing = 0, as the test is global
		zero? sing and cannotHaveExponentialSolution? L => empty;
		l:List Cross(Fx, PFX, V FX) := empty;
		fsing := RX2FX sing;
		for cand in candidates(L, analyze!(L, sing)) repeat {
			(u, v, LL) := cand;
			TRACE("diffexp::expSols: u = ", u);
			TRACE("diffexp::expSols: v = ", v);
			(alpha, LLL) := compose(numerator v, denominator v)(LL);
			TIME("diffexp::exponentialSols: operator twisted at ");
			(den, num) := kernel(LLL, fsing);
			TIME("diffexp::exponentialSols: rational kernel at ");
			if ~empty?(num) then l := cons((u + v, den, num), l);
		}
		l;
	}

	-- yield triples (u, v, LL) such that the candidate is u+v
	-- and LL is L twisted by u already
	candidates(L:EQ, l:List SING):Generator Cross(Fx, Fx, FXD) == generate {
		import from Fx,
			LinearOrdinaryDifferentialOperatorRegularSolutions(R,_
							F, inj, RX, RXD, FX),
			LinearOrdinaryDifferentialOperatorRegularSolutions(F,_
							F, fident, FX, FXD, FX);
		allsing := l;
		if irregularAtInfinity? L then
			allsing := cons(singularityAtInfinity! L, allsing);
		op := operator L;
		d := centers l;
		for u in irregularCandidates(L, allsing) repeat {
			(alpha, LL) := twist(op, u);
			eq := LL::EQF;
			lv:List Fx := candidates(eq, analyze!(eq, d));
			for v in lv repeat yield (u, v, LL);
		}
	}

	local centers(l:List SING):PFX == {
		import from Z, SING;
		d:PFX := 1;
		for s in l repeat d := times!(d, RX2FX center s, 1);
		d;
	}

	-- generates fractions u corresponding to the solution candidate
	--  y = f exp(int u)  where f is a rational function
	local irregularCandidates(L:EQ, l:List SING):Generator Fx == generate {
		import from Fx, SING, NEWTON;
		empty? l => yield 0;
		s := first l;
		LL := operator L;
		for u in irregularCandidates(L, rest l) repeat {
			yield u;		-- no contrib from s
			if irregular? polygon s then
				for r in expParts(s, LL) repeat yield(u+r);
		}
	}

	local expParts(s:SING, L:RXD):Generator Fx == {
		import from Z, NEWTON;
		import from LODOExponentialSolutionsTools1(R,F,inj,RX,RXD,FX);
		N := polygon s;
		assert(irregular? N);
		finite? N => finiteExpParts(L, s);
		infiniteExpParts(L, N, -1);
	}

	local RX2FX(p:RX):FX == {
		import from MonogenicAlgebra2(R, RX, F, FX);
		map(inj)(p);
	}

	local finiteExpParts(L:RXD, s:SING):Generator Fx == generate {
		import from Z, Q, R, RX, List SLOPE, List POINT, NEWTON,
			LODOExponentialSolutionsTools2(R, F, inj, RX, RXD, FX);
		assert(irregular? polygon s);
		d := degree(p := center s);
		pp:FX := { one? d => 0; RX2FX p }
		inv? := unit? leadingCoefficient p;
		for pair in slopes(N := polygon s) repeat {
			(sl, seg) := pair;
			if one?(denominator sl) and sl > 1 then {
				(a, b, ignore) := first seg;
				slp := numerator sl;
				mu := a * slp - b;
				gen := {
					one? d => {
						inv? => expRat(L, s, slp, mu);
						expRatQ(L, s, slp, mu);
					}
					expAlg(L, N, slp, mu, pp);
				}
				for u in gen repeat yield u;
			}
		}
	}

	local expRat(L:RXD, s:SING, sl:Z, mu:Z):Generator Fx == {
		import from Z, R, RX, Partial RX, NEWTON;
		import from LODOExponentialSolutionsTools2(R,F,inj,RX,RXD,FX);
		TRACE("diffexp::expRat: L = ", L);
		TRACE("diffexp::expRat: s = ", "*");
		TRACE("diffexp::expRat: sl = ", sl);
		TRACE("diffexp::expRat: mu = ", mu);
		p := center s;
		N := polygon s;
		assert(~failed? exactQuotient(center N, p));
		assert(unit? leadingCoefficient p);
		assert(one? degree p);
		p0 := coefficient(p, 0);
		a := quotient(-p0, leadingCoefficient p);
		expRat(L, N, sl, mu, a);
	}
		
	local expRatQ(L:RXD, s:SING, sl:Z, mu:Z):Generator Fx == generate {
		import from Boolean, Z, Q, R, F, FX, RX, Partial RX, Fx;
		import from NEWTON, NewtonPolygonEquations(R, RX, RXD, F, FX);
		TRACE("diffexp::expRatQ: L = ", L);
		TRACE("diffexp::expRatQ: s = ", "*");
		TRACE("diffexp::expRatQ: sl = ", sl);
		TRACE("diffexp::expRatQ: mu = ", mu);
		p := center s;
		N := polygon s;
		assert(~failed? exactQuotient(center N, p));
		assert(~unit? leadingCoefficient p);
		assert(one? degree p);
		p0 := coefficient(p, 0);
		assert(~zero? p0);
		pp0 := inj p0;
		a := pp0 / inj(leadingCoefficient p);
		pinj := map(inj)$MonogenicAlgebra2(R, RX, F, FX);
		pp := monom - a::FX;
		psl := pp^sl;
		for r in characteristicRoots(N, sl::Q, (q:RX):F +-> pinj(q)(a))
			repeat for e in finiteExpPart(L, pp0, psl, sl, mu, r)
				repeat yield e;
	}

	local finiteExpPart(L:RXD,p0:F,psl:FX,sl:Z,mu:Z,c:FR F):Generator Fx=={
		import from F;
		import from LODOExponentialSolutionsTools2(R,F,inj,RX,RXD,FX);
		TRACE("diffexp::finiteExpPart: L = ", L);
		TRACE("diffexp::finiteExpPart: p0 = ", p0);
		TRACE("diffexp::finiteExpPart: psl = ", psl);
		TRACE("diffexp::finiteExpPart: sl = ", sl);
		TRACE("diffexp::finiteExpPart: mu = ", mu);
		TRACE("diffexp::finiteExpPart: c = ", c);
		(a, b) := value c;
		-- TEMPO: OVERLOADING FNCS RETURN Generator = ALDOR1.0 BUG
		-- expRat(L, p0, psl, sl, mu, multiplicity c, a / b);
		expRat1(L, p0, psl, sl, mu, multiplicity c, a / b);
	}

}

