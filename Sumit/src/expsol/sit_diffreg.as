-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------- sit_diffreg.as --------------------------
--
-- Regular solutions of linear ordinary differential equations
--
-- Copyright (c) Manuel Bronstein 2001
-- Copyright (c) INRIA 2001, Version 0.1.13
-- Logiciel Sum^it (c) INRIA 2001, dans sa version 0.1.13
-----------------------------------------------------------------------------

#include "sumit"

macro {
	Z	== Integer;
	Q	== Fraction Z;
	FR	== FractionalRoot;
	Rx	== Fraction RX;
	Fx	== Fraction FX;
	RxD	== LinearOrdinaryDifferentialOperator Rx;
	FXD	== LinearOrdinaryDifferentialOperator FX;
	FxD	== LinearOrdinaryDifferentialOperator Fx;
	V	== Vector;
	PRX	== Product RX;
	PFX	== Product FX;
	RAD	== Cross(Q, RX);
	SING	== PlaneSingularity(R, RX, RXD);
	EQ	== LinearHolonomicDifferentialEquation(R, F, inj, RX, RXD);
	NEWTON	== NewtonPolygon(R, RX, RXD);
}

#if ALDOC
\thistype{LinearOrdinaryDifferentialOperatorRegularSolutions}
\History{Manuel Bronstein}{3/8/2001}{created}
\Usage{import from \this(R, F, $\iota$, RX, RXD, FX)}
\Params{
{\em R} & \alexttype{FactorizationRing}{} & The coefficient ring\\
        & \alexttype{CharacteristicZero}{} &\\
{\em F} & \alexttype{Field}{} & A field containing R\\
        & \alexttype{RationalRootRing}{} &\\
        & \alexttype{CharacteristicZero}{} &\\
$\iota$ & $R \to F$ & Injection\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
{\em RXD} & \altype{LinearOrdinaryDifferentialOperatorCategory} RX &
Differential operators\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
}
\Descr{\this(R, F, $\iota$, RX, RXD, FX) provides a regular solver
for linear ordinary differential operators with polynomial coefficients.
Regular solutions are solutions of the form $y = \prod_i {p_i}^{e_i}$
where the $p_i$ are polynomials and the $e_i$ are in the fraction field of R,
so they include in particular radical solutions, \ie{} solutions $y$ such
that $y^n$ is a rational function for some $n > 0$.}
\begin{exports}
\alexp{radicalSolutions}: & (EQ, Z) $\to$ RADSOL & Radical solutions\\
                          & (RXD, Z) $\to$ L RADSOL & \\
\alexp{regularSolutions}:
& EQ $\to$ L \albuiltin{Cross}(L \alexttype{Fraction}{} FX, P FX, V FX) &
Regular solutions\\
& RXD $\to$ L \albuiltin{Cross}(L \alexttype{Fraction}{} FX, P FX, V FX) &\\
\end{exports}
\begin{alwhere}
L &==& \alexttype{List}{}\\
V &==& \alexttype{Vector}{}\\
P &==& \alexttype{Product}{}\\
EQ &==& \altype{LinearHolonomicDifferentialEquation}(R, F, $\iota$, RX, RXD)\\
RADSOL &==& L \albuiltin{Cross}(L \albuiltin{Cross}(\alexttype{Fraction}{}
\alexttype{Integer}{}, RX), P RX, V FX)\\
\end{alwhere}
#endif

LinearOrdinaryDifferentialOperatorRegularSolutions(_
	R: Join(FactorizationRing, CharacteristicZero),
	F: Join(Field, RationalRootRing, CharacteristicZero), inj: R -> F,
	RX: UnivariatePolynomialCategory R,
	RXD:LinearOrdinaryDifferentialOperatorCategory RX,
	FX: UnivariatePolynomialCategory F): with {
		candidates: (EQ, Z, Boolean, List SING) -> Generator List RAD;
		radicalSolutions: (EQ,  Z) -> List Cross(List RAD, PRX, V FX);
		radicalSolutions: (RXD, Z) -> List Cross(List RAD, PRX, V FX);
		radicalSolutions: (EQ,  Z, RX) -> List Cross(List RAD,PRX,V FX);
		radicalSolutions: (RXD, Z, RX) -> List Cross(List RAD,PRX,V FX);
#if ALDOC
\alpage{radicalSolutions}
\Usage{\name(L, n)}
\Signatures{
\name: & (RXD, Z) $\to$ \alexttype{List}{} \albuiltin{Cross}(
\alexttype{List}{} \albuiltin{Cross}(\alexttype{Fraction}{} Z, RX), PRX, V FX)\\
\name: & (EQ, Z) $\to$ \alexttype{List}{} \albuiltin{Cross}(
\alexttype{List}{} \albuiltin{Cross}(\alexttype{Fraction}{} Z, RX), PRX, V FX)\\
}
\begin{alwhere}
Z &==& \alexttype{Integer}{}\\
V &==& \alexttype{Vector}{}\\
PRX &==& \alexttype{Product}{} RX\\
EQ &==& \altype{LinearHolonomicDifferentialEquation}(R, F, $\iota$, RX, RXD)\\
\end{alwhere}
\Params{
\emph{L} & EQ & A linear ordinary differential operator\\
         & RXD &\\
\emph{n} & \alexttype{Integer}{} & Degree of the radical solutions wanted\\
}
\Retval{Returns all the solutions $y$ of $Ly = 0$ such that $y^m$ is
a rational function for some positive integer $m$ dividing $n$.
The solutions are returned as a list of tuples
$([(r_1, p_1),\dots,(r_s,p_s)], d, [v_1,\dots,v_t])$ that correspond
to the subspace of radical solutions spanned by
$$
\frac{v_1}d \prod_i {p_i}^{r_i},\dots,\frac{v_t}d \prod_i {p_i}^{r_i}
$$
where the $v_j$ are linearly independent and the $p_i$ coprime.
The set of radical solutions of degree dividing $n$ is then
the union of those subspaces.
}
\Remarks{\name{} returns all the radical solutions when $n = 0$.}
#endif
		if F has FactorizationRing then {
			candidates: (EQ, List SING) -> List Fx;
			regularSolutions: EQ -> List Cross(Fx, PFX, V FX);
			regularSolutions: RXD -> List Cross(Fx, PFX, V FX);
			regularSolutions: (EQ,  RX) -> List Cross(Fx, PFX,V FX);
			regularSolutions: (RXD, RX) -> List Cross(Fx, PFX,V FX);
		}
#if ALDOC
\alpage{regularSolutions}
\Usage{\name~L}
\Signatures{
\name: & RXD $\to$ \alexttype{List}{} \albuiltin{Cross}(\alexttype{Fraction}{}
FX, \alexttype{Product}{} FX, \alexttype{Vector}{} FX)\\
\name: & EQ $\to$ \alexttype{List}{} \albuiltin{Cross}(\alexttype{Fraction}{}
FX, \alexttype{Product}{} FX, \alexttype{Vector}{} FX)\\
}
\begin{alwhere}
EQ &==& \altype{LinearHolonomicDifferentialEquation}(R, RX, RXD)\\
\end{alwhere}
\Params{
\emph{L} & EQ & A linear ordinary differential operator\\
         & RXD &\\
}
\Retval{Returns all the solutions $y$ of $Ly = 0$ of the form
$y = f e^{\int u}$ where $f$ is a rational function, and $u$ is
a rational function whose denominator is squarefree, and whose numerator
has stricly smaller degree than its denominator.
The solutions are returned as a list of tuples
$(u, d, [v_1,\dots,v_m])$ that correspond
to the subspace of regular solutions spanned by
$$
\frac{v_1}d e^{\int u},\dots,\frac{v_m}d e^{\int u}
$$
where the $v_j$ are linearly independent.
The set of all the regular solutions is then
the union of those subspaces.
}
#endif
		twist: (RXD, Fx) -> (FX, FXD);
		twist: (RXD, List RAD) -> (RX, RXD);
} == add {
	-- n = 0 means all radical solutions
	radicalSolutions(L:RXD, n:Z):List Cross(List RAD, PRX, V FX) == {
		import from EQ, RX;
		radicalSolutions(primitivePart(L)::EQ, n, 0);
	}

	radicalSolutions(L:RXD, n:Z, sing:RX):List Cross(List RAD,PRX,V FX) == {
		import from EQ;
		radicalSolutions(primitivePart(L)::EQ, n, sing);
	}

	radicalSolutions(L:EQ, n:Z):List Cross(List RAD, PRX, V FX) ==
		radicalSolutions(L, n, 0$RX);

	-- the operator of L should be primitive at this point
	-- n = 0 means all radical solutions
	-- returns a list of tuples ([(e1,p1),...,(es,ps)], d, [v1,...,vk])
	-- that describe the subspace  p1^e1...ps^es \sum_i ci vi/d
	-- only look for sols whose singularities are in sing if sing <> 0
	radicalSolutions(L:EQ, n:Z, sing:RX):List Cross(List RAD,PRX,V FX) == {
		import from RX, PRX, V FX, List RAD, List SING,
			LinearOrdinaryDifferentialOperatorRationalSolutions(R,_
							F, inj, RX, RXD, FX);
		assert(n >= 0);
		TRACE("diffreg::radicalSolutions: L = ", L);
		TRACE("diffreg::radicalSolutions: n = ", n);
		TIMESTART;
		one? n => {
			(den, num) := kernel(L, sing);
			empty? num => empty;
			cons((empty, den, num), empty);
		}
		noGoodExponent?(singularityAtInfinity! L, n) => empty;
		finsing := analyze!(L, sing);
		for s in finsing repeat { noGoodExponent?(s,n) => return empty;}
		TIME("diffreg::radicalSolutions: exponent check at ");
		l:List Cross(List RAD, PRX, V FX) := empty;
		for rad in candidates(L, n, zero? n, finsing) repeat {
			(alpha, LL) := twist(operator L, rad);
			TIME("diffreg::radicalSolutions: operator twisted at ");
			(den, num) := kernel(LL, sing);
			TIME("diffreg::radicalSolutions: rational kernel at ");
			TRACE("diffreg::radicalSolutions: den = ", den);
			TRACE("diffreg::radicalSolutions: num = ", num);
			if ~(empty? num) then l := cons((rad, den, num), l);
		}
		l;
	}

	-- for n > 0, a necessary condition for a sol y st y^n rational
	-- is that the denom of at least one exponent divides n
	-- otherwise a necessary condition for some radical solution
	-- is the existence of a rational exponent
	local noGoodExponent?(s:SING, n:Z):Boolean == {
		import from FR Z, List FR Z, Partial Z;
		for e in rationalExponents s repeat {
			(ignore, d) := value e;
			TRACE("diffreg::noGoodExponent?: d = ", d);
			~failed?(exactQuotient(n, d)) => return false;
		}
		true;
	}

	-- generates lists of the form [(e1,p1),...,(ek,pk)]
	-- corresponding to the solution candidate
	--  y = p1^e1 ... pk^ek f  where f is a rational function
	candidates(L:EQ, n:Z, all?:Boolean, l:List SING):_
		Generator List RAD == generate {
		import from Boolean, List RAD, SING, Set Q, NEWTON;
		assert(all? or n > 0);
		empty? l => yield empty;
		s := first l;
		assert(finite? polygon s);
		p := center s;
		hasInt? := numberOfIntegerExponents(s) > 0;
		for rad in candidates(L, n, all?, rest l) repeat {
			if hasInt? then yield rad;	-- no contrib from s
			for r in expSet(s,n,all?) repeat yield cons((r,p), rad);
		}
	}

	-- returns the rational exponents normalized modulo Z
	-- keep only those that are not integers
	-- and whose denominators divide n when n > 0
	local expSet(s:SING, n:Z, all?:Boolean):Set Q == {
		import from Boolean, Partial Z, Q, FR Z, List FR Z;
		TRACE("diffreg::expSet: n = ", n);
		TRACE("diffreg::expSet: all? = ", all?);
		assert(all? or n > 0);
		exps:Set Q := empty;
		TRACE("diffreg::expSet: exps = ", exps);
		for r in rationalExponents s | ~integral? r repeat {
			TRACE("diffreg::expSet: r = ", r);
			(num, den) := value r;
			assert(den > 0);
			if all? or ~failed?(exactQuotient(n, den)) then {
				a := num mod den;
				TRACE("diffreg::expSet: a = ", a);
				assert(a > 0);
				exps := union!(exps, a / den);
				TRACE("diffreg::expSet: exps = ", exps);
			}
		}
		exps;
	}

	if F has FactorizationRing then {
		local fident(f:F):F == f;

		regularSolutions(L:EQ):List Cross(Fx, PFX, V FX) ==
			regularSolutions(L, 0$RX);

		regularSolutions(L:RXD):List Cross(Fx, PFX, V FX) == {
			import from EQ, RX;
			regularSolutions(primitivePart(L)::EQ, 0);
		}

		regularSolutions(L:RXD, sing:RX):List Cross(Fx, PFX, V FX) == {
			import from EQ;
			regularSolutions(primitivePart(L)::EQ, sing);
		}

		-- the operator of L should be primitive at this point
		-- returns a list of tuples (u, d, [v1,...,vk])
		-- that describe the subspace  p1^e1...ps^es \sum_i ci vi/d
		-- only look for sols whose singularities are in sing if sing<>0
		regularSolutions(L:EQ, sing:RX):List Cross(Fx, PFX, V FX) == {
			import from FX, V FX, List Fx,
			 LinearOrdinaryDifferentialOperatorRationalSolutions(F,_
							F, fident, FX, FXD, FX);
			TRACE("diffreg::regularSolutions: L = ", L);
			TRACE("diffreg::regularSolutions: sing = ", sing);
			TIMESTART;
			l:List Cross(Fx, PFX, V FX) := empty;
			fsing := RX2FX sing;
			for u in candidates(L, analyze!(L, sing)) repeat {
				TRACE("diffreg::regSols:: u = ", u);
				(alpha,LL) := twist(operator L, u);
				TIME("diffreg::regSols: operator twisted at ");
				(den, num) := kernel(LL, fsing);
				TIME("diffreg::regSols: rational kernel at ");
				TRACE("diffreg::regularSolutions: den = ", den);
				TRACE("diffreg::regularSolutions: num = ", num);
				if ~empty?(num) then l := cons((u,den,num), l);
			}
			l;
		}

		candidates(L:EQ, l:List SING):List Fx == {
			import from Partial Fx;
			l:List Fx := empty;
			for u in candidates0(L, l) repeat {
				failed? u => return empty;
				l := cons(retract u, l);
			}
			l;
		}

		-- generates fractions u corresponding to the solution candidate
		--  y = f exp(int u)  where f is a rational function
		-- Only generate a failed when it can be proven
		-- that the equation cannot have a regular solution
		local candidates0(L:EQ, l:List SING):Generator Partial Fx ==
			generate {
			import from Z, SING, RX, Fx, Partial Fx, NEWTON;
			empty? l => yield [0];
			s := first l;
			assert(finite? polygon s);
			p := RX2FX center s;
			dp := RX2FX differentiate center s;
			hasInt? := numberOfIntegerExponents(s) > 0;
			cont?:Boolean := true;
			for u in candidates0(L, rest l) while cont? repeat {
				if failed? u then yield u;
				else {
					v := retract u;
					for r in regSet(s, p, dp) repeat {
						if zero? r then {
							cont? := false;
							yield failed;
						}
						yield [v + r];
					}
					if cont? and hasInt? then yield u;
				}
			}
		}

		-- generate fractions q/p with deg(q) < deg(p) such that
		-- exp(int q/p) is the local contribution to solution candidates
		-- dp is the derivative of p
		-- Only generate a 0 when it can be inferred from s
		-- that the equation cannot have a regular solution
		-- In that case, does not yield anything else than 0
		local regSet(s:SING, p:FX, dp:FX):Generator Fx == generate {
			import from Z, Q, Set Q, R, RX, Fx, List Fx, NEWTON;
			assert(finite? polygon s);
			ok? := numberOfRationalExponents(s) > 0;
			for r in expSet(s, 0, true) repeat {	-- rational exps
				num := numerator r;
				den := denominator r;
				assert(~zero? num); assert(~zero? den);
				yield((num * dp) / (den * p));
			}
			~allRationalExponents? s => {
				d := degree p;
				inv? := unit? leadingCoefficient center s;
				g := {
					one? d => {
						inv? => expSetRat(s, p, dp);
						expSetRatQ(s, p, dp);
					}
					expSetAlg(s, p);
				}
				for u in g repeat {
					ok? := true;
					if ~zero?(u) then yield u;
				}
			}
			~ok? => yield 0;
		}

		local expSetRat(s:SING, p:FX, dp:FX):Generator Fx == generate {
			import from Z, R, FR R, RX, Partial RX, Fx, NEWTON;
			import from NewtonPolygonEquations(R, RX, RXD, R, RX);
			h := center s;
			N := polygon s;
			assert(~failed? exactQuotient(center N, h));
			assert(unit? leadingCoefficient h);
			assert(one? degree h);
			(rat, l) := {
				zero?(h0 := coefficient(h, 0)) =>
					fractionalExponents(N,
						(q:RX):R +-> coefficient(q, 0));
				a := quotient(-h0, leadingCoefficient h);
				fractionalExponents(N, (q:RX):R +-> q a);
			}
			for r in l repeat {
				(num, den) := value r;
				yield((inj(num) * dp) / (inj(den) * p));
			}
		}

		local expSetRatQ(s:SING, p:FX, dp:FX):Generator Fx == generate {
			import from Z, R, F, FR F, RX, Partial RX, Fx, NEWTON;
			import from NewtonPolygonEquations(R, RX, RXD, F, FX);
			N := polygon s;
			assert(~failed? exactQuotient(center N, center s));
			assert(~unit? leadingCoefficient center s);
			assert(one? degree p);
			p0 := coefficient(p, 0);
			assert(~zero? p0);
			a := p0 / leadingCoefficient p;
			-- use fractionalExponents rather than integralExponents
			-- in order to eliminate all the rational ones
			-- can use integralValue however since F is a field
			(rat, l) :=
				fractionalExponents(N,(q:RX):F +-> RX2FX(q)(a));
			for r in l repeat yield((integralValue(r) * dp) / p);
		}

		-- p is known to be irreducible at this point
		-- note that this can generate 0 if there is a term
		-- of the form q(alpha)/x - alpha with null-trace
		local expSetAlg(s:SING, p:FX):Generator Fx == generate {
			macro E == SimpleAlgebraicExtension(F, FX, p);
			macro EX == DenseUnivariatePolynomial E;
			import from FR E, RX, EX, Fx, Partial RX, NEWTON;
			import from NewtonPolygonEquations(R, RX, RXD, E, EX);
			N := polygon s;
			assert(~failed? exactQuotient(center N, center s));
			ppinj := map(coerce)$MonogenicAlgebra2(F, FX, E, EX);
			pp := ppinj p;
			alpha:E := reduce(monom$FX);
			assert(zero? pp alpha);
			h := quotient(pp, monom$EX - alpha::EX); -- p / x-alpha
			-- use fractionalExponents rather than integralExponents
			-- in order to eliminate all the rational ones
			-- can use integralValue however since E is a field
			(rat, l) := fractionalExponents(N,
						(q:RX):E +-> reduce RX2FX q);
			trpol := trace(EX);
			for r in l repeat yield(trpol(integralValue(r)*h) / p);
		}
	}

	local RX2FX(p:RX):FX == {
		import from MonogenicAlgebra2(R, RX, F, FX);
		map(inj)(p);
	}

	-- returns (alpha, LL) st LL = alpha L(D + l'/l)
	twist(L:RXD, l:List RAD):(RX, RXD) == {
		import from LinearOrdinaryDifferentialOperatorTools(RX, RXD);
		compose(logDer l)(L);
	}

	twist(L:RXD, u:Fx):(FX, FXD) == {
		import from MonogenicAlgebra2(RX, RXD, FX, FXD);
		import from LinearOrdinaryDifferentialOperatorTools(FX, FXD);
		compose(numerator u, denominator u)(map(RX2FX)(L));
	}

	-- returns (a, b) s.t. d'/d = a/b
	local logDer(l:List RAD):(RX, RX) == {
		import from Q, R, RX;
		bstar:Z := 1;
		dstar:RX := 1;
		for rad in l repeat {
			(e, p) := rad;
			bstar := times!(bstar, denominator e);
			dstar := times!(dstar, p);
		}
		a:RX := 0;
		for rad in l repeat {
			(e, p) := rad;
			c := numerator(e) * quotient(bstar, denominator e);
			a := add!(a, c::R, quotient(dstar,p) * differentiate p);
		}
		(a, bstar * dstar);
	}
}

