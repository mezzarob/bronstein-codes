-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_radinv.as ------------------------------
-- Copyright (c) Manuel Bronstein 2003
-- Copyright (c) INRIA 2003, Version 1.0.2
-- Logiciel Sum^it (c) INRIA 2003, dans sa version 1.0.2
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I   == MachineInteger;
	Z   == Integer;
	Q   == Fraction Z;
	QR  == FractionalRoot Z;
	QmZ == SetsOfRationalsModuloZ;
	V   == Vector;
	M   == DenseMatrix;
	Fx  == Fraction FX;
	PRX == Product RX;
	RAD == Cross(Q, RX);
	SING== PlaneSingularity(R, RX, RXD);
	EQ  == LinearHolonomicDifferentialEquation(R, F, inj, RX, RXD);
	POLY== UnivariatePolynomialCategory;
	REGSOL == LinearOrdinaryDifferentialOperatorRegularSolutions;
	INVAR== LinearOrdinaryDifferentialOperatorInvariants(R,F,inj,RX,RXD,FX);
}

#if ALDOC
\thistype{LinearOrdinaryDifferentialOperatorRadicalInvariants}
\History{Manuel Bronstein}{28/7/2003}{created}
\Usage{import from \this(R, F, $\iota$, RX, RXD, FX)}
\Params{
{\em R} & \alexttype{FactorizationRing}{} & The coefficient ring\\
        & \alexttype{CharacteristicZero}{} &\\
{\em F} & \alexttype{Field}{} & A field containing R\\
        & \alexttype{RationalRootRing}{} & \\
        & \alexttype{CharacteristicZero}{} &\\
$\iota$ & $R \to F$ & Injection from R into F\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
\emph{RXD} & \altype{LinearOrdinaryDifferentialOperatorCategory} RX &
Differential operators\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
}
\Descr{\this(R, F, $\iota$, RX, RXD, FX) provides a solver for the
exponential solutions of symmetric powers of
linear ordinary differential operators with polynomial coefficients.}
\begin{exports}
\end{exports}
\begin{alwhere}
EQ &==& \altype{LinearHolonomicDifferentialEquation}(R, RX, RXD)\\
\end{alwhere}
#endif

LinearOrdinaryDifferentialOperatorRadicalInvariants(
	R:   Join(FactorizationRing, CharacteristicZero),
	F:   Join(Field, RationalRootRing, CharacteristicZero), inj: R -> F,
	RX:  UnivariatePolynomialCategory R,
	RXD: LinearOrdinaryDifferentialOperatorCategory RX,
	FX:  UnivariatePolynomialCategory F): with {
		candidates: (EQ, Z, Z, Boolean, SING) -> (Boolean, Set Q);
		exponential: % -> Fx;
		invariants: % -> INVAR;
		radical: % -> List RAD;
		radicalInvariants: (RXD, Z, Z) -> List %;
		radicalInvariants: (EQ,  Z, Z) -> List %;
		riccati: (FxY:POLY Fx) -> % -> V FxY;
} == add {
	Rep == Record(rad:List RAD, inv:INVAR);

	invariants(i:%):INVAR	== { import from Rep; rep(i).inv; }
	radical(i:%):List RAD	== { import from Rep; rep(i).rad; }
	exponential(i:%):Fx	== exponential radical i;
	local times(q:Q)(r:Q, p:RX):(Q, RX)	== (q * r, p);

	local RX2FX(p:RX):FX == {
		import from MonogenicAlgebra2(R, RX, F, FX);
		map(inj)(p);
	}

	riccati(FxY:POLY Fx):% -> V FxY == {
		f := riccati(FxY)$INVAR;
		(i:%):V FxY +-> {
			import from Z, Fx, FxY, List RAD, INVAR;
			v := f(iv := invariants i);
			empty?(s := radical i) => v;
			u := exponential nthRoot(s, degree(iv)::Z);
			[translate(p, u) for p in v];
		}
	}

	-- look for all radical semi-invariants if m = 0, m-th roots otherwise
	radicalInvariants(L:RXD, n:Z, m:Z):List % == {
		import from EQ;
		assert(n > 0);
		radicalInvariants(primitivePart(L)::EQ, n, m);
	}

	-- look for all radical semi-invariants if m = 0, m-th roots otherwise
	radicalInvariants(L:EQ, n:Z, m:Z):List % == {
		import from I, INVAR;
		assert(n > 0);
		TRACE("radinv::radicalInvariants:L = ", L);
		TRACE("radinv::radicalInvariants:n = ", n);
		TRACE("radinv::radicalInvariants:m = ", m);
		l:List % := empty;
		f := rad2invar(operator L, n);
		for rad in candidates(L, n, m, zero? m, finiteSingularities! L)_
		repeat {
			iv := f rad;
			if dimension(invariants iv) > 0 then l := cons(iv, l);
		}
		l;
	}

	local candidates(L:EQ, n:Z, m:Z, all?:Boolean, l:List SING):_
		Generator List RAD == {
		import from SING, RXD, REGSOL(R, F, inj, RX, RXD, FX);
		assert(all? or m > 0);
		empty? l => trivcand();
		allRationalExponents?(s := first l) =>
			candidates(L, n, m, all?, s, rest l);
		Ln := symmetricPower(operator L, n)::EQ;
		candidates(Ln, m, all?, analyze!(Ln, centers l));
	};

	local trivcand():Generator List RAD == generate {
		import from List RAD;
		yield empty;
	}

	-- l contains only finite singularities
	local centers(l:List SING):PRX == {
		import from Z, SING;
		d:PRX := 1;
		for s in l repeat d := times!(d, center s, 1);
		d;
	}

	local candidates(L:EQ, n:Z, m:Z, all?:Boolean, s:SING, l:List SING):_
		Generator List RAD == generate {
			import from Set Q, List RAD;
			assert(allRationalExponents? s);
			(hasInt?, lexp) := candidates(L, n, m, all?, s);
			p := center s;
			for rad in candidates(L, n, m, all?, l) repeat {
				if hasInt? then yield rad;
				for r in lexp repeat yield cons((r, p), rad);
			}
	}

	candidates(L:EQ, n:Z, m:Z, all?:Boolean, s:SING):(Boolean, Set Q) == {
		import from Partial Z, Q, QmZ;
		assert(allRationalExponents? s);
		lq := n * moduloZ(rationalExponents s);
		hasInt? := false;
		lxp:Set Q := empty;
		for r in lq repeat {
			a := numerator r;
			d := denominator r;
			if one? d then hasInt? := true;
			else if all? or ~failed?(exactQuotient(m, d)) then
					lxp := union!(lxp, (a mod d) / d);
		}
		(hasInt?, lxp);
	}

	-- TEMPO: DOES NOT REUSE THE SYMMETRIC POWER IF IT WAS COMPUTED
	local rad2invar(L:RXD, n:Z):(List RAD) -> % == {
		m := machine degree L;
		(u:List RAD):% +-> {
			import from Rep, F, FX;
			import from REGSOL(R, F, inj, RX, RXD, FX);
			TIMESTART;
			(alpha, LL) := twist(L, nthRoot(u, n));
			TIME("radinv::rad2invar: operator twisted at ");
			iv := invariants(LL, n);
			TIME("radinv::rad2invar: invariants at ");
			per [u, iv];
		}
	}

	local nthRoot(l:List RAD, n:Z):List RAD == {
		import from Q;
		f := times inv(n::Q);
		[f rad for rad in l];
	}

	local exponential(l:List RAD):Fx == {
		import from Q, RX, FX;
		s:Fx := 0;
		for rad in l repeat {
			(n, p) := rad;
			nn := numerator n;
			dn := denominator n;
			s := s + (nn * RX2FX differentiate p) / (dn * RX2FX p);
		}
		s;
	}
}
