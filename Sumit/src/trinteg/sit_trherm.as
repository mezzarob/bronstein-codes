-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
----------------------------- sit_trherm.as -----------------------------
--
-- Transcendental Hermite Reduction
--
-- Copyright (c) Manuel Bronstein 2004
-- Copyright (c) INRIA 2004, Version 1.0.3
-- Logiciel Sum^it (c) INRIA 2004, dans sa version 1.0.3
-----------------------------------------------------------------------------

#include "sumit"

macro {
	Z	== Integer;
	Rx	== Fraction RX;
	PRX	== Product RX;
	DER	== Derivation RX;
}

#if ALDOC
\thistype{TranscendentalHermiteReduction}
\History{Manuel Bronstein}{22/7/2004}{created}
\Usage{import from \this(R, RX)}
\Params{
\emph{R} & \alexttype{GcdDomain}{} & the coefficient ring\\
{\em RX}
& \alexttype{UnivariatePolynomialCategory}{} R & polynomials over {\em R}\\
}
\Descr{\this(R, RX) implements the Hermite reduction in simple
differential extensions.}
\begin{exports}
\alexp{hermite}:
& (Rx, \alexttype{Derivation}{} RX, RX) $\to$ (Rx, Rx) &
Hermite reduction\\
\end{exports}
\begin{alwhere}
Rx &==& \alexttype{Fraction}{} RX\\
\end{alwhere}
#endif

TranscendentalHermiteReduction(R:GcdDomain,
				RX:UnivariatePolynomialCategory R): with {
	hermite: (Rx, DER, RX) -> (Rx, Rx);
#if ALDOC
\alpage{hermite}
\Usage{\name(f, d, q)}
\Signature{(\alexttype{Fraction}{} RX, \alexttype{Derivation}{} RX, RX)}
{(\alexttype{Fraction}{} RX, \alexttype{Fraction}{} RX)}
\Params{
\emph{f} & \alexttype{Fraction}{} RX & a fraction\\
\emph{d} & \alexttype{Derivation}{} RX & a derivation\\
\emph{q} & RX & a nonzero polynomial\\
}
\Retval{Returns $(g, h)$ such that
$f = Dg + h$ where $D = q^{-1} d$ is a derivation on the field
of fractions of RX, and the denominator of $h$ is a product of
the form $s u v$ with $s \mid d s$, $u \mid q$ and $\gcd(v, d v) = 1$.}
#endif
} == add {
	hermite(f:Rx, d:DER, q:RX):(Rx, Rx) == {
		assert(~zero? q);
		(g, h) := hermite(q * f, d);
		(g, inv(q::Rx) * h);
	}

	local hermite(f:Rx, d:DER):(Rx, Rx) == {
		import from Z, RX, Partial Cross(R, RX, RX);
		import from SplittingFactorization(R, RX);
		import from UnivariatePolynomialDiophantineSolver(R, RX);
		g:Rx := 0;
		cont?:Boolean := ~zero? f;
		while cont? repeat {
			(ds, dn) := split(denominator f, d);
			assert(dn * ds = denominator f);
			(dminus, dstar, ignore) := gcdquo(dn, differentiate dn);
			assert(~zero? dminus);
			cont? := ~zero?(degree dminus);
			if cont? then {
				u := - quotient(dstar * d dminus, dminus);
				v := squareFreePart dminus;
				assert(unit? gcd(u, v));
				(r, b, c) := retract pseudoDiophantine(u, v,
								numerator f);
				d1 := quotient(dstar, v);
				rd := r * ds * dminus;
				g := add!(g, b / rd);
				f := (r * c - d1 * (r * d b - d(r::RX)
					- r * quotient(d ds, ds))) / (r*d1*rd);
			}
		}
		(g, f);
	}
}
