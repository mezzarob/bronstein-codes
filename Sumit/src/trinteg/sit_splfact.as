-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
----------------------------- sit_splfact.as -----------------------------
--
-- Splitting factorizations
--
-- Copyright (c) Manuel Bronstein 2004
-- Copyright (c) INRIA 2004, Version 1.0.3
-- Logiciel Sum^it (c) INRIA 2004, dans sa version 1.0.3
-----------------------------------------------------------------------------

#include "sumit"

macro {
	Z	== Integer;
	PRX	== Product RX;
	DER	== Derivation RX;
}

#if ALDOC
\thistype{SplittingFactorization}
\History{Manuel Bronstein}{22/7/2004}{created}
\Usage{import from \this(R, RX)}
\Params{
{\em R} & \alexttype{GcdDomain}{} & the ground ring\\
{\em RX}
& \alexttype{UnivariatePolynomialCategory}{} R & polynomials over {\em R}\\
}
\Descr{\this(R, RX) provides splitting factorization with respect
to arbitrary derivations.}
\begin{exports}
\alexp{split}: & (RX, $\partial$ RX) $\to$ (RX, RX) & splitting factorization\\
\alexp{splitSquareFree}:
& (RX, $\partial$ RX, RX) $\to$ (R, $\Pi$ RX, $\Pi$ RX) &
split-squarefree factorization\\
\end{exports}
\begin{alwhere}
$\partial$ &==& \alexttype{Derivation}{}\\
$\Pi$ &==& \alexttype{Product}{} RX\\
\end{alwhere}
#endif

SplittingFactorization(R:GcdDomain, RX:UnivariatePolynomialCategory R): with {
	split: (RX, DER) -> (RX, RX);
#if ALDOC
\alpage{split}
\Usage{\name(p, d)}
\Signature{(RX, \alexttype{Derivation}{} RX)}{(RX, RX)}
\Params{
{\em p} & RX & a polynomial\\
\emph{d} & \alexttype{Derivation}{} RX & a derivation\\
}
\Retval{Returns $(p_s,p_n)$ such that $p = p_s p_n$,
$p_s$ is the special part of $p$
with respect to $d$ and $p_n$ the normal part of $p$ with respect to $d$.}
#endif
	splitSquareFree: (RX, DER) -> (R, PRX, PRX);
#if ALDOC
\alpage{splitSquareFree}
\Usage{\name(p, d)}
\Signature{(RX, \alexttype{Derivation}{} RX)}
{(R, \alexttype{Product}{} RX, \alexttype{Product}{} RX)}
\Params{
{\em p} & RX & a polynomial\\
\emph{d} & \alexttype{Derivation}{} RX & a derivation\\
}
\Retval{Returns $(c,\prod_i p_i^i,\prod_j q_j^j)$
such that $p = c \prod_i p_i^i \prod_j q_j^j$,
the $p_i$ are all special with respect to $d$ and
the $q_j$ are all normal with respect to $d$.}
#endif
} == add {
	split(p:RX, d:DER):(RX, RX) == {
		import from Z;
		zero? p => (0, 0);
		pbar := quotient(gcd(p, d p), gcd(p, differentiate p));
		zero? degree pbar => (1, p);
		(ps, pn) := split(quotient(p, pbar), d);
		(pbar * ps, pn);
	}

	splitSquareFree(p:RX, d:DER):(R, PRX, PRX) == {
		import from Z;
		special:PRX := 1;
		normal:PRX := 1;
		(c, pr) := squareFree p;
		for trm in pr repeat {
			(q, n) := trm;
			(g, h, ignore) := gcdquo(q, d q);	-- h = q/g
			if degree(g) > 0 then special := times!(special, g, n);
			if degree(h) > 0 then normal := times!(normal, h, n);
		}
		(c, special, normal);
	}
}
