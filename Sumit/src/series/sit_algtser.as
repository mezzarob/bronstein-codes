-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_algtser.as ------------------------------
-- Copyright (c) Manuel Bronstein 2002
-- Copyright (c) INRIA 2002, Version 1.0
-- Logiciel Sum^it (c) INRIA 2002, dans sa version 1.0
-----------------------------------------------------------------------------

#include "sumit"

macro {
	Z == Integer;
	Q == Fraction Z;
	FR == FractionalRoot;
	NP == NewtonPolygon(R, RX, RXY);
	POINT == Cross(Z, Z, RX);       -- ( j, nu_j, a_j x^(-nu_j) )
	SLOPE == Cross(Q, List POINT);  -- ( s, [P1, P2, ..., Pm] )
}

#if ALDOC
\thistype{AlgebraicTaylorSeriesSolutions}
\History{Manuel Bronstein}{10/4/2002}{created}
\Usage{import from \this(R, Rx, RX, RXY)}
\Params{
{\em R} & \alexttype{FactorizationRing}{} & The coefficient domain\\
{\em Rx}
& \alexttype{UnivariateTaylorSeriesCategory}{} R & Series over \emph{R}\\
{\em RX}
& \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over \emph{R}\\
{\em RXY}
& \alexttype{UnivariatePolynomialCategory}{} RX& Polynomials over \emph{RX}\\
}
\Descr{\this(R, Rx, RX, RXY) provides a taylor series solver
for computing roots in $R[[x]]$ of polynomials in $R[x,y]$.}
\begin{exports}
\alexp{roots}: & RXY $\to$ \alexttype{List}{} Rx & Roots of a polynomial\\
\end{exports}
#endif

AlgebraicTaylorSeriesSolutions(R:Join(Field, FactorizationRing),
		Rx:UnivariateTaylorSeriesCategory R,
		RX:UnivariatePolynomialCategory R,
		RXY:UnivariatePolynomialCategory RX): with {
			roots: RXY -> List Rx;
#if ALDOC
\alpage{roots}
\Usage{\name~p}
\Signature{RXY}{\alexttype{List}{} Rx}
\Params{
\emph{p} & RXY & a nonzero polynomial\\
}
\Retval{Returns all the Taylor series $[s_1(x),\dots,s_m(x)] in $R[[x]]$
such that $p(x,s_i(x)) = 0$.}
\Remarks{This function does not return the Laurent or Puiseux series
solutions (\ie{} series with negative or nonintegral exponents),
nor the Taylor series with coefficients in extensions of \emph{R}
(\ie{} its fraction field or algebraic extensions).}
#endif
} == add {
	local identity(r:R):R	== r;
	local val0(q:RX):R	== coefficient(q, 0);

	roots(p:RXY):List Rx == {
		import from Z;
		assert(~zero? p);
		zero?(d := degree p) => empty;
		one? d => roots1 p;
		roots0 p;
	}

	local roots1(p:RXY):List Rx == {
		import from Z, R, RX, Rx, Partial Rx;
		import from UnivariateTaylorSeriesCategory2Poly(R, Rx, RX);
		assert(~zero? p); assert(one? degree p);
		zero?(b := coefficient(p, 0)) => [0];
		a := - leadingCoefficient p;
		failed?(u := expandFraction(0)(a, b)) => empty;
		[retract u];
	}

	local roots0(p:RXY):List Rx == {
		import from Z, Q, NP, RX, SLOPE, List SLOPE, FR R;
		import from NewtonPolygonEquations(R, RX, RXY, R, RX);
		assert(~zero? p); assert(1 < degree p);
		l:List Rx := empty;
		np := newtonPolygon(p, monom);
		for s in slopes np repeat {
			(sl, pts) := s;
			if sl <= 0 and one?(denominator sl) then {
				e := numerator sl;
				l := append!(l,
					[roots(p, c, e) for c in _
					characteristicRoots(np, s, val0)]);
			}
		}
		l;
	}

	-- MUST MOVE THE "SERIES" FUNCTIONS FROM NPEQS TO HERE REALLY!

	-- returns the roots of the form s(x) = c x^{-e} + ...
	local roots(p:RXY, r:FR R, e:Z):List Rx == {
		import from R;
		import from NewtonPolygonEquations(R, RX, RXY, R, RX);
		assert(~zero? p); assert(integral? r); assert(e >= 0);
		c := integralValue r;
		assert(~zero? c);
		one?(m := multiplicity r) => {
			-- TEMPO: MUST DETERMINE mu
			[series(p, 0, e, mu, c, identity)];
		}
		assert(m > 1);
		-- TO BE CONTINUED
	}
}
