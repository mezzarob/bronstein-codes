-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------sit_condcat.as------------------------------
-- Copyright (c) Marco Codutti 1995
-- Copyright (c) INRIA 1999, Version 0.1.12
-- Logiciel Sum^it (c) INRIA 1999, dans sa version 0.1.12
-- Copyright (c) Swiss Federal Polytechnic Institute Zurich, 1995
-----------------------------------------------------------------------------

#include "sumit"

#if ALDOC
\thistype{Condition}
\History {Marco Codutti}{14 June 95}{created}
\Usage   {\this: Category}
\Descr   {\this~is a category for conditions on parameters.}
\begin{exports}
\category{\alexttype{BooleanArithmeticType}{}}\\
\category{\alexttype{CopyableType}{}}\\
\category{\alexttype{ExpressionType}{}}\\
\alexp{Always}: & $\to$ \%      & a condition which is always satisfied \\
\alexp{always?}:
& \% $\to$ \alexttype{Boolean}{} & test is a condition is \alexp{Always}\\
\alexp{Never}:  & $\to$ \%      & a condition which is never satisfied \\
\alexp{never?}:
& \% $\to$ \alexttype{Boolean}{} & test is a condition is \alexp{Never}\\
\end{exports}
#endif

define Condition: Category ==
	Join(BooleanArithmeticType, CopyableType, ExpressionType) with {
     Always: %;
     Never: %;
#if ALDOC
\alpage{Always,Never}
\altarget{Always}
\altarget{Never}
\Usage     {Always\\Never}
\alconstant{\%}
\Retval    {Always returns a condition which is always satisfied,
		while Never returns a condition which is never satisfied.}
\Remarks   {The names {\tt always} and {\tt never} are reserved tokens
            in \aldor.}
#endif
     always?:%     -> Boolean;
     never?:%     -> Boolean;
#if ALDOC
\alpage{always?,never?}
\altarget{always?}
\altarget{never?}
\Usage     {always?~c\\never?~c}
\Signature{\%}{\alexttype{Boolean}{}}
\Params    {{\em c} & \% & a condition}
\Descr     {always?~c and never?~c test respectively whether $c$ is
		\alexp{Always} or \alexp{Never}.}
#endif
}

