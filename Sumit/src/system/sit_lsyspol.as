-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_lsyspol.as ------------------------------
-- Copyright (c) Manuel Bronstein 2000
-- Copyright (c) INRIA 2000, Version 0.1.12
-- Logiciel Sum^it (c) INRIA 2000, dans sa version 0.1.12
-----------------------------------------------------------------------------

#include "sumit"

macro {
	B == Boolean;
	I == MachineInteger;
	Z == Integer;
	V == Vector;
	A == Array;
	M == DenseMatrix;
	PFX == Partial FX;
	SYS == LinearOrdinaryFirstOrderSystem;
	RXE == LinearOrdinaryRecurrence(R, RX);
	LIN2REC == LinearOrdinaryOperatorToRecurrence(R, RX, RXY, RXE);
}

#if ALDOC
\thistype{LinearOrdinarySystemPolynomialSolutions}
\History{Manuel Bronstein}{12/10/2000}{created}
\Usage{import from \this(R, F, $\iota$, RX, RXY, FX)}
\Params{
{\em R} & \alexttype{IntegralDomain}{} & An integral domain\\
{\em F} & \alexttype{Field}{} & A field containing R\\
$\iota$ & $R \to F$ & Injection from R into F\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
{\em RXY}
& \altype{StandardUnivariateSkewPolynomialCategory} RX & Operators over RX\\
{\em FX} & \alexttype{MonogenicAlgebra}{} F & Polynomials over F\\
         & \alexttype{CommutativeRing}{} &\\
}
\Descr{\this(R, F, $\iota$, RX, FX) provides a polynomial solver
for first order linear ordinary differential or recurrence systems with
polynomial coefficients.}
\begin{exports}
\alexp{kernel}:
& (SYS RX, \alexttype{Integer}{}) $\to$ \alexttype{DenseMatrix}{} FX &
Polynomial solutions\\
\end{exports}
\begin{exports}[if \emph{R} has \alexttype{RationalRootRing}{} then]
\alexp{degreeBound}: & SYS RX $\to$ \alexttype{Integer}{} & Degree bound\\
\alexp{kernel}:
& SYS RX $\to$ \alexttype{DenseMatrix}{} FX & Polynomial solutions\\
\end{exports}
\begin{alwhere}
SYS &==& \altype{LinearOrdinaryFirstOrderSystem}\\
\end{alwhere}
#endif

LinearOrdinarySystemPolynomialSolutions(R:IntegralDomain,
		F:Field, inj: R -> F,
		RX: UnivariatePolynomialCategory R,
		RXY:StandardUnivariateSkewPolynomialCategory RX,
		FX: Join(IntegralDomain, MonogenicAlgebra F)):with {
			if R has RationalRootRing then {
				bound: SYS RX -> (A M RX, Z, I, Z); -- for mod
				bound: M RXY  -> (A M RX, Z, I, Z); -- for mod
				degreeBound: SYS RX -> Z;
				degreeBound: M RXY -> Z;
#if ALDOC
\alpage{degreeBound}
\Usage{\name~L}
\Signature{SYS RX}{\alexttype{Integer}{}}
\begin{alwhere}
SYS &==& \altype{LinearOrdinaryFirstOrderSystem}\\
\end{alwhere}
\Params{
{\em L} & SYS RX &
A linear ordinary first order differential or difference system\\
}
\Retval{Returns an upper bound for the degree of any polynomial solution
of $LY = 0$, $-1$ if there are no nonzero polynomial solution.}
#endif
				kernel: SYS RX -> M FX;
				kernel: M RXY -> M FX;
			}
			kernel: (SYS RX, Z) -> M FX;
			kernel: (M RXY, Z) -> M FX;
			kernel: (A M RX, Z, I, Z) -> M FX;-- for modular algos
#if ALDOC
\alpage{kernel}
\Usage{\name~L\\ \name(L, n)}
\Signatures{
\name: & SYS RX $\to$ \alexttype{DenseMatrix}{} FX\\
\name: & (SYS RX, \alexttype{Integer}{}) $\to$ \alexttype{DenseMatrix}{} FX\\
}
\begin{alwhere}
SYS &==& \altype{LinearOrdinaryFirstOrderSystem}\\
\end{alwhere}
\Params{
{\em L} & SYS RX &
A linear ordinary first order differential or difference system\\
{\em n} & \alexttype{Integer}{} & A degree bound (optional)\\
}
\Descr{\name(L) returns a matrix whose columns for a
basis for all the polynomial solutions of $Ly = 0$, while
\name(L,n) returns a matrix whose columns form a
basis for the polynomial solutions of $LY = 0$
of degree at most $n$.}
\Remarks{\name(L) is available only if R has \alexttype{RationalRootRing}{}.
The type of system (differential or difference) is determined from
the parameter RXY to the domain.
Produces an error if RXY is neither a differential or recurrence
operator type. The polynomials in the kernel are expressed in terms of
the basis $P_n$ described in
\alfunc{LinearOrdinaryOperatorToRecurrence}{recurrence}.}
#endif
			dimension: M RXY -> Z;
} == add {
	kernel(L:SYS RX, bound:Z):M FX == {
		import from B, I;
		(hom, part) := solve!(L, bound, zero(0, 0), true);
		assert(zero? numberOfColumns part);
		hom;
	}

	kernel(L:M RXY, bound:Z):M FX == {
		import from B, I;
		(hom, part) := solve!(L, bound, zero(0, 0), true);
		assert(zero? numberOfColumns part);
		hom;
	}

	-- return the maximal dimension of the solution space
	dimension(L:M RXY):Z == {
		import from I, RXY;
		d:Z := 0;
		(r, c) := dimensions L;
		for j in 1..c repeat {
			mx:Z := 0;
			for i in 1..r | ~zero? L(i,j) repeat {
				dg := degree L(i, j);
				if dg > mx then mx := dg;
			}
			d := d + mx;
		}
		d;
	}

	-- L is not the operator to solve, but its recurrence is L E^e
	-- dim = maximum dimension of the solution space
	-- bound = degree bound on solution
	-- this is used over finite fields by modular algorithms
	kernel(L:A M RX, e:Z, dim:I, bound:Z):M FX == {
		import from I;
		(hom, part) := solve(L, e, dim, bound, zero(0, 0), true);
		hom;
	}

	-- homsols? is true if a basis of the kernel is also wanted
	-- always returns (zero(*,0), partsol) if homsols? is false
	local solve!(L:SYS RX, bound:Z, g:M FX, homsols?:B):(M FX, M FX) == {
		import from I, LIN2REC;
		n := dimension L;
		assert(n > 0); assert(n = numberOfRows g);
		bound < 0 => (zero(n, 0), zero(n, numberOfColumns g));
		(r, e) := recurrence L;
		solve(r, e, n, bound, g, homsols?);
	}

	-- fills a with either a.i/b.i or all 0 if one quotient is not exact
	local exactQuotient!(a:V FX, b:V FX):Boolean == {
		import from I, FX, Partial FX;
		n := #a;
		assert(n = #b);
		for i in 1..n repeat {
			failed?(u := exactQuotient(a.i, b.i)$FX) => {
				zero! a;
				return false;
			}
			a.i := retract u;
		}
		true;
	}

	-- homsols? is true if a basis of the kernel is also wanted
	-- always returns (zero(*,0), partsol) if homsols? is false
	local solve!(L:M RXY, bound:Z, g:M FX, homsols?:B):(M FX, M FX) == {
		import from I, LIN2REC, V RX;
		import from UnivariatePolynomialAlgebraTools(RX, RXY);
		n := numberOfRows L;
		assert(n > 0); assert(n = numberOfRows g);
		nrhs := numberOfColumns g;
		bound < 0 => (zero(n, 0), zero(n, nrhs));
		(c, L) := primitive L;
		pinj := map(inj)$MonogenicAlgebra2(R, RX, F, FX);
		cc:V FX := [pinj p for p in c];		-- content of LHS
		-- there can be no particular solution for a given g(*,j)
		-- if c.i does not divide g(i,j) for each i,
		-- so replace those columns by 0
		for j in 1..nrhs repeat {
			gj := column(g, j);
			exactQuotient!(gj, cc);
			for i in 1..n repeat g(i,j) := gj.i;
		}
		(r, e) := recurrence L;
		solve(r, e, machine dimension L, bound, g, homsols?);
	}

	local forever(g:Generator F):Generator F == generate {
		import from F;
		for x in g repeat yield x;
		repeat yield 0;
	}

	local coefficients(g:V FX):Generator V F == generate {
		import from I, FX, Generator F;
		a:PrimitiveArray Generator F := new(n := #g);
		n1 := prev n;
		for i in 0..n1 repeat a.i := forever coefficients g(next i);
		v:V F := zero n;
		repeat {
			for i in 0..n1 repeat v(next i) := next!(a.i);
			yield v;
		}
	}

	-- simultaneous solving of several right-hand-sides with common bound
	-- the real recurrence is L E^e
	-- bound = degree bound on solution
	-- homsols? is true if a basis of the kernel is also wanted
	-- always returns (empty, partsol) if homsols? is false
	-- if col(g,i) <> 0 then col(partsol,i) = 0 means no particular solution
	-- if col(g,i) == 0 then col(partsol,i) is automatically 0
	-- TEMPORARY: ONLY HOMOGENEOUS SYSTEMS FOR NOW
	local solve(L:A M RX,e:Z,dim:I,bound:Z,g:M FX,hsols?:B):(M FX,M FX) == {
		import from I, M RX, FX;
                -- If e > 0, then any vector of polynomials of degrees at
                -- most e-1 is also a homogeneous solution
                -- Since there are n linearly independent homogeneous solutions,
                -- this implies that n*e <= dim and that the homogeneous
		-- sols are all the constant vectors plus eventual other sols
		hsols? and e > 0 => {
			-- TEMPORARY (solve doesn't do inhomogeneous for now)
			-- (ignore,partsol) := solve1(r, e, bound, g, false);
			partsol:M FX := zero(n := numberOfRows(L.0), 0);
			nee := n * (ee := machine e);
			assert(nee <= dim);
			homsols:M FX := zero(n, 0);
			if nee < dim then {
				-- TEMPORARY (only homogeneous for now)
				(homsols, ignore) :=
					solve1(L, 0, bound - e, g, hsols?);
			}
			dimsol := numberOfColumns homsols;
			homsol:M FX := zero(n, nee + dimsol);
			for j in 0..prev ee repeat {
				xj:FX := monomial(j::Z);
				for i in 1..n repeat homsol(i, i+j*n) := xj;
			}
			for j in 1..dimsol repeat {
				jj := j + nee;
				for i in 1..n repeat
					homsol(i, jj) := shift!(homsols(i,j),e);
			}
			(homsol, partsol);
		}
		solve1(L, e, bound, g, hsols?);
	}

	-- works for systems of arbitrary order with nonsingular recurrences
	local solve1(L:A M RX, e:Z, bound:Z, g:M FX, homsols?:B):(M FX,M FX) =={
		import from I, RX, FX, F, V F, V V F, M RX;
		import from LinearAlgebra(F, M F);
		import from UnivariatePolynomialCRTLinearAlgebra(R, RX, M RX);
		import from LinearOrdinaryRecurrenceSystemTools(R, F, inj, RX);
		-- TRACE("lsyspol::solve: recurrence = ", L);
		TRACE("lsyspol::solve: e = ", e);
		TRACE("lsyspol::solve: bound = ", bound);
		TIMESTART;
		nrhs := numberOfColumns g;
		n := numberOfRows(L.0);
		TRACE("lsyspol::solve: n = ", n);
		bound < 0 => (zero(n, 0), zero(n, nrhs));
		assert(e <= 0 or ~homsols?);
		inhom? := ~zero? g;
		assert(~inhom?);	-- TEMPORARY (HOMOGENEOUS)
		-- exit if g = 0 and the homogeneous sols were not requested
		~(inhom? or homsols?) => (zero(n, 0), zero(n, nrhs));
		m := prev(#L);	-- actual order of the recurrence
		TRACE("lsyspol::solve: m = ", m);
		maxsing := machine degreeBound L(prev m);
		TRACE("lsyspol::solve: maxsing = ", maxsing);
		sing:PrimitiveArray I := new next maxsing;
		constraints:V M F := zero maxsing;
		local kh:Stream V V F;
		kh := [kernel(L, e, sing, constraints)];
		TIME("lsyspol::solve: solution stream set at ");
		N := machine bound;
		-- forces all terms to be computed up to N+m
		w := kh(N+m);
		-- TRACE("lsyspol::solve: kh = ", kh);
		TIME("lsyspol::solve: solution stream computed at ");
		nsing := sing.0;	-- number of singularities crossed
		TRACE("lsyspol::solve: nsing = ", nsing);
		assert(0 <= nsing); assert(nsing <= maxsing);
		nconstr:I := 0;		-- count number of linear constraints
		for i in 1..nsing repeat
			nconstr := nconstr + numberOfRows(constraints.i);
		TRACE("lsyspol::solve: nconstr = ", nconstr);
		sys:M F := zero(n * m + nconstr, #w);
		for i in 1..m repeat {	-- beyond degree bound ==> 0 or -rh
			ni := n * prev i;
			v := kh(N+i);
			assert(#v <= #w);
			for j in 1..#v repeat {
				vj := v.j;
				for k in 1..n repeat sys(k + ni, j) := vj.k;
			}
		}
		index := next(n * m);
		for i in 1..nsing repeat {	-- singular constraints
			constr := constraints.i;
			(rconstr, cconstr) := dimensions constr;
			assert(cconstr <= #w);
			for k in 1..rconstr repeat {
				for j in 1..cconstr repeat
					sys(index, j) := constr(k, j);
				index := next index;
			}
		}
		TIME("lsyspol::solve: linear system set at ");
		-- TRACE("lsyspol::solve: sys = ", sys);
		sol:M F := zero(0, 0);
		psol?:PrimitiveArray B := new nrhs;
		psol1? := false;
		assert(homsols?);
		sol := kernel sys;
		TIME("lsyspol::solve: linear system solved at ");
		-- TRACE("lsyspol::solve: sol = ", sol);
		polker:M FX := zero(n, d := numberOfColumns sol);
		partsol:M FX := zero(n, nrhs);
		-- compute the solutions by descending order of degree
		buffer:V F := zero n;
		if d > 0 or psol1? then for i in N..0 by -1 repeat {
			ii := i::Z;
			v := kh.i;
			for j in 1..d repeat
				add!(polker, j, dot!(buffer, sol, j, v), ii);
		}
		TIME("lsyspol::solve: polynomial solutions at ");
		-- TRACE("lsyspol::solve: polker = ", polker);
		(polker, partsol);
	}

	-- add v x^n to j-th column of m
	local add!(m:M FX, j:I, v:V F, n:Z):() == {
		import from FX;
		r := numberOfRows m;
		assert(r = #v);
		for i in 1..r repeat m(i, j) := add!(m(i, j), v.i, n);
	}

	-- dot product of j-th column of m with v, which can be shorter
	local dot!(w:V F, m:M F, j:I, v:V V F):V F == {
		nv := #v;
		n := numberOfRows m;
		assert(n >= nv);
		zero! w;
		for i in 1..nv repeat w := add!(w, m(i, j), v.i);
		w;
	}

	if R has RationalRootRing then {
		degreeBound(L:SYS RX):Z		== { (r,e,n,b) := bound L; b; }
		degreeBound(L:M RXY):Z		== { (r,e,n,b) := bound L; b; }
		bound(L:SYS RX):(A M RX, Z,I,Z)	== bound(L, zero(0,0)$M(FX));
		bound(L:M RXY):(A M RX, Z, I,Z)	== bound(L, zero(0,0)$M(FX));

		kernel(L:SYS RX):M FX == {
			import from I;
			(hom, part) := solve!(L, zero(0, 0), true);
			assert(zero? numberOfColumns part);
			hom;
		}

		kernel(L:M RXY):M FX == {
			import from I;
			(hom, part) := solve!(L, zero(0, 0), true);
			assert(zero? numberOfColumns part);
			hom;
		}

		local indeq(r:A M RX):RX == {
			import from I, M RX, LinearAlgebra(RX, M RX),
			       LinearOrdinaryRecurrenceEGElimination(R,RX,M RX);
			mat := r.0;
			assert(square? mat);
			(rank?, rk) := rankLowerBound mat;
			rk < numberOfRows mat => trailingIndicialEquation r;
			determinant mat;
		}

		-- TEMPORARY: ONLY HOMOGENEOUS SYSTEMS FOR NOW
		-- returns an upper bound on the power of x in the denominator
		-- can be negative in theory
		local bound(L:M RXY, g:M FX):(A M RX, Z, I, Z) == {
			import from LIN2REC;
			(r, e) := recurrence L;
			(r, e, machine dimension L, bound(r, e, g));
		}

		local bound(L:SYS RX, g:M FX):(A M RX, Z, I, Z) == {
			import from LIN2REC;
			(r, e) := recurrence L;
			(r, e, dimension L, bound(r, e, g));
		}

		local bound(r:A M RX, e:Z, g:M FX):Z == {
			import from I, Partial Z, RX;
			eq := indeq r;
			assert(~zero?(eq));
			u := maxIntegerRoot eq;
			failed? u or (m := retract u) < 0 => e;
			e + m;
		}

		-- homsols? is true if a basis of the kernel is also wanted
		-- always returns (zero(*,0), partsol) if homsols? is false
		local solve!(L:SYS RX, g:M FX, homsols?:B):(M FX, M FX) == {
			(r, e, dim, bd) := bound(L, g);
			solve(r, e, dim, bd, g, homsols?);
		}

		local solve!(L:M RXY, g:M FX, homsols?:B):(M FX, M FX) == {
			(r, e, dim, bd) := bound(L, g);
			solve(r, e, dim, bd, g, homsols?);
		}
	}
}
