-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_rceigen.as ------------------------------
-- Copyright (c) Manuel Bronstein 2002
-- Copyright (c) INRIA 2002, Version 1.0.1
-- Logiciel Sum^it (c) INRIA 2002, dans sa version 1.0.1
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I   == MachineInteger;
	Z   == Integer;
	V   == Vector;
	M   == DenseMatrix;
	PRX == List Cross(RX, Z);
	SYS == LinearOrdinaryFirstOrderSystem;
	FFX == UnivariateFactorialPolynomial(F, FX);
	SYSRAT == LinearOrdinaryRecurrenceSystemRationalSolutions;
}

#if ALDOC
\thistype{LinearOrdinaryRecurrenceEigenring}
\History{Manuel Bronstein}{25/7/2002}{created}
\Usage{import from \this(R, F, $\iota$, RX, RXE, FX, FXE)}
\Params{
{\em R} & \alexttype{GcdDomain}{} & The coefficient ring\\
        & \alexttype{RationalRootRing}{} &\\
        & \alexttype{CharacteristicZero}{} &\\
{\em F} & \alexttype{Field}{} & A field containing R\\
$\iota$ & $R \to F$ & Injection from R into F\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
{\em RXE} & \altype{LinearOrdinaryRecurrenceCategory} RX &
Recurrence operators over RX\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
{\em FXE} & \altype{LinearOrdinaryRecurrenceCategory} FX &
Recurrence operators over FX\\
}
\Descr{\this(R, F, $\iota$, RX, RXE, FX, FXE) provides eigenring
computations for linear ordinary recurrence operators
with polynomial coefficients.}
\begin{exports}
\alexp{eigenring}:
& RXE $\to$ (RX, \alexttype{Vector}{} \alexttype{DenseMatrix}{} UFP(F, FX),
\alexttype{Vector}{} FXE) & Eigenring\\
\end{exports}
\begin{alwhere}
UFP &==& \alexttype{UnivariateFactorialPolynomial}{}\\
\end{alwhere}
#endif

LinearOrdinaryRecurrenceEigenring(
	R:Join(GcdDomain, RationalRootRing, CharacteristicZero),
	F:Field, inj: R -> F,
	RX:  UnivariatePolynomialCategory R,
	RXE: LinearOrdinaryRecurrenceCategory RX,
	FX:  UnivariatePolynomialCategory F,
	FXE: LinearOrdinaryRecurrenceCategory FX): with {
		eigenring: RXE -> (PRX, V RX, V FXE);
		matrixEigenring: RXE -> (RX, V M FFX, V FXE);
#if ALDOC
\alpage{eigenring}
\Usage{\name~L}
\Signature{RXE}{(RX, \alexttype{Vector}{} \alexttype{DenseMatrix}{} UFP(F, FX),
\alexttype{Vector}{} FXE)}
\begin{alwhere}
UFP &==& \alexttype{UnivariateFactorialPolynomial}{}\\
\end{alwhere}
\Params{ {\em L} & RXE & A linear recurrence operator\\ }
\Retval{Returns $(d, [M_1,\dots,M_s], [L_1,\dots,L_s])$
such that the $L_i/d$ form
a basis for the eigenring of \emph{L}, \ie~the set of operators \emph{R}
such that $L R = S L$ for some operator \emph{S}.
The $M_i/d$ form a corresponding basis of the eigenring of the companion
system associated to \emph{L}.}
\alseealso{\alfunc{LinearOrdinaryRecurrenceSystemRationalSolutions}{eigenring}}
#endif
} == add {
	local sigma(n:Z):FX -> FX == {
		import from F;
		m := (-n)::F;
		(p:FX):FX +-> translate(p, m);
	}

	local sigma(n:Z):FFX -> FFX == {
		f:FX -> FX := sigma n;
		(p:FFX):FFX +-> f(expand p)::FFX;
	}

	local translate(a:R)(pn:Cross(RX, Z)):Cross(RX, Z) == {
		import from RX;
		(p, n) := pn;
		(translate(p, a), n);
	}

	-- returns (d, [d1,...,dn], num) s.t. the eigenring is
	-- 1/d diag(1/d1,...,1/dn) num
	eigenring(L:RXE):(PRX, V RX, V FXE) == {
		import from Z, R, RX, PRX, FXE;
		import from SYSRAT(R, F, inj, RX, RXE, FX);
		import from UnivariatePolynomialAlgebraTools(RX, RXE);
		import from LinearOrdinarySystemPolynomialSolutions(R, F,_
							 inj, RX, RXE, FFX);
		import from LinearOrdinaryRecurrenceRationalSolutions(R, F,_
							 inj, RX, RXE, FX);
		TIMESTART;
		zero? L => (empty, empty, empty);
		e := trailingDegree L;
		zero?(order := degree(L) - e) => (empty, empty, empty);
		one? order => (empty, [1], [1]);
		e > 0 => {
			(den, d, eigen) := eigenring shift(L, -e);
			ee := (-e)::R;
			(translate!(den, ee), [translate(p, ee) for p in d],
						map!(map!(sigma e)) eigen);
		}
		(dvec, sys) := sigmaPrimitive eigenringSystem L;
		TIME("rceigen::eigenring: eigenring system s-primitive at ");
		-- eigenringDenominator is much tighter than generic solver
		den := eigenringDenominator L;
		TIME("rceigen::eigenring: eigenring denominator at ");
		empty? den => (empty, dvec, MFFX2VFXE kernel sys);
		(ignore, sys) := transform(sys, twist den);
		TIME("rceigen::eigenring: change of variable at ");
		(den, dvec, MFFX2VFXE kernel sys);
	}

	local MFFX2VFXE(m:M FFX):V FXE == {
		import from FFX, FXE, V FX, V FFX;
		[[expand p for p in c]::FXE for c in columns m];
	}

	matrixEigenring(L:RXE):(RX, V M FFX, V FXE) == {
		import from I, Z, R, SYS RX, FXE, M FFX;
		import from SYSRAT(R, F, inj, RX, RXE, FX);
		zero? L => (1, empty, empty);
		e := trailingDegree L;
		zero?(order := degree(L) - e) => (1, empty, empty);
		one? order => (1, [one 1], [1]);
		e > 0 => {
			(den, emat, escal) := matrixEigenring shift(L, -e);
			(translate(den, (-e)::R), map!(map!(sigma e)) emat,
						map!(map!(sigma e)) escal);
		}
		(den, num) := eigenring companionSystem L;
		(den, num, [operator m for m in num]);
	}

	local operator(m:M FFX):FXE == {
		import from I, Z, FFX;
		n := numberOfColumns m;
		assert(n > 0);
		n1 := prev n;
		L := term(expand m(1, n), n1::Z);
		for i in n1..1 by -1 repeat
			L := add!(L, expand m(1, i), prev(i)::Z);
		L;
	}
}

