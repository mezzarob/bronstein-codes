-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_recsys.as ------------------------------
-- Copyright (c) Manuel Bronstein 2000
-- Copyright (c) INRIA 2000, Version 0.1.12
-- Logiciel Sum^it (c) INRIA 2000, dans sa version 0.1.12
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I   == MachineInteger;
	Z   == Integer;
	V   == Vector;
	M   == DenseMatrix;
	PRX == List Cross(RX, Z);
	Rx  == Fraction RX;
	SYS == LinearOrdinaryFirstOrderSystem;
	FFX == UnivariateFactorialPolynomial(F, FX);
	POLSOL == LinearOrdinarySystemPolynomialSolutions;
	RECSOL == LinearOrdinaryRecurrenceRationalSolutions;
}

#if ALDOC
\thistype{LinearOrdinaryRecurrenceSystemRationalSolutions}
\History{Manuel Bronstein}{26/9/2000}{created}
\Usage{import from \this(R, F, $\iota$, RX, RXE, FX)}
\Params{
{\em R} & \alexttype{GcdDomain}{} & The coefficient ring\\
        & \alexttype{RationalRootRing}{} &\\
        & \alexttype{CharacteristicZero}{} &\\
{\em F} & \alexttype{Field}{} & A field containing R\\
$\iota$ & $R \to F$ & Injection from R into F\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
\emph{RXE} & \altype{LinearOrdinaryRecurrenceCategory} RX & Operators\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
}
\Descr{\this(R, F, $\iota$, RX, FX) provides a rational solver
for first order systems of linear ordinary recurrence operators
with polynomial coefficients.}
\begin{exports}
\alexp{eigenring}:
& SYS RX $\to$ (RX, \alexttype{Vector}{} \alexttype{DenseMatrix}{} UPF(F, FX)) &
Eigenring\\
\alexp{kernel}: & SYS RX $\to$ (RX, \alexttype{DenseMatrix}{} UFP(F, FX)) &
Rational solutions\\
\end{exports}
\begin{alwhere}
SYS &==& \altype{LinearOrdinaryFirstOrderSystem}\\
UFP &==& \alexttype{UnivariateFactorialPolynomial}{}\\
\end{alwhere}
#endif

LinearOrdinaryRecurrenceSystemRationalSolutions(
	R:Join(GcdDomain, RationalRootRing, CharacteristicZero),
	F:Field, inj: R -> F,
	RX:  UnivariatePolynomialCategory R,
	RXE: LinearOrdinaryRecurrenceCategory RX,
	FX:  UnivariatePolynomialCategory F): with {
		eigenring: SYS RX -> (RX, V M FFX);
		eigenringDenominator: RXE -> PRX;
		eigenringDenominator: SYS RX -> PRX;
#if ALDOC
\alpage{eigenring}
\Usage{\name~L}
\Signature{SYS RX}{(RX, \alexttype{Vector}{} \alexttype{DenseMatrix}{} FFX)}
\begin{alwhere}
FFX &==& \alexttype{UnivariateFactorialPolynomial}{}(F,FX)\\
SYS &==& \altype{LinearOrdinaryFirstOrderSystem}\\
\end{alwhere}
\Params{
{\em L} & \altype{LinearOrdinaryFirstOrderSystem} RX &
A first order recurrence system\\
}
\Retval{Returns $(d, [M_1,\dots,M_s])$ such that the $M_i/d$ form
a basis for the eigenring of \emph{L}, \ie~the set of matrices \emph{B}
satisfying $A B = B(n+1) A$ where \emph{L} is the system $Y(n+1) = A Y$.}
\alseealso{\alfunc{LinearOrdinaryRecurrenceEigenring}{eigenring}}
#endif
		kernel: SYS RX -> (RX, M FFX);
		kernel: M RXE -> (PRX, V RX, M FFX);
#if ALDOC
\alpage{kernel}
\Usage{\name~L}
\Signature{SYS RX} {(RX,
\alexttype{DenseMatrix}{} \alexttype{UnivariateFactorialPolynomial}{}(F, FX))}
\begin{alwhere}
SYS &==& \altype{LinearOrdinaryFirstOrderSystem}\\
\end{alwhere}
\Params{
{\em L} & \altype{LinearOrdinaryFirstOrderSystem} RX &
A first order recurrence system\\
}
\Retval{Returns $(d, M)$ such that the columns of $M/d$ form
a basis for all the rational solutions of $LY = 0$.}
#endif
	sigmaPrimitive: M RXE -> (V RX, M RXE);
} == add {
	local delta(p:Rx):Rx == 0;
	local sigma(p:RX, n:Z):RX == { import from R; translate(p, (-n)::R) }

	eigenringDenominator(L:RXE):PRX ==
		eigenringDenominator companionSystem L;

	eigenringDenominator(L:SYS RX):PRX == {
		import from I;
		zero?(n := dimension L) => empty;
		denom eigenTensorRecurrence L;
	}

	eigenring(L:SYS RX):(RX, V M FFX) == {
		import from I, Z, M FFX, V M FFX;
		zero?(n := dimension L) => (1, empty);
		(den, num) := kernel eigenTensorRecurrence L;
		assert(numberOfRows(num) = n * n);
		-- each column of num corresponds to a matrix stored by columns
		(den, [matrix(num, i, n) for i in 1..numberOfColumns num]);
	}

	-- returns the system corresponding to the eigenring of L
	-- TEMPORARY: THIS IS REALLY NOT THE EFFICIENT WAY TO BUILD IT
	local eigenTensorRecurrence(L:SYS RX):SYS RX == {
		import from I, RX, V RX, M RX, Rx, LinearAlgebra(RX, M RX);
		import from MatrixCategoryOverFraction(RX, M RX, Rx, M Rx);
		import from String;
		(v, m) := matrix L;
		TRACE("recsys::eigenTensorRecurrence: v = ", v);
		TRACE("recsys::eigenTensorRecurrence: m = ", m);
		n := numberOfRows m;
		TRACE("recsys::eigenTensorRecurrence: n = ", n);
		assert(square? m); assert(n = #v);
		(m1, d) := inverse m;
		for i in 1..n repeat {	-- TEMPORARY
			zero?(d.i)=> error "recsys::eigenring: singular matrix";
		}
		mat:M Rx := zero(n, n);
		tmat1:M Rx := zero(n, n);	-- tmat1=transpose(inverse mat)
		for j in 1..n repeat {
			cj := v.j / d.j;
			for i in 1..n repeat {
				mat(i, j) := m(i, j) / v.i;
				tmat1(j, i) := m1(i, j) * cj;
			}
		}
		assert(one?(mat * transpose(tmat1)));
		bigmat := tensor(tmat1, mat);
		TRACE("recsys::eigenTensorRecurrence: bigmat = ", bigmat);
		system makeRowIntegral bigmat;
	}

	local matrix(num:M FFX, c:I, n:I):M FFX == {
		k:I := 1;
		m := zero(n, n);
		for j in 1..n repeat for i in 1..n repeat {
			m(i, j) := num(k, c);
			k := next k;
		}
		m;
	}

	local sigma(p:Rx, n:Z):Rx == {
		import from RX;
		zero? n => p;
		m := (-n)::R;
		translate(numerator p, m) / translate(denominator p, m);
	}

	sigmaPrimitive(L:M RXE):(V RX, M RXE) == {
		import from I, RX, RXE;
		LL := copy L;
		(r, c) := dimensions L;
		v:V RX := new(r, 1);
		zero? c => (v, LL);
		s:Automorphism RX := shift$RXE;
		for i in 1..r repeat {
			ct := sigmaContent L(i, 1);
			for j in 2..c while ~unit?(ct) repeat
				ct := gcd(ct, sigmaContent L(i, j));
			v.i := ct;
			if ~zero?(ct) and ~unit?(ct) then for j in 1..c repeat {
				q:RXE := 0;
				for trm in L(i,j) repeat {
					(a, n) := trm;
					q := add!(q, quotient(a, s(ct, n)), n);
				}
				LL(i, j) := q;
			}
		}
		(v, LL);
	}

	-- Only handle systems with nonsingular leading and trailing matrices
	-- for now (this is the case for eigenring systems)
	-- returns (d, [d1,...,dn], num) s.t. the solutions are
	-- 1/d diag(1/d1,...,1/dn) num
	kernel(L:M RXE):(PRX, V RX, M FFX) == {
		import from RECSOL(R, F, inj, RX, RXE, FX);
		import from POLSOL(R, F, inj, RX, RXE, FFX);
		import from UnivariatePolynomialAlgebraTools(RX, RXE);
		TIMESTART;
		L := primitivePart L;
		TIME("recsys::kernel: L primitive at ");
		(den, L) := sigmaPrimitive L;
		TIME("recsys::kernel: L sigma-primitive at ");
		empty?(d := denom L) => (empty, den, kernel L);
		TIME("recsys::kernel: denominator at ");
		(ignore, LL) := transform(L, twist d);
		TIME("recsys::kernel: change of variable done at ");
		(d, den, kernel LL);
	}

	kernel(L:SYS RX):(RX, M FFX) == {
		import from I, RX, V RX, M RX, List RX, FFX, Automorphism RX;
		import from MonogenicAlgebra2(R, RX, F, FX);
		TIMESTART;
		(nonSing?, v, a, LL) := makeNonSingular(L, morphism sigma);
		TIME("recsys::kernel: non-singular system at ");
		nonSing? => kernelNS L;
		(den, num) := kernelNS LL;
		TIME("recsys::kernel: kernel of non-singular system at ");
		dv := lcm [vi for vi in v];
		dd := den * dv;
		(r, c) := dimensions num;
		zero? c => (dd, zero(numberOfRows a, 0));
		b:M FX := zero(r, c);
		for i in 1..r repeat for j in 1..c repeat
			b(i,j) := expand num(i, j);
		TIME("recsys::kernel: solutions expanded at ");
		(r, c) := dimensions a;
		aa:M FX := zero(r, c);
		for i in 1..r repeat for j in 1..c repeat
			aa(i, j) := map(inj)(a(i, j));
		nnum := aa * b;
		(r, c) := dimensions nnum;
		mat:M FFX := zero(r, c);
		for i in 1..r repeat for j in 1..c repeat
			mat(i, j) := nnum(i, j)::FFX;
		TIME("recsys::kernel: solutions reconverted to FFX at ");
		(dd, mat);
	}

	-- This is for system with nonsingular matrices
	local kernelNS(L:SYS RX):(RX, M FFX) == {
		import from PRX, Automorphism Rx, Rx;
		import from RECSOL(R, F, inj, RX, RXE, FX);
		import from POLSOL(R, F, inj, RX, RXE, FFX);
		TIMESTART;
		empty?(d := denom L) => (1, kernel L);
		TIME("recsys::kernelNS: denominator at ");
		dd := factorialExpansion d;
		TIME("recsys::kernelNS: denominator expanded at ");
		TRACE("recsys::kernelNS: dd = ", dd);
		LL := changeVariable(L, inv(dd::Rx), morphism sigma, delta);
		TIME("recsys::kernelNS: change of variable done at ");
		(dd, kernel LL);
	}

	local detOrLcm(m:M RX):RX == {
		import from I, List RX, LinearAlgebra(RX, M RX);
		assert(square? m);
		diagonal? m => lcm [m(i,i) for i in 1..numberOfRows m];
		determinant m;
	}

	-- return [(g1,e1),...,(gk,ek)] where
	-- (g,e) means g \sigma^{-1}(g) ... \sigma^{-e}(g)
	local denom(L:M RXE):PRX == {
		import from String;
		import from RECSOL(R, F, inj, RX, RXE, FX);
		import from Z, R, RX, UnivariatePolynomialAlgebraTools(RX, RXE);
		(n0, n) := degrees L;
		zero?(a0 := detOrLcm coefficient(L, n0)) =>
			error "recsys::denom: singular trailing matrix";
		rn0 := n0::R;
		zero?(order := n0 - n) => cons((translate(a0, rn0), 0), empty);
		assert(order < 0);
		zero?(an := detOrLcm coefficient(L, n)) =>
			error "recsys::denom: singular leading matrix";
		ans := universalBound(translate(an, (-order)::R), a0);
		zero? n0 or empty? ans => ans;
		translate!(ans, rn0);
	}

	-- return [(g1,e1),...,(gk,ek)] where
	-- (g,e) means g \sigma^{-1}(g) ... \sigma^{-e}(g)
	local denom(L:SYS RX):PRX == {
		import from Z, R, V RX, LinearAlgebra(RX, M RX);
		TIMESTART;
		(a, m) := matrix L;
		d := determinant m;
		TIME("recsys::denom: determinant at ");
		assert(~zero? d);
		a0:RX := 1;
		a1 := a0;
		for p in a repeat {
			(g, doverg, ignore) := gcdquo(d, p);
			a0 := lcm(a0, doverg);
			a1 := lcm(a1, p);
		}
		TIME("recsys::denom: a0 and a1 at ");
		universalBound(translate(a1, 1), a0);
	}
}

#if ALDORTEST
-------------------------   test for recsys.as   -------------------------
#include "sumit"
#include "aldortest"

macro {
	I == MachineInteger;
	Z == Integer;
	Q == Fraction Z;
	ZX == DenseUnivariatePolynomial Z;
	QX == DenseUnivariatePolynomial Q;
	QFX==UnivariateFactorialPolynomial(Q, QX);
	ZXE == LinearOrdinaryRecurrence(Z, ZX, -"E");
	Zx == Fraction ZX;
	Qx == Fraction QX;
	M == DenseMatrix;
}

-- non-simple example from Abramov & Barkatou, ISSAC'98
local nonsimple():Boolean == {
	import from I, Z, Q, ZX, Zx, QX, Qx, QFX, M QFX;
	import from LinearOrdinaryFirstOrderSystem ZX;
	import from MonogenicAlgebraOverFraction(Z, ZX, Q, QX);
	import from Symbol;
	import from LinearOrdinaryRecurrenceSystemRationalSolutions(Z, Q,_
							coerce, ZX, ZXE, QX);
	m:M Zx := zero(4, 4);
	x := monom$ZX :: Zx;
	f := 5@Z :: Zx;
	d := x + f;
	m(1, 1) := (x-1)/d;
	m(1, 2) := (x*x*x + 7*x*x + 4*x)/d;
	m(1, 3) := - x - 1;
	m(1, 4) := (1-x)*(x*x + 5*x + f)/d;
	m(2, 1) := (x-1)/(x*(x+1)*d);
	m(2, 2) := x * m(2, 1);
	m(2, 4) := m(2, 2);
	m(3, 1) := m(1, 1);
	m(3, 2) := x * m(3, 1);
	m(3, 3) := -x;
	m(3, 4) := - (x*x*x + 3*x*x - 5*x - f)/d;
	m(4, 1) := -m(1,1)/x;
	m(4, 2) := -m(1,1);
	m(4, 3) := 1;
	m(4, 4) := (x+4@Z::Zx)*m(1,1);
	L := system m;
	(den, num) := kernel L;
	numberOfColumns num ~= 2 => false;
	dden := makeRational den;
	sol:M Zx := zero(4, numberOfColumns num);
	ssol := copy sol;
	for i in 1@I..4 repeat for j in 1..numberOfColumns num repeat {
		p := expand(num(i, j)) / dden;
		(na, np) := makeIntegral numerator p;	-- p = (np/na)/(dp/da)
		(da, dp) := makeIntegral denominator p;	--   = (da np)/(na dp)
		sol(i, j) := (da * np) / (na * dp);
		ssol(i, j) := (da * translate(np,-1)) / (na * translate(dp,-1));
	}
	zero?(ssol - m * sol);
}

-- simple example with 2-dimensional kernel coming from eigenring computation
local simple():Boolean == {
	import from MachineInteger, Z, Q, ZX, Zx, QX, Qx, QFX, M QFX;
	import from LinearOrdinaryFirstOrderSystem ZX;
	import from MonogenicAlgebraOverFraction(Z, ZX, Q, QX);
	import from Symbol;
	import from LinearOrdinaryRecurrenceSystemRationalSolutions(Z, Q,_
							coerce, ZX, ZXE, QX);
	m:M Zx := zero(4, 4);
	x := monom$ZX :: Zx;
        q := 4*x*x + 2*x - 1;
        d := 2*x*(2*x-1)*(2*x+1)*(2*x+1);
        m(1, 1) := 4*x*x*q / d;
        m(1, 2) := 8*x*x*x / d;
        m(1, 3) := -q / d;
        m(1, 4) := -2*x / d;
        m(2, 1) := 4*x*x / d;
        m(2, 2) := 16*x*x*x*x / d;
        m(2, 3) := -1 / d;
        m(2, 4) := -m(2,1);
        m(3, 1) := -2*x*q / d;
        m(3, 2) := m(2,4);
        m(3, 3) := q*q / d;
        m(3, 4) := -m(3,1);
        m(4, 1) := m(1,4);
        m(4, 2) := -m(1,2);
        m(4, 3) := q / d;
        m(4, 4) := m(1,1);
	L := system m;
	(den, num) := kernel L;
	numberOfColumns num ~= 2 => false;
	dden := makeRational den;
	sol:M Zx := zero(4, numberOfColumns num);
	ssol := copy sol;
	for i in 1@I..4 repeat for j in 1..numberOfColumns num repeat {
		p := expand(num(i, j)) / dden;
		(na, np) := makeIntegral numerator p;	-- p = (np/na)/(dp/da)
		(da, dp) := makeIntegral denominator p;	--   = (da np)/(na dp)
		sol(i, j) := (da * np) / (na * dp);
		ssol(i, j) := (da * translate(np,-1)) / (na * translate(dp,-1));
	}
	zero?(ssol - m * sol);
}

stdout << "Testing sit__recsys..." << endnl;
aldorTest("simple 4th order system", simple);
aldorTest("non-simple 4th order system", nonsimple);
stdout << endnl;

#endif
