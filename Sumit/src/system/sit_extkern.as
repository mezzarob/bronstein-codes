-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_extkern.as ------------------------------
-- Copyright (c) Manuel Bronstein 2003
-- Copyright (c) INRIA 2003, Version 1.0.2
-- Logiciel Sum^it (c) INRIA 2003, dans sa version 1.0.2
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I   == MachineInteger;
	Z   == Integer;
	Q   == Fraction Z;
	V   == Vector;
	M   == DenseMatrix;
	FXD == LinearOrdinaryDifferentialOperator FX;
	Fx  == Fraction FX;
	SYS == LinearOrdinaryFirstOrderSystem FX;
	EQ  == LinearHolonomicDifferentialEquation(R, F, inj, RX, RXD);
}

#if ALDOC
\thistype{LinearOrdinaryDifferentialOperatorExteriorKernel}
\History{Manuel Bronstein}{30/7/2003}{created}
\Usage{import from \this(R, F, $\iota$, RX, RXD, FX)}
\Params{
{\em R} & \alexttype{FactorizationRing}{} & The coefficient ring\\
        & \alexttype{CharacteristicZero}{} &\\
{\em F} & \alexttype{Field}{} & A field containing R\\
        & \alexttype{FactorizationRing}{} & \\
        & \alexttype{CharacteristicZero}{} &\\
$\iota$ & $R \to F$ & Injection from R into F\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
\emph{RXD} & \altype{LinearOrdinaryDifferentialOperatorCategory} RX &
Differential operators\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
}
\Descr{\this(R, F, $\iota$, RX, RXD, FX) provides a solver for the
exponential solutions of exterior powers of
linear ordinary differential operators with polynomial coefficients.}
\begin{exports}
\end{exports}
\begin{alwhere}
EQ &==& \altype{LinearHolonomicDifferentialEquation}(R, RX, RXD)\\
\end{alwhere}
#endif

LinearOrdinaryDifferentialOperatorExteriorKernel(
	R:   Join(FactorizationRing, CharacteristicZero),
	F:   Join(Field, FactorizationRing, CharacteristicZero), inj: R -> F,
	RX:  UnivariatePolynomialCategory R,
	RXD: LinearOrdinaryDifferentialOperatorCategory RX,
	FX:  UnivariatePolynomialCategory F): with {
		dimension: % -> I;
		denominator: % -> FX;
		exponential: % -> Fx;
		exteriorKernel: (RXD, Z) -> List %;
		exteriorKernel: (EQ,  Z) -> List %;
		numerator: % -> M FX;
} == add {
	Rep == Record(exp:Fx, den:FX, num:M FX);

	local fident(f:F):F	== f;
	exponential(i:%):Fx	== { import from Rep; rep(i).exp; }
	denominator(i:%):FX	== { import from Rep; rep(i).den; }
	numerator(i:%):M FX	== { import from Rep; rep(i).num; }
	dimension(i:%):I == { import from M FX; numberOfColumns numerator i; }

	exteriorKernel(L:RXD, n:Z):List % == {
		import from EQ;
		assert(n > 0);
		exteriorKernel(primitivePart(L)::EQ, n);
	}

	exteriorKernel(L:EQ, n:Z):List % == {
		import from I, RXD, List Cross(Fx, Product FX, V FX);
		import from LinearOrdinaryFirstOrderSystem2(RX, FX),
			LinearOrdinaryDifferentialOperatorExponentialSolutions(_
							R, F, inj, RX, RXD, FX);
		assert(n > 0);
		TIMESTART;
		op := operator L;
		Ln := exteriorPower(op, n);
		TIME("exteriorKernel: scalar exterior power at ");
		expsols := exponentialSolutions(Ln, leadingCoefficient op);
		TIME("exteriorKernel: exponential solutions at ");
		l:List % := empty;
		empty? expsols => l;
		pinj := map(inj)$MonogenicAlgebra2(R, RX, F, FX);
		S := map(pinj)(exteriorPowerSystem(operator L, n));
		f := exp2kern S;
		for sol in expsols repeat {
			xkern := f sol;
			if dimension(xkern) > 0 then l := cons(xkern, l);
		}
		l;
	}

	local exp2kern(L:SYS)(u:Fx, dn:Product FX, nm:V FX):% == {
		import from Rep, Automorphism Fx,
			LinearOrdinaryDifferentialSystemRationalSolutions(F,_
						 F, fident, FX, FXD, FX);
		TIMESTART;
		TRACE("extkern::exp2kern:u = ", u);
		-- TEMPORARY: 1.0.0 COMPILER BUG IF OVERLOADED!
		-- LL := changeVariable(L, 1, u, 1, differentiate);
		LL := changeVariableHyper(L, 1, u, 1, differentiate);
		TIME("exp2kern: change of variable at ");
		(den, num) := kernel LL;
		TIME("exp2kern: rational kernel at ");
		per [u, den, num];
	}
}
