-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_diffsys.as ------------------------------
-- Copyright (c) Manuel Bronstein 2000
-- Copyright (c) INRIA 2000, Version 0.1.12
-- Logiciel Sum^it (c) INRIA 2000, dans sa version 0.1.12
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I   == MachineInteger;
	Z   == Integer;
	V   == Vector;
	M   == DenseMatrix;
	PRX == Product RX;
	Rx  == Fraction RX;
	FXD == LinearOrdinaryDifferentialOperator FX;
	SYS == LinearOrdinaryFirstOrderSystem;
	POLSOL== LinearOrdinaryDifferentialSystemPolynomialSolutions;
	TOOLS == LinearOrdinaryDifferentialSystemSolutionTools;
}

#if ALDOC
\thistype{LinearOrdinaryDifferentialSystemRationalSolutions}
\History{Manuel Bronstein}{19/6/2002}{created}
\Usage{import from \this(R, F, $\iota$, RX, RXD, FX)}
\Params{
{\em R} & \alexttype{FactorizationRing}{} & The coefficient ring\\
        & \alexttype{CharacteristicZero}{} & \\
{\em F} & \alexttype{Field}{} & A field containing R\\
        & \alexttype{RationalRootRing}{} & \\
        & \alexttype{CharacteristicZero}{} & \\
$\iota$ & $R \to F$ & Injection from R into F\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
\emph{RXD}
& \altype{LinearOrdinaryDifferentialOperatorCategory} RX & Operators\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
}
\Descr{\this(R, F, $\iota$, RX, FX) provides a rational solver
for first order systems of linear ordinary differential operators
with polynomial coefficients.}
\begin{exports}
\alexp{denominatorBound}:
& (SYS RX, RX) $\to$ \alexttype{Integer}{} &
Order bound for solutions\\
\alexp{kernel}: & SYS RX $\to$ (RX, M FX) & Rational solutions\\
\end{exports}
\begin{alwhere}
M &==& \alexttype{DenseMatrix}{}\\
SYS &==& \altype{LinearOrdinaryFirstOrderSystem}\\
\end{alwhere}
#endif

LinearOrdinaryDifferentialSystemRationalSolutions(
	R:   Join(FactorizationRing, CharacteristicZero),
	F:   Join(Field, RationalRootRing, CharacteristicZero), inj: R -> F,
	RX:  UnivariatePolynomialCategory R,
	RXD: LinearOrdinaryDifferentialOperatorCategory RX,
	FX:  UnivariatePolynomialCategory F): with {
		denominatorBound: (SYS RX, RX) -> Z;
		denominatorBound: (M RXD, RX) -> Z;
#if ALDOC
\alpage{denominatorBound}
\Usage{\name(L, p)}
\Signature{(SYS RX, RX)}{\alexttype{Integer}{}}
\begin{alwhere}
SYS &==& \altype{LinearOrdinaryFirstOrderSystem}\\
\end{alwhere}
\Params{
{\em L} & \altype{LinearOrdinaryFirstOrderSystem} RX &
A first order differential system\\
\emph{p} & RX & An irreducible polynomial\\
}
\Retval{Returns $n \ge 0$ such that $p^n$ is the largest power of $p$
that can appear in the denominator of a rational solution of \emph{L}.}
#endif
		kernel: SYS RX -> (RX, M FX);
		kernel: M RXD -> (PRX, M FX);
#if ALDOC
\alpage{kernel}
\Usage{\name~L}
\Signature{SYS RX}{(RX, \alexttype{DenseMatrix}{} FX)}
\begin{alwhere}
SYS &==& \altype{LinearOrdinaryFirstOrderSystem}\\
\end{alwhere}
\Params{
{\em L} & \altype{LinearOrdinaryFirstOrderSystem} RX &
A first order differential system\\
}
\Retval{Returns $(d, M)$ such that the columns of $M/d$ form
a basis for all the rational solutions of $LY = 0$.}
#endif
} == add {
	kernel(L:M RXD):(PRX, M FX) == {
		import from POLSOL(R, F, inj, RX, RXD, FX);
		import from UnivariatePolynomialAlgebraTools(RX, RXD);
		import from LinearOrdinaryDifferentialOperatorTools(RX, RXD);
		TIMESTART;
		L := primitivePart L;
		TIME("diffsys::kernel: L primitive at ");
		d := denom L;
		TIME("diffsys::kernel: denominator at ");
		TRACE("diffsys::kernel: d = ", d);
		one? d => (1, kernel L);
		(ignore, LL) := transform(L, twist d);
		TIME("diffsys::kernel: change of variable done at ");
		(d, kernel LL);
	}

	kernel(L:SYS RX):(RX, M FX) == {
		import from RX, PRX, Rx, Automorphism Rx;
		import from POLSOL(R, F, inj, RX, RXD, FX);
		TIMESTART;
		d := denom L;
		TIME("diffsys::kernel: denominator at ");
		dd := expand d;
		TIME("diffsys::kernel: denominator expanded at ");
		TRACE("diffsys::kernel: dd = ", dd);
		one? dd => (1, kernel L);
		LL := changeVariable(L, inv(dd::Rx), 1, differentiate);
		TIME("diffsys::kernel: change of variable done at ");
		(dd, kernel LL);
	}

	-- returns the product of the irreducible factors of p that are not in l
	local elim(p:RX, l:List RX):RX == {
		import from Partial RX;
		p := squareFreePart p;
		for q in l repeat {
			u := exactQuotient(p, q);
			if ~failed? u then p := retract u;
		}
		p;
	}

	-- returns all the distinct irreducible factors of v.1,...,v.n,
	local factors(v:V RX):List RX == {
		import from RX, PRX;
		l:List RX := empty;
		for p in v repeat {
			(c, prod) := factor elim(p, l);
			for trm in prod repeat {
				(q, n) := trm;
				l := cons(q, l);
			}
		}
		l;
	}

	local denom(L:M RXD):PRX == {
		import from Z, RX, LinearAlgebra(RX, M RX);
		import from UnivariatePolynomialAlgebraTools(RX, RXD);
		d:PRX := 1;
		(c, prod) := factor determinant coefficient(L, degree L);
		for trm in prod repeat {
			(p, e) := trm;
			b := denominatorBound(L, p);
			if b > 0 then d := times!(d, p, b);
		}
		d;
	}

	local denom(L:SYS RX):PRX == {
		import from Z, List RX;
		d:PRX := 1;
		(a, m) := matrix L;
		for p in factors a repeat {
			b := denominatorBound(L, p);
			if b > 0 then d := times!(d, p, b);
		}
		d;
	}

	local boundRat(L:M RXD, p:RX, inv?:Boolean):Z == {
		import from R, F, TOOLS(R, RX, RXD), TOOLS(F, FX, FXD);
		import from MatrixCategory2(RXD, M RXD, FXD, M FXD);
		assert(one? degree p);
		zero?(p0 := coefficient(p, 0)) => bound(L, 0$R);
		p1 := leadingCoefficient p;
		inv? => bound(L, - quotient(p0, p1));
		pinj := map(inj)$MonogenicAlgebra2(R, RX, F, FX);
		ppinj := map(pinj)$MonogenicAlgebra2(RX, RXD, FX, FXD);
		bound(map(ppinj)(L), inj(-p0) / inj(p1));
	}

	local boundRat(L:SYS RX, p:RX, inv?:Boolean):Z == {
		import from R, F, TOOLS(R, RX, RXD), TOOLS(F, FX, FXD);
		import from LinearOrdinaryFirstOrderSystem2(RX, FX);
		assert(one? degree p);
		zero?(p0 := coefficient(p, 0)) => bound(L, 0$R);
		p1 := leadingCoefficient p;
		inv? => bound(L, - quotient(p0, p1));
		pinj := map(inj)$MonogenicAlgebra2(R, RX, F, FX);
		bound(map(pinj)(L), inj(-p0) / inj(p1));
	}

	local boundAlg(L:SYS RX, p:RX):Z == {
		macro E == SimpleAlgebraicExtension(R, RX, p);
		macro EX == DenseUnivariatePolynomial E;
		macro EXD == LinearOrdinaryDifferentialOperator EX;
		import from R, E, TOOLS(E, EX, EXD);
		import from LinearOrdinaryFirstOrderSystem2(RX, EX);
		assert(degree p > 1); assert(unit? leadingCoefficient p);
		rinj := map(coerce)$MonogenicAlgebra2(R, RX, E, EX);
		bound(map(rinj)(L), reduce(monom$RX));
	}

	local boundAlg(L:M RXD, p:RX):Z == {
		macro E == SimpleAlgebraicExtension(R, RX, p);
		macro EX == DenseUnivariatePolynomial E;
		macro EXD == LinearOrdinaryDifferentialOperator EX;
		import from R, E, TOOLS(E, EX, EXD);
		import from MatrixCategory2(RXD, M RXD, EXD, M EXD);
		assert(degree p > 1); assert(unit? leadingCoefficient p);
		rinj := map(coerce)$MonogenicAlgebra2(R, RX, E, EX);
		rrinj := map(rinj)$MonogenicAlgebra2(RX, RXD, EX, EXD);
		bound(map(rrinj)(L), reduce(monom$RX));
	}

	-- case when the leading coeff of p is not a unit in R
	local boundAlgQ(L:SYS RX, p:FX):Z == {
		macro E == SimpleAlgebraicExtension(F, FX, p);
		macro EX == DenseUnivariatePolynomial E;
		macro EXD == LinearOrdinaryDifferentialOperator EX;
		import from F, E, TOOLS(E, EX, EXD);
		import from LinearOrdinaryFirstOrderSystem2(RX, EX);
		assert(degree p > 1); assert(~unit? leadingCoefficient p);
		rinj := map((r:R):E +-> inj(r)::E)$MonogenicAlgebra2(R,RX,E,EX);
		bound(map(rinj)(L), reduce(monom$FX));
	}

	-- case when the leading coeff of p is not a unit in R
	local boundAlgQ(L:M RXD, p:FX):Z == {
		macro E == SimpleAlgebraicExtension(F, FX, p);
		macro EX == DenseUnivariatePolynomial E;
		macro EXD == LinearOrdinaryDifferentialOperator EX;
		import from F, E, TOOLS(E, EX, EXD);
		import from MatrixCategory2(RXD, M RXD, EXD, M EXD);
		assert(degree p > 1); assert(~unit? leadingCoefficient p);
		rinj := map((r:R):E +-> inj(r)::E)$MonogenicAlgebra2(R,RX,E,EX);
		rrinj := map(rinj)$MonogenicAlgebra2(RX, RXD, EX, EXD);
		bound(map(rrinj)(L), reduce(monom$FX));
	}

	-- p must be irreducible
	denominatorBound(L:SYS RX, p:RX):Z == {
		import from R;
		assert(~zero? p);
		d := degree p;
		assert(d > 0);
		inv? := unit? leadingCoefficient p;
		one? d => boundRat(L, p, inv?);
		inv? => boundAlg(L, p);
		pinj := map(inj)$MonogenicAlgebra2(R, RX, F, FX);
		boundAlgQ(L, pinj p);
	}

	-- p must be irreducible
	denominatorBound(L:M RXD, p:RX):Z == {
		import from R;
		assert(~zero? p);
		d := degree p;
		assert(d > 0);
		inv? := unit? leadingCoefficient p;
		one? d => boundRat(L, p, inv?);
		inv? => boundAlg(L, p);
		pinj := map(inj)$MonogenicAlgebra2(R, RX, F, FX);
		boundAlgQ(L, pinj p);
	}
}

