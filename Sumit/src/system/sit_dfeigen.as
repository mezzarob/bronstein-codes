-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_dfeigen.as ------------------------------
-- Copyright (c) Manuel Bronstein 2002
-- Copyright (c) INRIA 2002, Version 1.0.1
-- Logiciel Sum^it (c) INRIA 2002, dans sa version 1.0.1
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I   == MachineInteger;
	Z   == Integer;
	V   == Vector;
	M   == DenseMatrix;
	Rx  == Fraction RX;
	SYS == LinearOrdinaryFirstOrderSystem;
	SYSRAT == LinearOrdinaryDifferentialSystemRationalSolutions;
	MODRAT == LinearOrdinaryDifferentialSystemModularRationalSolutions;
	MODRATN == LinearOrdinaryDifferentialNthSystemModularRationalSolutions;
}

#if ALDOC
\thistype{LinearOrdinaryDifferentialEigenring}
\History{Manuel Bronstein}{25/7/2002}{created}
\Usage{import from \this(R, F, $\iota$, RX, RXD, FX, FXD)}
\Params{
{\em R} & \alexttype{FactorizationRing}{} & The coefficient ring\\
{\em F} & \alexttype{Field}{} & A field containing R\\
$\iota$ & $R \to F$ & Injection from R into F\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
{\em RXD} & \altype{LinearOrdinaryDifferentialOperatorCategory} RX &
Diff.~operators over RX\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
{\em FXD} & \altype{LinearOrdinaryDifferentialOperatorCategory} FX &
Diff.~operators over FX\\
}
\Descr{\this(R, F, $\iota$, RX, RXD, FX, FXD) provides eigenring
computations for linear ordinary differential operators
with polynomial coefficients.}
\begin{exports}
\alexp{eigenring}:
& RXD $\to$ (RX, \alexttype{Vector}{} \alexttype{DenseMatrix}{} FX,
\alexttype{Vector}{} FXD) & Eigenring\\
& SYS RX $\to$ (RX, \alexttype{Vector}{} \alexttype{DenseMatrix}{} FX) & \\
\end{exports}
\begin{alwhere}
SYS &==& \altype{LinearOrdinaryFirstOrderSystem}\\
\end{alwhere}
#endif

LinearOrdinaryDifferentialEigenring(
	R:   FactorizationRing,
	F:   Field, inj: R -> F,
	RX:  UnivariatePolynomialCategory R,
	RXD: LinearOrdinaryDifferentialOperatorCategory RX,
	FX:  UnivariatePolynomialCategory F,
	FXD: LinearOrdinaryDifferentialOperatorCategory FX): with {
		if R has FiniteField then {
			eigenring: RXD -> (Product RX, V RXD);
			eigenring: SYS RX -> V M RX;
		}
		if R has CharacteristicZero and
			F has Join(RationalRootRing, CharacteristicZero) then {
				eigenring: RXD -> (Product RX, V FXD);
				eigenring: SYS RX -> (RX, V M FX);
				matrixEigenring: RXD -> (RX, V M FX, V FXD);
		}
#if ALDOC
\alpage{eigenring}
\Usage{\name~L\\ \name~A}
\Signatures{
\name: & RXD $\to$ (\alexttype{Product}{} RX, \alexttype{Vector}{} FXD)\\
\name: & SYS RX $\to$ (RX, \alexttype{Vector}{} \alexttype{DenseMatrix}{} FX)\\
}
\begin{alwhere}
SYS &==& \altype{LinearOrdinaryFirstOrderSystem}\\
\end{alwhere}
\Params{
{\em L} & RXD & A linear differential operator\\
{\em S} & \altype{LinearOrdinaryFirstOrderSystem} RX &
A first order differential system\\
}
\Retval{\name(L) returns $(d, [L_1,\dots,L_s])$
such that the $L_i/d$ form
a basis for the eigenring of \emph{L}, \ie~the set of operators \emph{R}
such that $L R = S L$ for some operator \emph{S}.
\name(S) returns $(d, [M_1,\dots,M_s])$ such that the $M_i/d$ form
a basis for the eigenring of \emph{A}, \ie~the set of matrices \emph{P}
such that $P' = A P - P A$ where \emph{S} is the system $Y' = A Y$.
}
#endif
} == add {
	if R has FiniteField then {
		macro RFF == (R pretend FiniteField);
		eigenring(L:RXD):(Product RX, V RXD) == {
			import from Z, M RX, MODRATN(RFF, RX, RXD, M RXD);
			zero? L or zero? degree L => (1, empty);
			one? degree L => (1, [1]);
			e := kernel eigenringSystem L;
			(1, [c::RXD for c in columns e]);
		}

		eigenring(L:SYS RX):V M RX == {
			import from I, M RX, MODRAT(RFF, RX);
			zero?(n := dimension L) => empty;
			k := kernel eigenTensorDifferential L;
			assert(numberOfRows(k) = n * n);
			-- each column of k contains a matrix stored by columns
			[matrix(k, i, n) for i in 1..numberOfColumns k];
		}
	}

	if R has CharacteristicZero
		and F has Join(RationalRootRing, CharacteristicZero) then {
		macro R0==R pretend Join(FactorizationRing, CharacteristicZero);
		macro F0==F pretend
			 Join(Field, RationalRootRing, CharacteristicZero);

		eigenring(L:SYS RX):(RX, V M FX) == eigenChar0 L;

		eigenring(L:RXD):(Product RX, V FXD) == {
			import from Z, FXD, M FX;
			import from SYSRAT(R0, F0, inj, RX, RXD, FX);
			zero? L or zero? degree L => (1, empty);
			one? degree L => (1, [1]);
			(den, num) := kernel eigenringSystem L;
			(den, [c::FXD for c in columns num]);
		}

		matrixEigenring(L:RXD):(RX, V M FX, V FXD) == {
			import from I, Z, M FX, SYS RX, FXD, Product RX;
			import from SYSRAT(R0, F0, inj, RX, RXD, FX);
			-- (d, v) := eigenring L;
			-- zero?(n := #v) => (1, empty, empty);
			-- one? n => (expand d, [one machine degree L], v);
			zero? L or zero? degree L => (1, empty, empty);
			one? degree L => (1, [one 1], [1]);
			(den, num) := eigenChar0 companionSystem L;
			(den, num, [row(m, 1)::FXD for m in num]);
		}

		local eigenChar0(L:SYS RX):(RX, V M FX) == {
			import from I, M FX, SYSRAT(R0, F0, inj, RX, RXD, FX);
			zero?(n := dimension L) => (1, empty);
			(den, num) := kernel eigenTensorDifferential L;
			assert(numberOfRows(num) = n * n);
			-- each column of num contains a matrix stored by columns
			(den, [matrix(num, i, n) for i in 1..numberOfColumns num]);
		}
	}

	-- returns the system corresponding to the eigenring of L
	-- TEMPORARY: THIS IS REALLY NOT THE EFFICIENT WAY TO BUILD IT
	local eigenTensorDifferential(L:SYS RX):SYS RX == {
		import from I, V RX, M RX, Rx;
		import from MatrixCategoryOverFraction(RX, M RX, Rx, M Rx);
		(v, m) := matrix L;
		n := numberOfRows m;
		assert(square? m); assert(n = #v);
		mat:M Rx := zero(n, n);
		for i in 1..n repeat for j in 1..n repeat mat(i, j) := m(i, j) / v.i;
		id:M Rx := one n;
		bigmat := minus!(tensor(id, mat), tensor(transpose mat, id));
		system makeRowIntegral bigmat;
	}

	local matrix(mat:M RX, c:I, n:I):M RX == {
		k:I := 1;
		m := zero(n, n);
		for j in 1..n repeat for i in 1..n repeat {
			m(i, j) := mat(k, c);
			k := next k;
		}
		m;
	}

	local matrix(mat:M FX, c:I, n:I):M FX == {
		k:I := 1;
		m := zero(n, n);
		for j in 1..n repeat for i in 1..n repeat {
			m(i, j) := mat(k, c);
			k := next k;
		}
		m;
	}
}
