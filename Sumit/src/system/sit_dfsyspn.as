-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-------------------------------- sit_dfsyspn.as ------------------------------
--
-- Rational solutions of systems of linear ordinary differential equations
-- of arbitrary order over finite fields
--
-- Copyright (c) Manuel Bronstein 2004
-- Copyright (c) INRIA 2004, Version 1.0.2
-- Logiciel Sum^it (c) INRIA 2004, dans sa version 1.0.2
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I	== MachineInteger;
	Z	== Integer;
	V	== Vector;
	M	== DenseMatrix;
	ARR	== Array;
	SUP	== SparseUnivariatePolynomial;
}

#if ALDOC
\thistype{LinearOrdinaryDifferentialNthSystemModularRationalSolutions}
\History{Manuel Bronstein}{30/8/2004}{created}
\Usage{import from\\ \hfill \this(F, FX, FXD, MFXD)}
\Params{
{\em F} & \alexttype{FiniteField}{} & A finite field\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
\emph{FXD} & \altype{LinearOrdinaryDifferentialOperatorCategory} FX &
Differential operators\\
\emph{MFXD} & \alexttype{MatrixCategory}{} FXD & Matrices of operators\\
}
\Descr{\this(F, FX, FXD, MFXD) provides a rational solver
for systems of arbitrary order linear ordinary differential operators
with polynomial coefficients over a finite field.}
\begin{exports}
\alexp{kernel}:& MFXD $\to$ \alexttype{DenseMatrix}{} FX& Rational solutions\\
\alexp{matrix}:& MFXD $\to$ \alexttype{DenseMatrix}{} FX& Matrix of operator\\
\end{exports}
#endif

LinearOrdinaryDifferentialNthSystemModularRationalSolutions(_
	F: FiniteField,
	FX: UnivariatePolynomialCategory F,
	FXD: LinearOrdinaryDifferentialOperatorCategory FX,
	MFXD: MatrixCategory FXD): with {
		kernel: MFXD -> M FX;
#if ALDOC
\alpage{kernel}
\Usage{\name~L}
\Signature{MFXD}{\alexttype{DenseMatrix}{} FX}
\Params{ {\em L} & MFXD & A differential system\\ }
\Retval{Returns a matrix whose columns form
a basis for all the rational solutions of $LY = 0$.}
#endif
		matrix: MFXD -> M FX;
#if ALDOC
\alpage{matrix}
\Usage{\name~L}
\Signature{MFXD}{\alexttype{DenseMatrix}{} FX}
\Params{ {\em L} & MFXD & A differential system\\ }
\Retval{Returns the matrix of \emph{L} as an $F(x^p)$-linear
map from $F(x)^n$ into $F(x)^n$ (seen as a vector space of dimension \emph{np}
over $F(x^p)$).}
#endif
} == add {
	local charac:Z	== characteristic$F;
	local icharac:I	== machine(charac)$Z;
	local p1:I	== prev icharac;
	local xp:FX	== monomial charac;

	kernel(L:MFXD):M FX == {
		import from I, M FX, LinearAlgebra(FX, M FX);
		TIMESTART;
		m := matrix(L)$%;
		TIME("dfsyspn::kernel: matrix computed at ");
		TRACE("dfsyspn::kernel: m = ", m);
		k := kernel m;
		TIME("dfsyspn::kernel: kernel computed at ");
		TRACE("dfsyspn::kernel: k = ", k);
		n := numberOfRows L;
		[poly(k, j, n) for j in 1..numberOfColumns k];
	}

	-- if the operator L is of the form L = \sum_{i,j} A_{ij}(t) x^i D^j
	-- where t = x^p is a constant, then in the tensor basis
	--   x^i tensor e_j, the matrix of L as a Fp(t)-linear map is
	--    \sum_{i,j} (X^i del^j) tensor A_{ij}(t)
	-- where del is the matrix of the derivation on (1,x,...,x^{p-1})
	-- and X is the matrix of multiplication by x on (1,x,...,x^{p-1})
	-- del only has (1,2,...,p-1) just above the diagonal,
	-- while X has 1's just below the diagonal and t in the top-right corner
	matrix(L:MFXD):M FX == {
		import from I, Z, FX, SUP M FX, ARR SUP M FX;
		delj:M FX := one icharac;
		del:M FX := zero(icharac, icharac);
		for i in 1..p1 repeat del(i, next i) := i::Z::FX;
		x:ARR M FX := new icharac;
		x.0 := one icharac;
		for i in 1..p1 repeat {
			x.i := shiftDown x(prev i);
			(x.i)(1, next(icharac) - i) := monom;
		}
		(r, c) := dimensions L;
		mat:M FX := zero(icharac * r, icharac * c);
		for q in matricize L repeat {
			for trm in q repeat {
				(aij, i) := trm;
				assert(~zero? aij);
				assert(r = numberOfRows aij);
				assert(c = numberOfColumns aij);
				mat := add!(mat, tensor(x(machine i)*delj,aij));
			}
			delj := times!(delj, del);
		}
		mat;
	}

	local shiftDown(m:M FX):M FX == {
		import from I;
		(r, c) := dimensions m;
		mm := zero(r, c);
		for i in r..2 by -1 repeat
			for j in 1..c repeat mm(i,j) := m(prev i,j);
		mm;
	}

	-- if the operator L is of the form L = \sum_{i,j} A_{ij}(t) x^i D^j
	-- returns [p_0,...,p_m] such that p_j = \sum_i A_{ij}(t) x^i
	-- and the entries of A_i are in F[x^p], represented as FX (X = x^p)
	local matricize(L:MFXD):ARR SUP M FX == {
		import from Z, FX, M FX, SUP M FX, FXD;
		ord:I := 0;
		(r, c) := dimensions L;
		for i in 1..r repeat for j in 1..c | ~zero? L(i,j) repeat {
			d := machine degree L(i,j);
			if d > ord then ord := d;
		}
		a := new(next ord, 0);
		for i in 1..r repeat for j in 1..c repeat for q in L(i,j) repeat
		{
			(op, m) := q;
			mm := machine m;
			for trm in op repeat {
				-- f x^n = f (x^p)^t x^s where n = t p + s
				(f, n) := trm;
				(t, s) := divide(n, charac);
				aij := coefficient(a.mm, s);
				fresh? := zero? aij;
				if fresh? then aij := zero(r, c);
				aij(i, j) := add!(aij(i, j), f, t);
				if fresh? then a.mm := add!(a.mm, aij, s);
			}
		}
		a;
	}

	-- inverse op of matricize: the col-th columns of m represents the
	-- coordinates of a p-vector in the tensor basis x^i x e_j
	-- must evaluate those coordinates (in F[X]) at X = x^p
	local poly(m:M FX, col:I, n:I):V FX == {
		import from Z, F;
		q:V FX := zero n;
		for i in 0..prev icharac repeat for j in 1..n repeat
			q.j := add!(q.j, 1, i::Z, m(j + n * i, col)(xp));
		q;
	}
}
