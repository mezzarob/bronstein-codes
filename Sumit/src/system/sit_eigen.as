-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------ sit_eigen.as --------------------------
-- Copyright (c) Manuel Bronstein 2002
-- Copyright (c) INRIA 2002, Version 1.0.1
-- Logiciel Sum^it (c) INRIA 2002, dans sa version 1.0.1
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I	== MachineInteger;
	Z	== Integer;
	Fx	== Fraction FX;
	FxY	== DenseUnivariatePolynomial Fx;
	V	== Vector;
	M	== DenseMatrix;
	ALGFAC	== Record(modulus:FX, mult:Z, eigen:FxZ, op:FxZ);
	RIGHTFAC== Union(ratfact:FxZ, algfact:ALGFAC);
	CAND	== List Cross(FxZ, FX, Z, FxZ);
}

#if ALDOC
\thistype{EigenringDecomposition}
#endif

-- This decomposition really works for arbitrary skew-polynomials!
EigenringDecomposition(F: Join(Field, FactorizationRing),
		FX:  UnivariatePolynomialCategory F,
		FxZ: UnivariateSkewPolynomialCategory Fx): with {
	decompose: (FxZ, V M Fx, V FxZ) -> List RIGHTFAC;
#if ALDOC
\alpage{decompose}
\Usage{\name($L, [M_1,\dots,M_d], [L_1,\dots,L_d]$)}
\Signature{(FxZ,
\alexttype{Vector}{} \alexttype{DenseMatrix}{} \alexttype{Fraction}{} FX,
\alexttype{Vector}{} FxZ)}
{\alexttype{List}{} \albuiltin{Union}(ratfact:FxZ, algfact:ALGFAC)}
\begin{alwhere}
ALGFAC &==& \albuiltin{Record}(modulus:FX, mult:\alexttype{Integer}{},
eigen:FxZ, op:FxZ)\\
\end{alwhere}
\Params{
\emph{L} & FxZ & A skew polynomial\\
$M_i$ & \alexttype{DenseMatrix}{} \alexttype{Fraction}{} FX &
A basis of the eigenring of the companion system of \emph{L}\\
$L_i$ & FxZ & A basis of the eigenring of \emph{L}\\
}
\Retval{Returns one of the following:
\begin{itemize}
\item an empty list, in which case \emph{L} has a trivial eigenring;
\item a list containing only one element \emph{R} of case {\tt ratfact},
in which case \emph{R} is a nontrivial right factor of \emph{L}, which
is indecomposable;
\item a list of right factors [$R_1,\dots,R_m$] with either $m > 1$
or $m = 1$ and $R_1$ is of case {\tt algfact}, in which case
\emph{L} is a left lcm of $R_1,\dots,R_m$. Note that the $R_i$ can
still be reducible or decomposable in that case.
A factor of case {\tt algfact} is returned in the form
$[h(x), n, R, L]$ where \emph{R} is in the eigenring of \emph{L}
and the corresponding right-factor is the right gcd of \emph{L}
and $R - \alpha$ for any $\alpha$ such that $h(\alpha) = 0$.}
\Remarks{In the input, $L_i$ must correspond to $M_i$ for each $i$.}
#endif
} == add {
	local Fx2F0(a:Fx):F	== FX2F(numerator a) / FX2F(denominator a);
	local poly(h:FX,n:Z):FX	== h;
	local expo(h:FX, n:Z):Z	== n;

	local FX2F(p:FX):F == {
		import from Z;
		assert(zero? p or zero? degree p);
		leadingCoefficient p;
	}

	local Fx2F(a:Fx):Fx -> F == {
		one? a => Fx2F0;
		(b:Fx):F +-> Fx2F0(b / a);
	}

	decompose(L:FxZ, emat:V M Fx, escal:V FxZ):List RIGHTFAC == {
		import from I, CAND, List CAND, RIGHTFAC;
		dim := #emat;
		assert(dim > 0); assert(dim = #escal);
		one? dim => empty;		-- trivial eigenring
		[rightFactor cand for cand in bestCandidate(_
			[candidates(L, emat.i, escal.i) for i in 1..dim])];
	}

	-- returns a list of tuples (L, h, n, LL) such that
	-- LL - a has a nontrivial GCDR with L whenever h(a) = 0
	-- n is the multiplicity of the eigenvalue a, so if n < deg(L)
	-- then (LL - a)^n has a non-trivial GCDR with L
	-- the number of eigenvalues is sum(degree(h)) over the list
	-- does not return the trivial candidate where LL - a = 0
	local candidates(L:FxZ, m:M Fx, LL:FxZ):CAND == {
		import from I, Z, FX, FxY, Product FX;
		import from MonogenicAlgebra2(Fx, FxY, F, FX);
		scalar? m => empty;
		p := charPoly m;
		a := leadingCoefficient p;
		q := map(Fx2F a)(p);
		dq := degree q;
		(ignore, prod) := factor q;
		[(L, poly trm, expo trm, LL) for trm in prod];
	}

	-- returns the characteristic polynomial of m
	local charPoly(m:M Fx):FxY == {
		import from I, Fx, LinearAlgebra(FxY, M FxY);
		assert(square? m);
		n := numberOfRows m;
		y:FxY := monom;
		mm:M FxY := zero(n, n);
		for i in 1..n repeat for j in 1..n repeat {
			mm(i, j) := m(i, j)::FxY;
			if i = j then mm(i, j) := mm(i, j) - y;
		}
		determinant mm;
	}

	-- returns (#eigenvalues, degree(extension))
	local extension(l:CAND):(Z, Z) == {
		import from FX;
		n:Z := 0;
		d:Z := 1;
		for cand in l repeat {
			(ignore0, h, ignore1, ignore2) := cand;
			dh := degree h;
			n := add!(n, dh);
			d := times!(d, dh);
		}
		(n, d);
	}

	local bestCandidate(l:List CAND):CAND == {
		best:CAND := empty;
		local n:Z; local d:Z;
		for cand in l | ~empty? cand repeat {
			(nc, dc) := extension cand;
			-- anything with n > 1 is better than n = 1
			-- among n > 1, minimal d is better
			if empty?(best) or (n=1 and nc>1) or dc<d then {
				best := cand;
				n := nc;
				d := dc;
			}
		}
		best;
	}

	local rightFactor(L:FxZ, h:FX, n:Z, LL:FxZ):RIGHTFAC == {
		assert(~zero? h); assert(0 < degree h); assert(n > 0);
		one?(degree h) => [rightFactorRat(L, h, n, LL)];
		[[h, n, LL, L]];
	}

	local rightFactorRat(L:FxZ, h:FX, n:Z, LL:FxZ):FxZ == {
		import from F, Fx;
		a := coefficient(h, 0) / leadingCoefficient h;
		eigen := LL + a::FX::Fx::FxZ;
		if n > 1 and n < degree L then eigen := eigen^n;
		rightGcd(L, eigen);
	}
}

