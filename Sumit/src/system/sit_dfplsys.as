-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_dfplsys.as -------------------------------
-- Copyright (c) Manuel Bronstein 2000
-- Copyright (c) INRIA 2000, Version 0.1.12
-- Logiciel Sum^it (c) INRIA 2000, dans sa version 0.1.12
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I   == MachineInteger;
	Z   == Integer;
	V   == Vector;
	M   == DenseMatrix;
	SYS == LinearOrdinaryFirstOrderSystem;
	RXE == LinearOrdinaryRecurrence(R, RX);
	SOLVER	== LinearOrdinarySystemPolynomialSolutions(R,F,inj,RX,RXD,FX);
	LIN2REC	== LinearOrdinaryOperatorToRecurrence(R,RX,RXD,RXE);
	TOOLS	== LinearOrdinaryDifferentialSystemSolutionTools(R,RX,RXD);
}

LinearOrdinaryDifferentialSystemSolutionTools(
	-- TEMPORARY: for libalgebra 1.0.3. AS 1.0.2-SAE does not export Char-0
	-- R:Join(IntegralDomain, CharacteristicZero),
	R:IntegralDomain,
	RX: UnivariatePolynomialCategory R,
	RXD: LinearOrdinaryDifferentialOperatorCategory RX): with {
		-- sends x=a to x=0
		translate: (SYS RX, R) -> SYS RX;
		translate: (M RXD, R) -> M RXD;
		if R has RationalRootRing then {
			bound: (SYS RX, R) -> Z;
			bound: (M RXD, R) -> Z;
		}
} == add {
	local translate(a:R)(p:RX):RX	== translate(p, a);
	local dtranslat(a:R)(L:RXD):RXD	== map(translate a)(L);
	translate(L:M RXD, a:R):M RXD	== map(dtranslat(-a))(L);

	translate(L:SYS RX, a:R):SYS RX == {
		import from V RX, M RX;
		zero? a => L;
		(v, m) := matrix L;
		f := translate(-a);
		system(map(f)(v), map(f)(m));
	}

	if R has RationalRootRing then {
		bound(L:SYS RX, a:R):Z	== bound0 translate(L, a);
		bound(L:M RXD, a:R):Z	== bound0 translate(L, a);

		local indeq(r:Array M RX):RX == {
			import from I, M RX, LinearAlgebra(RX, M RX);
			import from LinearOrdinaryRecurrenceEGElimination(R,_
								RX, M RX);
			TRACE("sys1pol::indeq: r = ", r);
			mat := r(prev #r);
			assert(square? mat);
			(rank?, rk) := rankLowerBound mat;
			TRACE("sys1pol::indeq: rk = ", rk);
			rk < numberOfRows mat => leadingIndicialEquation r;
			determinant mat;
		}

		-- returns an upper bound on the power of x in the denominator
		-- can be negative in theory
		local bound0(L:SYS RX):Z == {
			import from LIN2REC;
			bound0 recurrence L;
		}

		local bound0(L:M RXD):Z == {
			import from LIN2REC;
			bound0 recurrence L;
		}

		local bound0(r:Array M RX, e:Z):Z == {
			import from I, Partial Z, RX;
			eq := indeq r;
			TRACE("dfplsys::bound0: eq = ", eq);
			assert(~zero?(eq));
			mt := 1 - e - (#r)::Z;
			b := {
				failed?(u := minIntegerRoot eq) => mt;
				(m := retract u) > 0 => mt;
				mt - m;
			}
			TRACE("dfplsys::bound0: b = ", b);
			b;
		}
	}
}

#if ALDOC
\thistype{LinearOrdinaryDifferentialSystemPolynomialSolutions}
\History{Manuel Bronstein}{19/7/2001}{created}
\Usage{import from \this(R, F, $\iota$, RX, FX)}
\Params{
{\em R} & \alexttype{GcdDomain}{} & The coefficient ring\\
        & \alexttype{RationalRootRing}{} &\\
        & \alexttype{CharacteristicZero}{} & \\
{\em F} & \alexttype{Field}{} & A field containing R\\
$\iota$ & $R \to F$ & Injection from R into F\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
\emph{RXD}
& \altype{LinearOrdinaryDifferentialOperatorCategory} RX & Operators\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
}
\Descr{\this(R, F, $\iota$, RX, FX) provides a polynomial solver
for first order systems of linear ordinary differential operators
with polynomial coefficients.}
\begin{exports}
\alexp{kernel}: & SYS RX $\to$ \alexttype{DenseMatrix}{} FX &
Polynomial solutions\\
\end{exports}
\begin{alwhere}
SYS &==& \altype{LinearOrdinaryFirstOrderSystem}\\
\end{alwhere}
#endif

LinearOrdinaryDifferentialSystemPolynomialSolutions(
	R:Join(GcdDomain, RationalRootRing, CharacteristicZero),
	F:Field, inj: R -> F,
	RX: UnivariatePolynomialCategory R,
	RXD: LinearOrdinaryDifferentialOperatorCategory RX,
	FX: UnivariatePolynomialCategory F): with {
		kernel: SYS RX -> M FX;
		kernel: M RXD -> M FX;
#if ALDOC
\alpage{kernel}
\Usage{\name~L}
\Signature{\altype{LinearOrdinaryFirstOrderSystem} RX}
{\alexttype{DenseMatrix}{} FX}
\Params{
{\em L} & \altype{LinearOrdinaryFirstOrderSystem} RX &
A first order differential system\\
}
\Retval{Returns a basis for all the polynomial solutions of $LY = 0$.}
#endif
} == add {
	local ftrans(a:F)(p:FX):FX == translate(p, a);

	kernel(L:M RXD):M FX == {
		import from String, UnivariatePolynomialAlgebraTools(RX, RXD);
		import from Z, R, RX, M RX, LinearAlgebra(RX, M RX), TOOLS;
		assert(square? L);
		L := primitivePart L;
		m := coefficient(L, degree L);
		zero?(d := determinant m) =>
			error "dfplsys::kernel: singular leading matrix";
		zero? trailingDegree d => kernel(L)$SOLVER;
		a := ordinaryPoint(d)::R;
		La := translate(L, a);
		map(ftrans inj a)(kernel(La)$SOLVER);
	}

	kernel(L:SYS RX):M FX == {
		import from R, RX, V RX, M RX, TOOLS;
		(v, m) := matrix L;
		nonSingular?(v, 0) => kernel(L)$SOLVER;
		a := ordinaryPoint v;
		La := translate(L, a);
		map(ftrans inj a)(kernel(La)$SOLVER);
	}

	local nonSingular?(v:V RX, a:R):Boolean == {
		import from Z, RX;
		for p in v repeat { zero?(p a) => return false }
		true;
	}

	local ordinaryPoint(v:V RX):R == {
		n:Z := 1;
		repeat {
			a := n::R;
			nonSingular?(v, a) => return a;
			nonSingular?(v, -a) => return -a;
			n := next n;
		}
	}
}

