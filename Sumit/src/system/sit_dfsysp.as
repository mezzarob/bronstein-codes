-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-------------------------------- sit_dfsysp.as ------------------------------
--
-- Rational solutions of first order systems of linear ordinary differential
-- equations over finite fields
--
-- Copyright (c) Manuel Bronstein 2002
-- Copyright (c) INRIA 2002, Version 1.0.2
-- Logiciel Sum^it (c) INRIA 2002, dans sa version 1.0.2
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I	== MachineInteger;
	Z	== Integer;
	V	== Vector;
	M	== DenseMatrix;
	ARR	== Array;
	SYS	== LinearOrdinaryFirstOrderSystem;
}

#if ALDOC
\thistype{LinearOrdinaryDifferentialSystemModularRationalSolutions}
\History{Manuel Bronstein}{2/7/2003}{created}
\Usage{import from \this(F, FX)}
\Params{
{\em F} & \alexttype{FiniteField}{} & A finite field\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
}
\Descr{\this(F, FX) provides a rational solver
for first order systems of linear ordinary differential operators
with polynomial coefficients over a finite field.}
\begin{exports}
\alexp{kernel}:& SYS FX $\to$ \alexttype{DenseMatrix}{} FX& Rational solutions\\
\alexp{matrix}:& SYS FX $\to$ \alexttype{DenseMatrix}{} FX& Matrix of operator\\
\end{exports}
\begin{alwhere}
SYS &==& \altype{LinearOrdinaryFirstOrderSystem}\\
\end{alwhere}
#endif

LinearOrdinaryDifferentialSystemModularRationalSolutions(_
	F: FiniteField,
	FX: UnivariatePolynomialCategory F): with {
		kernel: SYS FX -> M FX;
#if ALDOC
\alpage{kernel}
\Usage{\name~L}
\Signature{SYS FX}{\alexttype{DenseMatrix}{} FX}
\begin{alwhere}
SYS &==& \altype{LinearOrdinaryFirstOrderSystem}\\
\end{alwhere}
\Params{
{\em L} & \altype{LinearOrdinaryFirstOrderSystem} FX &
A first order differential system\\
}
\Retval{Returns a matrix whose columns form
a basis for all the rational solutions of $LY = 0$.}
#endif
		matrix: SYS FX -> M FX;
#if ALDOC
\alpage{matrix}
\Usage{\name~L}
\Signature{SYS FX}{\alexttype{DenseMatrix}{} FX}
\begin{alwhere}
SYS &==& \altype{LinearOrdinaryFirstOrderSystem}\\
\end{alwhere}
\Params{
{\em L} & \altype{LinearOrdinaryFirstOrderSystem} FX &
A first order differential system\\
}
\Retval{Returns the matrix of \emph{L} as an $F(x^p)$-linear
map from $F(x)^n$ into $F(x)^n$ (seen as a vector space of dimension \emph{np}
over $F(x^p)$).}
#endif
} == add {
	local charac:Z	== characteristic$F;
	local icharac:I	== machine(charac)$Z;
	local p1:I	== prev icharac;
	local xp:FX	== monomial charac;

	kernel(L:SYS FX):M FX == {
		import from I, LinearAlgebra(FX, M FX);
		TIMESTART;
		m := matrix(L)$%;
		TIME("dfsysp::kernel: matrix computed at ");
		TRACE("dfsysp::kernel: m = ", m);
		k := kernel m;
		TIME("dfsysp::kernel: kernel computed at ");
		TRACE("dfsysp::kernel: k = ", k);
		n := dimension L;
		[poly(k, j, n) for j in 1..numberOfColumns k];
	}

	-- if the operator L is of the form L Y = diag(d1,...,dn) Y' - A Y
	-- and A = \sum_{i=0}^{p-1} A_i(t) x^i
	-- and diag(dj) = \sum_{i=0}^{p-1} diag(V_i(t)) x^i
	-- where t = x^p is a constant, then in the tensor basis
	--   x^i tensor e_j, the matrix of L as a Fp(t)-linear map is
	--    (\sum_{i=0}^{p-1} X^i tensor diag(V_i(t))) (del tensor 1)
	--                              - (\sum_{i=0}^{p-1} X^i tensor A_i(t)
	-- where del is the matrix of the derivation on (1,x,...,x^{p-1})
	-- and X is the matrix of multiplication by x on (1,x,...,x^{p-1})
	-- del only has (1,2,...,p-1) just above the diagonal,
	-- while X has 1's just below the diagonal and t in the top-right corner
	matrix(L:SYS FX):M FX == {
		import from I, Z, FX, V FX, ARR M FX, ARR V FX;
		del := zero(icharac, icharac);
		for i in 1..p1 repeat del(i, next i) := i::Z::FX;
		mat := tensor(del, one dimension L);
		mrhs := zero dimensions mat;
		mlhs := zero dimensions mat;
		(a, v) := matricize L;
		TRACE("dfsysp::matrix: a = ", a);
		TRACE("dfsysp::matrix: v = ", v);
		b := one icharac;
		for i in 0..icharac-2 repeat {
			if ~zero?(a.i) then mrhs := add!(mrhs, tensor(b, a.i));
			if ~zero?(v.i) then
				mlhs := add!(mlhs, tensor(b, diagonal(v.i)));
			shiftDown! b;			-- compute X^{i+1}
			b(1, icharac - i) := monom;
		}
		if ~zero?(a.p1) then mrhs := add!(mrhs, tensor(b, a.p1));
		if ~zero?(v.p1) then
			mlhs := add!(mlhs, tensor(b, diagonal(v.p1)));
		minus!(times!(mlhs, mat), mrhs);
	}

	local shiftDown!(m:M FX):M FX == {
		import from I;
		(r, c) := dimensions m;
		for i in r..2 by -1 repeat
			for j in 1..c repeat m(i,j) := m(prev i,j);
		for j in 1..c repeat m(1,j) := 0;
		m;
	}

	-- returns [A_0,...,A_{p-1}] and vectors [V_0,...,V_{p-1}] such that
	-- the system is (\sum_i diagonal(V_i) x^i) Y' = (\sum_i A_i x^i) Y
	-- the entries of A_i and V_i are in F[x^p], represented as FX (X = x^p)
	local matricize(L:SYS FX):(ARR M FX, ARR V FX) == {
		import from I, Z, FX, V FX, M FX;
		a:ARR M FX := new icharac;
		v:ARR V FX := new icharac;
		dim := dimension L;
		for i in 0..prev icharac repeat {
			a.i := zero(dim, dim);
			v.i := zero dim;
		}
		(w, m) := matrix(L)$SYS(FX);
		TRACE("dfsysp::matricize: w = ", w);
		TRACE("dfsysp::matricize: m = ", m);
		for i in 1..dim repeat {
			for trm in w.i repeat {
				-- c x^n = c (x^p)^t x^r where n = t p + r
				(c, n) := trm;
				(t, r) := divide(n, charac);
				rr := machine r;
				-- add c (x^p)^t to V_r(i)
				v(rr)(i) := add!(v(rr)(i), c, t);
			}
			for j in 1..dim repeat for trm in m(i, j) repeat {
				-- c x^n = c (x^p)^t x^r where n = t p + r
				(c, n) := trm;
				(t, r) := divide(n, charac);
				rr := machine r;
				-- add c (x^p)^t to A_r(i,j)
				a(rr)(i, j) := add!(a(rr)(i, j), c, t);
			}
		}
		(a, v);
	}

	-- inverse op of matricize: the col-th columns of m represents the
	-- coordinates of a p-vector in the tensor basis x^i x e_j
	-- must evaluate those coordinates (in F[X]) at X = x^p
	local poly(m:M FX, col:I, n:I):V FX == {
		import from Z, F;
		q:V FX := zero n;
		for i in 0..prev icharac repeat for j in 1..n repeat
			q.j := add!(q.j, 1, i::Z, m(j + n * i, col)(xp));
		q;
	}
}
