# ======================================================================
# This code was written all or part by Dr. Manuel Bronstein from
# Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
# decided to publish this code under the CeCILL open source license in
# memory of Dr. Manuel Bronstein.
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and Inria at the following URL :
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
# 
# As a counterpart to the access to the source code and rights to copy,
# modify and redistribute granted by the license, users are provided
# only with a limited warranty and the software's author, the holder of
# the economic rights, and the successive licensors have only limited
# liability.
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading, using, modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate, and that also
# therefore means that it is reserved for developers and experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards
# their requirements in conditions enabling the security of their
# systems and/or data to be ensured and, more generally, to use and
# operate it in the same conditions as regards security.
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# ======================================================================
# 
# Targets:
#
# gmake gmp
#   makes the libsumit-gmp.al library with the .ao files
#
# gmake release
#   makes the libsumit.al library with the .ao files
#
# gmake debug
#   makes the libsumitd.al library with the .ao files
#
# gmake doc
#   extracts all the .tex files from the sources
#
# gmake test
#   extracts all the test files from the sources
#
# External variables
#
# SUMITROOT: Root directory of sumit

COMPILER=aldor

DocDir  = $(SUMITROOT)/doc/tex
TestDir = $(SUMITROOT)/test
LibDir  = $(SUMITROOT)/lib
IncDir  = $(SUMITROOT)/include

SUMIT=$(LibDir)/$(SumitLibName)

# Settings for variables
include Makefile.Rules

# Various default values
SumitLibName=libsumit


# All directories that require building
DIRS = hompoly util cond newton trinteg\
	skewpoly skewpoly/lodo skewpoly/lodifo\
	series ratsol expsol system liouvsol

TESTTARGS  = $(DIRS:%=%.test)
DOCTARGS   = $(DIRS:%=%.doc)
AOTARGS    = $(DIRS:%=%.aobj)
OBJTARGS   = $(DIRS:%=%.objects)
CLEANTARGS = $(DIRS:%=%.clean)

gmp:
	$(MAKE) aobj0 SumitLibName=$(SumitLibName)-gmp.al FLAGS="$(AOGFLAGS)"

release:
	$(MAKE) aobj0 SumitLibName=$(SumitLibName).al FLAGS="$(AOFLAGS)"

debug:
	$(MAKE) aobj0 SumitLibName=$(SumitLibName)d.al FLAGS="$(AODFLAGS)"

doc: $(DOCTARGS)

test: $(TESTTARGS)

clean: $(CLEANTARGS)

SUBFLAGS = FLAGS="$(FLAGS)" OFLAGS="$(OFLAGS)" \
           LibDir=$(LibDir) SUMITROOT=$(SUMITROOT) \
	   TestDir=$(TestDir) DocDir=$(DocDir) \
           SUMIT=$(SUMIT) SUMITM=$(SUMITM)

SUBMAKE = $(MAKE) $(SUBFLAGS)


aobj0: $(AOTARGS)

%.include: $(IncDir)
	@(cd $*; $(SUBMAKE) include)

%.aobj:
	@(cd $*; $(SUBMAKE) aobj)

%.doc: $(DocDir)
	@(cd $*; $(SUBMAKE) doc)

%.test: $(TestDir)
	@(cd $*; $(SUBMAKE) test)

%.clean: 
	@(cd $*; $(SUBMAKE) clean)

