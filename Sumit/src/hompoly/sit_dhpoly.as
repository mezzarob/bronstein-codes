-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------------- sit_dhpoly.as ----------------------------------
-- Copyright (c) Manuel Bronstein 1995
-- Copyright (c) INRIA 1999, Version 0.1.12
-- Logiciel Sum^it �INRIA 1999, dans sa version 0.1.12
-- Copyright (c) Swiss Federal Polytechnic Institute Zurich, 1995
-----------------------------------------------------------------------------

#include "sumit"

#if ALDOC
\thistype{DenseHomogeneousPolynomial}
\History{Manuel Bronstein}{18/12/95}{created}
\Usage{ import from \this(R, n, m)\\ }
\Params{
{\em R} & \alexttype{Ring}{} & The coefficient ring of the polynomials\\
{\em n} & \alexttype{MachineInteger}{} & The total degree\\
{\em m} & \alexttype{MachineInteger}{} & The number of variables\\
}
\Descr{\this~(R, n, m) implements dense homogeneous polynomials
of total degree $n$ in $m$ variables with coefficients in R.}
\begin{exports}
\category{
\altype{FiniteFreeModule}(R, \altype{DenseHomogeneousPowerProduct}(n,m))}\\
\alexp{eval}:
& (S:\alexttype{ArithmeticType}{}, R $\to$ S, T:BFLST S, T)
$\to$ \% $\to$ S & Evaluation\\
\end{exports}
\begin{alwhere}
I &==& \alexttype{MachineInteger}{}\\
BFLST &==& \alexttype{BoundedFiniteLinearStructureType}{}\\
\end{alwhere}
#endif

macro {
	I	== MachineInteger;
	Z	== Integer;
	MAT	== MatrixCategory;
	BFLS	== BoundedFiniteLinearStructureType;
	POWPROD	== DenseHomogeneousPowerProduct(n, m);
	FREEMOD	== DenseFreeModule(R, POWPROD, avar);
	RR	== R pretend Join(ArithmeticType, ExpressionType);
	RCOM	== R pretend CommutativeRing;
}

DenseHomogeneousPolynomial(R:ArithmeticType, n:I, m:I, avar:Symbol == new()):
	FiniteFreeModule(R, POWPROD) with {
		eval: (S:ArithmeticType, R -> S, T:BFLS S, T) -> % -> S;
#if ALDOC
\alpage{eval}
\Usage{\name(S, $\sigma$, T, $[a_1,\dots,a_m]$)(p)}
\Signature{(S:\alexttype{ArithmeticType}{}, R $\to$ S,
T:\alexttype{BoundedFiniteLinearStructureType}{} S, T)}{\% $\to$ S}
\Params{
{\em S} & \alexttype{ArithmeticType}{} & A ring\\
$\sigma$ & R $\to$ S & A morphism\\
{\em T} & \alexttype{BoundedFiniteLinearStructureType}{} S & A structure\\
{\em $a_i$} & S & A value for $X_i$\\
{\em p} & \% & A power product\\
}
\Retval{Returns $\sum_e \sigma(r_e) \prod_{i=1}^m a_i^{e_i}$
where $p = \sum_e r_e \prod_{i=1}^m X_i^{e_i}$.}
#endif
		if R has ExpressionType and commutative?$R then {
			power: (T:BFLS R, Z -> R) -> T -> %;
			power: (M:MAT(RR), Z -> R) -> M -> %;
		}
		if R has CommutativeRing then {
			power: (T:BFLS R) -> T -> %;
			power: (M:MAT(RCOM)) -> M -> %;
		}
} == FREEMOD add {
	eval(S:ArithmeticType, sigma:R -> S, T:BFLS S, a:T):% -> S == {
		import from POWPROD;
		phi := eval(S, T, a);
		(p:%):S +-> {
			s:S := 0;
			for trm in p repeat {
				(r, v) := trm;
				s := add!(s, sigma(r) * phi v);
			}
			s;
		}
	}

	if R has CommutativeRing then {
		power(T:BFLS R):T -> %		== power(T, coerce$R);
		power(M:MAT(RCOM)):M -> %	== power(M, coerce$R);
	}

	if R has ExpressionType and commutative?$R then {
		power(T:BFLS R,inj:Z->R)(a:T):% ==
			power(DenseMatrix(RR), inj)(powerMatrix(T, a));

		-- returns a matrix whose rows are the powers of the 1st row
		local powerMatrix(T:BFLS R, a:T):DenseMatrix(RR) == {
			import from I, R;
			assert(m <= #a);
			A := zero(m, m);
			for x in a for j in 1..m repeat A(1,j) := x;
			for i in 2..m repeat for j in 1..m repeat
				A(i, j) := A(1, j) * A(prev i, j);
			A;
		}

		-- the rows of A must be powers of the first row
		power(M:MAT(RR), inj:Z -> R)(A:M):% == {
			import from I, Integer, POWPROD, R;
			assert(n <= numberOfRows A);
			assert(m <= numberOfColumns A);
			p:% := 0;
			N := #$POWPROD;
			f := eval(RR, M, A);	-- evaluation at 1st row of A
			for i in 1..N repeat {
				pp := lookup i;
				p := add!(p, inj(multinomial pp) * f(pp), pp);
			}
			p;
		}
	}
}
