-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
----------------------------- sit_balfact.as -----------------------------
--
-- Balanced factorisation
--
-- Copyright (c) Manuel Bronstein 2001
-- Copyright (c) INRIA 2001, Version 0.1.13
-- Logiciel Sum^it (c) INRIA 2001, dans sa version 0.1.13
-- Copyright (c) Swiss Federal Polytechnic Institute Zurich, 1995-97
-----------------------------------------------------------------------------

#include "sumit"

macro Z	== Integer;

#if ALDOC
\thistype{BalancedFactorization}
\History{Manuel Bronstein}{24/5/95}{created}
\Usage{import from \this(R, RX)}
\Params{
{\em R} & \alexttype{GcdDomain}{} & the ground ring\\
{\em RX}
& \alexttype{UnivariatePolynomialCategory}{} R & polynomials over {\em R}\\
}
\Descr{\this(R, RX) implements balanced factorisation of polynomials.}
\begin{exports}
\alexp{balance}:
& (RX, RX) $\to$ \alexttype{Product}{} RX & balanced factorisation\\
& (RX, \alexttype{List}{} RX) $\to$ \alexttype{Product}{} RX & \\
\end{exports}
#endif

BalancedFactorization(R:GcdDomain, RX:UnivariatePolynomialCategory R): with {
	balance: (RX, RX) -> Product RX;
	balance: (RX, List RX) -> Product RX;
#if ALDOC
\alpage{balance}
\Usage{\name(p, q)\\ \name($p, [q_1,\dots,q_n]$)}
\Signatures{
\name: & (RX, RX) $\to$ \alexttype{Product}{} RX\\
\name: & (RX, \alexttype{List}{} RX) $\to$ \alexttype{Product}{} RX\\
}
\Params{
{\em p} & RX & a nonzero polynomial\\
{\em q}, $q_i$ & RX & polynomials\\
}
\Retval{Returns a balanced factorisation of some nonzero constant
multiple of {\em p} with respect to $p,q$ or $p,q_1,\dots,q_n$.}
#endif
} == add {
	balance(a:RX, b:RX):Product RX == { import from List RX;balance(a,[b]) }

	balance(a:RX, l:List RX):Product RX == {
		import from Boolean;
		assert(~zero? a);
		(c, sa) := squareFree a;
		empty? l => sa;
		p:Product RX := 1;
		for t in sa repeat {
			(b, n) := t;
			p := p * balance(b, n, l);
		}
		p;
	}

	local balance(a:RX, n:Z, l:List RX):Product RX == {
		import from Boolean;
		assert(~zero? a); assert(n > 0);
		empty? l => term(a, n);
		zero?(b := first l) => balance(a, n, rest l);
		empty? rest l => balance(a, n, b);
		p:Product RX := 1;
		for t in balance(a, n, rest l) repeat {
			(c, m) := t;
			p := p * balance(c, n, b);
		}
		p;
	}

	local balance(a:RX, n:Z, b:RX):Product RX == {
		import from Boolean;
		assert(~zero? a); assert(~zero? b); assert(n > 0);
		(g, a, dummy) := gcdquo(a, b);
		p:Product RX := { zero? degree a => 1; term(a, n) }
		zero? degree g => p;
		(m, q) := orderquo(g)(b);
		assert(m > 0);
		p * balance(g, n, q);
	}
}
