-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_newton.as ---------------------------------
--
-- Algebraic Newton Polygon with features for linear ordinary differential eqs
--
-- Copyright (c) Manuel Bronstein 2001
-- Copyright (c) INRIA 2001, Version 0.1.13
-- Logiciel Sum^it (c) INRIA 2001, dans sa version 0.1.13
-- Copyright (c) Swiss Federal Polytechnic Institute Zurich, 1995-97
-----------------------------------------------------------------------------

#include "sumit"

macro {
	I     == MachineInteger;
	Z     == Integer;
	Q     == Fraction Z;
	POINT == Cross(Z, Z, RX);	-- ( j, nu_j, a_j x^(-nu_j) )
	SLOPE == Cross(Q, List POINT);	-- ( s, [P1, P2, ..., Pm] )
	POCH  == UnivariateFactorialPolynomial;
	FR    == FractionalRoot;
	NP    == NewtonPolygon(R, RX, RXY);
	GEN   == Generator;
	EXX   == DenseUnivariateTaylorSeries E;
	EXY   == DenseUnivariatePolynomial EX;
}

#if ALDOC
\thistype{NewtonPolygon}
\History{Manuel Bronstein}{24/5/95}{created}
\Usage{import from \this(R, Rx, Rxy)}
\Params{
{\em R} & \alexttype{IntegralDomain}{} & the ground ring\\
{\em Rx}
& \alexttype{UnivariatePolynomialCategory}{} R & polynomials over {\em R}\\
{\em Rxy}
& \alexttype{UnivariatePolynomialAlgebra}{} Rx &polynomials over {\em Rx}\\
}
\Descr{\this(R, Rx, Rxy) implements the algebraic Newton polygon
for bivariate polynomials over {\em R}. When {\em Rxy} is
a \altype{LinearOrdinaryDifferentialOperatorCategory} over {\em Rx},
the points of the algebraic Newton polygon are in bijective correspondence
with the points of the differential Newton polygon, so \this(R, Rx, Rxy)
also provides functions related to the differential Newton polygon.}
\begin{exports}
\alexp{center}: & \% $\to$ Rx & point of localisation\\
\alexp{degree}: & \% $\to$ \alexttype{Integer}{} & degree of polynomial used\\
\alexp{finite?}: & \% $\to$ \alexttype{Boolean}{} & test for a finite center\\
\alexp{infinite?}:
& \% $\to$ \alexttype{Boolean}{} & test for a center at infinity\\
\alexp{irregular?}:
& \% $\to$ \alexttype{Boolean}{} & test for an irregular singularity\\
\alexp{maximalSlope}: & \% $\to$ \alexttype{Fraction}{} \alexttype{Integer}{}
& largest slope\\
\alexp{newtonPolygon}: & Rxy $\to$ \% & creation of a Newton polygon\\
                       & (Rxy, Rx) $\to$ \% & \\
\alexp{ordinary?}:
& \% $\to$ \alexttype{Boolean}{} & test for an ordinary point\\
\alexp{orderDrop}: & \% $\to$ \alexttype{Integer}{} & generic order drop\\
\alexp{points}: & \% $\to$ \alexttype{List}{} POINT &
points on lower enveloppe\\
\alexp{ramified?}:
& \% $\to$ \alexttype{Boolean}{} & test for a ramification\\
\alexp{regular?}:
& \% $\to$ \alexttype{Boolean}{} & test for a regular singularity\\
\alexp{singular?}: & \% $\to$ \alexttype{Boolean}{} & test for a singularity\\
\alexp{slopes}:
& \% $\to$ \alexttype{List}{} \albuiltin{Cross}(Q, \alexttype{List}{} POINT) &
slopes of lower enveloppe\\
\end{exports}
\begin{alwhere}
Z &==& \alexttype{Integer}{}\\
Q &==& \alexttype{Fraction}{} \alexttype{Integer}{}\\
POINT &==& \albuiltin{Cross}(Z, Z, Rx)\\
\end{alwhere}
#endif

NewtonPolygon(R:IntegralDomain, RX:UnivariatePolynomialCategory R,
		RXY:UnivariatePolynomialAlgebra RX): with {
	center: % -> RX;
#if ALDOC
\alpage{center}
\Usage{\name~N}
\Signature{\%}{Rx}
\Params{ {\em N} & \% & a Newton polygon\\ }
\Retval{Returns the polynomial {\em p} such that {\em N} is
a Newton polygon over the roots of {\em p}, returns {\em 0} if
{\em N} is a Newton polygon above infinity.}
#endif
	degree: % -> Z;
#if ALDOC
\alpage{degree}
\Usage{\name~N}
\Signature{\%}{\alexttype{Integer}{}}
\Params{ {\em N} & \% & a Newton polygon\\ }
\Retval{Returns the degree in the outer variable of the polynomial
used to create the Newton polygon.}
#endif
	finite?: % -> Boolean;
	infinite?: % -> Boolean;
#if ALDOC
\alpage{finite?, infinite?}
\altarget{finite?}
\altarget{infinite?}
\Usage{finite?~N\\ infinite?~N}
\Signature{\%}{\alexttype{Boolean}{}}
\Params{ {\em N} & \% & a Newton polygon\\ }
\Retval{finite?(N) returns \true{} if {\em N} is over a finite center,
\false{} if {\em N} is above infinity, while infinite?(N) returns
the opposite.}
\alseealso{\alexp{center}}
#endif
	maximalSlope: % -> Q;
#if ALDOC
\alpage{maximalSlope}
\Usage{\name~N}
\Signature{\%}{\alexttype{Fraction}{} \alexttype{Integer}{}}
\Params{\emph{N} & \% & a Newton polygon\\}
\Retval{Returns the maximal slope of {\em N} viewed as an algebraic
Newton polygon.
The return value is undetermined if \emph{N} consists of
only one point.}
\Remarks{When {\em Rxy} is
a \altype{LinearOrdinaryDifferentialOperatorCategory} over {\em Rx},
then each slope {\em q} in the algebraic Newton polygon corresponds
to the slope $q-1$ in the differential Newton polygon at a finite place,
and to the slope $q+1$ in the differential Newton polygon at infinity.}
\alseealso{\alexp{slopes}}
#endif
	newtonPolygon: RXY -> %;
	newtonPolygon: (RXY, RX) -> %;
#if ALDOC
\alpage{newtonPolygon}
\Usage{\name(f,p)}
\Signature{(Rxy, Rx)}{\%}
\Params{
{\em f} & Rxy & a bivariate polynomial over {\em R}\\
{\em p} & Rx & the place where to localize (optional)\\
}
\Retval{Returns the algebraic Newton polygon of {\em f} at any root of {\em p},
at infinity if {\em p} is omitted.}
\Remarks{This function requires that the Newton polygons at all
the roots of {\em p} be the same. This is always the case when
{\em p} is irreducible over {\em R}, or balanced with respect to
all the coefficients of {\em f}.
This assumption is not checked by this function, the resulting polygon
will be wrong if it is not satisfied.}
#endif
	orderDrop: % -> Z;
#if ALDOC
\alpage{orderDrop}
\Usage{\name~N}
\Signature{\%}{\alexttype{Integer}{}}
\Params{ {\em N} & \% & a Newton polygon\\ }
\Descr{When {\em Rxy} is
a \altype{LinearOrdinaryDifferentialOperatorCategory} over {\em Rx},
then \name(N) returns $m = \nu(L(f)) - \nu(f)$ for a generic {\em f}
for which cancellation does not occur.}
\Remarks{This function is only meaningful for differential Newton polygons.}
#endif
	ordinary?: % -> Boolean;
	singular?: % -> Boolean;
	regular?: % -> Boolean;
	irregular?: % -> Boolean;
	ramified?: % -> Boolean;
#if ALDOC
\alpage{ordinary?,singular?,regular?,irregular?,ramified?}
\altarget{ordinary?}
\altarget{singular?}
\altarget{regular?}
\altarget{irregular?}
\altarget{ramified?}
\Usage{ordinary?~N\\ singular?~N\\ regular?~N\\ irregular?~N\\ ramified?~N}
\Signature{\%}{\alexttype{Boolean}{}}
\Params{ {\em N} & \% & a Newton polygon\\ }
\Descr{When {\em Rxy} is
a \altype{LinearOrdinaryDifferentialOperatorCategory} over {\em Rx},
then those functions characterize the roots of the center of {\em N}
with respect to the differential operator. They test
whether those roots are ordinary points are singularities,
and whether singularities are regular or irregular, and whether
there is a ramification, \ie~a non integral slope.}
\Remarks{Those functions are only meaningful for differential Newton polygons,
except for ramified?, which is meaningful for both algebraic and differential
Newton polygons.}
\alseealso{\alexp{center}}
#endif
	points: % -> List POINT;
#if ALDOC
\alpage{points}
\Usage{\name~N}
\Signature{\%}{\alexttype{List}{}
\albuiltin{Cross}(\alexttype{Integer}{}, \alexttype{Integer}{}, Rx)}
\Params{ {\em N} & \% & a Newton polygon\\ }
\Descr{Returns a list of triples $(i,j,b_i)$ with increasing {\em i},
where $(i,j)$ describes all the points of the lower enveloppe of {\em N}
and $b_i = a_i p^{-j} \in R[x]$ where $a_i \in R[x]$ is the coefficient of the
original polynomial in $y^i$. For a polygon above infinity, $b_i$ is the
leading coefficient of $a_i$.}
\Remarks{When {\em Rxy} is
a \altype{LinearOrdinaryDifferentialOperatorCategory} over {\em Rx},
then each point $(i,j)$ in the algebraic Newton polygon corresponds
to the point $(i,j-i)$ in the differential Newton polygon at a finite place,
and to the point $(i,j+i)$ in the differential Newton polygon at infinity.}
\alseealso{\alexp{slopes}}
#endif
	slopes: % -> List SLOPE;
#if ALDOC
\alpage{slopes}
\Usage{\name~N}
\Signature{\%}{\alexttype{List}{} \albuiltin{Cross}(\alexttype{Fraction}{}
\alexttype{Integer}{}, \alexttype{List}{}
\albuiltin{Cross}(\alexttype{Integer}{}, \alexttype{Integer}{}, Rx))}
\Params{ {\em N} & \% & a Newton polygon\\ }
\Descr{Returns a list of slopes $(q, l_q)$ with increasing {\em q},
where {\em q} is the slope
of a segment of the lower enveloppe of {\em N}, and $l_q$ is the list
of points on that segment, sorted by decreasing first coordinate
(see \alexp{points} for the description of the format of $l_q$).}
\Remarks{When {\em Rxy} is
a \altype{LinearOrdinaryDifferentialOperatorCategory} over {\em Rx},
then each slope {\em q} in the algebraic Newton polygon corresponds
to the slope $q-1$ in the differential Newton polygon at a finite place,
and to the slope $q+1$ in the differential Newton polygon at infinity.}
\alseealso{\alexp{maximalSlope},\alexp{points}}
#endif
	<<: (TextWriter, %) -> TextWriter;	-- for debugging purposes
} == add {
	-- deg is the degree of the polynomial used to create the operator
	-- cls is: -1 = unitialized, 0 = ordinary, 1 = regular, 2 = irregular
	-- drp is the (differential) order drop = min_j(nu_j - j)
	-- ctr is 0 for infinity, of posititive degree otherwise
	-- pts is never empty, sorted low to high in powers of y
	-- slp can be empty, sorted by increasing slope
	-- inside each slope, the segment is sorted high to low in powers of y
	-- maxslp is the highest slope (only when slp is not empty)
	-- ram? tells whether the sing is ramified (only when slp is not empty)
	Rep == Record(deg:Z, cls:I, drp:Z, ctr:RX, pts:List POINT,
			slp:List SLOPE, maxslp:Q, ram?:Boolean);

	points(N:%):List POINT	== { import from Rep; rep(N).pts }
	center(N:%):RX		== { import from Rep; rep(N).ctr }
	degree(N:%):Z		== { import from Rep; rep(N).deg }
	orderDrop(N:%):Z	== { import from Rep; rep(N).drp }
	ordinary?(N:%):Boolean	== { import from I; zero? class N }
	regular?(N:%):Boolean	== { import from I; one? class N }
	irregular?(N:%):Boolean	== { import from I; 1 < class N }
	singular?(N:%):Boolean	== { import from I; 0 < class N }
	finite?(N:%):Boolean	== { import from RX; ~zero? center N }
	infinite?(N:%):Boolean	== { import from RX; zero? center N }

	-- at infinity: use (j, - degree(a_j))
	newtonPolygon(f:RXY):% == {
		import from Boolean, Q, R, List SLOPE, Rep;
		assert(~zero? f); assert(degree(f) > 0);
		l:List POINT := empty;
		drop:Z := 0;
		first? := true;
		for trm in f repeat {
			(a, j) := trm;		-- f has a(x) y^j in it
			nua := - degree a;
			l := cons((j, nua, leadingCoefficient(a)::RX), l);
			m := nua + j;
			if first? or m < drop then drop := m;
			first? := false;
		}
		per [degree f, -1, drop, 0, l, empty, 0, false];
	}

	-- at a finite point: use (j, nu_p(a_j))
	newtonPolygon(f:RXY, p:RX):% == {
		import from Boolean, Q, R, List SLOPE, Rep;
		assert(~zero? f); assert(degree(f) > 0);
		assert(~zero? p); assert(degree(p) > 0);
		l:List POINT := empty;
		nu := orderquo p;
		drop:Z := 0;
		first? := true;
		for trm in f repeat {		-- high to low
			(a, j) := trm;		-- f has a(x) y^j in it
			(nua, b) := nu a;	-- a = b p^nua, p \nodiv b
			l := cons((j, nua, b), l);
			m := nua - j;
			if first? or m < drop then drop := m;
			first? := false;
		}
		per [degree f, -1, drop, p, l, empty, 0, false];
	}

	slopes(N:%):List SLOPE == {
		import from Boolean, Z, List POINT, Rep;
		~empty?(sl := rep(N).slp) => sl;
		ramif? := false;
		mxslp:Q := 0;
		l := points N;
		while ~empty?(l) repeat {
			(l, slop, pts) := slope l;
			if ~empty?(pts) then {
				if ~one?(denominator slop) then ramif? := true;
				sl := cons((slop, pts), sl);
				mxslp := slop;	-- increasing (except from 0)
			}
		}
		if ~empty?(sl) then rep(N).maxslp := mxslp;
		rep(N).ram? := ramif?;
		rep(N).slp := reverse! sl;
	}

	maximalSlope(N:%):Q == {
		import from List SLOPE, Rep;
		empty? slopes N => 0;	-- computes the slopes if not yet done
		rep(N).maxslp;
	}

	ramified?(N:%):Boolean == {
		import from List SLOPE, Rep;
		empty? slopes N => false;-- computes the slopes if not yet done
		rep(N).ram?;
	}

	local slope(x:Z, y:Z, X:Z, Y:Z):Q == {
		assert(x ~= X);
		(Y - y) / (X - x);
	}

	-- return (pointer to last point in segment, slope, segment)
	-- where the points in the segment are stored in descending degree
	local slope(l:List POINT):(List POINT, Q, List POINT) == {
		import from Boolean, Q;
		pt0 := first l;
		empty?(l := rest l) => (l, 0, l);
		(x, y, ignore) := pt0;
		(X, Y, ignore) := first l;
		s := slope(x, y, X, Y);
		seg:List POINT := [first l, pt0];
		lst := l;
		while ~empty?(l := rest l) repeat {
			(X, Y, ignore) := first l;
			ss := slope(x, y, X, Y);
			if ss < s then {
				s := ss;
				seg := [first l, pt0];
				lst := l;
			}
			else if ss = s then {
				seg := cons(first l, seg);
				lst := l;
			}
		}
		(lst, s, seg);
	}

	local class(N:NP):I == {
		import from Rep;
		(n := rep(N).cls) >= 0 => n;
		rep(N).cls := classify N;
	}

	local last(l:List POINT):POINT == {
		import from Boolean;
		assert(~empty? l);
		while ~empty?(rest l) repeat l := rest l;
		first l;
	}

	local classify(N:NP):I == {
		import from Z, List SLOPE;
		empty?(l := slopes N) => {
			(j, nu, ignore) := first points N;
			j = degree N => 0;	-- ordinary
			2;			-- irregular
		}
		targetSlope:Q := { finite? N => 1; -1 }
		seg:List POINT := empty;
		for slp in l repeat {
			(sl, seg) := slp;
			sl = targetSlope => {
				(j, nu, ignore) := first seg;
				j = degree N => return 1;	-- regular
				return 2;			-- irregular
			}
			sl > targetSlope => {
				(j, nu, ignore) := last seg;
				j = degree N => return 0;	-- ordinary
				return 2;			-- irregular
			}
		}
		(j, nu, ignore) := first seg;
		j = degree N => 0;	-- ordinary
		2;			-- irregular
	}

	(p:TextWriter) << (N:%):TextWriter == {
		import from Z, RX, List POINT, Character, String;
		p := p << center N << newline;
		for pt in points N repeat {
			(i, j, a) := pt;
			p := p << "(" << i << "," << j << ")" << newline;
		}
		p << newline;
	}
}

#if ALDOC
\thistype{NewtonPolygonEquations}
\History{Manuel Bronstein}{24/5/95}{created}
\Usage{import from \this(R, Rx, Rxy, E, Ex)}
\Params{
{\em R} & \alexttype{IntegralDomain}{} & The coefficient ring\\
{\em Rx} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
{\em Rxy}
& \alexttype{UnivariatePolynomialAlgebra}{} Rx &polynomials over {\em Rx}\\
{\em E} & \alexttype{CommutativeRing}{} & An extension of R\\
{\em Ex} & \alexttype{UnivariatePolynomialCategory}{} E & Polynomials over E\\
}
\Descr{\this(R, Rx, Rxy, E, Ex) provides tools to compute
exponents and indicial and characteristic equations associated
with Newton polygons.}
\begin{exports}
\alexp{exponentialPart}:
&(Rxy, E, \alexttype{Integer}{}, \alexttype{Integer}{}, E, R $\to$ E) $\to$ Ex &
Exponential part\\
\alexp{series}:
& (Rxy, E, \alexttype{Integer}{}, \alexttype{Integer}{}, E, R $\to$ E)
$\to E[[x]]$ & Laurent series\\
\end{exports}
\begin{alwhere}
$E[[x]]$ &==& \alexttype{DenseUnivariateTaylorSeries}{} E\\
\end{alwhere}
#endif

NewtonPolygonEquations(R:IntegralDomain, RX:UnivariatePolynomialCategory R,
		RXY:UnivariatePolynomialAlgebra RX,
		E:CommutativeRing, EX:UnivariatePolynomialCategory E): with {
	characteristicEquation: (NP, Q, RX -> E) -> EX;
	if E has Field then {
		-- TEMPORARY: ALDOR 1.0.0 BAD OVERLOADING BUG
		-- exponentialPart: (RXY, Z, Z, E, R -> E) -> EX;
		exponentialPartAtInfinity: (RXY, Z, Z, E, R -> E) -> EX;
		exponentialPart: (RXY, E, Z, Z, E, R -> E) -> EX;
#if ALDOC
\alpage{exponentialPart}
\Usage{\name(L, a, m, $\mu$, c, $\iota$)}
\Signature{(Rxy, E, \alexttype{Integer}{}, \alexttype{Integer}{}, E, R $\to$ E)}
{Ex}
\Params{
\emph{L} & Rxy & A linear ordinary differential operator\\
\emph{a} & E & A point such that $x=a$ is an irregular singularity of \emph{L}\\
\emph{m} & Z & A slope of the algebraic newton polygon of \emph{L} at $x=a$\\
$\mu$ & Z & An integer such that $\mu \ge i m - \nu_a(a_i)$ for all $i$\\
\emph{c} & E & An invertible simple root of
the characteristic equation for the slope \emph{m}\\
$\iota$ & $R \to E$ & Injection from R into E\\
}
\Retval{Returns $q(x)$ of degree at most $m-2$ such that $q(a) = c$ and
the $p$-adic expansion of the solution of the associated Riccati equation
starting with $u = c/(x-a)^m + \cdots$ must be of the form
$$
u = \frac{q(x)}{(x-a)^m} + \frac{\lambda}{(x-a)} + \cdots
$$
}
\Remarks{van Hoeij's \emph{generalized exponent} $e(x)$ is related to
our exponent $q(x)$ by
$$
e(x) = \frac{q(x)}{(x-a)^{m-1}} + \lambda\,.
$$
Note that the constant $\lambda$ is not computed by \name,
and that there could be several $\lambda$'s corresponding to
a given exponent.}
#endif
	}
	indicialEquation: (NP, RX -> E) -> POCH(E, EX);
	if E has Field then {
		series: (RXY, Z, Z, E, R -> E) -> EXX;
		series: (RXY, E, Z, Z, E, R -> E) -> EXX;
#if ALDOC
\alpage{series}
\Usage{\name(p, a, m, $\mu$, c, $\iota$)}
\Signature{(Rxy, E, \alexttype{Integer}{}, \alexttype{Integer}{}, E, R $\to$ E)}
{\alexttype{DenseUnivariateTaylorSeries} E}
\Params{
\emph{p} & Rxy & A bivariate polynomial\\
\emph{a} & E & A singularity of \emph{p}\\
\emph{m} & Z & A slope of the algebraic newton polygon of \emph{L} at $x=a$\\
$\mu$ & Z & An integer such that $\mu \ge i m - \nu_a(a_i)$ for all $i$\\
\emph{c} & E & An invertible simple root of
the characteristic equation for the slope \emph{m}\\
$\iota$ & $R \to E$ & Injection from R into E\\
}
\Retval{Returns $c + \sum_{n \ge 1} c_n t^n$ such that
the Laurent expansion at a branch of $y$ over $x = a$ is
$$
y = (x-a)^{-m} \paren{c + \sum_{n \ge 1} c_n (x-a)^n}\,.
$$
}
#endif
	}
	if E has RationalRootRing then {
		integerExponents: (NP, RX -> E) -> List FR Z;
		rationalExponents: (NP, RX -> E) -> List FR Z;
	}
	if E has FactorizationRing then {
		integralExponents: (NP, RX -> E) -> (List FR Z, GEN FR E);
		fractionalExponents: (NP, RX -> E) -> (List FR Z, GEN FR E);
		characteristicRoots: (NP, Q, RX -> E) -> GEN FR E;
	}
} == add {
	local last(l:List POINT):POINT == {
		import from Boolean;
		assert(~empty? l);
		while ~empty?(rest l) repeat l := rest l;
		first l;
	}

	-- only makes sense for polygons of differential operators
	-- at finite centers, the zeroes of the equation are the exponents
	--                    use algebraic slope 1  (= diff slope 0)
	-- at infinity, the zeroes of the equation are the degrees,
	--              so the exponents are the opposite of the roots
	--                    use algebraic slope -1  (= diff slope 0)
	indicialEquation(N:NP, val:RX -> E):POCH(E, EX) == {
		import from List SLOPE;
		empty?(l := slopes N) => indicialEquation first points N;
		s:Q := { finite? N => 1; -1 }
		seg:List POINT := empty;
		for slp in l repeat {
			(sl, seg) := slp;
			s = sl => return indicialEquation(N, seg, val);
			s < sl => return indicialEquation(last seg);
		}
		indicialEquation first seg;
	}

	-- case of just one point contributing to the equation
	local indicialEquation(j:Z, nu:Z, ignore:RX):POCH(E,EX) == term(1$E, j);

	local indicialEquation(N:NP, seg:List POINT, val:RX -> E):POCH(E,EX) ==
		computeEquation(N, seg, val, true, POCH(E, EX));

	-- case of more than one point contributing to the equation
	-- works for both indicial and characteristic equation for a segment
	-- only POL differs (factorials for indicial, regular polys otherwise)
	-- and ind? is true for indicial equation, false for charac equation
	--
	-- at infinity, the indicial equation is
	--   \sum_{j on slope} (-1)^j lc(a_j) r^j ascending_fact(\lambda, j)
	-- But  ascending_fact(\lambda, j) = (-1)^j descending_fact(-\lambda, j)
	-- so the indicial equation is also
	--   \sum_{j on slope} lc(a_j) r^j  descending_fact(-\lambda, j)
	-- We return instead
	--   \sum_{j on slope} lc(a_j) r^j descending_fact(\lambda, j)
	-- whose roots are the degrees (opposites of the exponents)
	-- For characteristic equations, POL = EX and we return
	--   \sum_{j on slope} lc(a_j) r^{\nu_j} \lambda^j
	local computeEquation(N:NP, seg:List POINT, val:RX -> E, ind?: Boolean,
					POL:MonogenicAlgebra E):POL == {
		import from Z, RX;
		eq:POL := 0;
		r:E := { finite? N => val(differentiate center N); 1 }
		TRACE("newton::computeEquation: r = ", r);
		assert(~zero? r);
		-- one should normally add val(a) * r^j or val(a) * r^\nu
		-- for each term (j,nu,a), can remove the content r^min
		(j, nu, a) := last seg;		-- lowest for both j and nu
		emin := { ind? => j; nu }
		TRACE("newton::computeEquation: emin = ", emin);
		for pt in seg repeat {		-- go high to low
			(j, nu, a) := pt;
			TRACE("newton::computeEquation: j = ", j);
			TRACE("newton::computeEquation: nu = ", nu);
			e := { ind? => j; nu }
			eq := add!(eq, val(a) * r^(e-emin), j);
			TRACE("newton::computeEquation: eq = ", eq);
		}
		eq;
	}

	local negate(r:FR Z):FR Z == {
		import from Z;
		(a, b) := value r;
		fractionalRoot(-a, b, multiplicity r);
	}

	if E has RationalRootRing then {
		-- the multiplicities are for each root of the center
		-- and should be multiplied by the degree of the center
		-- to get all of them
		integerExponents(N:NP, value:RX -> E):List FR Z == {
			import from POCH(E, EX);
			l := integerRoots indicialEquation(N, value);
			finite? N => l;
			map!(negate)(l);
		}

		rationalExponents(N:NP, value:RX -> E):List FR Z == {
			import from POCH(E, EX);
			l := rationalRoots indicialEquation(N, value);
			finite? N => l;
			map!(negate)(l);
		}
	}

	if E has FactorizationRing then {
		local negate(r:FR E):FR E == {
			import from E;
			(a, b) := value r;
			fractionalRoot(-a, b, multiplicity r);
		}

		-- the multiplicities are for each root of the center
		-- and should be multiplied by the degree of the center
		-- to get all of them
		integralExponents(N:NP, val:RX -> E):(List FR Z, GEN FR E) == {
			import from Z, FR Z, E, POCH(E, EX);
			x:EX := monom;
			eq := indicialEquation(N, val);
			l := integerRoots eq;
			(n, q) := trailExpand eq;
			for rt in l repeat {
				a := integralValue rt;
				m := multiplicity rt;
				assert(m > 0);
				if a >= 0 and a < n then m := prev m;
				if m > 0 then q := quotient!(q, (x - a::EX)^m);
			}
			gen := roots(EX)(q);
			finite? N => (l, gen);
			(map!(negate)(l), negate zr for zr in gen);
		}

		fractionalExponents(N:NP, val:RX->E):(List FR Z, GEN FR E) == {
			import from Z, FR Z, E, POCH(E, EX);
			x:EX := monom;
			eq := indicialEquation(N, val);
			l := rationalRoots eq;
			(n, q) := trailExpand eq;
			for rt in l repeat {
				m := multiplicity rt;
				assert(m > 0);
				if integral? rt then {
					a := integralValue rt;
					if a >= 0 and a < n then m := prev m;
					if m > 0 then
						q := quotient!(q, (x-a::EX)^m);
				}
				else {
					(a, b) := value rt;
					q := quotient!(q, (b::E*x - a::EX)^m);
				}
			}
			gen := fractionalRoots(EX)(q);
			finite? N => (l, gen);
			(map!(negate)(l), negate zr for zr in gen);
		}

		-- in the algebraic case,
		-- valid for all points (finite/infinity) and all slopes s
		-- when used for Riccati solutions of differential equations,
		-- valid only for:  s > 1 at finite points, s >= 0 at infinity
		-- never returns the root 0
		characteristicRoots(N:NP, s:Q, value:RX -> E):GEN FR E == {
			import from Z, E, EX;
			p := characteristicEquation(N, s, value);
			d := trailingDegree p;
			if d > 0 then p := shift!(p, -d);
			fractionalRoots(EX)(p);
		}
	}

	-- Gives the characteristic equation for the algebraic Newton polygon,
	-- where it's valid for all points (finite/infinity) and all slopes s
	-- When used for Riccati solutions of differential equations, this
	-- is valid only for:  s > 1 above finite points, s >= 0 above infinity
	characteristicEquation(N:NP, s:Q, value:RX -> E):EX == {
		import from List SLOPE;
		l := slopes N;
		for slp in l repeat {
			(sl, seg) := slp;
			s = sl => return characteristicEquation(N, seg, value);
			s < sl => return 1;
		}
		1;
	}

	local characteristicEquation(N:NP, seg:List POINT, val:RX -> E):EX ==
		computeEquation(N, seg, val, false, EX);

	if E has Field then {
		-- above a finite point
		series(L:RXY, a:E, m:Z, mu:Z, c:E, inj:R -> E):EXX == {
			import from UnivariateTaylorSeriesNewtonSolver(E,_
								EXX, EX, EXY);
			root(transform(L, -a, m, mu, inj, ~zero? a), c);
		}

		-- above infinity
		series(L:RXY, m:Z, mu:Z, c:E, inj:R -> E):EXX == {
			import from UnivariateTaylorSeriesNewtonSolver(E,_
								EXX, EX, EXY);
			root(transform(L, m, mu, inj), c);
		}

		-- above a finite point, m = slope, order(sol) = -m
		exponentialPart(L:RXY, a:E, m:Z, mu:Z, c:E, inj:R -> E):EX == {
			import from
				UnivariateTaylorSeriesCategory2Poly(E, EXX, EX),
				UnivariateTaylorSeriesNewtonSolver(E,_
								EXX, EX, EXY);
			TRACE("newton::exponentialPart: L = ", L);
			TRACE("newton::exponentialPart: a = ", a);
			TRACE("newton::exponentialPart: m = ", m);
			TRACE("newton::exponentialPart: mu = ", mu);
			TRACE("newton::exponentialPart: c = ", c);
			assert(m > 1);
			one?(m1 := prev m) => c::EX;
			translate? := ~zero? a;
			p := transform(L, -a, m, mu, inj, translate?);
			q := truncate(root(p, c), m1);
			translate? => translate(q, a);
			q;
		}

		-- above infinity, m = slope, order(sol) = -m, degree(sol) = m
		-- TEMPORARY: ALDOR 1.0.0 BAD OVERLOADING BUG
		-- exponentialPart(L:RXY, m:Z, mu:Z, c:E, inj:R -> E):EX == {
		exponentialPartAtInfinity(L:RXY, m:Z, mu:Z, c:E, inj:R -> E):EX == {
			import from
				UnivariateTaylorSeriesCategory2Poly(E, EXX, EX),
				UnivariateTaylorSeriesNewtonSolver(E,_
								EXX, EX, EXY);
			TRACE("newton::exponentialPart: L = ", L);
			TRACE("newton::exponentialPart: m = ", m);
			TRACE("newton::exponentialPart: mu = ", mu);
			TRACE("newton::exponentialPart: c = ", c);
			assert(m >= 0);
			zero? m => c::EX;
			p := transform(L, m, mu, inj);
			import from EXY;
			TRACE("newton::exponentialPart: p = ", p);
			q := truncate(root(p, c), next m);
			TRACE("newton::exponentialPart: q = ", q);
			d := degree(q);
			TRACE("newton::exponentialPart: d = ", d);
			assert(d <= m);
			q := shift!(revert! q, m - d);
			TRACE("newton::exponentialPart: returning ", q);
			q
		}
	}

	-- given    L(x,y) = sum a_i(x) y^i,
	-- returns  p(t,z) = sum a_i(t-b) t^{mu-i m} z^i
	local transform(L:RXY, b:E, m:Z, mu:Z, inj:R -> E, tr?:Boolean):EXY == {
		import from EX;
		p:EXY := 0;
		pinj := map(inj)$MonogenicAlgebra2(R, RX, E, EX);
		for trm in L repeat {
			(an, n) := trm;
			q := pinj an;
			if tr? then q := translate(q, b);
			assert(trailingDegree(q) >= n * m - mu);
			p := add!(p, shift!(q, mu - n * m), n);
		}
		p;
	}

	-- given    L(x,y) = sum a_i(x) y^i,
	-- returns  p(t,z) = sum a_i*(t) t^{mu-i m-degree(a_i)} z^i
	local transform(L:RXY, m:Z, mu:Z, inj:R -> E):EXY == {
		import from RX, EX;
		p:EXY := 0;
		pinj := map(inj)$MonogenicAlgebra2(R, RX, E, EX);
		for trm in L repeat {
			(an, n) := trm;
			q := pinj revert an;
			assert(trailingDegree(q) >= n * m + degree an - mu);
			p := add!(p, shift!(q, mu - n * m - degree an), n);
		}
		p;
	}
}
