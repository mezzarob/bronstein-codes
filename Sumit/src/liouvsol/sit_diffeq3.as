-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_diffeq3.as -------------------------------
-- Copyright (c) Manuel Bronstein 2000
-- Copyright (c) INRIA 2000, Version 0.1.12
-- Logiciel Sum^it (c) INRIA 2000, dans sa version 0.1.12
-----------------------------------------------------------------------------

#include "sumit"

#if ALDOC
\thistype{ThirdOrderLinearOrdinaryDifferentialSolver}
\History{Manuel Bronstein}{16/7/2002}{created}
\Usage{import from \this(R, F, $\iota$, RX, RXD, FX, FxD, FxY)}
\Params{
{\em R} & \alexttype{FactorizationRing}{} & The coefficient ring\\
        & \alexttype{CharacteristicZero}{} &\\
{\em F} & \alexttype{Field}{} & A field containing R\\
        & \alexttype{FactorizationRing}{} &\\
        & \alexttype{CharacteristicZero}{} &\\
$\iota$ & $R \to F$ & Injection from R into F\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
{\em RXD} & LODOCAT RX & Differential operators\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
{\em FxD} & LODOCAT \alexttype{Fraction}{} FX &
For returning results\\
{\em FxY} &
\alexttype{UnivariatePolynomialCategory}{} \alexttype{Fraction}{} FX &
For returning results\\
}
\begin{alwhere}
LODOCAT &==& \altype{LinearOrdinaryDifferentialOperatorCategory}\\
\end{alwhere}
\Descr{\this(R, F, $\iota$, RX, RXD, FX, FxD, FxY) provides tools for solving
third order linear ordinary differential equations in $R[x,\frac d{dx}]$.}
\begin{exports}
\category{\altype{UnivariateSkewPolynomialDecompositionCategory}(R, F, $\iota$,
RX, RXD, FX, FxD, FxY)}\\
\alexp{symmetricSquareRoot}:
& RXD $\to$ \alexttype{Partial}{} RXD & test for symmetric square\\
\end{exports}
#endif

macro {
	I	== MachineInteger;
	Z	== Integer;
	V	== Vector;
	M	== DenseMatrix;
	RR	== FractionalRoot;
	HEQ	== LinearHolonomicDifferentialEquation(R, F, inj, RX, RXD);
	RXY	== DenseUnivariatePolynomial RX;
	FXY	== DenseUnivariatePolynomial FX;
	FXYZ	== DenseUnivariatePolynomial FXY;
	FXYZT	== DenseUnivariatePolynomial FXYZ;
	Rx	== Fraction RX;
	Fx	== Fraction FX;
	Fxy	== Fraction FxY;
	FxYZ	== DenseUnivariatePolynomial FxY;
	EXPSOLS	== LinearOrdinaryDifferentialOperatorExponentialSolutions;
	FXD	== LinearOrdinaryDifferentialOperator FX;
	ALGFAC	== Record(modulus:FX, mult:Z, eigen:FxD, op:FxD);
	RIGHTFAC== Union(ratfact:FxD, algfact:ALGFAC);
	RATRED	== Record(mode:I, rat: V FxD);
	ALGRED	== Record(mode:I, modulus:FX, c0:Fxy, ratpart:V FxD);
	FACLODO	== Union(rat: RATRED, alg: ALGRED);
	LODO2	== SecondOrderLinearOrdinaryDifferentialSolver(F, F, ident,_
							 FX, FXD, FX, FxD, FxY);
	INVAR	== LinearOrdinaryDifferentialOperatorInvariants(R, F, inj,_
								RX, RXD, FX);
	RINVAR	== LinearOrdinaryDifferentialOperatorRadicalInvariants(R, F,_
							inj, RX, RXD, FX);
	LINCOMB == Union(alg:FX, rat:V F, fail:I);
	ALGRIC	== Record(modulus:FX, den:FxY, num:FxYZ);
	SOLUTION== Union(ratric:FxY, algric:ALGRIC, inv:INVAR, radinv:RINVAR);
}

-- Group return codes for primitiveUnimodular
-- No liouvillian sols for 1-4, liouvillian sols for 5-13
macro {
	PSL2				== 1;
	PSL2xC3				== 2;
	SL3				== 3;
	SL3orPSL2xC3			== 4;
	H72                             == 5;
	A5				== 6;
	G168				== 7;
	H216				== 8;
	F36				== 9;
	A6				== 10;
	G168xC3				== 11;
	A5xC3				== 12;
	IMPRIM				== 13;
}

ThirdOrderLinearOrdinaryDifferentialSolver(_
	R: Join(FactorizationRing, CharacteristicZero),
	F: Join(Field, FactorizationRing, CharacteristicZero), inj: R -> F,
	RX: UnivariatePolynomialCategory R,
	RXD: LinearOrdinaryDifferentialOperatorCategory RX,
	FX: UnivariatePolynomialCategory F,
	FxD: LinearOrdinaryDifferentialOperatorCategory Fx,
	FxY: UnivariatePolynomialCategory Fx):
		UnivariateSkewPolynomialDecompositionCategory(R, F, inj, RX,
						RXD, FX, FxD, FxY) with {
	solveIrreducible: (RXD, Boolean) -> (Rx, I, SOLUTION);
	solveIrreducible: (HEQ, Boolean) -> (Rx, I, SOLUTION);
	symmetricSquareRoot: RXD -> Partial RXD;
#if ALDOC
\alpage{symmetricSquareRoot}
\Usage{\name~L}
\Signature{RXD}{\alexttype{Partial}{} RXD}
\Params{ \emph{L} & RXD & A linear differential operator of order 3\\ }
\Retval{Returns either an operator \emph{S} of order 2 such that
$L = \sympow{S}{2}$, or \failed{} if \emph{L} is not a symmetric
square.}
#endif
} == add {
	local ident(f:F):F	== f;
	local F2FXY(f:F):FXY	== { import from FX; f::FX::FXY; }
	local F2FXYZ(f:F):FXYZ	== F2FXY(f)::FXYZ;
	local fact(p:FX,n:Z):FX	== p;

	solveIrreducible(L:RXD, group?:Boolean):(Rx, I, SOLUTION) == {
		import from HEQ;
		solveIrreducible(L::HEQ, group?);
	}

	solveIrreducible(L:HEQ, group?:Boolean):(Rx, I, SOLUTION) == {
		(ignore, U, s) := unimodularize L;
		(group, sol) := irreducibleUnimodular(U, group?);
		(s, group, sol);
	}

	local irreducibleUnimodular(L:HEQ, group?:Boolean):(I, SOLUTION) == {
		import from Z, RINVAR, List RINVAR, LINCOMB;
		rinvar := radicalInvariants(L, 3, 2);
		for riv in rinvar repeat {
			iv := invariants riv;
			~((lcb := factorsLinearly iv) case fail) =>
				return(IMPRIM,
					riccati(iv, lcb, riccati(FxY)(riv)));
		}
		primitiveUnimodular(L, group?);
	}

	local primitiveUnimodular(L:HEQ, group?:Boolean):(I, SOLUTION) == {
		import from Z, FxY;
		allRationalExponents? L =>fuchsianPrimitiveUnimodular(L,group?);
		invar := invariants(L, 2);
		one?(d := dimension invar) => (PSL2, [invar]);
		assert(zero? d);
		group? => {
			invar := invariants(L, 6);
			one?(d := dimension invar) => (PSL2xC3, [invar]);
			assert(zero? d);
			(SL3, [0]);
		}
		(SL3orPSL2xC3, [0]);
	}

	local fuchsianPrimitiveUnimodular(L:HEQ,group?:Boolean):(I,SOLUTION)=={
		import from Z, RINVAR, List RINVAR, LINCOMB, FxY, V FxY;
		d := dimension invariants(L, 2);
		one? d => {
			invar := invariants(L, 6);
			one?(d := dimension invar) => (PSL2, [invar]);
			assert(d = 2);
			group? => (A5, [invar]);
			(A5, riccati(invar, factorsLinearly invar,
							riccati(FxY)(invar)));
		}
		assert(zero? d);
		invar := invariants(L, 4);
		one?(d := dimension invar) => (G168, [invar]);
		assert(zero? d);
		invar := invariants(L, 6);
		zero?(d := dimension invar) => {
			invar := invariants(L, 9);
			one?(d := dimension invar) => {
				group? => (H216, [invar]);
				(H216, [riccati(FxY)(invar).1]);
			}
			assert(zero? d);
			(SL3, [0]);
		}
		one? d => {
			power?(invar, 3) => (PSL2xC3, [invar]);
			i9 := invariants(L, 9);
			one?(d := dimension i9) => {
				group? => (H72, [i9]);
				(H72, [riccati(FxY)(i9).1]);
			}
			assert(zero? d);
			rinvar := radicalInvariants(L, 4, 3);
			empty? rinvar => (A6, [invar]);
			assert(empty? rest rinvar);
			(G168xC3, [first rinvar]);
		}
		assert(d = 2);
		(lcomb := factorsLinearly invar) case fail => {
			group? => (F36, [invar]);
			invar := invariants(L, 9);
			assert(one? dimension invar);
			(F36, [riccati(FxY)(invar).1]);
		}
		group? => (A5xC3, [invar]);
		(A5xC3, riccati(invar, lcomb, riccati(FxY)(invar)));
	}

	local riccati(invar:INVAR, lcomb:LINCOMB, vric:V FxY):SOLUTION == {
		import from I, Z, Product RX, V FX, Fx, FxY, FxYZ;
		import from MonogenicAlgebra2(R, RX, F, FX);
		assert(~(lcomb case fail));
		lcomb case rat => [riccati(invar, lcomb.rat, vric)];
		assert(dimension invar = 2); assert(#vric = 2);
		(den, vnum) := value invar;
		assert(#vnum = 2);
		(nden, dden) := expandFraction den;
		invden := map(inj)(dden) / map(inj)(nden);
		[ [lcomb.alg, term(vnum.1 * invden, 1) + (vnum.2 * invden)::FxY,
				term(vric.1, 1) + (vric.2)::FxYZ]$ALGRIC ];
	}

	local riccati(invar:INVAR, lcomb:V F, vric:V FxY):FxY == {
		import from I, Product RX, FX, V FX, Fx, V FxY;
		import from MonogenicAlgebra2(R, RX, F, FX);
		TRACE("diffeq3::riccati: lcomb = ", lcomb);
		TRACE("diffeq3::riccati: vric = ", vric);
		d := dimension invar;
		TRACE("diffeq3::riccati: d = ", d);
		assert(d = #lcomb); assert(d = #vric);
		(den, vnum) := value invar;
		assert(d = #vnum);
		TRACE("diffeq3::riccati: den = ", den);
		TRACE("diffeq3::riccati: vnum = ", vnum);
		val:FX := 0;
		ric:FxY := 0;
		for i in 1..d repeat {
			c := lcomb.i;
			val := add!(val, c, vnum.i);
			ric := add!(ric, c::FX::Fx, vric.i);
		}
		TRACE("diffeq3::riccati: val = ", val);
		TRACE("diffeq3::riccati: ric = ", ric);
		(nden, dden) := expandFraction den;
		times!(map(inj)(nden) / (val * map(inj)(dden)), ric);
	}

	local power?(invar:INVAR, n:Z):Boolean == {
		import from I, Partial Z, V FXY, Product FXY;
		assert(n > 0);
		one? n or zero? dimension invar => true;
		x:FX := monom;
		y:FXY := monom;
		v := eval(FXY, F2FXY, [x::FXY, y, 1])(invar);
		(c, prod) := squareFree(v.1);
		for trm in prod repeat {
			(p, m) := trm;
			failed? exactQuotient(m, n) => return false;
		}
		true;
	}

	-- return [f1,...,fd] s.t. f1 I1 + ... + fd Id factors linearly,
	-- or an irr. (x) if alpha I1 + I2 factors linearly for q(alpha) = 0
	-- or a machine integer (case fail) if no invariants factors linearly
	local factorsLinearly(invar:INVAR):LINCOMB == {
		import from String;
		import from Z, Product FX, V FX, V FXYZ, Partial FXYZ;
		import from MonogenicAlgebra2(FX, FXY, F, FX);
		import from MonogenicAlgebra2(FXY, FXYZ, FX, FXY);
		import from MonogenicAlgebra2(FXYZ, FXYZT, FXY, FXYZ);
		none:I := 0;
		zero?(d := dimension invar) => [none];
		(ignore, vals) := value invar;
		x:FX := monom;
		y:FXY := monom;
		z:FXYZ := monom;
		v := eval(FXYZ, F2FXYZ, [x::FXY::FXYZ, y::FXYZ, z])(invar);
		assert(d = #v);
		for i in 1..d | ~zero?(vals.i) repeat {
			factorsLinearly?(v.i) => {
				ans:V F := zero d;
				ans.i := 1;
				return [ans];
			}
		}
		one? d or zero? vals => [none];
		d > 2 => error "diffeq3: more than 2 invariants";
		t:FXYZT := monom;
		cand := v.1 * t + (v.2)::FXYZT;
		hh := Hessian cand;
		t0 := ordinaryPoint hh;
		hh0 := hh(t0::FXYZ);
		-- the following a is such that hh(z -> a, t -> t0) <> 0
		a := ordinaryPoint hh0;
		aa := a::FXY;
		h := map((p:FXYZ):FXY +-> p aa)(hh);	-- hh(z -> a);
		iv := map((p:FXYZ):FXY +-> p aa)(cand);	-- cand(z -> a);
		assert(~zero? h); assert(~zero? iv);
		h0 := h(t0::FXY);			-- hh(z -> a, t -> t0);
		lc := leadingCoefficient swap h0;	-- lcoeff wrt x
		b := ordinaryPoint(lc, next abs a);
		bb := b::FX;
		hxt := map((p:FXY):FX +-> p bb)(h);	-- hh(y -> b, z -> a);
		ivxt := map((p:FXY):FX +-> p bb)(iv);	-- cand(y -> b, z -> a);
		assert(~zero? hxt); assert(~zero? ivxt);
		ncond1 := resultant(swap hxt, swap ivxt);
		~zero?(ncond1) and zero? degree ncond1 => [none];
		lc := leadingCoefficient h0;		-- lcoeff wrt y
		c := ordinaryPoint(lc, next abs b);
		cc := c::F;
		hxt := map(map((p:FX):F +-> p cc))(h);	-- hh(x -> c, z -> a);
		ivxt := map(map((p:FX):F +-> p cc))(iv);-- cand(x -> c, z -> a);
		assert(~zero? hxt); assert(~zero? ivxt);
		ncond2 := resultant(swap hxt, swap ivxt);
		~zero?(ncond2) and zero? degree ncond2 => [none];
		ncond1 := gcd(ncond1, ncond2);
		~zero?(ncond1) and zero? degree ncond1 => [none];
		(cont, prod) := factor ncond1;
		-- sort candidates values for t by increasing degree
		fcts:Array FX := [fact pair for pair in prod];
		fcts := sort!(fcts,(p:FX,q:FX):Boolean +-> degree(p)<degree(q));
		for f in fcts repeat {
			if one?(degree f) then {
				zr := - coefficient(f,0) / leadingCoefficient f;
				alpha := zr::FX::FXY::FXYZ;
				factorsLinearly?(cand alpha) => return [[zr,1]];
			}
			else if factorsLinearly?(cand, f) then return [f];
		}
		[none];
	}

	local factorsLinearly?(p:FXYZ):Boolean == {
		import from Partial FXYZ;
		h := Hessian(q := squareFreePart p);
		~failed? exactQuotient(h, q);
	}

	local factorsLinearly?(p:FXYZT, q:FX):Boolean == {
		macro E == SimpleAlgebraicExtension(F, FX, q);
		macro EX == DenseUnivariatePolynomial E;
		macro EXY == DenseUnivariatePolynomial EX;
		macro EXYZ == DenseUnivariatePolynomial EXY;
		macro EXYZT == DenseUnivariatePolynomial EXYZ;
		import from I, Z, E, EX, EXY, EXYZ, EXYZT, Partial EXYZ, M EXYZ;
		import from LinearAlgebra(EXYZ, M EXYZ);
		import from MonogenicAlgebra2(F, FX, E, EX);
		import from MonogenicAlgebra2(FX, FXY, EX, EXY);
		import from MonogenicAlgebra2(FXY, FXYZ, EXY, EXYZ);
		import from MonogenicAlgebra2(FXYZ, FXYZT, EXYZ, EXYZT);
		assert(~zero? p); assert(~zero? q); assert(degree q > 1);
		-- TEMPORARY: BAD COMPILER BUG, THE LAST 2 COERCIONS CRASH
		-- a := (monom$E)::EX::EXY::EXYZ;
		a := term(term((monom$E)::EX, 0), 0);
		cand := map(map map map coerce)(p)(a);
		dx := map map(differentiate$EX);
		dy := map(differentiate$EXY);
		der:Array(EXYZ -> EXYZ) := [dx, dy, differentiate$EXYZ];
		m := zero(3,3);
		for i in 1@I..3 repeat {
			dp := der(prev i)(cand);
			for j in 1@I..3 repeat m(i, j) := der(prev j)(dp);
		}
		~failed?(exactQuotient(determinant m, cand));
	}

	-- exchanges X and Y
	local swap(p:FXY):FXY == {
		q:FXY := 0;
		lift := map((f:F):FX +-> f::FX)$MonogenicAlgebra2(F,FX,FX,FXY);
		for trm in p repeat {
			(px, ny) := trm;
			q := add!(q, monomial(ny)$FX, lift px);
		}
		q;
	}

	local ordinaryPoint(p:FX, n:Z):Z == {
		import from F;
		repeat {
			~zero?(p(n::F)) => return n;
			~zero?(p((-n)::F)) => return(-n);
			n := next n;
		}
		never;
	}

	local Hessian(p:FXYZ):FXYZ == {
		import from I, FXY, M FXYZ, LinearAlgebra(FXYZ, M FXYZ);
		dx := map map(differentiate$FX);
		dy := map(differentiate$FXY);
		der:Array(FXYZ -> FXYZ) := [dx, dy, differentiate$FXYZ];
		m := zero(3,3);
		for i in 1..3 repeat {
			dp := der(prev i)(p);
			for j in 1..3 repeat m(i, j) := der(prev j)(dp);
		}
		determinant m;
	}

	local Hessian(p:FXYZT):FXYZT == {
		import from I, FXY, FXYZ, M FXYZT, LinearAlgebra(FXYZT,M FXYZT);
		dx := map map map(differentiate$FX);
		dy := map map(differentiate$FXY);
		dz := map(differentiate$FXYZ);
		der:Array(FXYZT -> FXYZT) := [dx, dy, dz];
		m := zero(3,3);
		for i in 1..3 repeat {
			dp := der(prev i)(p);
			for j in 1..3 repeat m(i, j) := der(prev j)(dp);
		}
		determinant m;
	}

	symmetricSquareRoot(L:RXD):Partial RXD == {
		import from Z, RX, Rx;
		assert(degree(L) = 3);
		b3 := leadingCoefficient L;
		a1 := coefficient(L, 2) / (3*b3);
		b1 := coefficient(L, 1);
		a0 := b1 / (4*b3) - differentiate(a1)/(4::Rx) - (a1^2)/(2::Rx);
		(2 * differentiate(a0) + 4*a0*a1) = (coefficient(L,0)/b3) => {
			d := lcm(denominator a0, denominator a1);
			[term(d, 2) + term(numerator(d * a1), 1)
						+ numerator(d * a0)::RXD];
		}
		failed;
	}

	local RX2Fx(p:RX):Fx == {
		import from MonogenicAlgebra2(R, RX, F, FX);
		map(inj)(p) :: Fx;
	}

	local eigenDecompose(L:RXD):(FxD, List RIGHTFAC) == {
		import from I, Fx, V M FX, V FXD, V M Fx, V FxD, HEQ;
		import from MonogenicAlgebra2(RX, RXD, Fx, FxD);
		import from MonogenicAlgebraOverFraction(FX, FXD, Fx, FxD);
		import from MatrixCategoryOverFraction(FX, M FX, Fx, M Fx);
		import from EigenringDecomposition(F, FX, FxD),
		     LinearOrdinaryDifferentialEigenring(R,F,inj,RX,RXD,FX,FXD);
		LL := map(RX2Fx)(L);
		cannotHaveExponentialSolution?(L::HEQ) and
			cannotHaveExponentialSolution?(adjoint(L)::HEQ) =>
								(LL, empty);
		(d, em, es) := matrixEigenring L;
		one?(#em) => (LL, empty);		-- trivial eigenring
		d1 := inv RX2Fx d;
		(LL, decompose(LL, [d1*m for m in em], [d1*op for op in es]));
	}

	factorDecomp(L:RXD):FACLODO == {
		import from List RIGHTFAC;
		import from UnivariateSkewPolynomialDecompositionTools(_
					R, F, inj, RX, RXD, FX, FXD, FxD, FxY);
		(Lfrac, l) := eigenDecompose L;
		empty? l => trivialEigenring(L, Lfrac, false);
		factorDec3(Lfrac, l, factorDecomp$LODO2, rightFactor);
	}

	-- adj? = true means this is already the adjoint being tried
	local trivialEigenring(L:RXD, Lfrac:FxD, adj?:Boolean):FACLODO == {
		import from I, Z, FXD, RATRED, ALGRED;
		import from List Cross(Fx, Product FX, V FX);
		import from EXPSOLS(R, F, inj, RX, RXD, FX);
		import from MonogenicAlgebra2(RX, RXD, Fx, FxD);
		import from MonogenicAlgebraOverFraction(FX, FXD, Fx, FxD);
		import from UnivariateSkewPolynomialDecompositionTools(_
					R, F, inj, RX, RXD, FX, FXD, FxD, FxY);
		TRACE("diffeq3::trivialEigenring: adj? = ", adj?);
		TIMESTART;
		es := exponentialSolutions L;
		TIME("diffeq3::trivialEigenring: exponential solutions at ");
		TRACE("diffeq3::trivialEigenring: #es = ", #es);
		empty? es => {
			adj? => [[1, [Lfrac]]];			-- irreducible
			Ladj := adjoint L;
			ans := trivialEigenring(Ladj, map(RX2Fx)(Ladj), true);
			TIME("diffeq3::trivialEigenring:factor of adjoint at ");
			ans case rat => [[1, irrOr12(Lfrac, ans.rat.rat)]];
			-- Ladj = lc(Ladj) * LCLM(D-u,D-ubar) * (D-v)
			assert(ans.alg.mode = 4);
			vec := ans.alg.ratpart;
			assert(#vec = 2);
			LL1 := adjoint(vec.2);		-- left factor of L
			LL2 := leftQuotient(Lfrac, LL1);	-- L = LL1 * LL2
			TIME("diffeq3::trivialEigenring: left quotient at ");
			assert(zero? leftRemainder(Lfrac, LL1));
			(f2, L2) := makeIntegral LL2;		-- L2 = f2 * LL2
			assert(degree(L2) = 2);
			uu := decompose(L2)$LODO2;
			assert(uu case alg);
			-- L = LL1 * LL2 = LL1 * lc(LL2) * LCLM(1-1)
			uu.alg.mode := 3;
			uu.alg.ratpart:= [LL1 * (leadingCoefficient(LL2)::FxD)];
			uu;
		}
		Lr:List FxD := empty;
		for sol in es repeat Lr := append!(Lr, expsol2factors sol);
		factorDecTriv3(Lfrac, Lr, factorDecomp$LODO2);
	}

	-- case where the eigenring is trivial, the operator has no
	-- rational exponential solution, and adjoint decomposes rationally
	local irrOr12(L:FxD, v:V FxD):V FxD == {
		import from I, Z, Fx, FxD;
		n := #v;
		assert(n = 1 or n = 2);
		one? n => [L];				-- irreducible
		assert(degree(v.1) = 2); assert(degree(v.2) = 1);
		Lleft := adjoint(v.2);
		Lright := adjoint(v.1);
		one?(a := leadingCoefficient Lright) => [Lleft, Lright];
		[Lleft * (a::FxD), inv(a) * Lright];
	}

	-- returns u(x,y) such that GCRD(rec.op, rec.eigen - a) = D + u(a,x)
	local rightFactor(rec:ALGFAC, h:FX):Fxy == {
		macro E == SimpleAlgebraicExtension(F, FX, h);
		macro EX == DenseUnivariatePolynomial E;
		macro ExD == LinearOrdinaryDifferentialOperator Fraction EX;
		import from UnivariateSkewPolynomialDecompositionTools(_
					R, F, inj, RX, RXD, FX, FXD, FxD, FxY);
		rightFactor(rec, h, E, ExD);
	}

	decompose(L:RXD):FACLODO == {
		import from UnivariateSkewPolynomialDecompositionTools(_
					R, F, inj, RX, RXD, FX, FXD, FxD, FxY);

		(Lfrac, l) := eigenDecompose L;
		decompose3(Lfrac, l, decompose$LODO2, rightFactor);
	}

	Loewy(L:RXD):V FxD == {
		import from Z, List RIGHTFAC;
		import from UnivariateSkewPolynomialDecompositionTools(_
					R, F, inj, RX, RXD, FX, FXD, FxD, FxY);
		assert(degree L = 3);
		TIMESTART;
		(Lfrac, l) := eigenDecompose L;
		TIME("diffeq3::Loewy: eigenring decomposition at ");
		empty? l => trivialEigenringLoewy(L, Lfrac, false);
		Loewy3(Lfrac, l, Loewy$LODO2);
	}

	-- adj? = true means this is already the adjoint being tried
	local trivialEigenringLoewy(L:RXD, Lfrac:FxD, adj?:Boolean):V FxD == {
		import from List Cross(Fx, Product FX, V FX);
		import from EXPSOLS(R, F, inj, RX, RXD, FX);
		import from MonogenicAlgebra2(RX, RXD, Fx, FxD);
		import from MonogenicAlgebraOverFraction(FX, FXD, Fx, FxD);
		import from UnivariateSkewPolynomialDecompositionTools(_
					R, F, inj, RX, RXD, FX, FXD, FxD, FxY);
		es := exponentialSolutions L;
		empty? es => {
			adj? => [Lfrac];			-- irreducible
			Ladj := adjoint L;
			irrOr12(Lfrac,
			    trivialEigenringLoewy(Ladj,map(RX2Fx)(Ladj),true));
		}
		Lr:List FxD := empty;
		for sol in es repeat Lr := append!(Lr, expsol2factors sol);
		LoewyTriv3(Lfrac, Lr, Loewy$LODO2);
	}

	local expsol2factors(v:Fx, den:Product FX, num:V FX):List FxD ==
		[expsol2factor(v, den, p) for p in num];

	local expsol2factor(v:Fx, den:Product FX, num:FX):FxD ==
		monom - (logder(den, num) + v)::FxD;

	local logder(den:Product FX, num:FX):Fx == {
		import from LinearOrdinaryDifferentialOperatorTools(FX, FXD);
		(numdpd,dendpd) := logarithmicDerivative den;
		differentiate(num) / num - numdpd / dendpd;
	}
}

#if ALDORTEST
-------------------------   test for diffeq3.as   -------------------------
#include "sumit"
#include "aldortest"

macro {
	I == MachineInteger;
	Z == Integer;
	Q == Fraction Z;
	ZX == DenseUnivariatePolynomial Z;
	QX == DenseUnivariatePolynomial Q;
	Qx == Fraction QX;
	QxY == DenseUnivariatePolynomial Qx;
	Qxy == Fraction QxY;
	ZXD == LinearOrdinaryDifferentialOperator ZX;
	QxD == LinearOrdinaryDifferentialOperator Qx;
	RATRED	== Record(mode:I, rat: Vector QxD);
	ALGRED	== Record(mode:I, modulus:QX, c0:Qxy, ratpart:Vector QxD);
	FACLODO	== Union(rat: RATRED, alg: ALGRED);
}

local onetwo():Boolean == {
	import from I, Z, Q, Vector QxD;
	import from ThirdOrderLinearOrdinaryDifferentialSolver(Z, Q, coerce,_
							 ZX, ZXD, QX, QxD, QxY);
	x:ZX := monom;
	D:ZXD := monom;
	L := D*D*D - x*D - 1;
	xx:Qx := monom$QX :: Qx;
	DD:QxD := monom;
	LL := DD*DD*DD - xx*DD - 1;
	v := Loewy L;
	#v = 2 and one?(degree(v.1)) and degree(v.2) = 2
		and one?(leadingCoefficient(v.1))
		and one?(leadingCoefficient(v.2)) and v.1 * v.2 = LL;
}

local LCLM(L1:QxD, L2:QxD):QxD == {
	import from Qx;
	a := leadingCoefficient(L := leftLcm(L1, L2));
	one? a => L;
	inv(a) * L;
}

local lcmonetwored():Boolean == {
	import from I, Z, Q, Vector QxD, RATRED, FACLODO;
	import from ThirdOrderLinearOrdinaryDifferentialSolver(Z, Q, coerce,_
							 ZX, ZXD, QX, QxD, QxY);
	x:ZX := monom;
	D:ZXD := monom;
	L := D*D*D - 2*x*D*D + (x*x-1)*D;
	xx:Qx := monom$QX :: Qx;
	DD:QxD := monom;
	LL := DD*DD*DD - 2*xx*DD*DD + (xx*xx-1)*DD;
	u := factorDecomp L;
	u case rat and u.rat.mode = 2 and #(u.rat.rat) = 3
		and one?(degree(u.rat.rat.1)) and one?(degree(u.rat.rat.2))
		and one?(degree(u.rat.rat.3))
		and one?(leadingCoefficient(u.rat.rat.1))
		and one?(leadingCoefficient(u.rat.rat.2))
		and one?(leadingCoefficient(u.rat.rat.3))
		and LCLM(u.rat.rat.1, u.rat.rat.2 * u.rat.rat.3) = LL;
}

stdout << "Testing sit__diffeq3..." << endnl;
aldorTest("1 * 2-irr", onetwo);
aldorTest("lclm(1, 1 * 1)", lcmonetwored);
stdout << endnl;

#endif

