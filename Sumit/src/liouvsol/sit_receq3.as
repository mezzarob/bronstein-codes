-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
----------------------------- sit_receq3.as ---------------------------------
-- Copyright (c) Manuel Bronstein 2002
-- Copyright (c) INRIA 2002, Version 1.0.1
-- Logiciel Sum^it (c) INRIA 2002, dans sa version 1.0.1
-----------------------------------------------------------------------------

#include "sumit"

#if ALDOC
\thistype{ThirdOrderLinearOrdinaryRecurrenceSolver}
\History{Manuel Bronstein}{30/7/2002}{created}
\Usage{import from \this(R, F, $\iota$, RX, RXE, FX, FxE, FxY)}
\Params{
{\em R} & \alexttype{FactorizationRing}{} & The coefficient ring\\
        & \alexttype{CharacteristicZero}{} &\\
{\em F} & \alexttype{Field}{} & A field containing R\\
        & \alexttype{FactorizationRing}{} &\\
        & \alexttype{CharacteristicZero}{} &\\
$\iota$ & $R \to F$ & Injection from R into F\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
{\em RXE} & \altype{LinearOrdinaryRecurrenceCategory} RX &
Recurrence operators\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
{\em FxE} & \altype{LinearOrdinaryRecurrenceCategory} Fx &
Recurrence operators\\
{\em FxY} &
\alexttype{UnivariatePolynomialCategory}{} \alexttype{Fraction}{} FX &
For returning curves\\
}
\Descr{\this(R, F, $\iota$, RX, RXE, FX, FxE, FxY) provides tools for solving
third order linear ordinary recurrence equations in $R[x,\sigma]$,
where $\sigma$ is the automorphism of $R[x]$ over \emph{R} that
sends \emph{x} to $x+1$.}
\begin{exports}
\category{\altype{UnivariateSkewPolynomialDecompositionCategory}(R, F, $\iota$,
RX, RXE, FX, FxE, FxY)}\\
\end{exports}
#endif

macro {
	I	== MachineInteger;
	Z	== Integer;
	V	== Vector;
	M	== DenseMatrix;
	FXE	== LinearOrdinaryRecurrence(F, FX);
	FFX	== UnivariateFactorialPolynomial(F, FX);
	Rx	== Fraction RX;
	Fx	== Fraction FX;
	Fxy	== Fraction FxY;
	ALGFACT	== Record(modulus:FX, left1: Fxy, left2: Fxy, right: Fxy);
	FACTLODO== Union(rat: V FxE, algRed: ALGFACT);
	ALGFAC	== Record(modulus:FX, mult:Z, eigen:FxE, op:FxE);
	RIGHTFAC== Union(ratfact:FxE, algfact:ALGFAC);
	RATRED	== Record(mode:I, rat: V FxE);
	ALGRED	== Record(mode:I, modulus:FX, c0:Fxy, ratpart:V FxE);
	FACLODO	== Union(rat: RATRED, alg: ALGRED);
	EXPSOLS	== LinearOrdinaryRecurrenceHypergeometricSolutions;
	RECEQ2	== SecondOrderLinearOrdinaryRecurrenceSolver(F, F, ident, FX,_
							FXE, FX, FxE, FxY);
}

ThirdOrderLinearOrdinaryRecurrenceSolver(_
	R:   Join(FactorizationRing, CharacteristicZero),
	F:   Join(Field, FactorizationRing, CharacteristicZero), inj: R -> F,
	RX:  UnivariatePolynomialCategory R,
	RXE: LinearOrdinaryRecurrenceCategory RX,
	FX:  UnivariatePolynomialCategory F,
	FxE: LinearOrdinaryRecurrenceCategory Fx,
	FxY: UnivariatePolynomialCategory Fx):
		UnivariateSkewPolynomialDecompositionCategory(R, F, inj, RX,
						RXE, FX, FxE, FxY) == add {

	local ident(f:F):F			== f;
	local hypsol2factor(g:Fx, p:FX):FxE	== monom - (g * logder p)::FxE;
	local logder(p:FX):Fx	== { import from F; translate(p, -1) / p; }

	local hypsol2factors(gamma:Fx, v:V FX):Set FxE ==
		[hypsol2factor(gamma, p) for p in v];

	local sigman(n:Z):Fx -> Fx == {
		zero? n => (f:Fx):Fx +-> f;
		import from F, FX;
		m := (-n)::F;
		(f:Fx):Fx +->
			translate(numerator f, m) / translate(denominator f, m);
	}

	local RX2Fx(p:RX):Fx == {
		import from MonogenicAlgebra2(R, RX, F, FX);
		map(inj)(p) :: Fx;
	}

	-- returns u(x,y) such that GCRD(rec.op, rec.eigen - a = E + u(a,x)
	local rightFactor(rec:ALGFAC, h:FX):Fxy == {
		macro E == SimpleAlgebraicExtension(F, FX, h);
		macro EX == DenseUnivariatePolynomial E;
		macro ExE == LinearOrdinaryRecurrenceQ(E, EX);
		import from UnivariateSkewPolynomialDecompositionTools(_
					R, F, inj, RX, RXE, FX, FXE, FxE, FxY);
		rightFactor(rec, h, E, ExE);
	}

	local eigenDecompose(L:RXE):(FxE, List RIGHTFAC) == {
		import from I, Fx, V M FFX, V FXE, V M Fx, V FxE;
		import from MonogenicAlgebra2(RX, RXE, Fx, FxE);
		import from MonogenicAlgebraOverFraction(FX, FXE, Fx, FxE);
		import from MatrixCategoryOverFraction(FX, M FX, Fx, M Fx);
		import from EigenringDecomposition(F, FX, FxE),
		     LinearOrdinaryRecurrenceEigenring(R,F,inj,RX,RXE,FX,FXE);
		(d, em, es) := matrixEigenring L;
		LL := map(RX2Fx)(L);
		one?(#em) => (LL, empty);		-- trivial eigenring
		d1 := inv RX2Fx d;
		(LL, decompose(LL, [times(d1, m) for m in em],
						[d1 * op for op in es]));
	}

	local times(d:Fx, m:M FFX):M Fx == {
		import from I, FFX, Fx;
		(r, c) := dimensions m;
		mat:M Fx := zero(r, c);
		for i in 1..r repeat for j in 1..c repeat
			mat(i, j) := expand(m(i, j)) * d;
		mat;
	}

	decompose(L:RXE):FACLODO == {
		import from UnivariateSkewPolynomialDecompositionTools(_
					R, F, inj, RX, RXE, FX, FXE, FxE, FxY);
		(Lfrac, l) := eigenDecompose L;
		decompose3(Lfrac, l, decompose$RECEQ2, rightFactor);
	}

	factorDecomp(L:RXE):FACLODO == {
		import from List RIGHTFAC;
		import from UnivariateSkewPolynomialDecompositionTools(_
					R, F, inj, RX, RXE, FX, FXE, FxE, FxY);
		(Lfrac, l) := eigenDecompose L;
		empty? l => trivialEigenring(L, Lfrac, false);
		factorDec3(Lfrac, l, factorDecomp$RECEQ2, rightFactor);
	}

	-- adj? = true means this is already the adjoint being tried
	local trivialEigenring(L:RXE, Lfrac:FxE, adj?:Boolean):FACLODO == {
		import from I, Z, FXE, RATRED, ALGRED, List Cross(Fx, V FX);
		import from List FxE, EXPSOLS(R, F, inj, RX, RXE, FX);
		import from MonogenicAlgebra2(RX, RXE, Fx, FxE);
		import from MonogenicAlgebraOverFraction(FX, FXE, Fx, FxE);
		import from UnivariateSkewPolynomialDecompositionTools(_
					R, F, inj, RX, RXE, FX, FXE, FxE, FxY);
		TRACE("receq3::trivialEigenring: L = ", L);
		TIMESTART;
		es := allHypergeometricSolutions L;
		TIME("receq3::trivialEigenring: hypergeometric solutions at ");
		TRACE("receq3::trivialEigenring: #hyper-sols = ", #es);
		empty? es => {
			adj? => [[1, [Lfrac]]];			-- irreducible
			Ladj := adjoint L;	-- Ladj = adjoint(L) E^(3+d)
			ans := trivialEigenring(Ladj, map(RX2Fx)(Ladj), true);
			TIME("receq3::trivialEigenring:factor of adjoint at ");
			ans case rat => [[1, irrOr12(Lfrac, ans.rat.rat)]];
			-- Ladj = lc(Ladj) * LCLM(E-u,E-ubar) * (E-v)
			assert(ans.alg.mode = 4);
			vec := ans.alg.ratpart;
			assert(#vec = 2);
			d := trailingDegree L;
			LL1 := map!(sigman(3+d))(adjoint(vec.2)); -- lft-fc of L
			LL2 := leftQuotient(Lfrac, LL1);	-- L = LL1 * LL2
			TIME("receq3::trivialEigenring: left quotient at ");
			assert(zero? leftRemainder(Lfrac, LL1));
			(f2, L2) := makeIntegral LL2;		-- L2 = f2 * LL2
			assert(degree(L2) = 2);
			uu := decompose(L2)$RECEQ2;
			assert(uu case alg);
			-- L = LL1 * LL2 = LL1 * lc(LL2) * LCLM(1-1)
			uu.alg.mode := 3;
			uu.alg.ratpart:= [LL1 * (leadingCoefficient(LL2)::FxE)];
			uu;
		}
		-- Use a set because es could be redundant
		Lr:Set FxE := empty;
		for sol in es repeat Lr := union!(Lr, hypsol2factors sol);
		TRACE("receq3::trivialEigenring: Lr = ", Lr);
		factorDecTriv3(Lfrac, [LL for LL in Lr], factorDecomp$RECEQ2);
	}

	-- case where the eigenring is trivial, the operator has no
	-- rational hypergeometric solution, and adjoint decomposes rationally
	 local irrOr12(L:FxE, v:V FxE):V FxE == {
		import from I, Z, Fx, FxE;
		n := #v;
		assert(n = 1 or n = 2);
		one? n => [L];				-- irreducible
		assert(degree(v.1) = 2); assert(degree(v.2) = 1);
		d := trailingDegree L;
		d1 := trailingDegree(v.2);
		assert(d = d1 + trailingDegree(v.1));
		Lleft := map!(sigman(3+d))(adjoint(v.2));
		Lright := map!(sigman(2+d-d1))(adjoint(v.1));
		one?(a := leadingCoefficient Lright) => [Lleft, Lright];
		[Lleft * (a::FxE), inv(a) * Lright];
	}

	Loewy(L:RXE):V FxE == {
		import from List RIGHTFAC;
		import from UnivariateSkewPolynomialDecompositionTools(_
					R, F, inj, RX, RXE, FX, FXE, FxE, FxY);
		(Lfrac, l) := eigenDecompose L;
		empty? l => trivialEigenringLoewy(L, Lfrac, false);
		Loewy3(Lfrac, l, Loewy$RECEQ2);
	}

	-- adj? = true means this is already the adjoint being tried
	local trivialEigenringLoewy(L:RXE, Lfrac:FxE, adj?:Boolean):V FxE == {
		import from List Cross(Fx, V FX), List FxE;
		import from EXPSOLS(R, F, inj, RX, RXE, FX);
		import from MonogenicAlgebra2(RX, RXE, Fx, FxE);
		import from MonogenicAlgebraOverFraction(FX, FXE, Fx, FxE);
		import from UnivariateSkewPolynomialDecompositionTools(_
					R, F, inj, RX, RXE, FX, FXE, FxE, FxY);
		es := allHypergeometricSolutions L;
		empty? es => {
			adj? => [Lfrac];	-- irreducible
			Ladj := adjoint L;	-- Ladj = adjoint(L) E^(3+d)
			irrOr12(Lfrac,
			    trivialEigenringLoewy(Ladj,map(RX2Fx)(Ladj),true));
		}
		-- Use a set because es could be redundant
		Lr:Set FxE := empty;
		for sol in es repeat Lr := union!(Lr, hypsol2factors sol);
		LoewyTriv3(Lfrac, [LL for LL in Lr], Loewy$RECEQ2);
	}
}

#if ALDORTEST
-------------------------   test for receq3.as   -------------------------
#include "sumit"
#include "aldortest"

macro {
	I == MachineInteger;
	Z == Integer;
	Q == Fraction Z;
	ZX == DenseUnivariatePolynomial Z;
	QX == DenseUnivariatePolynomial Q;
	Qx == Fraction QX;
	QxY == DenseUnivariatePolynomial Qx;
	Qxy == Fraction QxY;
	ZXD == LinearOrdinaryRecurrence(Z, ZX);
	QxD == LinearOrdinaryRecurrenceQ(Q, QX);
	RATRED	== Record(mode:I, rat: Vector QxD);
	ALGRED	== Record(mode:I, modulus:QX, c0:Qxy, ratpart:Vector QxD);
	FACLODO	== Union(rat: RATRED, alg: ALGRED);
}

local oneoneone():Boolean == {
	import from I, Z, Q, Vector QxD;
	import from ThirdOrderLinearOrdinaryRecurrenceSolver(Z, Q, coerce,_
							 ZX, ZXD, QX, QxD, QxY);
	x:ZX := monom;
	D:ZXD := monom;
	L := x*D*D*D + (x-1)*D*D - (x*x*x+2*x*x+x+1)*D + x*x::ZXD;
	xx:Qx := monom$QX :: Qx;
	DD:QxD := monom;
	LL := xx*DD*DD*DD+(xx-1)*DD*DD-(xx*xx*xx+2*xx*xx+xx+1)*DD + xx*xx::QxD;
	v := Loewy L;
	#v = 3 and one?(degree(v.1)) and one?(degree(v.2)) and one?(degree(v.3))
		and leadingCoefficient(v.1) = leadingCoefficient(LL)
		and one?(leadingCoefficient(v.2))
		and one?(leadingCoefficient(v.3)) and v.1 * v.2 * v.3 = LL;
}

local LCLM(L1:QxD, L2:QxD):QxD == {
	import from Qx;
	a := leadingCoefficient(L := leftLcm(L1, L2));
	one? a => L;
	inv(a) * L;
}

local LCLM(h:QX, c:Qxy):QxD == {
	macro K == SimpleAlgebraicExtension(Q, QX, h);
	macro KX == DenseUnivariatePolynomial K;
	macro Kx == Fraction KX;
	macro KxZ == LinearOrdinaryRecurrenceQ(K, KX);
	import from Z, Q, QX, KX, Kx, KxZ;
	import from MonogenicAlgebra2(Q, QX, K, KX);
	import from MonogenicAlgebra2(Qx, QxY, K, KX);
	import from MonogenicAlgebra2(K, KX, Q, QX);
	import from MonogenicAlgebra2(Kx, KxZ, Qx, QxD);
	conj:QX := (-coefficient(h,1)/leadingCoefficient(h))::QX - monom$QX;
	local QX2KX(p:QX):KX == map(coerce$K)(p);
	local Qx2K(f:Qx):K == reduce(numerator f)/reduce(denominator f);
	local QxY2KX(p:QxY):KX == map(Qx2K)(p);
	local Qx2Kbar(f:Qx):K ==
		reduce((numerator f)(conj))/reduce((denominator f)(conj));
	local QxY2KXbar(p:QxY):KX == map(Qx2Kbar)(p);
	local K2Q(p:K):Q == leadingCoefficient lift p;
	local Kx2Qx(q:Kx):Qx == map(K2Q)(numerator q) / map(K2Q)(denominator q);
	L1 := monom + (QxY2KX(numerator c)/QxY2KX(denominator c))::KxZ;
	L2 := monom + (QxY2KXbar(numerator c)/QxY2KXbar(denominator c))::KxZ;
	a := leadingCoefficient(L := leftLcm(L1, L2));
	if ~one?(a) then L := inv(a) * L;
	map(Kx2Qx)(L);
}

local lcmoneoneone():Boolean == {
	import from I, Z, Q, QX, Vector QxD, ALGRED, FACLODO;
	import from ThirdOrderLinearOrdinaryRecurrenceSolver(Z, Q, coerce,_
							 ZX, ZXD, QX, QxD, QxY);
	x:ZX := monom;
	D:ZXD := monom;
	d := (2@Z)::ZX;
	L := D*D*D-(x+d)*D*D+3*(x*x+3*x+d)*D-3*x*(x*x+3*x+d)::ZXD;
	xx:Qx := monom$QX :: Qx;
	DD:QxD := monom;
	dd := (2@Z)::Qx;
	LL :=
	  DD*DD*DD-(xx+dd)*DD*DD+3*(xx*xx+3*xx+dd)*DD-3*xx*(xx*xx+3*xx+dd)::QxD;
	u := factorDecomp L;
	u case alg and zero?(u.alg.mode)
		and one?(#(u.alg.ratpart)) and degree(u.alg.modulus) = 2
		and LCLM(u.alg.ratpart.1, LCLM(u.alg.modulus, u.alg.c0)) = LL;
}

stdout << "Testing sit__receq3..." << endnl;
aldorTest("1 * 1 * 1", oneoneone);
aldorTest("lclm(1, 1, 1)", lcmoneoneone);
stdout << endnl;

#endif

