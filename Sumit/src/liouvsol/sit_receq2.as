-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
----------------------------- sit_receq2.as ---------------------------------
-- Copyright (c) Manuel Bronstein 2002
-- Copyright (c) INRIA 2002, Version 1.0.1
-- Logiciel Sum^it (c) INRIA 2002, dans sa version 1.0.1
-----------------------------------------------------------------------------

#include "sumit"

#if ALDOC
\thistype{SecondOrderLinearOrdinaryRecurrenceSolver}
\History{Manuel Bronstein}{30/7/2002}{created}
\Usage{import from \this(R, F, $\iota$, RX, RXE, FX, FxE, FxY)}
\Params{
{\em R} & \alexttype{FactorizationRing}{} & The coefficient ring\\
        & \alexttype{CharacteristicZero}{} &\\
{\em F} & \alexttype{Field}{} & A field containing R\\
        & \alexttype{FactorizationRing}{} &\\
$\iota$ & $R \to F$ & Injection from R into F\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
{\em RXE} & \altype{LinearOrdinaryRecurrenceCategory} RX &
Recurrence operators\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
{\em FxE} & \altype{LinearOrdinaryRecurrenceCategory} Fx &
Recurrence operators\\
{\em FxY} &
\alexttype{UnivariatePolynomialCategory}{} \alexttype{Fraction}{} FX &
For returning curves\\
}
\Descr{\this(R, F, $\iota$, RX, RXE, FX, FxE, FxY) provides tools for solving
second order linear ordinary recurrence equations in $R[x,\sigma]$,
where $\sigma$ is the automorphism of $R[x]$ over \emph{R} that
sends \emph{x} to $x+1$.}
\begin{exports}
\category{\altype{UnivariateSkewPolynomialDecompositionCategory}(R, F, $\iota$,
RX, RXE, FX, FxE, FxY)}\\
\alexp{factor}: & RXE $\to$
\albuiltin{Union}(rat: \alexttype{Vector}{} FxE, algRed: ALGF) &
Factorization\\
\end{exports}
\begin{alwhere}
ALGF &==& \albuiltin{Record}(modulus:FX, left1:Fxy, left2:Fxy, right:Fxy)\\
Fxy &==& \alexttype{Fraction}{} FxY\\
\end{alwhere}
#endif

macro {
	I	== MachineInteger;
	Z	== Integer;
	V	== Vector;
	M	== DenseMatrix;
	FXE	== LinearOrdinaryRecurrence(F, FX);
	FFX	== UnivariateFactorialPolynomial(F, FX);
	Rx	== Fraction RX;
	Fx	== Fraction FX;
	Fxy	== Fraction FxY;
	ALGFACT	== Record(modulus:FX, left1: Fxy, left2: Fxy, right: Fxy);
	FACTLODO== Union(rat: V FxE, algRed: ALGFACT);
	ALGFAC	== Record(modulus:FX, mult:Z, eigen:FxE, op:FxE);
	RIGHTFAC== Union(ratfact:FxE, algfact:ALGFAC);
	RATRED	== Record(mode:I, rat: V FxE);
	ALGRED	== Record(mode:I, modulus:FX, c0:Fxy, ratpart:V FxE);
	FACLODO	== Union(rat: RATRED, alg: ALGRED);
	EXPSOLS	== LinearOrdinaryRecurrenceHypergeometricSolutions;
}

SecondOrderLinearOrdinaryRecurrenceSolver(
		R:   Join(FactorizationRing, CharacteristicZero),
		F:   Join(Field, FactorizationRing), inj: R -> F,
		RX:  UnivariatePolynomialCategory R,
		RXE: LinearOrdinaryRecurrenceCategory RX,
		FX:  UnivariatePolynomialCategory F,
		FxE: LinearOrdinaryRecurrenceCategory Fx,
		FxY: UnivariatePolynomialCategory Fx):
			UnivariateSkewPolynomialDecompositionCategory(R, F,
					inj, RX, RXE, FX, FxE, FxY) with {
			factor: RXE -> FACTLODO;
#if ALDOC
\alpage{factor}
\Usage{\name~L}
\Signature{RXE}{\albuiltin{Union}(rat: \alexttype{Vector}{} FxE, algRed: ALGF)}
\begin{alwhere}
ALGF &==& \albuiltin{Record}(modulus:FX, left1:Fxy, left2:Fxy, right:Fxy)\\
\end{alwhere}
\Params{ {\em L} & RXE & A second order recurrence operator\\ }
\Descr{Returns one of the following possible results:
\begin{description}
\item[case {\tt rat}:]~returns $[L_1,\dots,L_n]$ such that
$L = L_1 \cdots L_n$ and each $L_i$
is irreducible over $K(x)$, where \emph{K} is the algebraic
closure of \emph{F},
\item[case {\tt algRed}:]~returns $[h(x), a_1(x,y), a_0(x,y), u(x,y)]$, with
$h(x)$ irreducible over \emph{F}, such that
$L(x,E) = (a_1(\alpha, x) E + a_0(\alpha, x)) (E + u(\alpha, x))$
for any root $\alpha$ of $h(x)$.
\end{description}
}
\alseealso{\alfunc{UnivariateSkewPolynomialDecompositionCategory}{decompose},
\alfunc{UnivariateSkewPolynomialDecompositionCategory}{factorDecomp},
\alfunc{UnivariateSkewPolynomialDecompositionCategory}{Loewy}}
#endif
} == add {
	local RX2Fx(p:RX):Fx == {
		import from MonogenicAlgebra2(R, RX, F, FX);
		map(inj)(p) :: Fx;
	}

	local eigenDecompose(L:RXE):(FxE, List RIGHTFAC) == {
		import from I, Fx, V M FFX, V FXE, V M Fx, V FxE;
		import from MonogenicAlgebra2(RX, RXE, Fx, FxE);
		import from MonogenicAlgebraOverFraction(FX, FXE, Fx, FxE);
		import from MatrixCategoryOverFraction(FX, M FX, Fx, M Fx);
		import from EigenringDecomposition(F, FX, FxE),
		     LinearOrdinaryRecurrenceEigenring(R,F,inj,RX,RXE,FX,FXE);
		(d, em, es) := matrixEigenring L;
		LL := map(RX2Fx)(L);
		one?(#em) => (LL, empty);		-- trivial eigenring
		d1 := inv RX2Fx d;
		(LL, decompose(LL, [times(d1, m) for m in em],
						[d1 * op for op in es]));
	}

	local times(d:Fx, m:M FFX):M Fx == {
		import from I, FFX, Fx;
		(r, c) := dimensions m;
		mat:M Fx := zero(r, c);
		for i in 1..r repeat for j in 1..c repeat
			mat(i, j) := expand(m(i, j)) * d;
		mat;
	}

	factorDecomp(L:RXE):FACLODO == {
		import from I, Z, FxE, V FxE, RATRED, ALGRED;
		import from RIGHTFAC, List RIGHTFAC;
		import from UnivariateSkewPolynomialDecompositionTools(_
					R, F, inj, RX, RXE, FX, FXE, FxE, FxY);
		assert(degree L = 2);
		TIMESTART;
		(Lfrac, l) := eigenDecompose L;
		TIME("receq2::factorDecomp: eigenring decomposition at ");
		empty? l => [trivialEigenring(L, Lfrac)];
		u := first l;
		l := rest l;
		u case algfact => {
			assert(empty? l);
			algf := u.algfact;
			assert(degree(algf.modulus) = 2);
			[[0,algf.modulus,rightFactor(algf,algf.modulus),empty]];
		}
		LL2 := normalize(u.ratfact);		-- right-factor of L
		assert(one? degree LL2);
		empty? l => {				-- not CR-red
			assert(zero? rightRemainder(Lfrac, LL2));
			[[1, [rightQuotient(Lfrac, LL2), LL2]]];
		}
		u := first l;
		assert(empty? rest l);
		assert(u case ratfact);
		assert(one? degree(u.ratfact));
		[[0, [LL2, normalize(u.ratfact)]]];	-- LCLM(LL2, u.ratfact)
	}

	local trivialEigenring(L:RXE, Lfrac:FxE):RATRED == {
		import from I, Fx, Partial Fx;
		import from EXPSOLS(R, F, inj, RX, RXE, FX);
		TIMESTART;
		es := hypergeometricSolution L;
		TIME("receq2::trivialEigenring: hypergeometric solutions at ");
		failed? es => [1, [Lfrac]];			-- irreducible
		LL2 := monom - retract(es)::FxE;
		assert(zero? rightRemainder(Lfrac, LL2));
		[1, [rightQuotient(Lfrac, LL2), LL2]];
	}

	-- returns u(x,y) such that GCRD(rec.op, rec.eigen - a = E + u(a,x)
	local rightFactor(rec:ALGFAC, h:FX):Fxy == {
		macro E == SimpleAlgebraicExtension(F, FX, h);
		macro EX == DenseUnivariatePolynomial E;
		macro ExE == LinearOrdinaryRecurrenceQ(E, EX);
		import from UnivariateSkewPolynomialDecompositionTools(_
					R, F, inj, RX, RXE, FX, FXE, FxE, FxY);
		rightFactor(rec, h, E, ExE);
	}

	decompose(L:RXE):FACLODO == {
		import from I, Z, FxE, V FxE, RATRED, ALGRED;
		import from RIGHTFAC, List RIGHTFAC;
		import from UnivariateSkewPolynomialDecompositionTools(_
					R, F, inj, RX, RXE, FX, FXE, FxE, FxY);
		assert(degree L = 2);
		TIMESTART;
		(Lfrac, l) := eigenDecompose L;
		TIME("receq2::decompose: eigenring decomposition at ");
		empty? l => [[1, [Lfrac]]];			-- not CR-red
		u := first l;
		l := rest l;
		u case algfact => {
			assert(empty? l);
			algf := u.algfact;
			assert(degree(algf.modulus) = 2);
			[[0,algf.modulus,rightFactor(algf,algf.modulus),empty]];
		}
		empty? l => [[1, [Lfrac]]];		-- not CR-red
		LL2 := normalize(u.ratfact);		-- right-factor of L
		assert(one? degree LL2);
		u := first l;
		assert(empty? rest l);
		assert(u case ratfact);
		assert(one? degree(u.ratfact));
		[[0, [LL2, normalize(u.ratfact)]]];	-- LCLM(LL2, u.ratfact)
	}

	Loewy(L:RXE):V FxE == {
		import from UnivariateSkewPolynomialDecompositionTools(_
					R, F, inj, RX, RXE, FX, FXE, FxE, FxY);
		Loewy2(L, factorDecomp);
	}

	factor(L:RXE):FACTLODO == {
		import from UnivariateSkewPolynomialDecompositionTools(_
					R, F, inj, RX, RXE, FX, FXE, FxE, FxY);
		factor2(L, algFactor, factorDecomp);
	}

	-- returns [h(x), L1(x,y)[D], L2(x,y)[D]] such that
	-- L = lc(L) LCLM_{h(a)=0}(D + u(a,x)) = L1(a,x)[D] * L2(a,x)[D]
	local algFactor(L:RXE, u:Fxy, h:FX):ALGFACT == {
		macro E == SimpleAlgebraicExtension(F, FX, h);
		macro EX == DenseUnivariatePolynomial E;
		macro ExE == LinearOrdinaryRecurrenceQ(E, EX);
		import from UnivariateSkewPolynomialDecompositionTools(_
					R, F, inj, RX, RXE, FX, FXE, FxE, FxY);
		algFactor2(L, u, h, E, ExE);
	}
}
