-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
----------------------------- sit_diffeq2.as --------------------------------
-- Copyright (c) Manuel Bronstein 2000
-- Copyright (c) INRIA 2000, Version 0.1.12
-- Logiciel Sum^it (c) INRIA 2000, dans sa version 0.1.12
-----------------------------------------------------------------------------

#include "sumit"

#if ALDOC
\thistype{SecondOrderLinearOrdinaryDifferentialSolver}
\History{Manuel Bronstein}{2/7/96}{created}
\Usage{import from \this(R, F, $\iota$, RX, RXD, FX, FxD, FxY)}
\Params{
{\em R} & \alexttype{GcdDomain}{} & The coefficient ring\\
        & \alexttype{RationalRootRing}{} &\\
        & \alexttype{CharacteristicZero}{} &\\
{\em F} & \alexttype{Field}{} & A field containing R\\
        & \alexttype{RationalRootRing}{} &\\
        & \alexttype{CharacteristicZero}{} &\\
$\iota$ & $R \to F$ & Injection from R into F\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
{\em RXD} & LODOCAT RX & Differential operators\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
{\em FxD} & LODOCAT \alexttype{Fraction}{} FX & For returning results\\
{\em FxY} &
\alexttype{UnivariatePolynomialCategory}{} \alexttype{Fraction}{} FX &
For returning curves\\
}
\begin{alwhere}
LODOCAT &==& \altype{LinearOrdinaryDifferentialOperatorCategory}\\
\end{alwhere}
\Descr{\this(R, F, $\iota$, RX, RXD, FX, FxD, FxY) provides tools for solving
second order linear ordinary differential equations in $R[x,\frac d{dx}]$.}
\begin{exports}
\alexp{Darboux}: & RXD $\to$ \alexttype{List}{} FxY & Darboux polynomials\\
\alfunc{UnivariateSkewPolynomialDecompositionCategory}{decompose}:
& RXD $\to$ \albuiltin{Union}(rat:RAT, alg:ALG) & LCLM decomposition\\
\end{exports}
\begin{exports}
[if \emph{R} has \alexttype{FactorizationRing}{}
and \emph{F} has \alexttype{FactorizationRing}{} then]
\category{\altype{UnivariateSkewPolynomialDecompositionCategory}(R, F, $\iota$,
RX, RXD, FX, FxD, FxY)}\\
\alexp{factor}: & RXD $\to$
\albuiltin{Union}(rat: \alexttype{Vector}{} FxD, algRed: ALGF) &
Factorization\\
\end{exports}
\begin{alwhere}
I &==& \alexttype{MachineInteger}{}\\
RAT &==& \albuiltin{Record}(mode:I, rat: \alexttype{Vector}{} FxD)\\
ALG &==& \albuiltin{Record}(mode:I, modulus:FX, c0:\alexttype{Fraction}{} FxY,
ratpart: \alexttype{Vector}{} FxD)\\
ALGF &==& \albuiltin{Record}(modulus:FX, left1:Fxy, left2:Fxy, right:Fxy)\\
Fxy &==& \alexttype{Fraction}{} FxY\\
\end{alwhere}
#endif

macro {
	V	== Vector;
	I	== MachineInteger;
	Z	== Integer;
	RR	== FractionalRoot;
	HEQ	== LinearHolonomicDifferentialEquation(R, F, inj, RX, RXD);
	RXY	== DenseUnivariatePolynomial RX;
	FXD	== LinearOrdinaryDifferentialOperator FX;
	Rx	== Fraction RX;
	Fx	== Fraction FX;
	Fxy	== Fraction FxY;
	ALGFACT	== Record(modulus:FX, left1: Fxy, left2: Fxy, right: Fxy);
	FACTLODO== Union(rat: V FxD, algRed: ALGFACT);
	RATRED	== Record(mode:I, rat: V FxD);
	ALGRED	== Record(mode:I, modulus:FX, c0:Fxy, ratpart:V FxD);
	FACLODO	== Union(rat: RATRED, alg: ALGRED);
	EXPSOLS	== LinearOrdinaryDifferentialOperatorExponentialSolutions;
	RFAC	== R pretend Join(FactorizationRing, CharacteristicZero);
	FFAC	== F pretend Join(Field, FactorizationRing, CharacteristicZero);
	RATSOL	== LinearOrdinaryDifferentialOperatorRationalSolutions;
	IRRSOL	== Record(grp:I, exp:Fx, const:F, pullback:Fx);
	SOLUTION== Union(red:List FxY, irr:IRRSOL);
}

-- Group return codes for irreducibleUnimodular
macro {
	SL2		== 1;
	S4		== 2;
	A4		== 3;
	A5		== 4;
	D2		== 5;
	DINF		== 6;
	DIHEDRAL	== 7;
}

SecondOrderLinearOrdinaryDifferentialSolver(_
	R: Join(GcdDomain,RationalRootRing, CharacteristicZero),
	F:   Join(Field, RationalRootRing, CharacteristicZero), inj: R -> F,
	RX:  UnivariatePolynomialCategory R,
	RXD: LinearOrdinaryDifferentialOperatorCategory RX,
	FX:  UnivariatePolynomialCategory F,
	FxD: LinearOrdinaryDifferentialOperatorCategory Fx,
	FxY: UnivariatePolynomialCategory Fx): with {
		Darboux: RXD -> List FxY;
#if ALDOC
\alpage{Darboux}
\Usage{\name~L}
\Signature{RXD}{\alexttype{List}{} FxY}
\Params{ {\em L} & RXD & A second order differential operator\\ }
\Retval{Returns rational Darboux polynomials for \emph{L} of lowest
possible degree.}
\Remarks{
If \name(L) returns $[]$, then this proves that $L$ is irreducible and has no
nontrivial Darboux curves, and hence that $L y = 0$ has no Liouvillian
solutions.
When \emph{R} or \emph{F} does not have \alexttype{FactorizationRing}{},
then irreducibility cannot always be proven, and it is possible
that \name(L) returns $[0]$, which means that either \emph{L}
is irreducible, or \emph{L} is not completely reducible, \ie~it has
a unique factorisation as a product of two first order operators.
}
#endif
		decompose: RXD -> FACLODO;
		if R has FactorizationRing and F has FactorizationRing then {
			UnivariateSkewPolynomialDecompositionCategory(RFAC,_
					 FFAC, inj, RX, RXD, FX, FxD, FxY);
			factor: RXD -> FACTLODO;
#if ALDOC
\alpage{factor}
\Usage{\name~L}
\Signature{RXD}{\albuiltin{Union}(rat: \alexttype{Vector}{} FxD, algRed: ALGF)}
\begin{alwhere}
ALGF &==& \albuiltin{Record}(modulus:FX, left1:Fxy, left2:Fxy, right:Fxy)\\
\end{alwhere}
\Params{ {\em L} & RXD & A second order differential operator\\ }
\Descr{Returns one of the following possible results:
\begin{description}
\item[case {\tt rat}:]~returns $[L_1,\dots,L_n]$ such that
$L = L_1 \cdots L_n$ and each $L_i$
is irreducible over $E(x)$, where \emph{E} is the algebraic
closure of \emph{F}, 
\item[case {\tt algRed}:]~returns $[h(x), a_1(x,y), a_0(x,y), u(x,y)]$, with
$h(x)$ irreducible over \emph{F}, such that
$L(x,D) = (a_1(\alpha, x) D + a_0(\alpha, x)) (D + u(\alpha, x))$
for any root $\alpha$ of $h(x)$.
\end{description}
}
\alseealso{\alfunc{UnivariateSkewPolynomialDecompositionCategory}{decompose},
\alfunc{UnivariateSkewPolynomialDecompositionCategory}{factorDecomp},
\alfunc{UnivariateSkewPolynomialDecompositionCategory}{Loewy}}
#endif
		}
		solve: RXD -> SOLUTION;
} == add {
	local ident(f:F):F	== f;
	local ident(f:Fx):Fx	== f;
	local Rx2Fx(f:Rx):Fx	== RX2FX(numerator f) / RX2FX(denominator f);
	local RX2Fx(p:RX):Fx	== RX2FX(p)::Fx;
	local Fx2op(u:Fx):FxD	== monom + u::FxD;
	local Fx2Fxy(f:Fx):Fxy	== FX2FxY(numerator f) / FX2FxY(denominator f);
	local F2Fx(f:F):Fx	== { import from FX; f::FX::Fx }

	local factor?:Boolean ==
		R has FactorizationRing and F has FactorizationRing;

	local FX2FxY(p:FX):FxY == {
		import from MonogenicAlgebra2(F, FX, Fx, FxY);
		map(F2Fx)(p);
	}

	local RX2FX(p:RX):FX == {
		import from MonogenicAlgebra2(R, RX, F, FX);
		map(inj)(p);
	}

	local logder(den:Product FX, num:FX):Fx == {
		import from LinearOrdinaryDifferentialOperatorTools(FX, FXD);
		(numdpd, dendpd) := logarithmicDerivative den;
		differentiate(num) / num - numdpd / dendpd;
	}

	-- returns -v'/v for each v = p/den for p in num
	local mlogder(den:Product RX, num:V FX):List Fx == {
		import from FX, Fx;
		import from LinearOrdinaryDifferentialOperatorTools(RX, RXD);
		TIMESTART;
		-- for p in num the corresponding solution is v = p / d.
		--  All we need is -v'/v which is the logarithmic
		-- derivative of 1/v = d / p, whence -v'/v = d'/d - p'/p
		(numdpd, dendpd) := logarithmicDerivative den;
		fdpd := RX2FX(numdpd) / RX2FX(dendpd);
		TIME("diffeq2::mlogder: denominator expanded at ");
		[fdpd - differentiate(p) / p for p in num];
	}

	decompose(L:RXD):FACLODO == {
		import from I, HEQ, RATRED, V FxD;
		import from MonogenicAlgebra2(RX, RXD, Fx, FxD);
		(u, ignore1, ignore2, ignore3) := innerDecompose(L::HEQ);
		u case alg => u;
		zero?(u.rat.mode) => u;		-- LCLM
		assert(one?(u.rat.mode)); assert(empty?(u.rat.rat));
		[[0, [map(RX2Fx)(L)]]];
	}

	-- the last 3 results are only needed by Darboux:
	--     only needs to return darboux curves in case algRed
	--        or when an irreducible curve of degree 2 has been found
	--     only needs to return unimodular operator in case irred
	-- returns [1, []] whenever E is either irreducible or (non-CR,no Fring)
	local innerFactor(L:RXD):(FACLODO, List FxY, HEQ, Fx) == {
		import from RATRED, V FxD;
		TRACE("diffeq2::innerFactor:L = ", L);
		(ans, darb, U, s) := innerDecompose(E := L::HEQ);
		ans case rat and empty?(ans.rat.rat) => (irrOrNonCr E,darb,U,s);
		(ans, darb, U, s);
	}

	-- the last 3 results are only needed by Darboux:
	--     only needs to return darboux curves in case alg
	--        or when an irreducible curve of degree 2 has been found
	--     only needs to return unimodular operator in case irred
	-- returns [1, []] whenever E is either irreducible or non-CR
	-- a HEQ value of 0 means that the operator has not been unimodularized
	local innerDecompose(E:HEQ):(FACLODO, List FxY, HEQ, Fx) == {
		import from I, Z, RXD, Rx, V FX, List Fx, Partial FX;
		import from Product RX, Fx, FxY, List FxY, V FxD, RATRED;
		import from RATSOL(R, F, inj, RX, RXD, FX);
		import from RATSOL(F, F, ident, FX, FXD, FX);
		TRACE("diffeq2::innerDecompose:E = ", E);
		TIMESTART;
		L := operator E;
		TRACE("diffeq2::innerDecompose: L = ", L);
		assert(degree L = 2);
		cannotHaveExponentialSolution? E =>
			([[1, empty]], empty, 0::HEQ, 0);	-- irreducible
		-- check for rational solutions before transforming
		-- the equation to unimodular, since those might disappear
		(den, num) := kernel E;
		m := #num;
		TRACE("diffeq2::innerDecompose: #rational solutions = ", m);
		TIME("diffeq2::innerDecompose: rational kernel at ");
		assert(m >= 0); assert(m <= 2);
		m = 2 => {
			l := mlogder(den, num);
			([[0, [Fx2op u for u in l]]], empty, E, 0);
		}
		m = 1 => {
			TRACE("diffeq2::innerDecompose: num = ", num);
			TRACE("diffeq2::innerDecompose: den = ", den);
			u := first mlogder(den, num);	-- -f'/f, f = rat sol
			TRACE("diffeq2::innerDecompose: -f'/f = ", u);
			a := Rx2Fx(coefficient(L, 1) / leadingCoefficient L);
			TRACE("diffeq2::innerDecompose: a = ", a);
			b := a - u;
			TRACE("diffeq2::innerDecompose: b = a + f'/f = ", b);
			Lr := Fx2op u;
			unimodular? E => ([[1, [Fx2op b, Lr]]], empty, E, 0);
			-- solve the Risch DE  y' - (a + f'/f) y = 1/f
			(nden, dden) := expandFraction den; -- f=num.1*dden/nden
			f1 := RX2FX(nden) / (RX2FX(dden) * num.1);	-- 1/f
			TRACE("diffeq2::innerDecompose: 1/f = ", f1);
			db := denominator b;
			L1:FXD := term(db, 1) - numerator(b)::FXD;
			TRACE("diffeq2::innerDecompose: L1 = ", L1);
			TRACE("diffeq2::innerDecompose: rhs = ", db * f1);
			(fden, pnum) := particularSolution(L1, db * f1);
			failed? pnum => ([[1, [Fx2op b, Lr]]], empty, E, 0);
			y := logder(fden, retract pnum);
			TRACE("diffeq2::innerDecompose: y = ", y);
			([[0, [Fx2op(a - y), Lr]]], empty, E, 0);
		}
		(ignor, U, ss) := unimodularize E;
		s := Rx2Fx ss;
		TIME("diffeq2::innerDecompose: unimodular operator at ");
		TRACE("diffeq2::innerDecompose: U = ", U);
		TRACE("diffeq2::innerDecompose: s = ", s);
		if zero? s then m := 0;		-- op was already unimodular
		else {
			(den, num) := kernel U;
			m := #num;
		}
		TIME("diffeq2::innerDecompose: unimodular rational kernel at ");
		m = 2 => {
			l := mlogder(den, num);
			([[0, [Fx2op(uu - s) for uu in l]]], empty, U, s);
		}
		m = 1 => {
			u := first mlogder(den, num);	-- -f'/f, f = rat sol
			v := u - s;
			aa := Rx2Fx(coefficient(L, 1) / leadingCoefficient L);
			([[1, [Fx2op(aa - v), Fx2op v]]], empty, U, s);
		}
		(ignore, den, num) := symmetricKernel(U, 2);
		m := #num;
		TIME("diffeq2::innerDecompose: quadratic kernel at ");
		m > 0 => {	-- operator must be reducible
			assert(m = 1 or m = 3);
			mvpv := first mlogder(den, num);
			aa := Rx2Fx(coefficient(L, 1) / leadingCoefficient L);
			crv := Darboux(operator U, 2, mvpv);
			ans := factor(crv, aa, s);
			ans case rat => (ans, empty, U, s);
			(ans, [translate(crv, s)], U, s);
		}
		([[1, empty]], empty, U, s);	-- irreducible or non-CR
	}

	-- p is a monic Darboux curve of degree <= 2
	-- it is of degree 1 only if the quadratic curve was a square
	-- p comes from a unimodular op, which must be reducible
	local factor(p:FxY, a:Fx, s:Fx):FACLODO == {
		import from I, Z, F, FX, Fx, FxY, FxY, Fxy, V FxD, Partial F;
		assert(degree p <= 2); assert(one? leadingCoefficient p);
		TRACE("diffeq2::factor: p = ", p);
		TRACE("diffeq2::factor: a = ", a);
		TRACE("diffeq2::factor: s = ", s);
		zp1? := zero?(p1 := coefficient(p, 1));
		TRACE("diffeq2::factor: p1 = ", p1);
		p0 := coefficient(p, 0);
		TRACE("diffeq2::factor: p0 = ", p0);
		one? degree p => {
			v := (p0 / p1) - s;		-- p0/p1 = -f'/f, f sol
			[[1, [Fx2op(a - v), Fx2op v]]];
		}
		delta := { zp1? => - p0; p1^(2@I) - 4 * p0; }
		TRACE("diffeq2::factor: delta = ", delta);
		half := inv(2::Fx);
		zero? delta => {
			v := half * p1 - s;		-- p1/2 = -f'/f, f sol
			[[1, [Fx2op(a - v), Fx2op v]]];
		}
		(sq?, c, sqr) := isSquare delta;
		TRACE("diffeq2::factor: sq? = ", sq?);
		TRACE("diffeq2::factor: c = ", c);
		TRACE("diffeq2::factor: sqr = ", sqr);
		assert(sq?);	-- dixit JAW, in contradiction with JSC 22
		sq? => {	-- curve factors (test is not really necessary)
			failed?(sqrc := isSquare c) => {
				local q:Fxy;
				local gcy:Fxy;
				if zp1? then {
					gcy := Fx2Fxy sqr;
					q := Fx2Fxy(-s);
				}
				else {
					gcy := Fx2Fxy(half * sqr);
					q := Fx2Fxy(half * p1 - s);
				}
				modul:FX := monomial(2) - c::FX;
				alpha := (monom$FX)::Fx::FxY;
				[[0, modul, q + alpha * gcy, empty]];
			}
			gc := retract(sqrc)::FX * sqr;	-- sqrt(delta)
			zp1? => [[0, [Fx2op(-gc - s), Fx2op(gc - s)]]];
			[[0, [Fx2op(half * (p1 - gc) - s),
						Fx2op(half * (p1 + gc) - s)]]];
		}
		never;		-- dixit JAW, in contradiction with JSC 22
	}

	if R has FactorizationRing and F has FactorizationRing then {
		factor(L:RXD):FACTLODO == {
			import from UnivariateSkewPolynomialDecompositionTools(_
				RFAC, FFAC, inj, RX, RXD, FX, FXD, FxD, FxY);
			factor2(L, algFactor, factorDecomp);
		}

		-- returns [h(x), L1(x,y)[D], L2(x,y)[D]] such that
		-- L = lc(L) LCLM_{h(a)=0}(D + u(a,x)) = L1(a,x)[D] * L2(a,x)[D]
		local algFactor(L:RXD, u:Fxy, h:FX):ALGFACT == {
			macro E == SimpleAlgebraicExtension(F, FX, h);
			macro EX == DenseUnivariatePolynomial E;
			macro Ex == Fraction EX;
			macro ExD == LinearOrdinaryDifferentialOperator Ex;
			import from UnivariateSkewPolynomialDecompositionTools(_
				RFAC, FFAC, inj, RX, RXD, FX, FXD, FxD, FxY);
			algFactor2(L, u, h, E, ExD);
		}

		factorDecomp(L:RXD):FACLODO == {
			import from I, RX, FxD, V FxD, RATRED;
			import from MonogenicAlgebra2(RX, RXD, Fx, FxD);
			(ans, ignore1, ignore2, ignore3) := innerFactor L;
			ans case alg => ans;
			empty?(ans.rat.rat) => [[1, [map(RX2Fx)(L)]]];
			one?(ans.rat.mode) => {
				one?(lc := leadingCoefficient L) => ans;
				ans.rat.rat.1 := RX2Fx(lc) * ans.rat.rat.1;
				ans;
			}
			ans;
		}

		Loewy(L:RXD):V FxD == {
			import from UnivariateSkewPolynomialDecompositionTools(_
				RFAC, FFAC, inj, RX, RXD, FX, FXD, FxD, FxY);
			Loewy2(L, factorDecomp);
		}

		irrOrNonCr(E:HEQ):FACLODO == {
			import from I, Z, Rx, RXD, Fx, V FX, V FxD;
			import from List Cross(Fx, Product FX, V FX);
			import from EXPSOLS(RFAC, FFAC, inj, RX, RXD, FX);
			TIMESTART;
			l := exponentialSolutions E;
			TIME("diffeq2::irrOrNonCr: exponential solutions at ");
			empty? l => [[1, empty]];
			assert(empty? rest l);
			(v, den, num) := first l;
			assert(#num = 1);
			u := logder(den, num.1) + v;
			TIME("diffeq2::irrOrNonCr: logarithmic derivative at ");
			L := operator E;
			a := coefficient(L, 1) / leadingCoefficient L;
			[[1, [Fx2op(Rx2Fx(a) + u), Fx2op(-u)]]];
		}
	}
	else { irrOrNonCr(E:HEQ):FACLODO == [[1, empty$V(FxD)]$RATRED] }

	if F has FactorizationRing then {
		isSquare(f:F):Partial F == {
			import from I, Z, FX, Product FX;
			assert(~zero? f);
			(c, prod) := factor(monomial(2) - f::FX);
			#prod = 1 => failed;
			for trm in prod repeat {
				(q, n) := trm;
				assert(n = 1); assert(degree q = 1);
				return [-coefficient(q,0)/leadingCoefficient q];
			}
			never;
		}
	}
	else { isSquare(f:F):Partial F == { one? f => [f]; failed; } }

	-- for reducible ops, returns a list of Darboux curves
	-- for irreducible ops, returns (GROUP,m,v,c,f,s) to describe the solution
	-- v^(1/m) H(c^(1/2) f) e^{\int s}  where H is a fundamental sol of std_Group
	-- (except for D2 where H is a fundamental sol of std_A4)
	-- returns [empty] is RX has no factorisation algorithm and the
	-- operator is either irreducible with no solution or reducible 1-1
	-- When RX has a factorisation algorithm, returns a complete answer
	-- i.e. [SL2,0,0,0,0,s] means no Darboux curve (s=0 means unimodular)
	solve(L:RXD):SOLUTION == {
		import from HEQ, List FxY;
		TRACE("diffeq2::solve:L = ", L);
		(ans, darb, U, s) := Darboux(L, false);
		empty? ans => {			-- irreducible
			if zero? operator U then {	-- not yet unimodularized
				(ignore, U, ss) := unimodularize(L::HEQ);
				s := Rx2Fx ss;
			}
			(group, v, c, f) := irreducibleUnimodular U;
			group = SL2 => {
				factor? => [[group, v+s, c, f]];
				empty? darb => [empty];
			 	[darb];	-- irreducible (?) degree 2 curve
			}
			[[group, v+s, c, f]];
		}
		[ans];
	}

	-- solver for unimodular and assumed irreducible inputs
	-- returns (GROUP, v, c, f) to describe the solution
	--   exp(int(v)) H(c^(1/2) f) where H is a fundamental sol of std_Group
	-- (except for D2 where H is a fundamental sol of std_A4)
	local irreducibleUnimodular(L:HEQ):(I, Fx, F, Fx) == {
		import from Z, List Z, I, V FX, RXD, RATSOL(R, F, inj, RX, RXD, FX);
		LL := operator L;
		assert(degree LL = 2);
		TIMESTART;
		(ignore, den, num) := symmetricKernel(L, 4);
		m := #num;
		TIME("diffeq2::irreducibleUnimodular: quartic kernel at ");
		noRat? := ~allRationalExponents? L;
		one? m => {
			(group, v, c, f) := irrSolve(LL, 4, den, num);
			if noRat? then group := DINF;
			(group, v, c, f);
		}
		d2? := m > 1;
		zero? m and noRat? => (SL2, 0, 0, 0);
		for n in [6, 8, 12] repeat {
			(ignore, den, num) := symmetricKernel(L, n);
			TIME("diffeq2::irreducibleUnimodular: symmetric kernel at ");
			assert(1 >= #num);
			0 < #num => {
				(group, v, c, f) := irrSolve(LL, n, den, num);
				if d2? then group := D2;
				return (group, v, c, f);
			}
		}
		(SL2, 0, 0, 0);
	}

	local irrSolve(L:RXD, n:Z, den:Product RX, num:V FX):(I, Fx, F, Fx) == {
		import from FX, Fx, FXD, MonogenicAlgebra2(RX, RXD, FX, FXD);
		import from LinearOrdinaryDifferentialOperatorTools(RX, RXD);
		-- import from LinearOrdinaryDifferentialOperatorTools(FX, FXD);
		TRACE("diffeq2::irrSolve:L = ", L);
		TRACE("diffeq2::irrSolve:n = ", n);
		TRACE("diffeq2::irrSolve:den = ", den);
		TRACE("diffeq2::irrSolve:num = ", num);
		assert(n >= 4);
		-- compute dvv = logarithmic derivative of the invariant num.1/den
		(numdpd, dendpd) := logarithmicDerivative den;
		dvv := differentiate(num.1) / num.1 - RX2FX(numdpd) / RX2FX(dendpd);
		TRACE("diffeq2::irrSolve:dvv = ", dvv);
		dvnv := dvv / (n::Fx);
		lcL := RX2FX leadingCoefficient L;
		b1 := RX2FX(coefficient(L,1)) / lcL;
		-- compute L(D + v'/n v) = D^2 + a1 D + a0
		a0 := RX2FX(coefficient(L,0)) / lcL +
					b1 * dvnv + dvnv^(2@I) + differentiate dvnv;
		TRACE("diffeq2::irrSolve:a0 = ", a0);
		n = 4 => (DIHEDRAL, dvnv, 1, -a0);
		a1 := b1 + 2 * dvnv;
		TRACE("diffeq2::irrSolve:a1 = ", a1);
		gl2 := (2 * a1 + differentiate(a0)/a0)^(2@I);
		TRACE("diffeq2::irrSolve:gl^2 = ", gl2);
		n = 6 => {
			(sqr?, c, g) := isSquare(1 + (64 * a0) / (5 * gl2));
			TRACE("diffeq2::irrSolve:sqr? = ", sqr?);
			TRACE("diffeq2::irrSolve:c = ", c);
			TRACE("diffeq2::irrSolve:g = ", g);
			assert(sqr?);
			(A4, dvnv, c, g);
		}
		n = 8 => (S4, dvnv, 1, (-7 * gl2) / (144 * a0));
		n = 12 => (A5, dvnv, 1, (11 * gl2) / (400 * a0));
		never;
	}

	-- returns [0] is RX has no factorisation algorithm and the
	-- operator is either irreducible with no solution or reducible 1-1
	-- When RX has a factorisation algorithm, returns a complete answer
	-- i.e. [] means no Darboux curve
	Darboux(L:RXD):List FxY	== {
		(darb, ignore1, ignore2, ignore3) := Darboux(L, true);
		darb;
	}

	local Darboux(L:RXD, irredAlso?:Boolean):(List FxY, List FxY, HEQ, Fx) == {
		import from HEQ, RATRED, FACLODO, FxY;
		import from MonogenicAlgebra2(Fx, FxD, Fx, FxY);
		TRACE("diffeq2::Darboux:L = ", L);
		(ans, darb, U, s) := innerFactor L;
		ans case alg => (darb, darb, U, s);	-- reducible CR over alg ext
		empty?(v := ans.rat.rat) => {	-- irreducible
			irredAlso? => (irrDarboux(L, darb, U, s), darb, U, s);
			(empty, darb, U, s);
		}
		-- reducible over ground field, ans.rat.rat = [L1,L2]
		c1 := map(ident)(ans.rat.rat.2);
		zero?(ans.rat.mode) =>		-- CR, L = LLCM(L1, L2)
			([c1, map(ident)(ans.rat.rat.1)], darb, U, s);
		([c1], darb, U, s);	-- not completely reducible, L = L1 * L2
	}

	local irrDarboux(L:RXD, darb:List FxY, U:HEQ, s:Fx):List FxY == {
		if zero? operator U then {	-- not yet unimodularized
			(ignore, U, ss) := unimodularize(L::HEQ);
			s := Rx2Fx ss;
		}
		c := irrDarboux U;
		empty? c => {
			factor? => c;	-- proven irreducible
			empty? darb => [0$FxY];
			darb;	-- irreducible (?) degree 2 curve
		}
		translate(c, s);
	}

	-- convert curves of the unimodular transform to curves of the operator
	local translate(curves:List FxY, s:Fx):List FxY == {
		import from FxY;
		zero? s => curves;
		map((p:FxY):FxY +-> translate(p, s))(curves);
	}

	-- Darboux curves for unimodular and assumed irreducible inputs
	local irrDarboux(L:HEQ):List FxY == {
		import from Z, List Z, I, V FX, RXD, RATSOL(R, F, inj, RX, RXD, FX);
		LL := operator L;
		assert(degree LL = 2);
		TIMESTART;
		(ignore, den, num) := symmetricKernel(L, 4);
		m := #num;
		TIME("diffeq2::irrDarboux: quartic kernel at ");
		m > 0 => Darboux(LL, 4, den, num, m);
		~allRationalExponents? L => empty;
		for n in [6, 8, 12] repeat {
			(ignore, den, num) := symmetricKernel(L, n);
			TIME("diffeq2::irrDarboux: symmetric kernel at ");
			m := #num;
			m > 0 => return Darboux(LL, n, den, num, m);
		}
		empty;
	}

	local Darboux(L:RXD, n:Z, den:Product RX, num:V FX, m:I):List FxY == {
		import from List Fx;
		assert(m = #num); assert(m > 0);
		curves:List FxY := empty;
		for mvpv in mlogder(den, num)  repeat {		-- mvpv = -v'/v
			if n > 5 then
				curves := cons(Darboux(L, n, mvpv), curves);
			else curves :=
				append!(curves, refine(n, Darboux(L, n, mvpv)));
		}
		curves;
	}

	-- return (false,.,.)  if f is never a square
	--        (true, c, g) if f = c g^2
	local isSquare(f:Fx):(Boolean, F, Fx) == {
		import from Product FX;
		(sq?, cn, gn) := isSquare numerator f;
		sq? => {
			(sq?, cd, gd) := isSquare denominator f;
			sq? => (sq?, cn / cd, expand(gn) / expand(gd));
			(sq?, 1, 0);
		}
		(sq?, 1, 0);
	}

	-- return (false,.,.)  if f is never a square
	--        (true, c, g) if f = c g^2
	local isSquare(f:FX):(Boolean, F, Product FX) == {
		import from Z;
		g:Product FX := 1;
		(c, prod) := squareFree f;
		for trm in prod repeat {
			(p, n) := trm;
			odd? n => return (false, 1, 1);
			g := times!(g, p, n quo 2);
		}
		(true, c, g);
	}

	local first(p:FxY, n:Z):FxY == p;
	local refine(n:Z, p:FxY):List FxY == {
		import from Product FxY;
		assert(n = degree p);
		assert(n <= 5);
		(c, prod) := squareFree p;
		[first trm for trm in prod];
	}

	-- construct the Darboux curve corresponding to v where mvpv = -v'/v
	-- returns a monic curve
	local Darboux(L:RXD, n:Z, mvpv:Fx):FxY == {
		assert(degree L = 2);
		assert(n >= 1);
		p:FxY := monomial n;
		lc := RX2FX leadingCoefficient L;
		a0 := RX2FX(coefficient(L, 0)) / lc;
		a1 := RX2FX(coefficient(L, 1)) / lc;
		bip1:Fx := 1;
		bi := bn1 := mvpv;
		p := add!(p, bi, nm1 := prev n);
		for i in nm1 .. 1 by -1 repeat {
			b := (- differentiate(bi) + bn1 * bi + (i-n)*a1 * bi
					+ (i+1) * a0 * bip1) / ((n-i+1)::Fx);
			p := add!(p, b, prev i);
			bip1 := bi;
			bi := b;
		}
		p;
	}
}
