-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sit_dcompct.as -------------------------------
-- Copyright (c) Manuel Bronstein 2000
-- Copyright (c) INRIA 2000, Version 0.1.12
-- Logiciel Sum^it (c) INRIA 2000, dans sa version 0.1.12
-----------------------------------------------------------------------------

#include "sumit"

#if ALDOC
\thistype{UnivariateSkewPolynomialDecompositionCategory}
\History{Manuel Bronstein}{30/7/2002}{created}
\Usage{\this(R, F, $\iota$, RX, RXZ, FX, FxZ, FxY):\\ Category}
\Params{
{\em R} & \alexttype{FactorizationRing}{} & The coefficient ring\\
{\em F} & \alexttype{Field}{} & A field containing R\\
        & \alexttype{FactorizationRing}{} &\\
$\iota$ & $R \to F$ & Injection from R into F\\
{\em RX} & \alexttype{UnivariatePolynomialCategory}{} R & Polynomials over R\\
{\em RXZ} & \altype{UnivariateSkewPolynomialCategory} RX & Operators over RX\\
{\em FX} & \alexttype{UnivariatePolynomialCategory}{} F & Polynomials over F\\
{\em FxZ} &\altype{UnivariateSkewPolynomialCategory} \alexttype{Fraction}{} FX &
Operators over Fx\\
{\em FxY} &
\alexttype{UnivariatePolynomialCategory}{} \alexttype{Fraction}{} FX &
For returning algebraics\\
}
\Descr{\this(R, F, $\iota$, RX, RXZ, FX, FxZ, FxY) is the category of
types providing various decompositions of operators in \emph{RXZ}.}
\begin{exports}
\alexp{decompose}:
& RXZ $\to$ \albuiltin{Union}(rat:RAT, alg:ALG) & LCLM decomposition\\
\alexp{factorDecomp}:
& RXZ $\to$ \albuiltin{Union}(rat:RAT, alg:ALG) & Factorization\\
\alexp{Loewy}:
& RXZ $\to$ \alexttype{Vector}{} FxZ & Loewy decomposition\\
\end{exports}
\begin{alwhere}
I &==& \alexttype{MachineInteger}{}\\
RAT &==& \albuiltin{Record}(mode:I, rat: \alexttype{Vector}{} FxZ)\\
ALG &==& \albuiltin{Record}(mode:I, modulus:FX, c0:\alexttype{Fraction}{} FxY,
ratpart: \alexttype{Vector}{} FxZ)\\
\end{alwhere}
#endif

macro {
	I	== MachineInteger;
	Z	== Integer;
	V	== Vector;
	Fx	== Fraction FX;
	Fxy	== Fraction FxY;
	RATRED	== Record(mode:I, rat: V FxZ);
	ALGRED	== Record(mode:I, modulus:FX, c0:Fxy, ratpart:V FxZ);
	FACLODO	== Union(rat: RATRED, alg: ALGRED);
	ALGFACT	== Record(modulus:FX, left1: Fxy, left2: Fxy, right: Fxy);
	FACTLODO== Union(rat: V FxZ, algRed: ALGFACT);
	ALGFAC	== Record(modulus:FX, mult:Z, eigen:FxZ, op:FxZ);
	RIGHTFAC== Union(ratfact:FxZ, algfact:ALGFAC);
	SAEC	== SimpleAlgebraicExtensionCategory;
	USKPC	== UnivariateSkewPolynomialCategory;
	KX	== DenseUnivariatePolynomial K;
	Kx	== Fraction KX;
	KxY	== DenseUnivariatePolynomial Kx;
}

define UnivariateSkewPolynomialDecompositionCategory(
		R:   FactorizationRing,
		F:   Join(Field, FactorizationRing), inj: R -> F,
		RX:  UnivariatePolynomialCategory R,
		RXZ: UnivariateSkewPolynomialCategory RX,
		FX:  UnivariatePolynomialCategory F,
		FxZ: UnivariateSkewPolynomialCategory Fx,
		FxY: UnivariatePolynomialCategory Fx): Category ==  with {
	decompose: RXZ -> FACLODO;
#if ALDOC
\alpage{decompose}
\Usage{\name~L}
\Signature{RXZ}{\albuiltin{Union}(ratRed:RAT, algRed:ALG)}
\begin{alwhere}
I &==& \alexttype{MachineInteger}{}\\
RAT &==& \albuiltin{Record}
(mode:\alexttype{MachineInteger}{}, rat: \alexttype{Vector}{} FxZ)\\
ALG &==& \albuiltin{Record}(mode:I, modulus:FX, c0:\alexttype{Fraction}{} FxY,
ratpart: \alexttype{Vector}{} Fx)\\
\end{alwhere}
\Params{ {\em L} & RXZ & A skew polynomial\\ }
\Descr{Returns one of the following possible results:
\begin{description}
\item[case {\tt rat}:]~returns $[0, [L_1,\dots,L_n]]$ such that
\emph{L} is a least common left multiple of $L_1,\dots,L_n$.
If $n = 1$, then \emph{L} cannot be written as a
least common left multiple of lower order operators
(\emph{L} can still be either reducible or irreducible in that case).
\item[case {\tt alg}:]~returns $[0, h(x),u(x,y),[L_1,\dots,L_n]]$,
such that $h(x)$ is irreducible over \emph{F} and
\emph{L} is a least common left multiple of
$L_1,\dots,L_n$ and of $D + u(\alpha, x)$
where $\alpha$ ranges over all the roots of $h(x)$.
\end{description}
}
\alseealso{\alexp{factorDecomp},\alexp{Loewy}}
#endif
	factorDecomp: RXZ -> FACLODO;
#if ALDOC
\alpage{factorDecomp}
\Usage{\name~L}
\Signature{RXZ}{\albuiltin{Union}(ratRed:RAT, algRed:ALG)}
\begin{alwhere}
I &==& \alexttype{MachineInteger}{}\\
RAT &==& \albuiltin{Record}
(mode:\alexttype{MachineInteger}{}, rat: \alexttype{Vector}{} FxZ)\\
ALG &==& \albuiltin{Record}(mode:I, modulus:FX, c0:\alexttype{Fraction}{} FxY,
ratpart: \alexttype{Vector}{} Fx)\\
\end{alwhere}
\Params{ {\em L} & RXZ & A skew polynomial\\ }
\Descr{Returns one of the following possible results:
\begin{description}
\item[case {\tt rat}:]~returns $[m, [L_1,\dots,L_n]]$ such that
each $L_i$ is irreducible over $E(x)$, where \emph{E} is the algebraic
closure of \emph{F}, and \emph{L} can be expressed in terms of the
$L_i$'s depending on \emph{m} as in the following table:
\begin{center}
\begin{tabular}{c|l}
\emph{m} & \emph{L}\\
\hline
0 & LeftLcm($L_1,\dots,L_n$)\\
1 & $L_1 \cdots L_n$\\
2 & LeftLcm($L_1, L_2 L_3$)\\
3 & $L_1$ LeftLcm($L_2, L_3$)\\
4 & $L_1$ LeftLcm($L_2, L_3$) $L_4$\\
\end{tabular}
\end{center}
Note that if $m = n = 1$, then \emph{L} is irreducible over $E(x)$,
where \emph{E} is the algebraic closure of \emph{F}. When \emph{L}
has order $2$, then $m \in \{0,1\}$.
\item[case {\tt alg}:]~returns $[m, h(x), u(x,y), [L_1,\dots,L_n]]$ such that
each $L_i$ is irreducible over $E(x)$, where \emph{E} is the algebraic
closure of \emph{F}, and \emph{L} can be expressed in terms of the
$L_i$'s depending on \emph{m} as in the following table:
\begin{center}
\begin{tabular}{c|l}
\emph{m} & \emph{L}\\
\hline
0 & LeftLcm($D + u(\alpha, x), L_1,\dots,L_n$)\\
3 & $L_1$ LeftLcm($D + u(\alpha, x)$)\\
4 & $L_1$ LeftLcm($D + u(\alpha, x)$) $L_2$\\
\end{tabular}
\end{center}
where $\alpha$ ranges over all the roots of $h(x)$.
When \emph{L} has order $2$, then $m = 0$.
\end{description}
}
\alseealso{\alexp{decompose},\alexp{Loewy}}
#endif
	Loewy: RXZ -> V FxZ;
#if ALDOC
\alpage{Loewy}
\Usage{\name~L}
\Signature{RXZ}{\alexttype{Vector}{} FxZ}
\Params{ {\em L} & RXZ & A skew polynomial\\ }
\Descr{Returns a right Loewy decomposition $[L_1,\dots,L_n]$
where $L = L_1 \cdots L_n$ and each $L_i$ is a completely reducible
right-factor of maximal order of $L_1 \cdots L_{i-1}$.}
\alseealso{\alexp{decompose},\alexp{factorDecomp}}
#endif
}

UnivariateSkewPolynomialDecompositionTools(R:FactorizationRing,
		F:   Join(Field, FactorizationRing), inj: R -> F,
		RX:  UnivariatePolynomialCategory R,
		RXZ: UnivariateSkewPolynomialCategory RX,
		FX:  UnivariatePolynomialCategory F,
		FXZ: UnivariateSkewPolynomialCategory FX,
		FxZ: UnivariateSkewPolynomialCategory Fx,
		FxY: UnivariatePolynomialCategory Fx): with {
	Loewy2: (RXZ, RXZ -> FACLODO) -> V FxZ;
	Loewy3: (FxZ, List RIGHTFAC, FXZ -> V FxZ) -> V FxZ;
	LoewyTriv3: (FxZ, List FxZ, FXZ -> V FxZ) -> V FxZ;
	factor2: (RXZ, (RXZ, Fxy, FX) -> ALGFACT, RXZ -> FACLODO) -> FACTLODO;
	algFactor2: (RXZ, Fxy, FX, K:SAEC(F, FX), KxZ:USKPC Kx) -> ALGFACT;
	rightFactor: (ALGFAC, FX, K:SAEC(F, FX), KxZ:USKPC Kx) -> Fxy;
	decompose3: (FxZ,List RIGHTFAC,FXZ->FACLODO,(ALGFAC,FX)->Fxy)-> FACLODO;
	factorDec3: (FxZ,List RIGHTFAC,FXZ->FACLODO,(ALGFAC,FX)->Fxy)-> FACLODO;
	factorDecTriv3: (FxZ, List FxZ, FXZ -> FACLODO) -> FACLODO;
	normalize: FxZ -> FxZ;
} == add {
	local RX2Fx(p:RX):Fx == RX2FX(p)::Fx;

	local RX2FX(p:RX):FX == {
		import from MonogenicAlgebra2(R, RX, F, FX);
		map(inj)(p);
	}

	normalize(L:FxZ):FxZ == {
		import from Fx;
		one?(a := leadingCoefficient L) => L;
		inv(a) * L;
	}

	Loewy2(L:RXZ, fd2: RXZ -> FACLODO):V FxZ == {
		import from I, Z,  RATRED, FACLODO;
		import from MonogenicAlgebra2(RX, RXZ, Fx, FxZ);
		assert(degree L = 2);
		u := fd2 L;
		u case alg or zero?(u.rat.mode) => [map(RX2Fx)(L)];
		u.rat.rat;
	}

	factor2(L:RXZ, algFactor: (RXZ, Fxy, FX) -> ALGFACT,
		fd2: RXZ -> FACLODO):FACTLODO == {
			import from I, Z, FxZ, V FxZ, RATRED, ALGRED, FACLODO;
			import from MonogenicAlgebra2(RX, RXZ, Fx, FxZ);
			assert(degree L = 2);
			u := fd2 L;
			u case rat => {
				if zero?(u.rat.mode) then {
					assert(#(u.rat.rat) = 2);
					LL := map(RX2Fx)(L);
					u.rat.rat.1 :=
						rightQuotient(LL, u.rat.rat.2);
				}
				[u.rat.rat];

			}
			assert(zero?(u.alg.mode));
			assert(empty?(u.alg.ratpart));
			[algFactor(L, u.alg.c0, u.alg.modulus)];
	}

	-- returns [h(x), L1(x,y)[D], L2(x,y)[D]] such that
	-- L = lc(L) LCLM_{h(a)=0}(D + u(a,x)) = L1(a,x)[D] * L2(a,x)[D]
	algFactor2(L:RXZ, u:Fxy, h:FX, K:SAEC(F, FX), KxZ:USKPC Kx):ALGFACT == {
		import from Z, Kx, KxZ;
		import from MonogenicAlgebra2(F, FX, K, KX);
		import from MonogenicAlgebra2(RX, RXZ, Kx, KxZ);
		import from MonogenicAlgebra2(Fx, FxY, K, KX);
		import from MonogenicAlgebra2(K, KX, Fx, FxY);
		assert(degree L = 2);
		local FX2KX(p:FX):KX == map(coerce$K)(p);
		local RX2Kx(p:RX):Kx == FX2KX(RX2FX p) :: Kx;
		local Fx2K(f:Fx):K == reduce(numerator f)/reduce(denominator f);
		local FxY2KX(p:FxY):KX == map(Fx2K)(p);
		local K2Fx(p:K):Fx == lift(p)::Fx;
		local Kx2Fxy(q:Kx):Fxy ==
			map(K2Fx)(numerator q) / map(K2Fx)(denominator q);
		Lr := monom + (FxY2KX(numerator u)/FxY2KX(denominator u))::KxZ;
		Lleft := rightQuotient(map(RX2Kx)(L), Lr);
		[h, Kx2Fxy(leadingCoefficient Lleft),
			Kx2Fxy coefficient(Lleft,0), Kx2Fxy coefficient(Lr,0)];
	}

	-- returns u(x,y) such that GCRD(rec.op, rec.eigen - a = Z + u(a,x)
	rightFactor(rec:ALGFAC, h:FX, K:SAEC(F,FX), KxZ:USKPC Kx):Fxy == {
		import from Z, KX, Kx, KxZ;
		assert(one?(rec.mult));
		assert(h = rec.modulus);
		import from MonogenicAlgebra2(F, FX, K, KX);
		import from MonogenicAlgebra2(Fx, FxZ, Kx, KxZ);
		import from MonogenicAlgebra2(K, KX, Fx, FxY);
		local FX2KX(p:FX):KX == map(coerce$K)(p);
		local Fx2Kx(f:Fx):Kx == FX2KX(numerator f)/FX2KX(denominator f);
		local K2Fx(a:K):Fx == lift(a)::Fx;
		g := rightGcd(map(Fx2Kx)(rec.op),
		map(Fx2Kx)(rec.eigen) - (monom$K)::KX::Kx::KxZ);
		assert(one? degree g);
		c0 := coefficient(g, 0) / leadingCoefficient g;
		map(K2Fx)(numerator c0) / map(K2Fx)(denominator c0);
	}

	decompose3(L:FxZ, l:List RIGHTFAC, decomp2: FXZ -> FACLODO,
		rf:(ALGFAC, FX) -> Fxy):FACLODO == {
			import from I, Z, Fx, FxZ, V FxZ, RATRED, RIGHTFAC,
				List RIGHTFAC, MonogenicAlgebraOverFraction(FX,
								FXZ, Fx, FxZ);
			assert(degree L = 3);
			empty? l => [[1, [L]]];		-- not CR-red
			u := first l;
			l := rest l;
			u case algfact => [lcm111(u.algfact, l, rf)];
			empty? l => [[1, [L]]];		-- not CR-red
			LL2 := u.ratfact;		-- right-factor of L
			d := degree LL2;
			assert(d = 1 or d = 2);
			(u := first l) case algfact =>
						[lcm111d2(u.algfact, LL2, rf)];
			LL3 := u.ratfact;	-- another right-factor of L
			~empty?(l := rest l) => [[0, lcm111rat(LL2, LL3, l)]];
			d3 := degree LL3;
			assert(d3 = 1 or d3 = 2);
			one? d and one? d3 => [[1, [L]]];  -- reducible not CR
			-- L = LCLM(LL2, LL3) the quadratic might be LCLM
			assert(d = 2 or d3 = 2);
			if one? d then (LL2, LL3, d, d3) := (LL3, LL2, d3, d);
			assert(d = 2 and d3 = 1);
			(f2, L2) := makeIntegral LL2;	-- L2 = f2 * LL2
			v := decomp2 L2;
			v case alg => [lcm111d2!(v.alg, LL3)];	-- 1+1+1 alg
			assert(zero?(v.rat.mode));
			LL3 := normalize LL3;
			one?(#(v.rat.rat)) => [[0, [normalize LL2, LL3]]];
			[[0, [LL3, v.rat.rat.1, v.rat.rat.2]]];
	}

	factorDec3(L:FxZ, l:List RIGHTFAC, fd2: FXZ -> FACLODO,
		rf:(ALGFAC, FX) -> Fxy):FACLODO == {
			import from I, Z, FXZ, Fx, V FxZ;
			import from RATRED, ALGRED, RIGHTFAC;
			import from MonogenicAlgebraOverFraction(FX,FXZ,Fx,FxZ);
			assert(degree L = 3);
			assert(~empty? l);
			u := first l;
			l := rest l;
			u case algfact => [lcm111(u.algfact, l, rf)];
			LL2 := u.ratfact;		-- right-factor of L
			d := degree LL2;
			assert(d = 1 or d = 2);
			empty? l => {			-- not CR-red
				LL1 := rightQuotient(L, LL2);	-- L = LL1 * LL2
				assert(zero? rightRemainder(L, LL2));
				d = 2 => {	--  L2 = f2 * LL2
					(f2, L2) := makeIntegral LL2;
					w := fd2 L2;
					w case alg => {
					-- L = LL1*(lc(L2)/f2) * LCLM(D+rec.c0)
						a := leadingCoefficient(L2)/f2;
						w.alg.mode := 3;
						w.alg.ratpart := [LL1*(a::FxZ)];
						w;
					}
					one?(#(v:=w.rat.rat))=> [[1,[LL1,LL2]]];
					zero?(w.rat.mode) => {
					-- L = LL1 * (lc(L2)/f2) * LCLM(v.1,v.2)
						a := leadingCoefficient(L2)/f2;
						[[3, [LL1*(a::FxZ), v.1, v.2]]];
					}
					-- L = LL1 * (1/f2) * v.1 * v.2
					[[1, [LL1, inv(f2::Fx) * v.1, v.2]]];
				}
				assert(d = 1);
				(f1, L1) := makeIntegral LL1;	-- L1 = f1 * LL1
				w := fd2 L1;
				w case alg => {
					a := leadingCoefficient(L1) / f1;
					w.alg.mode := 4;
					w.alg.ratpart := [a::FxZ, LL2];
					w;
				}
				one?(#(v := w.rat.rat)) => [[1, [LL1, LL2]]];
				zero?(w.rat.mode) => {
					a := leadingCoefficient(L1) / f1;
					[[4, [a::FxZ, v.1, v.2, LL2]]];
				}
				-- L = 1/f1 * v.1 * v.2 * LL2
				[[1, [inv(f1::Fx) * v.1, v.2, LL2]]];
			}
			(u:=first l) case algfact=>[lcm111d2(u.algfact,LL2,rf)];
			LL3 := u.ratfact;	-- another right-factor of L
			~empty?(l := rest l) => [[0, lcm111rat(LL2, LL3, l)]];
			d3 := degree LL3;
			assert(d3 = 1 or d3 = 2);
			one? d and one? d3 => {	-- 1*(1+1), not CR, not LCLM
				v := d1timesLcm11(L, LL2, LL3);
				[[3, [v.1, LL2, LL3]]];
			}
			-- L = LCLM(LL2, LL3) the quadratic might be LCLM
			assert(d = 2 or d3 = 2);
			if one? d then (LL2, LL3, d, d3) := (LL3, LL2, d3, d);
			assert(d = 2 and d3 = 1);
			LL3 := normalize LL3;
			(f2, L2) := makeIntegral LL2;	-- L2 = f2 * LL2
			w := fd2 L2;
			w case alg => [lcm111d2!(w.alg, LL3)];
			one?(#(v := w.rat.rat)) => [[0, [normalize LL2, LL3]]];
			zero?(w.rat.mode) => [[0, [LL3, v.1, v.2]]];
			[[2, [LL3, normalize(v.1), v.2]]];
	}

	-- case where the eigenring of L is trivial, but it has hyperexponential
	-- solutions yielding the right factors Lr
	factorDecTriv3(L:FxZ, Lr:List FxZ, fd2: FXZ -> FACLODO):FACLODO == {
		import from I, Z, FXZ, Fx, RATRED, ALGRED;
		import from MonogenicAlgebraOverFraction(FX, FXZ, Fx, FxZ);
		LL2 := leftLCM Lr;			-- normalized
		assert(degree LL2 < 3); -- eigen wouldn't be trivial otherwise
		LL1 := rightQuotient(L, LL2);		-- L = LL1 * LL2
		assert(zero? rightRemainder(L, LL2));
		degree(LL2) = 2 => [[3, [LL1, first Lr, first rest Lr]]];
		assert(one? degree LL2);
		(f1, L1) := makeIntegral LL1;		-- L1 = f1 * LL1
		uu := fd2 L1;
		uu case alg => {
			a := leadingCoefficient(L1) / f1;
			uu.alg.mode := 4;
			uu.alg.ratpart := [a::FxZ, LL2];
			uu;
		}
		one?(#(v := uu.rat.rat)) => [[1, [LL1, LL2]]];
		zero?(uu.rat.mode) => {
			a := leadingCoefficient(L1) / f1;
			[[4, [a::FxZ, v.1, v.2, LL2]]];
		}
		[[1, [inv(f1::Fx) * v.1, v.2, LL2]]]; -- L= (1/f1)*v.1*v.2*LL2
	}

	Loewy3(L:FxZ, l:List RIGHTFAC, lw2: FXZ -> V FxZ):V FxZ == {
		import from I, Z, Fx, RIGHTFAC;
		import from MonogenicAlgebraOverFraction(FX, FXZ, Fx, FxZ);
		assert(degree L = 3);
		assert(~empty? l);
		(u := first l) case algfact => [L];	-- CR-reducible 1+1+1
		LL2 := u.ratfact;			-- right-factor of L
		d := degree LL2;
		assert(d = 1 or d = 2);
		empty?(l := rest l) => {		-- not CR-reducible
			LL2 := normalize LL2;
			LL1 := rightQuotient(L, LL2);	-- L = LL1 * LL2
			assert(zero? rightRemainder(L, LL2));
			d = 2 => {
				(f2, L2) := makeIntegral LL2;	-- L2 = f2 * LL2
				vec := lw2 L2;
				one?(#vec) => [LL1, LL2];	-- L2 is CR
				-- L = LL1 * (1/f2) * vec.1 * vec.2
				[LL1, inv(f2::Fx) * vec.1, vec.2];
			}
			assert(d = 1);
			(f1, L1) := makeIntegral LL1;		-- L1 = f1 * LL1
			vec := lw2 L1;
			one?(#vec) => [LL1, LL2];		-- L1 is CR
			-- L = (1/f1) * vec.1 * vec.2 * LL2
			[inv(f1::Fx) * vec.1, vec.2, LL2];
		}
		assert(d = 1 or d = 2);
		(u := first l) case algfact => [L];	-- CR-reducible 1+1+1
		~empty?(rest l) => [L];		-- CR-reducible 1-1-1 rat
		LL3 := u.ratfact;		-- another right-factor of L
		d3 := degree LL3;
		assert(d3 = 1 or d3 = 2);
		one? d and one? d3 => d1timesLcm11(L, LL2, LL3);
		-- L = LCLM(LL2, LL3) the quadratic might be LCLM
		assert(d = 2 or d3 = 2);
		if one? d then (LL2, LL3, d, d3) := (LL3, LL2, d3, d);
		assert(d = 2 and d3 = 1);
		(f2, L2) := makeIntegral LL2;	-- L2 = f2 * LL2
		vec := lw2 L2;
		one?(#vec) => [L];		-- CR-reductible 1+2 or 1+1+1
		-- L2 = vec.1 * vec.2,
		-- L = LCLM(LL3, vec.1 * vec.2) = OP * LCLM(LL3, vec.2)
		LLright := normalize leftLcm(LL3, vec.2);
		assert(degree(LLright) = 2 or degree(LLright) = 1);
		degree(LLright) = 2 => [rightQuotient(L, LLright), LLright];
		-- At this point, L = LCLM(LL3, vec.1 * vec.2) is 2*1 or 1*1*1
		LL1 := rightQuotient(L, LL2);		-- L = LL1 * LL2
		assert(zero? rightRemainder(L, LL2));
		-- L = LL1 * (1 / f2) * vec.1 * vec.2
		LL0 := LL1 * (inv(f2::Fx) * vec.1);
		LLright := vec.2;			-- L = LL0 * LLright
		(f0, L0) := makeIntegral LL0;		-- L0 = f0 * LL0
		vec := lw2 L0;
		one?(#vec) => [LL0, LLright];		-- non CR, 1+(1*1), 2/1
		-- L = 1/f0 * vec.1 * vec.2 * LLright
		[inv(f0::Fx) * vec.1, vec.2, LLright];
	}

	-- case where the eigenring of L is trivial, but it has hyperexponential
	-- solutions yielding the right factors Lr
	LoewyTriv3(L:FxZ, Lr:List FxZ, lw2: FXZ -> V FxZ):V FxZ == {
		import from I, Z, Fx;
		import from MonogenicAlgebraOverFraction(FX, FXZ, Fx, FxZ);
		LL2 := leftLCM Lr;			-- normalized
		assert(degree LL2 < 3);	-- eigen wouldn't be trivial otherwise
		LL1 := rightQuotient(L, LL2);		-- L = LL1 * LL2
		assert(zero? rightRemainder(L, LL2));
		degree(LL2) = 2 => [LL1, LL2];		-- 1 * (1 + 1)
		assert(one? degree LL2);
		(f1, L1) := makeIntegral LL1;		-- L1 = f1 * LL1
		v := lw2 L1;
		one?(#v) => [LL1, LL2];			-- L1 is CR
		[inv(f1::Fx) * v.1, v.2, LL2];	-- L= 1/f1 * v.1 * v.2 * LL2
	}

	-- always normalizes
	local leftLCM(l:List FxZ):FxZ == {
		assert(~empty? l);
		L := first l;
		for op in rest l repeat L := normalize leftLcm(L, op);
		L;
	}

	-- case where operator factors as L = L0 * LCLM(L1, L2), all of degree 1
	local d1timesLcm11(L:FxZ, L1:FxZ, L2:FxZ):V FxZ == {
		import from Z;
		LL := normalize leftLcm(L1, L2);	-- must be normalized
		assert(2 = degree LL);
		L0 := rightQuotient(L, LL);	-- L = L0 * LL
		assert(zero? rightRemainder(L, LL));
		assert(one? degree L0);
		[L0, LL];
	}

	-- case where operator is an LCM of 3 first-order operators
	-- over an algebraic extension of degree 2 or 3
	local lcm111(rec:ALGFAC,l:List RIGHTFAC,rf:(ALGFAC,FX)->Fxy):ALGRED == {
		import from I, Z, FX, V FxZ, RIGHTFAC;
		empty? l => {           -- 1+1+1 all conjugates
			assert(degree(rec.modulus) = 3);
			[0, rec.modulus, rf(rec, rec.modulus), empty];
		}
		assert(degree(rec.modulus) = 2);
		assert(empty? rest l);
		assert(first(l) case ratfact);
		lcm111d2(rec, first(l).ratfact, rf);
	}

	-- case where operator is an LCM of 3 first-order operators
	-- 2 over an algebraic extension of degree 2, one rational
	local lcm111d2(rec:ALGFAC, L:FxZ, rf:(ALGFAC, FX) -> Fxy):ALGRED == {
		import from I, Z, FX, V FxZ, RIGHTFAC;
		assert(one? degree L);
		assert(degree(rec.modulus) = 2);
		[0, rec.modulus, rf(rec, rec.modulus), [normalize L]];
	}

	-- case where operator is an LCM of 3 first-order operators
	-- 2 over an algebraic extension of degree 2, one rational
	local lcm111d2!(rec:ALGRED, L:FxZ):ALGRED == {
		import from V FxZ;
		rec.mode := 0;
		rec.ratpart := [L];
		rec;
	}

	-- case where operator is an LCM of 3 rational first-order operators
	local lcm111rat(L1:FxZ, L2:FxZ, l:List RIGHTFAC):V FxZ == {
		import from Z, RIGHTFAC;
		assert(empty? rest l);
		assert(one? degree L1); assert(one? degree L2);
		u := first l;
		assert(u case ratfact); assert(one? degree(u.ratfact));
		[normalize L1, normalize L2, normalize(u.ratfact)];
	}
}
