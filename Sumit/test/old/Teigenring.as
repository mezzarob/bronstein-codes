-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------- test eigenring.as --------------------------
#include "sumittest"

macro {
	Z == Integer;
	ZX == DenseUnivariatePolynomial(Z, "x");
	Zx == Quotient ZX;
	ZXD == LinearOrdinaryDifferentialOperator(ZX, "D");
	ZxD == LinearOrdinaryDifferentialOperator(Zx, "D");
}

local zeigen():Boolean == {
	import from SingleInteger, Z, ZX, Zx, ZXD, ZxD, Vector ZxD;
	import from Eigenring(Z, ZX, ZXD, ZxD);
	x:ZX := monom;
	D:ZXD := monom;
	L := 16*x^2*D^2 + (3@Z)::ZXD;
	LL := (16*x^2)::Zx * (monom$ZxD)^2 + (3@Z)::ZxD;
	E := eigenring L;
	#E ~= 2 => false;
	for i in 1..#E repeat
		~zero?(rightRemainder(LL * E.i, LL)) => return false;
	true;
}

local qeigen():Boolean == {
	import from SingleInteger, Z, ZX, Zx, ZxD, Vector ZxD;
	import from Eigenring(Z, ZX, ZXD, ZxD);
	x:ZX := monom;
	D:ZxD := monom;
	L := (16*x^2)::Zx * D^2 + (3@Z)::ZxD;
	E := eigenring L;
	#E ~= 2 => false;
	for i in 1..#E repeat
		~zero?(rightRemainder(L * E.i, L)) => return false;
	true;
}

print << "Testing eigenrings..." << newline;
sumitTest("2nd order over polynomials", zeigen);
sumitTest("2nd order over fractions", qeigen);
print << newline;
