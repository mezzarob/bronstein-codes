-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------- test extree.as --------------------------
#include "sumittest.as"

macro {
	Z		== SingleInteger;
	Plus		== ExpressionTreePlus;
	Minus		== ExpressionTreeMinus;
	Times		== ExpressionTreeTimes;
	Expt		== ExpressionTreeExpt;
	Quotient	== ExpressionTreeQuotient;
	Tree		== ExpressionTree;
}

height(t:Tree):Z == {
	import from List Tree;
	m:Z := 1;
	leaf? t => m;
	for a in arguments t repeat {
		h := height a;
		if h > m then m := h;
	}
	m + 1;
}

extree0(t:Tree):Boolean == {
	import from Z, String, List Tree;
	2 = arity$operator(t) and "+" = name$operator(first arguments t);
}

extree():Boolean == {
	import from Z, DoubleFloat, Tree, List Tree, ExpressionTreeLeaf;

	x := extreeSymbol "x";
	y := extreeSymbol "y";

	t2 := extree leaf 2;
	t3 := extree leaf 3;

	-- t is the fraction ((x-1.5)y^3 + 2x^2y^2 + (x+2)^3 y+x+1) / (y-2)

	t := Plus [Times [Plus [x, Minus [extree leaf 1.5]], Expt [y, t3]],_
		Times [t2, Expt [x, t2], Expt [y, t2]],_
		Times [Expt [Plus [x, t2], t3], y], x, extree leaf(1@Z)];

	t := Quotient [t, Plus [y, Minus [t2]]];

	6 = height t and extree0 t;
}

print << "Testing extree..." << newline;
sumitTest("extree", extree);
print << newline;
