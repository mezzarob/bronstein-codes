-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------- test zfactor.as --------------------------
#include "sumittest"

macro {
	Z == Integer;
	Q == Quotient Z;
	P == DenseUnivariatePolynomial(Z, "x");
	RR == RationalRoot;
}

integerRoots():Boolean == {
	import from Z, P, RR, List RR;
	x == monom;
	f := x^5 + 8120 * x^4 - 6299183 * x^3 - 9446096526 * x^2 +
					3671637382172 * x + 51412443096::P;
	l := integerRoots f;
	mult:Z := 1;
	count:Z := 0;
	t1234:Boolean := false; tm987:Boolean := false; t346:Boolean := false;
	for rt in l repeat {
		~integer? rt => return false;
		n := integerValue rt;
		mult := mult * multiplicity(rt);
		count := next count;
		if n = 1234 then t1234 := true;
		else if n = 346 then t346 := true;
		else if n = -987 then tm987 := true;
	}
	count = 3 and mult = 1 and t1234 and t346 and tm987;
}

rationalRoots():Boolean == {
	import from Z, Q, P, RR, List RR;
	x := monom;
	f := 13*x^9+384*x^8-1485*x^7+13*x^3+397*x^2-1101*x-1485::P;
	l := rationalRoots f;
	mult:Z := 1;
	count:Z := 0;
	tm33:Boolean := false; t4513:Boolean := false;
	for rt in l repeat {
		(n, d) := value rt;
		q := n / d;
		mult := mult * multiplicity(rt);
		count := next count;
		if q = -33::Q then tm33 := true;
		else if q = 45/13 then t4513 := true;
	}
	count = 2 and mult = 1 and tm33 and t4513;
}

factor():Boolean == {
	import from Z, P, Product P;
	x == monom;
	f := 20*x^5-96*x^4+1191439*x^3-6889560*x^2-21718242087*x+17378393004::P;
	(lcoeff, p) := factor f;
	count:Z := 0;
	for term in p repeat count := count + 1;
	lcoeff = 1 and count = 4 and expand p = f;
}

print << "Testing zfactor..." << newline;
sumitTest("integerRoots", integerRoots);
sumitTest("rationalRoots", rationalRoots);
sumitTest("factor", factor);
print << newline;
