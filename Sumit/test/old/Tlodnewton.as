-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------- test lodnewton.as --------------------------
#include "sumittest"

macro {
	Z  == Integer;
	Q  == Quotient Z;
	Qx == DenseUnivariatePolynomial Q;
	Fd == LinearOrdinaryDifferentialOperator Qx;
	X0 == RationalPlace(Q, Qx, 0);
	NP == LinearOrdinaryDifferentialNewtonPolygon(Qx, Q, X0, Fd);
	GPOCH	== GeneralizedPochammerUnivariatePolynomial Q;
}

euler():Boolean == {
	import from Z, Q, Qx, Fd, X0, NP, GPOCH;
	x:Qx := monom;
	dx:Fd := monom;
	euler := x^3 * dx^2 + (x^2 + x) * dx - 1;
	-- euler has an irregular singularity with exponent 1 at x = 0
	np := newtonPolygon euler;
	ieq := expand(indicialEquation np, Qx);
	zero?(ieq(1@Q)) and degree(ieq) = 1 and irregular? np;
}

legendre():Boolean == {
	import from Z, Q, Qx, Fd, X0, NP, GPOCH;
	x:Qx := monom;
	dx:Fd := monom;
	legendre := (1 - x^2) * dx^2 - 2 * x * dx + 30::Fd;
	np := newtonPolygon legendre;
	ieq := expand(indicialEquation np, Qx);
	zero?(ieq(1@Q)) and zero?(ieq(0@Q)) and degree(ieq)=2 and ordinary? np;
}

print << "Testing lodnewton..." << newline;
sumitTest("euler", euler);
sumitTest("legendre", legendre);
print << newline;
