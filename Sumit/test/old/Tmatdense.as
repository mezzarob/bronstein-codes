-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-------------------------   test for matdense.as   -------------------------
#include "sumittest"

basic():Boolean ==
{
     macro Z  == Integer;
     macro V  == Vector Z;
     macro MZ == DenseMatrix Z;
     import from Z, V, MZ, List Z, List List Z,SingleInteger;

     a := matrix [[1,2,3],[4,5,6],[7,8,9]];
     b := matrix [[9,8,7],[6,5,4],[3,2,1]];
     v := vector [1,2,3];
     t := new(3,3,1);
     a := add!(a,b);
     a ~= times!(10,t) => false;
     w := b*v;
     w ~= vector [46,28,10] => false;
     7*one(5) ~= map!( (x:Z):Z +-> 7*x, one(5) ) => false;
     b := random(5,7);
     b ~= transpose transpose b => false;
     true;     
}

nullSpace():Boolean ==
{
     macro Z == Integer;
     macro V == Vector Integer;
     macro M == DenseMatrix Integer;
     import from Z,V,M,List V, List Z, List List Z, SingleInteger;
     a          := matrix [[1,2,-1,3],[3,4,-3,7],[5,6,-5,11],[7,8,-7,15]];
     ns:List(V) := nullSpace a;
     r          := rank a;
     (cols a - r) ~= #ns => false;
     for v in ns repeat
     {
          ~zero?(a*v) => return false;
     }
     true;
}

print << "Testing matdense..." << newline;
sumitTest("basic operations", basic);
sumitTest("nullSpace", nullSpace);
print << newline;
