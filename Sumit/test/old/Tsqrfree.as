-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------ test sqrfree.as ---------------------
#include "sumittest.as"

macro {
	Z == Integer;
	Zx == SparseUnivariatePolynomial(Z, "x");
	F == ZechPrimeField 5;
	Fx == SparseUnivariatePolynomial(F, "x");
}

import from SingleInteger;

local char0(f:Zx -> (Z, Product Zx)):Boolean == {
	import from Z, Zx, PrimitiveArray Z, Product Zx;
	x := monom;
	p:Zx := 1;
	for i in 1..5@Z repeat p := p * (x - i::Zx)^i;
	(c, pr) := f p;
	hit := new(5@SingleInteger, 0);
	i:Z := 0;
	(degree p ~= 15) or (c ~= 1) or ((c * expand pr) ~= p) => false;
	for term in pr repeat {
		(q, n) := term;
		degree(q) ~= 1 => return false;
		hit(retract n) := 1;
		i := next i;
	}
	for j in 1..5@SingleInteger repeat { zero?(hit.j) => return false; }
	i = 5;
}

local char0():Boolean == {
	import from UnivariatePolynomialSquareFree(Z, Zx);
	char0(musser) and char0(yun);
}

local charp(f:Fx -> (F, Product Fx)):Boolean == {
	import from Z, F, Fx, PrimitiveArray Z, Product Fx;
	x := monom;
	p:Fx := (3@Z)*1;
	for i in 1..4@Z repeat p := p * (x^2 + i*1)^i;
	for i in 1..3@Z repeat p := p * (x^2 + x + i*1)^(i+4);
	p := p * (x^2 + (3@Z)*x + (4@Z)*1)^8;
	for i in 1..4@Z repeat p := p * (x + i*1)^(i+8);
	(c, pr) := f p;
	c ~= 3 => false;
	(c ~= 3) or ((c * expand pr) ~= p) => false;
	hit := new(23@SingleInteger,0);
	for term in pr repeat {
		(q,n) := term;
		(n>8) and degree(q)~=1 => return false;
		(n<=8) and degree(q)~=2 => return false;
		n>23 => return false;
		hit(n::SingleInteger):=1;
	}
	hit(2):=hit(2)-1;
	hit(3):=hit(3)-1;
	hit(5):=hit(5)-1;
	hit(6):=hit(6)-1;
	hit(8):=hit(8)-1;
	hit(12):=hit(12)-1;
	hit(13):=hit(13)-1;
	hit(18):=hit(18)-1;
	hit(23):=hit(23)-1;
	for j in 1..23@SingleInteger repeat {
		if hit(j)~=0 then return false;
	}
	true;
}

local charp():Boolean == {
	import from UnivariatePolynomialSquareFree(F, Fx);
	charp(musser) and charp(yun);
}
	
print << "Testing sqrfree..." << newline;
sumitTest("Characteristic zero", char0);
sumitTest("Characteristic p", charp);
print << newline;
