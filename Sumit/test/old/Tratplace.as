-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------- test ratplace.as --------------------------
#include "sumittest"

macro {
	Z  == Integer;
	Q  == Quotient Z;
	Qx == DenseUnivariatePolynomial(Q, "x");
	X0 == RationalPlace(Q, Qx, 0);
}

x0():Boolean == {
	import from Z, Q, Qx, X0;

	x := monomial(1, 1);
	a := x^3; b := x^2 + x; c := -(1@Qx);
	na := orderOf a; nb := orderOf b; nc := orderOf c;
	D := derivation()$Qx;
	ok? := normal? D; mu := orderDrop D; r := residue D;
	na = 3 and nb = 1 and zero? nc and ok? and mu = -1 and r = 1;
}

print << "Testing ratplace..." << newline;
sumitTest("x = 0", x0);
print << newline;
