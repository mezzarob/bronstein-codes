-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------- test sup.as --------------------------
#include "sumittest"

macro {
        Z == Integer;
        Zx == SparseUnivariatePolynomial(Z, "x");
        Zxt == SparseUnivariatePolynomial(Zx, "t");
}

degree():Boolean == {
        import from Z, Zx;
        x := monom;
        p := (x - 1) * (x + 1);
        degree p = 2 and leadingCoefficient p = 1 and zero? p(-1@Z);
}

exactQuotient():Boolean == {
        import from Zx, Partial Zx;

        x := monom;
        a := x - 1;
        b := x + 1;
        p := a * b;
        q := exactQuotient(p, a);	-- must be b
        f := exactQuotient(p, x);	-- must be failed
        ~(failed? q) and failed? f and retract(q) = b;
}

diff():Boolean == {
        import from Z, Zx, Zxt;
        x:Zx := monom;
	t:Zxt := monom;
	p := x * t + (x^2)::Zxt;
	-- TEMPORARY: NO CONDITIONAL CONSTANTS (969)
	-- D:Derivation(Zxt) := lift(derivation, t);	-- t' = t
	D:Derivation(Zxt) := lift(derivation(), t);	-- t' = t
	q := D p;			-- must be (1 + x) t + 2 x
	r := q - p;			-- must be t + 2 x - x^2
	m := x * (x - 2::Zx);
	degree r = 1 and leadingCoefficient r = 1
		and zero?(reductum(r) + m::Zxt);
}

hgcd(a:Zx, b:Zx):Zx == {
	import from Partial Zx, HeuristicGcd(Z, Zx);
	(g, a, b) := heuristicGcd(a, b);
	retract g;
}

mgcd(a:Zx, b:Zx):Zx == {
	import from Partial Zx, ModularUnivariateGcd(Z, Zx);
	(g, a, b) := modularGcd(a, b);
	retract g;
}

heugcd():Boolean == gcd hgcd;

modgcd():Boolean == gcd mgcd;

gcd(ggt:(Zx,Zx) -> Zx):Boolean == {
	import from Z, Zx;

	x := monom;
	p := x^8 + x^6 - 3*x^4 - 3*x^3 + 8*x^2 +2*x - 5@Z ::Zx;
	q := 3*x^6 + 5*x^4 -4*x^2 -9*x + 21@Z ::Zx;
	r := x^2 + 1;
	g := ggt(p, q);
	rg := ggt(r * p, r * q);
	g = 1 and rg = r;
}

print << "Testing sup..." << newline;
sumitTest("degree", degree);
sumitTest("exactQuotient", exactQuotient);
sumitTest("diff", diff);
sumitTest("heugcd", heugcd);
sumitTest("modgcd", modgcd);
print << newline;
