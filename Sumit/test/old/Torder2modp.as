-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-------------------------- test order2modp.as --------------------------
#include "sumittest"

macro {
	I   == SingleInteger;
	Z   == Integer;
	F   == ZechPrimeField p;
	FX  == DenseUnivariatePolynomial F;
	Fx  == Quotient FX;
	Fxd == LinearOrdinaryDifferentialOperator Fx;
}

-- must pass a prime for which D^2-x is irreducible
local airy(p:I):Boolean == {
	import from Z, F, FX, Fx, Fxd, List Fxd, List List Fxd;
	import from ModularSecondOrderLODOFactorizer(F, FX, Fxd);
	x:FX := monom;
	D:Fxd := monom;
	L := D^2 - x::Fx::Fxd;
	l := factor L;
	~empty?(l) and empty?(rest l) and first(first l) = L;
}

local airy():Boolean == {
	import from I;
	airy(3) and airy(5) and airy(7) and airy(11) and airy(13);
}

local kamke(p:I, zerocurv?:Boolean):Boolean == {
	import from Z, F, FX, Fx, Fxd, List Fxd, List List Fxd;
	import from ModularSecondOrderLODOFactorizer(F, FX, Fxd);
	x:FX := monom;
	D:Fxd := monom;
	L := (x^4 + x)::Fx * D^2 + (x^3 - 1)::Fx * D - (x^2)::Fx::Fxd;
	l := factor L;
	empty? l => zerocurv?;
	for lf in l repeat {
		P:Fxd := 1;
		for f in lf repeat P := P * f;
		~zero?(L - P) => return false;
	}
	true;
}

local kamke():Boolean == {
	import from I;
	kamke(3, false) and kamke(5, false) and kamke(7, true)
		and kamke(11, false) and kamke(13, true);
}

print << "Testing second order factorization in Fp[x,d/dx]..." << newline;
sumitTest("Airy", airy);
sumitTest("Kamke", kamke);
print << newline;
