-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------- test sysratsol.as --------------------------
#include "sumittest"

macro {
	I == SingleInteger;
	Z == Integer;
	ZX == DenseUnivariatePolynomial(Z, "x");
	Zx == Quotient ZX;
	M == DenseMatrix;
	SYS == LinearOrdinaryDifferentialSystem;
}


local qkernel():Boolean == {
	import from I, Z, ZX, Zx, M Zx, SYS Zx, LODSRationalSolutions(Z, ZX);
	A := zero(2, 2);
	x := monom;
	A(1,1) := A(2,2) := x::Zx;
	A(2,1) := (x^2 - x^4)::Zx;
	A(1,2) := - inv((x^2)::Zx);
	K := rationalKernel system(1, A);
	cols K ~= 1 => false;
	AK := A * K;
	differentiate(K(1,1)) = AK(1,1) and differentiate(K(2,1)) = AK(2,1);
}

local zkernel():Boolean == {
	import from I, Z, ZX, M ZX, Zx, M Zx, SYS ZX;
	import from LODSRationalSolutions(Z, ZX);
	import from MatrixCategoryOverQuotient(ZX, M ZX, Zx, M Zx);
	A:M ZX := zero(2, 2);
	x := monom;
	A(1,1) := A(2,2) := x^3;
	A(2,1) := x^4 - x^6;
	A(1,2) := - 1;
	K := rationalKernel system(x^2, A);
	cols K ~= 1 => false;
	AK := makeRational(A) * K;
	x^2 * differentiate(K(1,1)) = AK(1,1) and
		x^2 * differentiate(K(2,1)) = AK(2,1);
}

print << "Testing rational solutions of Y' = A Y..." << newline;
sumitTest("2nd order over polys", zkernel);
sumitTest("2nd order over fractions", qkernel);
print << newline;
