-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-- sockets.as
-- Copyright(c)'96 by ETH Zurich
-- Purpose: include file for Socket type. It contains the necessary 
--	    constant definitions.
-- Author:  Niklaus Mannhart
-- Created:  27 Dec 1996
-- changes: 
--
-- don't remove next line. It contains CVS information.
-- CVS = "$Id: sockets.as,v 1.3 1997/01/08 14:43:29 mannhart Exp $"

-- constants for Sockets type implemented in axlsocket.as

macro {
	
	-- socket family
	AFUNIX == 1;
	AFINET == 2;

	-- socket types
	SOCKSTREAM == 1;
	SOCKDGRAM  == 2;
	SOCKRAW    == 3;
	
	-- error codes
	SOCKETSNOERROR       == 0;
	SOCKETSSOCKETERROR   == 1;
	SOCKETSBINDERROR     == 2;
	SOCKETSCONNECTERROR  == 3;
	SOCKETSLISTENERROR   == 4;
	SOCKETSACCEPTERROR   == 5;
	SOCKETSCLOSEERROR    == 6;
	SOCKETSWRITEERROR    == 7;
	SOCKETSREADERROR     == 8;
	SOCKETSSENDERROR     == 9;
	SOCKETSSENDTOERROR   == 10;
	SOCKETSRECVERROR     == 11;
	SOCKETSRECVFROMERROR == 12;
}

