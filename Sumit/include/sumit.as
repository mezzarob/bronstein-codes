-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ sumit.as ------------------------------------
--
-- Header file for sum^it clients - version 1.0.1
--
-- Copyright (c) Manuel Bronstein 1994-2002
-- Copyright (c) INRIA 1997-2002, Version 1.0.1
-- Logiciel Sum^it (c) INRIA 1997-2002, dans sa version 1.0.1
-- Copyright (c) Swiss Federal Polytechnic Institute Zurich, 1994-1997
-----------------------------------------------------------------------------

-- This allows code to check whether it is built on top of libaldor
#assert LibrarySumit

#include "algebra"

-- Selection of the appropriate portable object library
#if GMP
#library sumit "libsumit-gmp.al"
#elseif DEBUG
#library sumit "libsumitd.al"
#else
#library sumit "libsumit.al"
#endif

import from sumit;
inline from sumit;

-- After a meeting of M. Bronstein, R. Hemmecke, M. Moreno Maza, and
-- S. Watt at INRIA (May 2004), these identifiers were renamed
-- in LibAlgebra.
-- Bronstein committed those modifications to the aldor.org CVS
-- repository on 2004/11/19.
macro {
	UnivariatePolynomialCategory  == UnivariatePolynomialAlgebra;
	UnivariatePolynomialCategory0 == UnivariatePolynomialAlgebra0;
	MonogenicAlgebra == UnivariateFreeRing;
	MonogenicAlgebra2 == UnivariateFreeRing2;
	MonogenicAlgebraOverFraction == UnivariateFreeRingOverFraction;
	UnivariateTaylorSeriesCategory == UnivariateTaylorSeriesType;
	UnivariateTaylorSeriesCategory2Poly == UnivariateTaylorSeriesType2Poly;
}