% ======================================================================
% This code was written all or part by Dr. Manuel Bronstein from
% Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
% decided to publish this code under the CeCILL open source license in
% memory of Dr. Manuel Bronstein.
% 
% This software is governed by the CeCILL license under French law and
% abiding by the rules of distribution of free software. You can use,
% modify and/or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and Inria at the following URL :
% http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
% 
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided
% only with a limited warranty and the software's author, the holder of
% the economic rights, and the successive licensors have only limited
% liability.
% 
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards
% their requirements in conditions enabling the security of their
% systems and/or data to be ensured and, more generally, to use and
% operate it in the same conditions as regards security.
% 
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.
% ======================================================================
% 
\section{User Guide}

This guide introduces the common types and categories provided by \sumit,
and presupposes some familiarity with programming in \aldor, \libaldor
and \libalgebra.
If you are unfamiliar with \aldor{} or \salli{}, we suggest that you first go
through the tutorial in {\tt aldorlib/tutorial/}, which
will familiarize you both with \aldor and \salli{}.

\sumit{} provides over 60 categories and domains, and around 200 different
exported functions, which can look daunting at first. Remember however
that a large part of those are low--level functionalities that make it
possible to programmers to write efficient applications. Most of the
high-level functionalities are found in a small subset of types and
those are the ones described in this short guide. As you become more
familiar with \sumit, you will find the reference guide to be more
useful when browsed on line.

As you learn programming with \sumit, make sure to check the
{\tt sumit/samples} directory for various programming samples.

\subsection{Skew polynomials and linear operators}
\sumit{} provides several types for computing with univariate
skew-polynomials or specific linear operators.
The most general is \altype{UnivariateSkewPolynomial},
for which you provide the \alexttype{Automorphism}{} $\sigma$
and the $\sigma$--derivation $\delta$ as parameters.
In addition, you also provide a usual polynomial type
that is meant to be used as internal representation for
the skew--polynomials. This lets you choose between dense
or sparse representation by selecting the appropriate
representation type. As for polynomials, naming the
variable is not necessary since the name is used only for output.
If you want to name the variable, pass a polynomial type with
a variable name as reprensentation type. There are several
ways to create the \alexttype{Automorphism}{} $\sigma$:
since \alexttype{Automorphism}{} is of category \alexttype{Monoid}{},
the constant {\tt 1} can be used for the identity
map, while {\tt morphism} is used for more general maps.

There are also types for common linear operators:
\begin{itemize}
\item
\altype{LinearOrdinaryDifferentialOperator} provides
linear ordinary differential operators with respect
to an arbitrary \alexttype{Derivation}{} that is given as
parameter. The derivation can be omitted if the coefficient
type is a \alexttype{DifferentialRing}{}.
\item
\altype{LinearOrdinaryDifferenceOperator} provides
linear ordinary generalized difference operators with respect
to an arbitrary \alexttype{Automorphism}{} that is given as
parameter.
\item
\altype{LinearOrdinaryRecurrence} provides
linear ordinary difference operators with coefficients
in a polynomial ring $R[x]$ and with respect
to the shift $x \to x + 1$.
\altype{LinearOrdinaryRecurrenceQ} provides
the same operators but over the rational fraction field $R(x)$.
\item
\altype{LinearOrdinaryQDifferenceOperator} provides
linear ordinary $q$--difference operators with coefficients
in a polynomial ring $R[x]$ and with respect
to the shift $x \to q x$ for a given $q \in R$, which
is given as parameter to the type.
\altype{LinearOrdinaryQDifferenceOperatorQ} provides
the same operators but over the rational fractions $R(x)$.
\end{itemize}
All of the above are implemented using a dense representation,
and allow an optional \alexttype{Symbol}{} as last parameter if you
want to name the variable for output.

Finally, to manipulate and solve first order linear systems rather
than scalar operators, use the \altype{LinearOrdinaryFirstOrderSystem}
type for both differential and generalized difference systems.

Because polynomials and skew--polynomials share
many common operations, \sumit{} provides a
category hierarchy for operators that fits within the
polynomial hierarchy of \libaldor{} and \libalgebra{}, as
shown in Figure~\ref{fig:sumitpolcat}.
\begin{figure}[htb!]
\begin{center}
\includegraphics{sumitpolcat}
\end{center}
\caption{The \sumit{} univariate skew--polynomial category hierarchy}
\label{fig:sumitpolcat}
\end{figure}

Those categories make it possible to write functions
that work for skew--polynomials at various levels of generality,
and also to apply all the \libalgebra{} functionalities designed
for the category
\alexttype{UnivariatePolynomialAlgebra}{} to skew--polynomials.
When writing generic code for manipulating
skew-polynomials or operators, use a type parameter with the
appropriate category selected from Figure~\ref{fig:sumitpolcat}.

\subsection{Solutions of differential and difference equations}
\sumit{} provides efficient solvers for linear ordinary differential
and difference equations and systems. All the rational solvers are
usually called {\tt kernel}, the exact functionality depends
on the type used. The following tables indicate which solver to
use depending on the type of equation and the desired type of solutions.

\begin{table}[ht]
\begin{center}
\begin{tabular}{|l|c|} \hline
Equation type & Polynomial solver\\ \hline
Differential equations & \altype{LinearOrdinaryOperatorPolynomialSolutions} \\
Recurrence equations & \altype{LinearOrdinaryOperatorPolynomialSolutions} \\
Differential systems &
\altype{LinearOrdinaryFirstOrderSystemPolynomialSolutions} \\
Recurrence systems &
\altype{LinearOrdinaryFirstOrderSystemPolynomialSolutions} \\ \hline
\end{tabular}
\caption{\sumit{} solvers for polynomial solutions}
\end{center}
\end{table}

Note that the same types are used to compute
the polynomial solutions of differential and difference equations
and systems. However, the
\emph{RXY} and \emph{FX} parameters to the polynomial solvers tell them
which type of equation to solve.
For differential equations and systems, \emph{RXY} should be a type
of category \altype{LinearOrdinaryDifferentialOperatorCategory}
and \emph{FX} should be a type of
category \alexttype{UnivariatePolynomialCategory}{}.
For recurrence equations and systems, \emph{RXY} should be a type
of category \altype{LinearOrdinaryRecurrenceCategory}
and \emph{FX} should be the type \alexttype{UnivariateFactorialPolynomial}{}
since the solutions are computed as factorial polynomials.

\begin{table}[ht]
\begin{center}
\begin{tabular}{|l|c|} \hline
Equation type & Rational solver\\ \hline
Differential equations &
\altype{LinearOrdinaryDifferentialOperatorRationalSolutions} \\
Recurrence equations & \altype{LinearOrdinaryRecurrenceRationalSolutions} \\
Differential systems &
\altype{LinearOrdinaryDifferentialSystemRationalSolutions} \\
Recurrence systems &
\altype{LinearOrdinaryRecurrenceSystemRationalSolutions} \\ \hline
\end{tabular}
\caption{\sumit{} solvers for rational solutions}
\end{center}
\end{table}

For solving systems, first create a system as
an element of \altype{LinearOrdinaryFirstOrderSystem}.
That type does not specify whether its elements represent
differential or difference equations, they are interpreted
differently by the various solvers depending on the other
parameters.

\begin{table}[ht]
\begin{center}
\begin{tabular}{|l|c|} \hline
Equation type & Hyperexponential solver\\ \hline
Differential equations &
  \altype{LinearOrdinaryDifferentialOperatorRegularSolutions} \\
& \altype{LinearOrdinaryDifferentialOperatorExponentialSolutions} \\
Recurrence equations &
\altype{LinearOrdinaryRecurrenceHypergeometricSolutions} \\
Differential systems & N/A \\
Recurrence systems & N/A \\ \hline
\end{tabular}
\caption{\sumit{} solvers for hyperexponential solutions}
\end{center}
\end{table}

Note that the above solvers find radical, regular or hyperexponential
solutions over the constant field given as parameter, and not over
its algebraic closure.

\begin{table}[ht]
\begin{center}
\begin{tabular}{|l|c|} \hline
Equation type & Series solver\\ \hline
Differential equations &
\altype{LinearOrdinaryDifferentialOperatorSeriesSolutions} \\
Recurrence equations & N/A \\
Differential systems &
\altype{LinearOrdinaryDifferentialSystemSeriesSolutions} \\
Recurrence systems & N/A \\ \hline
\end{tabular}
\caption{\sumit{} solvers for series solutions}
\end{center}
\end{table}

Note that the series solver are meant for solving only around an ordinary point
of the equation or system.

Finally, there are specialized solvers and factorizers for second and
third order differential and difference equations.

\begin{table}[ht]
\begin{center}
\begin{tabular}{|l|c|} \hline
Equation type & Solver\\ \hline
Second order differential equations &
\altype{SecondOrderLinearOrdinaryDifferentialSolver} \\
Second order recurrence equations &
\altype{SecondOrderLinearOrdinaryRecurrenceSolver} \\
Third order differential equations &
\altype{ThirdOrderLinearOrdinaryDifferentialSolver} \\
Third order recurrence equations &
\altype{ThirdOrderLinearOrdinaryRecurrenceSolver} \\ \hline
\end{tabular}
\caption{\sumit{} solvers for second and third order equations}
\end{center}
\end{table}

\subsection{Compatibility with C types}
All the types of \libalgebra{} that are compatible with their C counterparts
remain so inside \sumit.

\subsection{Using GMP}
\altarget{sec:gmp}
As in \libaldor{} and \libalgebra, the type {\tt Integer} is a macro,
which defaults to \alexttype{AldorInteger}{}, the software integers provided
by the \aldor{} runtime. For efficiency or other reasons, you may prefer
to use the GMP library, which is supported by \sumit.
The easiest way
to use GMP is to compile all your source files with the option {\tt -dGMP}
and then to use the options
\begin{center}
{\tt -lsumit-gmp -lalgebra-gmp -laldor -cruntime=foam-gmp,gmp}\\
{\tt -y\$SUMITROOT/lib/-y\$ALGEBRAROOT/lib}
\end{center}
when linking your final executable. All you need is GMP 3.0 or later
installed in a file called {\tt libgmp.a} to produce executables.
Using GMP generally produces more efficient programs,
but programs calling GMP cannot be interpreted by the \aldor{}
compiler, nor can they run inside its interactive loop.

Using the {\tt -dGMP} option allows you to compile the same sources
either with or without GMP, which can be appreciable, but you must
ensure that you do not mix files compiled with and without {\tt -dGMP} since
the macro {\tt Integer} would then be expanded into two different types.

An additional advantage of using GMP, is that \alexttype{GMPInteger}{}
exports and uses internally several of the in--place or higher--level
operations of GMP, which are not available with \alexttype{AldorInteger}{}.
In addition, variables of type \alexttype{GMPInteger}{} are compatible
with the C type {\tt mpz\_t} from GMP, so you can directly call
C programs that use GMP from your \aldor{} code.

\subsection{Exceptions}
\sumit{} does not define or throw any exception.

\subsection{Profiling and debugging}
\altarget{sec:debug}
The macros TIME, TRACE and AGAT provided by \libaldor{} remain available
in \sumit. In addition,
\sumit{} also comes with a debug version, which makes many assertions
about the arguments of the functions called as well as their results.
While those assertions slow down the code considerably they tend to
be quite useful when chasing bugs since the release version of \sumit{}
does not make validity checks to most of its inputs.
The debug version of \sumit{} must be used jointly with the debug
versions of \libaldor{} and \libalgebra.
To use them, just compile your application with the
\begin{center}
{\tt -lsumitd -lalgebrad -laldord -dDEBUG}
\end{center}
options rather than {\tt -lsumit -lalgebra -laldor}.
It is also preferable when debugging to add the {\tt -q1} option
in order to prevent inlining.

