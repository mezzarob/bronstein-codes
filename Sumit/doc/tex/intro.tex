% ======================================================================
% This code was written all or part by Dr. Manuel Bronstein from
% Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
% decided to publish this code under the CeCILL open source license in
% memory of Dr. Manuel Bronstein.
% 
% This software is governed by the CeCILL license under French law and
% abiding by the rules of distribution of free software. You can use,
% modify and/or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and Inria at the following URL :
% http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
% 
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided
% only with a limited warranty and the software's author, the holder of
% the economic rights, and the successive licensors have only limited
% liability.
% 
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards
% their requirements in conditions enabling the security of their
% systems and/or data to be ensured and, more generally, to use and
% operate it in the same conditions as regards security.
% 
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.
% ======================================================================
% 
\section{Introduction}

\subsection*{What is \sumit?}
\sumit{} is a computer algebra library designed to provide reusable and
efficient implementations of algorithms for manipulating and solving
linear ordinary differential and difference equations and systems.
Built as an extension of the
\libalgebra{} library, it provides \aldor{} programmers with a extensible
computer algebra layer with many low--level functions for handling
skew--polynomials and their related skew--linear systems.

\subsection*{How do I get and install \sumit?}
You can download \sumit{} by anonymous ftp from the \cafe server at\\
\vspace{-5mm}
\begin{center}
{\tt ftp-sop.inria.fr} in {\tt cafe/software/sumit}
\end{center}
or from the URL:\\
\vspace{-5mm}
\begin{center}
\url{http://www.inria.fr/cafe/Manuel.Bronstein/sumit/}
\end{center}
After downloading the file {\tt sumit.tar.gz}, issue
``{\tt tar -xvzf sumit.tar.gz}'' in order to unpack it.
This will create the following directories:
\begin{itemize}
\item{\tt sumit/doc}: this user guide,
\item{\tt sumit/lib}: the library,
\item{\tt sumit/include}: the required include files,
\item{\tt sumit/test}: some test files,
\item{\tt sumit/samples}: \sumit{} programming samples,
\end{itemize}
Once the above file is unpacked, do the following:
\begin{itemize}
\item add the option {\tt -csys=XXX} to your ALDORARGS environment variable,
where {\tt XXX}
depends on your hardware and operating system. Common values for {\tt XXX}
are {\tt axposf1v4} for OSF1 V4.0 on a DEC Alpha, {\tt linux-486} for linux on
a 486 or above PC, and {\tt sun4os55g-v8} for SunOS 5.5 on a SPARC v8 machine.
See the file {\tt \$ALDORROOT/include/aldor.conf} for other values;
\item go to the {\tt sumit/lib} directory and execute {\tt sh makesumit};
\item if you want to build the GMP version of the library,
execute {\tt sh makesumit-gmp}.
See the subsection on~\alalias{\this}{sec:gmp}{using GMP}
for more information about using the GMP version of \sumit;
\item if you want to build the debug version of the library,
execute {\tt sh makesumitd}.
See the subsection on~\alalias{\this}{sec:debug}{debugging}
for more information on using the debug library.
\end{itemize}

\subsection*{How do I use \sumit{} in my programs?}
Once \sumit{} is properly built, you need to set the following environment
variables before using it:
\begin{itemize}
\item the environment variables ALGEBRAROOT and SUMITROOT should be set
respectively to
the main {\tt algebra} and {\tt sumit} directories;
\item {\tt \$ALGEBRAROOT/include} and {\tt \$SUMITROOT/include}
should be appended to your INCPATH environment variable;
\item {\tt \$ALGEBRAROOT/lib} and {\tt \$SUMITROOT/lib}
should be appended to your LIBPATH environment variable;
\end{itemize}
In your \aldor{} programs, use {\tt \#include "sumit"} {\em instead of}
{\tt \#include "aldor"} or {\tt \#include "algebra"}.
When building your final executable, add the
options
\begin{center}
{\tt -lsumit -lalgebra -laldor -y\$SUMITROOT/lib -y\$ALGEBRAROOT/lib}
\end{center}
to your compiler command line, or
\begin{center}
{\tt -lsumitd -lalgebrad -laldord -dDEBUG -y\$SUMITROOT/lib -y\$ALGEBRAROOT/lib}
\end{center}
to link to the debug version of \sumit.
Check the subsection on~\alalias{\this}{sec:gmp}{using GMP}
for the options required if you want to use the GMP library and the GMP
version of \sumit.
As with any \aldor{} program, do not forget
the {\tt -q} option in order to optimize your programs, specially
if performance is an issue.

Before using \sumit{} for the first time, please check your installation
by running {\tt make} in the {\tt sumit/test} directory, followed by
running {\tt testall}.

Please report any installation problem or bugs you encounter
to {\tt sumit@sophia.inria.fr}.
