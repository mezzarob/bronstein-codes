-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
--------------------------- slowroot2.as --------------------------
#if IGNORETHIS
Timings (ru7/msecs):			1700

Axiom (ru7/Q[x], rational roots):	 570
Maple(ru7/rational roots):		 233
#endif

#include "sumit"

macro {
	Z == Integer;
	P == DenseUnivariatePolynomial(F, "x");
	IR == Record(root:Z,multiplicity:Z);
	NTIMES == 2;
}

#if RATIONAL
macro F == Quotient Integer;
macro PKG == UnivariateRationalFactorizer(F, P);
f(n:Z):P == n::F::P;
#else
macro F == Integer;
macro PKG == UnivariateIntegralFactorizer(F, P);
f(n:Z):P == n::P;
#endif

go(q:P):(Z,Z) == {
	import from SingleInteger, Timer, PKG;
	t := timer(); tfac := timer();
	for i in 1@SingleInteger..NTIMES repeat {
		start! t;
		l := integerRoots q;
		stop! t;
		start! tfac;
		qf := factor q;
		stop! tfac;
	}
	print << "# of integer roots = " << #l << newline;
	for r in l repeat
	  print<<"root = " << r.root << ", mult = " << r.multiplicity <<newline;
	((read(t) quo NTIMES)::Z, (read(tfac) quo NTIMES)::Z);
}

import from Z, F, P, IR, List IR;

x:P := monom;
p := (x-1)^2*(x-f 3)^2*(3*x-f 4)^2*(3*x-f 7)^2*(2*x-f 3)^2;
p := p *(2*x-f 5)^2*(3*x-f 5)^2*(3*x-f 8)^2*(x-f 2)^4;
(tr,tf) := go p;
print << "time per iroot = " << tr << " millisecs" << newline;
print << "time per factor = " << tf << " millisecs" << newline;
#if RATIONAL
p := -3125/104976 * p;
(tr,tf) := go p;
print << "time per iroot = " << tr << " millisecs" << newline;
print << "time per factor = " << tf << " millisecs" << newline;
#endif

