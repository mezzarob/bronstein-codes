-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
#include "sumit"

macro {
	SI == SingleInteger;
	Z == Integer;
	Q == Quotient Z;
	Px == DenseUnivariatePolynomial(Q, "x");
	P == DenseUnivariatePolynomial(Px, "y");
	-- COST == 100;
	-- COST == 10;
	-- COST == 2;
	COST == 1;
}

-- returns the time for m naive multiplications
-- n = cutoff
local time(p:P, n:SI, m:SI):SI == {
	import from Timer;
	t := timer();
	start! t;
	for i in 1..m repeat pp := p * p;
	tm := stop! t;
	reset! t;
	start! t;
	for i in 1..m repeat kp := karatsuba(p, p, n);
	tk := stop! t;
	if ~zero?(kp - pp) then print << "ERROR" << newline;
	print << tk << newline;
	tm;
}

local time(q:P, m:SI):() == {
	import from Z;
	maxcut := 10 + retract degree q;
	tm:SI := 0;
	for i in 1 .. maxcut repeat tm := tm + time(q, i, m);
	print << "time for * = " << tm quo maxcut << newline << newline;
}

local kara(d:SI, m:SI):() == {
	import from Z;
	print << "degree = " << d << newline;
	print << "products = " << m << newline;
	import from P;
	q:P := 0;
	for i in 0..d repeat {
		c:Px := random 5;
		q := add!(q, c, i::Z);
	}
	time(q, m);
}

main():() == {
	import from SI;
	-- kara(5, 10 * COST);
	-- kara(10, 5 * COST);
	kara(5, COST);
	kara(10, COST);
	kara(15, COST);
	kara(20, COST);
}

main();
