-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ mpoly.as ------------------------
--
-- test from "Serge D. Mechveliani" <mechvel@botik.ru> :
--
--      P := MPOLY([x,y], Fraction Integer)
--     )set message time on
--     f :P := x^2 + (2/3)*x*y + (3/4)*y;
--     n := ...;
--     f^n;
--
-- Axiom 2.3 on panoramix:   N   10   100   1000
-------------------------------------------------
--                         secs 0.01  0.71   430
--
-- Aldor 1.0.1 Q/SUP:        N   10   100
------------------------------------------
--     Z/old SUP (secs)         0.01   10
-- old Q/old SUP (secs)         0.01   42
--     Z/new SUP (secs)         0.01   10
-- old Q/new SUP (secs)         0.01   42

#include "algebra"
#include "aldorio"

macro {
	Z == Integer;
	Q == Fraction Z;
	-- R == Z;
	R == Q;
	-- OS == OrderedSymbol;
	OS == OrderedVariableList([-"x",-"y"]);
	UP == SparseUnivariatePolynomial;
	-- UP == DenseUnivariatePolynomial;
	-- MP == RecursiveMultivariatePolynomial0;
	MP == DistributedMultivariatePolynomial1;
	-- P == MP(UP, R, OS);
	P == MP(R, OS, MachineIntegerDegreeLexicographicalExponent OS);
}

import from Symbol, List Symbol;

local time(f:P, n:Z):() == {
	import from MachineInteger, Timer;
	t := timer();
	start! t;
	fn := f ^ n;
	msecs := stop! t;
	stdout << "n = " << n << "  time = " << msecs << " msecs" << newline;
}

local main():() == {
	import from Z, Q, OS, P, Partial OS;
	-- x := orderedSymbol("x") :: P;
	x := retract(variable(-"x")) :: P;
	-- y := orderedSymbol("y") :: P;
	y := retract(variable(-"y")) :: P;
	-- f := 12*x^2 + 8*x*y + 9*y;
	f := x^2 + (2/3)*x*y + (3/4)*y;
	n:Z := 1;
	while n > 0 repeat {
		time(f, n);
		stdout << "n = ";
		n := << stdin;
	}
}

main();
