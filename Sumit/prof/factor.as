-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------------- factor.as -------------------------------

#if IGNORETHIS
#endif

#include "sumit"
#include "sallio"

macro {
	I == MachineInteger;
	Z == Integer;
	ZX == DenseUnivariatePolynomial(Z, -"x");
}

import from Symbol;

local fact(A:ZX):(Z, Product ZX) == {
	import from I, Timer;
	t := timer();
	start! t;
	(c, pr) := factor A;
	stdout << "time to factor = " << stop! t << endnl;
	(c, pr);
}

local main():() == {
	import from Z, ZX, Product ZX;

#if GINTERP
local initSalliInterpreterLoop():() == {
        import from RandomNumberGenerator, MachineInteger;
        seed(randomGenerator 0, 123456789);
}

initSalliInterpreterLoop();
#endif

	x:ZX := monom;

p := (x+1)*(5*x^2+28*x+40::ZX)*(x^5+2*x^4+x^3-1)
		*(35*x^12+1197*x^11+18601*x^10+173641*x^9+1084359*x^8
		+4771975*x^7+15174249*x^6+35131483*x^5+58780078*x^4
		+69325040*x^3+54719543*x^2+25961546*x+5601201::ZX);

q := (x+3::ZX)*(x^5+2*x^4+x^3-1)*(x^5+12*x^4+57*x^3+134*x^2+156*x+71::ZX)
		*(144800364971930088*x^14-429527084948195328*x^2
		+2727617808607025392*x^5+526750699558788800*x^4
		-430897267459156992*x^3+5427063366840711488*x^6
		+7272119265411872568*x^7+6009552308755872207*x^9
		+7396739615573128276*x^8+2236211972940487486*x^11
		+4010326561809781100*x^10+422470602712010383*x^13
		+1053598887889034148*x^12+11139225*x^25+293731160*x^24
		+3125*x^27+269500*x^26+10690640000690488*x^16
		+42510496130232295*x^15+2298674737737014*x^17
		+420938184680612*x^18+65241125847569*x^19
		+79944064908*x^22+913004285131*x^21+5548882037*x^23
		+8481124303928*x^20-167979774364640256*x-26905062168395776::ZX)
		*(x+2::ZX)^2*(x+1::ZX)^3;

	stdout << "p = " << p << endnl;
	(cp, f) := fact p;
	stdout << "c = " << cp << endnl;
	stdout << "f = " << f << endnl << endnl;

	stdout << "q = " << q << endnl;
	(cq, g) := fact q;
	stdout << "c = " << cq << endnl;
	stdout << "g = " << g << endnl << endnl;
}

main();
