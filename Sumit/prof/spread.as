-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------------- spread.as -------------------------------

#if IGNORETHIS
SMALL: Spread of a and b = {-5,-2,0}
BIG:   Spread of a and b = {-5,-4,-3,-2,-1,0,1}

TIME (obelix)
                              E(Hendriks)  E(2-section)
9/2000 - naive resultant:     45'56"              *
9/2000 - orbital spread:          7"            30'34"
9/2000 - content-based:           0.5"              2"
9/2000 - one-candidate:           0.5"              2"

Maple V (LREtools)                0.2"		    0.7"
#endif


#include "sumit"
#include "sallio"

macro {
	I == MachineInteger;
	Z == Integer;
	ZX == DenseUnivariatePolynomial(Z, -"x");
}

import from Symbol;

local dist(A:ZX, B:ZX):List Z == {
	import from I, Timer;
	t := timer();
	start! t;
	l := integerDistances(A, B);
	stdout << "time to compute spread = " << stop! t << endnl;
	l;
}

local main():() == {
	import from Z, ZX, List Z;

#if GINTERP
local initSalliInterpreterLoop():() == {
        import from RandomNumberGenerator, MachineInteger;
        seed(randomGenerator 0, 123456789);
}

initSalliInterpreterLoop();
#endif

	x:ZX := monom;

#if SMALL
	stdout << "SMALL" << endnl;
an := (x+3::ZX)*(5*x^2+18*x+17::ZX)*(x^5+22*x^4+193*x^3+844*x^2+1840*x+1599::ZX)
		*(35*x^12+777*x^11+7744*x^10+45766*x^9+178455*x^8+483349*x^7
		+932168*x^6+1289968*x^5+1272162*x^4+873128*x^3+396817*x^2
		+107648*x+13184::ZX);

b := (x+1)*(5*x^2+28*x+40::ZX)*(x^5+2*x^4+x^3-1)
		*(35*x^12+1197*x^11+18601*x^10+173641*x^9+1084359*x^8
		+4771975*x^7+15174249*x^6+35131483*x^5+58780078*x^4
		+69325040*x^3+54719543*x^2+25961546*x+5601201::ZX);
#else
	stdout << "BIG" << endnl;
an := (x+5::ZX)*(x^5+27*x^4+291*x^3+1565*x^2+4200*x+4499::ZX)
		*(x^5+17*x^4+115*x^3+387*x^2+648*x+431::ZX)
		*(490367762691281*x^14-208474635621632*x^2-3745648645542864*x^5
		-1965825468781248*x^4-766044956286848*x^3-5480408761012796*x^6
		-6250811660851864*x^7-3658984585854737*x^9-5534541040081419*x^8
		-8870124852842*x^11-1542589274160634*x^10+713910569980330*x^13
		+670444665914848*x^12+5229100*x^25+93697410*x^24+3125*x^27
		+185125*x^26+109567425779371*x^16+258021709813841*x^15
		+38335538197539*x^17+11138015776133*x^18+2688698283069*x^19
		+11546104467*x^22+87698662176*x^21+1195245447*x^23
		+536675395558*x^20-35216529233920*x-2758087073792::ZX)
		*(x+3::ZX)^2*(x+4::ZX)^3;
b := (x+3::ZX)*(x^5+2*x^4+x^3-1)*(x^5+12*x^4+57*x^3+134*x^2+156*x+71::ZX)
		*(144800364971930088*x^14-429527084948195328*x^2
		+2727617808607025392*x^5+526750699558788800*x^4
		-430897267459156992*x^3+5427063366840711488*x^6
		+7272119265411872568*x^7+6009552308755872207*x^9
		+7396739615573128276*x^8+2236211972940487486*x^11
		+4010326561809781100*x^10+422470602712010383*x^13
		+1053598887889034148*x^12+11139225*x^25+293731160*x^24
		+3125*x^27+269500*x^26+10690640000690488*x^16
		+42510496130232295*x^15+2298674737737014*x^17
		+420938184680612*x^18+65241125847569*x^19
		+79944064908*x^22+913004285131*x^21+5548882037*x^23
		+8481124303928*x^20-167979774364640256*x-26905062168395776::ZX)
		*(x+2::ZX)^2*(x+1::ZX)^3;
#endif


	a := an(x - 4::ZX);

	-- stdout << "a = " << a << endnl;
	-- stdout << "b = " << b << endnl;

	l := dist(a, b);
	stdout << "l = " << l << endnl;
}

main();
