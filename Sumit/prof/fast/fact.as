-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
#include "algebra"
#include "aldorio"

#if IGNORETHIS

Obelix:              spf           zpf
1.1.11e gcd!:       4'57          2'50
shoup:				  2'50
new upmod:			  2'50
new charppower:                   2'50
new prime fields:		  2'30
1.1.13p1-4/01:	    1'56	  1'09

Axiom/obelix/sparse/spf = 1:00'00
MapleV.5/obelix/modp1   =    2'14
Maple6/obelix/modp1     =    1'53

#endif

macro Z == Integer;

#if ZECH
macro F == ZechPrimeField C;
macro ARITH == "ZPF";
#else
macro F == SmallPrimeField C;
macro ARITH == "SPF";
#endif

macro {
	C == (11@MachineInteger);
	N == 3@MachineInteger;
	Fx == DenseUnivariatePolynomial(F, -"x");
}


getp(R:IntegralDomain, Rx:UnivariatePolynomialCategory R):Rx == {
	import from Z;
	x:Rx := monom;
	3*x^1000+x^842+6*x^798+2*x^633+5*x^595+2*x^284+5*x^86+(2@Z)::Rx;
}

main():() == {
	import from Symbol, MachineInteger, Z, Fx, Timer;
	import from PrimeFieldUnivariateFactorizer(F, Fx), List Fx;

	stdout << "N = " << N << newline;
	stdout << "Arithmetic = " << ARITH << newline;

	p := getp(F, Fx);
	t := timer();
	for i in 1..N repeat {
		start! t;
		l := cantorZassenhaus p;
		tt := stop! t;
		stdout << "Time = " << tt << " millisecs" << endnl;
	}
	stdout << newline << newline;
	for q in l repeat stdout << degree q << " ";
	stdout << newline << newline;
}

main();
