-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
--------------------------- deter.as --------------------------
#if IGNORETHIS
Timings (obelix/msecs):
SIZE = 100
N	10	 50	 100	 200
FF2GE    3	425	4905	82133
CRT      0      276	4561	74233
VERIF		 56	 766	12816
k-VERIF	 0	 15	 166	 1867

k-CRT	 3	113	1122	15850
Hensel2 23     1323	9661    73125
Hensel1		780	5422	42450

MapleV.5         4"	1'37"	 45'
#endif

#include "sumit"

macro {
	I	== SingleInteger;
	Z	== Integer;
	N	== 50@I;		-- matrix size
	NTIMES	== 10@I;		-- number of repetitions
	SIZE	== 100@Z;	-- -SIZE < entries < +SIZE
	M	== DenseMatrix Z;
	FFGE	== TwoStepFractionFreeGaussElimination(Z, M);
}

import from I, Z, M;

local ffdet(mat:M):(I, Z) == {
	import from Timer;
	t := timer();
	start! t;
	for i in 1..NTIMES repeat d := determinant mat;
	stop! t;
	(read(t) quo NTIMES, d);
}

local crtdet(mat:M):(I, Z) == {
	import from Timer, ModularLinearAlgebra(Z, M), Partial Z;
	t := timer();
	start! t;
	for i in 1..NTIMES repeat d := crtDeterminant mat;
	stop! t;
	failed? d => error "CRT determinant failed";
	(read(t) quo NTIMES, retract d);
}

local moddet(mat:M):(I, Z, Z) == {
	import from Timer, ModularLinearAlgebra(Z, M), Partial Z;
	t := timer();
	start! t;
	for i in 1..NTIMES repeat d := modularDeterminant mat;
	stop! t;
	failed? d => error "HENSEL determinant failed";
	(read(t) quo NTIMES, retract d, hadamard mat);
}

print << "Matrix size = " << N << newline;
print << "Entry size = " << SIZE << newline;
m := random(N, N);
for i in 1..N repeat for j in 1..N repeat
	m(i, j) := (m(i, j) rem (SIZE+SIZE)) - SIZE;
-- (t0, d0) := ffdet m;
(t1, d1) := crtdet m;
(t2, d2, h) := moddet m;
d1 ~= d2 => {
	print << "m = " << m << newline;
	-- print << "FF-det = " << d0 << newline;
	print << "CRT-det = " << d1 << newline;
	print << "HENSEL-det = " << d2 << newline;
	error "determinants do not match!";
}

print << "log-determinant = " << length d1 << newline;
print << "log-hadamard = " << length h << newline;
-- print << "time per det (FF) = " << t0 << " millisecs" << newline;
print << "time per det (CRT) = " << t1 << " millisecs" << newline;
print << "time per det (HENSEL) = " << t2 << " millisecs" << newline;
