-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
--------------------------- deterp.as --------------------------
#include "algebra"
#include "aldorio"

#if IGNORETHIS
Timings (obelix/msecs):
N          50    100     200     300
LAZY-spf    6     40     280     827
SPF-spf    13    100     766    2528
OGE-spf    33    280    2360    7250
SPF-zpf     6     45     336
OGE-zpf    21    170    1513    4555

0.1.11	    5     35	 222	 750    milliseconds
0.1.12-3/00 5     25     220     706        "
0.1.12-6/00 4     25     188     690
0.1.13-4/01 4	  28	 200	 626

Maple V.5   2     23     270		seconds...
#endif

macro {
	I	== MachineInteger;
	p	== 10007@I;
	N	== 300@I;		-- matrix size
	NTIMES	== 5@I;		-- number of repetitions
	M	== DenseMatrix F;
}

#if ZECH
macro F == ZechPrimeField p;
macro ARITH == "ZPF";
#else
macro F == SmallPrimeField p;
macro ARITH == "SPF";
#endif

import from I, F, M;

local spfdet(mat:M):(I, F) == {
	import from Timer, LinearAlgebra(F, M);
	t := timer();
	start! t;
	for i in 1..NTIMES repeat d := determinant mat;
	stop! t;
	(read(t) quo NTIMES, d);
}

local ogedet(mat:M):(I, F) == {
	import from Timer, OrdinaryGaussElimination(F, M);
	t := timer();
	start! t;
	for i in 1..NTIMES repeat d := determinant mat;
	stop! t;
	(read(t) quo NTIMES, d);
}

stdout << "Matrix size = " << N << newline;
stdout << "Arithmetic = " << ARITH << newline;

m := random(N, N);
(t1, d1) := spfdet m;
(t2, d2) := ogedet m;
d1 ~= d2 => {
	stdout << "m = " << m << newline;
	stdout << "SPF-det = " << d1 << newline;
	stdout << "OGE-det = " << d2 << newline;
	error "determinants do not match!";
}

stdout << "determinant = " << d1 << newline;
stdout << "time per det (SPF) = " << t1 << " millisecs" << newline;
stdout << "time per det (OGE) = " << t2 << " millisecs" << newline;
