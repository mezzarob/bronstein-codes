-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
#include "sumit"

macro {
	I == SingleInteger;
	Z == Integer;
	ZX == DenseUnivariatePolynomial(Z, "x");
	ZXD == LinearOrdinaryDifferentialOperator(ZX, "D");
	Zx == Quotient ZX;
	ZxD == LinearOrdinaryDifferentialOperator(Zx, "D");
	V == Vector
}

local trace(L:ZxD, i:SingleInteger):() == {
	import from ExpressionTree;
	print << "L" << i << " := ";
	maple(print, extree L);
	print << ";" << newline;
}

local trace(v:V ZxD):() == {
	import from I;
	for i in 1..#v repeat trace(v.i, i);
}

main():() == {
	import from Z, ZX, ZXD, Zx, ZxD, V ZxD, Eigenring(Z, ZX, ZXD, ZxD);
	x:ZX := monom;
	-- D:ZXD := monom;
	-- L := 16 * x^2 * D^2 + 3::ZXD;
	-- trace eigenring L;
	xx := x::Zx;
	DD:ZxD := monom;
	-- LL := 16 * xx^2 * DD^2 + 3::ZxD;
	-- LL := DD^2;
	-- LL := DD^3 - xx*DD^2 -xx*DD + (xx^2 - 1)::ZxD;	-- (D-x)(D^2-x)
	LL := (xx^2-xx+1)*DD^3+(-xx^3+xx^2-3*xx+1)*DD^2		-- llcm(D-x,
	-- 	+(-xx^3+xx^2-xx)*DD+ (xx^4-xx^3+2*xx^2-1)::ZxD;	--      D^2-x)

	-- LL := DD^4-2*xx*DD^2-2*DD+(xx^2)::ZxD;	-- (D^2 - x)^2
	trace eigenring LL;
}

main();
