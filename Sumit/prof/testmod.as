-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-------------------- mod.as ---------------
#include "sumit"

macro {
	I == SingleInteger;
	Z == Integer;
	N == 10000;
}

local fact(n:Z):Z == { zero? n => 1; n * fact prev n };

local modp(p:I):I == {
	import from Timer, Z;
	t := timer();
	a := fact 150; pz := p::Z;
	for i in 1@I .. N repeat {
		start! t; b := a mod pz; stop! t; a := next a;
	}
	read t;
}

import from I;

t32 := modp 2147481337;
t16 := modp 32749;

print << "Time for " << N << " reductions mod 32 bit = " << t32 << newline;
print << "Time for " << N << " reductions mod 16 bit = " << t16 << newline;
