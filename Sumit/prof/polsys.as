-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-------------------------- polsys.as -----------------------
#if IGNORETHIS
Timings (obelix/msecs):

Legendre:

  N    K    bound=N/2   bound=N    ratsolde
--------------------------------------------
 100   1        26        58        3000
 100   5       151       368        9036
 100  10       440      1273       37056
1000   1       298       900       38262
1000   5      1798      6515      149741
1000  10      5472     21089      768165

#endif

#include "sumit"

macro {
	I == SingleInteger;
	Z == Integer;
	R == Z;
	ZX == DenseUnivariatePolynomial(R, "x");
	M == DenseMatrix;
	N == 100@Z;
	K == 1@I;
	NSOL == 10@I;
	SYS == LinearOrdinaryDifferentialSystem ZX;
}

-- legendre polynomials repeated k times
local legendre(n:Z, k:I):SYS == {
	import from ZX, M ZX;
	m := zero(k+k, k+k);
	a := monom * monom - 1;
	b := (n*(n+1))::ZX;
	c := -2 * monom;
	for i in 1..k repeat {
		j := 2 * prev i;
		m(j + 1, j + 2) := a;
		m(j + 2, j + 1) := b;
		m(j + 2, j + 2) := c;
	}
	system(a, m);
}

local solve(L:SYS, bound:Z):I == {
	import from Timer;
	import from ZX, M ZX, LODSPolynomialSolutions(Z, ZX);
	t := timer();
	for i:I in 1..NSOL repeat {
		start! t;
		k := polynomialKernel(L, bound);
		stop! t;
	}
	print << "solution for N = " << N << " and bound = " << bound<< newline;
	if zero? cols k or bound < 10 then print  << k << newline;
	else print << "has degree " << degree k(1,1) << newline;
	read(t) quo NSOL;
}

local main():() == {
	import from I, Z, ZX, M ZX, SYS;
	L := legendre(N, K);
	(a, A) := matrix L;
	print << "solving  " << a << " Y' = " << A << " Y" << newline;

	t1 := solve(L, N quo 2);
	t2 := solve(L, N);

	print << "time for bound N/2 = " << t1 << newline;
	print << "time for bound N = " << t2 << newline;
}

main();

