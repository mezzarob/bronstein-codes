-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
#include "sumit"

#if IGNORETHIS
Legendre - idefix / 0.1.9 - 0.1.10
N		 10	 100	1000	5000	10000	15000
ratlode2/V.3	0.07	0.07	0.57	 6.08	28.22	88.07
ratlode2/V.4	0.08	0.16	0.67	 9.63	27.93	81.98
Q[x]		0.08	0.16	1.16	 7.24	26.80	47.03
Z[x]		0.03	0.06	0.78	53.98	  *	  *
undet.coeffs	0.15	4.32	 *	 *	  *	  *
Q[x] poch. rec. 0.03	0.23	1.57	11.60	28.78	60.45
0.1.10 / Q[x]	0.04	0.09	0.81	 7.95	25.09	55.44

Legendre - darwin3 / 0.1.10
N		 10	 100	1000	5000	10000	15000
0.1.10 / Q[x]	0.02    0.06	0.52	 5.05   16.34	 37.53
Z[x] via Q[x]	0.02	0.07	0.58	 7.39	29.08	 64.48
Z[x] direct	0.02            0.68    23.66
ISOLDE-R4	0.02	0.04	0.58	 8.57	41.02	120.85

Barkatou - darwin3 / 0.1.10
N		 10	 20	 30	  40	  50	 60	 70	 80
0.1.10 / Q[x]	0.05	0.20	0.50	 1.16	 2.68	 7.35	11.46	27.22
Z[x] via Q[x]	0.06	0.35	1.90	 8.12	31.97	93.87
ISOLDE-R4	0.05	0.33	1.25	 3.96	10.44	24.19

Barkatou - nirvana / 0.1.10
N		 10	 20	 30	  40	  50	 60	 70	 80
bernina/no-gmp	0.31	0.95	3.03	 8.71	36.58

macro {
	I == SingleInteger;
	Z == Integer;
	Zx == DenseUnivariatePolynomial(Z, "x");
	ZxD == LinearOrdinaryDifferentialOperator(Zx, "D");
	Q == Quotient Z;
	Qx == DenseUnivariatePolynomial(Q, "x");
	QxD == LinearOrdinaryDifferentialOperator(Qx, "D");
	NSOL == 2;
	N == 5000;
}

local solve(op:ZxD):I == {
	import from I, Z, Vector Zx, Zx, Timer;
	import from LODOPolynomialSolutions(Z, Zx, ZxD);
	print << "Solving $" << op << "$\\" << newline;
	t := timer();
	start! t;
	sols := polynomialKernel op;
	msecs := stop! t;
	if zero? #sols then print << "No polynomial solutions" << newline;
	else {
		print << #sols;
		print << "-dimensional space of polynomial solutions found:\\";
		print << newline;
		for i in 1..#sols repeat {
			d := degree(sols.i);
			print << "degree = " << d << newline;
			if d <= 10 then
				print << "$$" << sols.i << "$$" << newline;
		}
	}
	print << newline;
	msecs;
}

local solve(op:QxD):I == {
	import from I, Z, Q, Vector Qx, Qx, Timer;
	import from LODOPolynomialSolutions(Q, Qx, QxD);
	print << "Solving $" << op << "$\\" << newline;
	t := timer();
	start! t;
	sols := polynomialKernel op;
	msecs := stop! t;
	if zero? #sols then print << "No polynomial solutions" << newline;
	else {
		print << #sols;
		print << "-dimensional space of polynomial solutions found:\\";
		print << newline;
		for i in 1..#sols repeat {
			d := degree(sols.i);
			print << "degree = " << d << newline;
			if d <= 10 then
				print << "$$" << sols.i << "$$" << newline;
		}
	}
	print << newline;
	msecs;
}

demo():() == {
	import from I, Z, Z, Zx, ZxD, Q, Qx, QxD;
	x:Zx := monom;
	dx:ZxD := monom;
	n:Z := N;
	deq := (1 - x^2) * dx^2 - 2 * x * dx + (n * (n+1))::ZxD; -- legendre
	-- deq := n * x * dx^2 - x * dx + (n * n)::ZxD;	            -- barkatou
	t:I := 0;
	for i in 1@I ..NSOL repeat t := t + solve deq;
	print << "Time per rational kernel-Z = " << t quo NSOL << " millisecs";
	print << newline;
	-- qx:Qx := monom;
	-- qdx:QxD := monom;
	-- qdeq := (1-qx^2) * qdx^2 - 2 * qx * qdx + (n*(n+1))::QxD; -- legendre
	-- qdeq := n * qx * qdx^2 - qx * qdx + (n * n)::QxD;	     -- barkatou
	-- t := 0;
	-- for i in 1@I ..NSOL repeat t := t + solve qdeq;
	-- print << "Time per rational kernel-Q = " << t quo NSOL << " millisecs";
	-- print << newline;
}

demo();
