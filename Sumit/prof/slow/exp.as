-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ exp.as -------------------------------
#if IGNORETHIS
Timings: (obelix/msecs)

	 		Spf	Zpf
1.1.11e:    	    	430	226
new upmod:		266	106
new charppower:		121	 60
new prime fields:        83	 55

Maple V/modp1:	  30
Axiom/SAE/SUP:  2870
#endif

#include "sumit"

macro ND == 10@SingleInteger;

#if ZECH
macro F  == ZechPrimeField 11;
#else
macro F  == SmallPrimeField 11;
#endif

macro Px == DenseUnivariatePolynomial(F, "x");

import from SingleInteger, Integer, Px, Timer;

tpownew := timer(); tpow := timer();

local foo(modulus:Px, qq:Px, e:Integer):Px == {
	macro A == UnivariatePolynomialMod(F, Px, modulus, "a");
	import from A;
	qqq := reduce qq;
	start! tpow;
	for i in 1..ND repeat qe := qqq^e;
	stop! tpow;
	lift qe;
}

x := monom;

xponent:Integer := 54173529716941861020915125;

print << "exponent = " << xponent << newline;

p := _
x^50+4*x^49+7*x^48+9*x^47+5*x^46+4*x^45+2*x^44+10*x^43+10*x^42+9*x^41+6*x^39+_
3*x^38+5*x^37+7*x^36+5*x^35+4*x^34+6*x^33+9*x^32+3*x^31+9*x^30+x^29+x^28+_
5*x^27+6*x^26+5*x^23+2*x^22+3*x^20+6*x^19+8*x^18+x^17+5*x^16+9*x^15+5*x^14+_
7*x^13+9*x^11+2*x^10+9*x^9+8*x^7+8*x^6+5*x^5+7*x^4+3*x^3+9*x^2+3*x+_
5@Integer::Px;

q := x^49 + x^10 + 1;

ans := foo(p, q, xponent);

print << "q^e = " << ans << newline;
print << "total time per power = " << read(tpow) quo ND;
print << " millisecs" << newline;

