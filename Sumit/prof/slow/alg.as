-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
--------------------------- alg.as --------------------------
#if IGNORETHIS
Timings on obelix (msecs):

Maple V.4:        24
Axiom 2.0a:      850
1.1.10a:	3200   (euclidean algorithm)
1.1.11e:	3300   (euclidean algorithm)

#endif

#include "sumit"

macro {
	Z == Integer;
	R == Quotient Z;
	Ra == DenseUnivariatePolynomial(R, "a");
	-- F == UnivariatePolynomialMod(R, Ra, modulus, "a");
	F == SimpleAlgebraicExtension(R, Ra, modulus, "a");
	FX == DenseUnivariatePolynomial(F, "x");
	modulus == (monom$Ra)^2 + 222::Ra;
}

import from Z, R, Ra;

local timegcd(a:FX, b:FX):FX == {
	import from SingleInteger, Timer;
	t := timer();
	start! t;
	g := gcd(a, b);
	tm := stop! t;
	print << "time for first gcd = " << tm << newline;
	reset! t;
	start! t;
	h := gcd(a, b);
	tm := stop! t;
	print << "time for second gcd = " << tm << newline;
	g;
}

local main():() == {
	import from F, FX;
	x:FX := monom;
	A := 27*x^18+81*x^16+2079*x^14+6021*x^12+55278*x^10+149850*x^8
		+553076*x^6+1264956*x^4+1215672*x^2+405224::FX;


	a:F := reduce(monom$Ra);

	B := -3744*x^16-65759616*x^15+(1320*a-13608::F)*x^14
	     +(46368960*a-280742976::F)*x^13+(5400*a-195264::F)*x^12
	     +(214983360*a-36673632::F)*x^11+(48280*a-563688::F)*x^10
	     +(-254326720*a+5540247648::F)*x^9+(198120*a-3026896::F)*x^8
	     +(1032763200*a+21627607040::F)*x^7+(485440*a-4006656::F)*x^6
	     +(2183556480*a+310065020160::F)*x^5+(1645760*a-10075840::F)*x^4
	     +(16116726400*a+102336013696::F)*x^3+(2409440*a+39909088::F)*x^2
	     +(15388874240*a+1861284339328::F)*x+(48626880::F+1095200*a)::FX;

	g := timegcd(A, B);

	print << "gcd = " << g << newline;
}

main();
