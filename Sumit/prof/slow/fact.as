-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
#include "sumit"
#include "sallio"

#if IGNORETHIS
Obelix/msecs:
                   spf            zpf
1.1.11e euclid:    683            350
1.1.11e gcd!:      650            366
shoup:		   650		  366
new upmod:	   500		  266
new charppower:    333            166
new prime fields:  200		  166

Axiom/obelix/sparse/spf = 1850
Maple/obelix/modp1      =   90
#endif

#if GMP
macro Z == GMPInteger;
macro ARITH == "GMP";
#else
macro Z == AldorInteger;
macro ARITH == "Aldor";
#endif

#if ZECH
macro F == ZechPrimeField C;
#else
macro F == SmallPrimeField C;
#endif

macro {
	C == (11@MachineInteger);
	N == 10@MachineInteger;
	Fx == DenseUnivariatePolynomial(F, -"x");
}

getp(R:IntegralDomain, Rx:UnivariatePolynomialCategory R):Rx == {
	import from Z;
	x:Rx := monom;
	x^79+3*x^65+6*x^59+4*x^58+10*x^53+4*x^15+1;
}

main():() == {
	import from Symbol, MachineInteger, Z, Fx, Timer;
	import from PrimeFieldUnivariateFactorizer(F, Fx), List Fx;

	stdout << "N = " << N << newline;
	stdout << "Arithmetic = " << ARITH << newline;

	p := getp(F, Fx);
	t := timer();
	for i in 1..N repeat {
		start! t;
		l := cantorZassenhaus p;
		tt := stop! t;
		stdout << "Time = " << tt << " millisecs" << newline;
	}
	stdout << newline << newline;
	for q in l repeat stdout << "$$ " << q << " $$" << newline;
}

main();
