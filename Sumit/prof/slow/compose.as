-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
#include "sumit"

#if IGNORETHIS
Obelix - msecs:
                            compose       pth-power
kara:                         854           388

#endif

macro {
	I == SingleInteger;
	Z == Integer;
	N == 30@I;
	F == ZechPrimeField(11@I);
	Fx == DenseUnivariatePolynomial(F,"x");
	x == monom;
	p == 3*x^1000+x^842+6*x^798+2*x^633+5*x^595+2*x^284+5*x^86+(2@Z)::Fx;
	A == UnivariatePolynomialMod(F, Fx, p);
}

local main():() == {
	import from Timer, I, Z, Fx, A;
	xp := pthPower reduce x;
	t := timer();
	start! t;
	comp := compose xp;	-- comp(f) = f(x^p);
	stop! t;
	a := xp;
	t1 := timer(); t2 := timer();
	for i in 1..N repeat {
		start! t1;
		aa := pthPower a;
		stop! t1;
		start! t2;
		bb := comp a;
		stop! t2;
		aa ~= bb => error "Mismatch!";
		a := aa;
	}
	print << "Time for pth-power = " << read(t1) quo N << " msec" <<newline;
	print << "Time for compose = " << read(t2) quo N << " msec"<<newline;
	print << "Time for init-compose = " << read t << " msec" << newline;
}

main();

