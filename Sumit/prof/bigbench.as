-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-------------------------- bigbench.as -----------------------------
--
-- This file starts a long and memory massive computation
--
-- Adjust the following parameter before compiling to select how long
-- you want to wait...

macro EXPONENT == 4;

-- Approximate CPU times on a 333MHZ DEC Alpha:
--    EXPONENT      CPU time
------------------------------
--       3            5"
--       4           40"
--       5	      5'
--       6           30'
--
-- Multiply the above CPU times by 4 for a SPARCstation 5.
--

#include "sumit"

macro {
	Z  == Integer;
	Q  == Quotient Z;
	QX == DenseUnivariatePolynomial(Q, "x");
	QXd == LinearOrdinaryDifferentialOperator(QX, "D");
}

local hurwitz():QXd == {
	import from Z, Q, QX, QXd;
	x:QX := monom;
	dx:QXd := monom;
	op := x^2*(x-1)^2*dx^3 + x*(7*x-4::QX)*(x-1)*dx^2;
	op := op + (72/7*x^2-2963/252*x+(20/9)::QX)*dx;
	op := op + (792/343*x-(40805/24696)::QX)::QXd;
	print << "Hurwitz operator = " << op << newline;
	op;
}

local sympow(L:QXd, N:Z):QXd == {
	import from SingleInteger, Z, Timer;
	t := timer();
	print << "Starting to compute the " << N << "-th power" << newline;
	start! t;
	Ln:QXd := symmetricPower(L, N);
	(hours, minutes, secs, millisecs) := HMS stop! t;
	print << N << "-th power computed in ";
	print << hours << ":" << minutes << "'" << secs << "''" << newline;
	Ln;
}

import from Z;
sympow(hurwitz(), EXPONENT);
