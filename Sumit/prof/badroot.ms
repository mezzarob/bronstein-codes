{VERSION 1 0 "X11/Motif" "1.0"}{GLOBALS 3 1}{FONT 0 "-adobe-helve
tica-bold-r-normal--*-140-*" "helvetica" "Helvetica-Bold" 8 14 0 
"Helvetica-Bold" 12}{FONT 1 "-adobe-times-medium-r-normal--*-140-
*" "times" "Times-Roman" 4 14 64 "Times-Roman" 12}{FONT 2 "-adobe
-courier-medium-r-normal--*-140-*" "courier" "Courier" 4 14 192 "
Courier" 12}{SCP_R 1 0 15{SEP_R 2 0}{INP_R 3 0 "> "{TEXT 0 98 "q \+
:= -(x-4)*(x-5)*(x-6)*(x-7)*(2*x-9)*(2*x-11)*(2*x-13)*(3*x-5)*(3*
x-8)*(3*x-11)*(3*x-14)*(3*x-17)"}}{INP_R 4 0 "> "{TEXT 0 90 "*(3*
x-20)*(3*x-23)*(4*x-15)*(4*x-17)*(4*x-19)*(4*x-21)*(4*x-23)*(4*x-
25)*(4*x-27)*(4*x-29)"}}{INP_R 5 0 "> "{TEXT 0 73 "*(6*x-13)*(6*x
-19)*(6*x-25)*(6*x-31)*(6*x-37)*(6*x-43)*(6*x-49)*(6*x-55);"}}
{OUT_R 6 0 5{DAG :3n3\`q`+3*3D+5n3\`x`j2x0001i2x0004p8p8+5p6p8i2x
0005p8p8+5p6p8i2x0006p8p8+5p6p8i2x0007p8p8+5p6j2x0006i2x0013p8p8+
5p6p25i2x0019p8p8+5p6p25i2x0025p8p8+5p6p25i2x0031p8p8+5p6p25i2x00
37p8p8+5p6p25i2x0043p8p8+5p6p25i2x0049p8p8+5p6p25i2x0055p8p8+5p6j
2x0004i2x0017p8p8+5p6p5Ei2x0021p8p8+5p6p5Ep35p8p8+5p6p5Ei2x0029p8
p8+5p6j2x0002i2x0009p8p8+5p6p7Ai2x0011p8p8+5p6p7Ap27p8p8+5p6j2x00
03p11p8p8+5p6p8Fi2x0008p8p8+5p6p8Fp83p8p8+5p6p8Fi2x0014p8p8+5p6p8
Fp60p8p8+5p6p8Fi2x0020p8p8+5p6p8Fi2x0023p8p8+5p6p5Ei2x0015p8p8+5p
6p5Ep2Ep8p8+5p6p5EpB8p8p8+5p6p5Ei2x0027p8p8i2x0001}}{SEP_R 7 0}
{INP_R 8 0 "> "{TEXT 0 15 "p := expand(q):"}}{SEP_R 9 0}{INP_R 10
 0 "> "{TEXT 0 39 "t := time(): l := roots(p): time() - t;"}}
{OUT_R 11 0 10{DAG e3j2x0100i2x0003}}{SEP_R 12 0}{INP_R 13 0 "> "
{TEXT 0 2 "l;"}}{OUT_R 14 0 13{DAG [2,1F[2,3/3j2x0013j2x0006j2x00
01[2,3/3j2x0029j2x0004p9[2,3p10p9[2,3/3j2x0020j2x0003p9[2,3j2x000
5p9[2,3p7p9[2,3/3j2x0019p7p9[2,3j2x0007p9[2,3/3j2x0009j2x0002p9[2
,3/3j2x0023p1Cp9[2,3/3j2x0025p7p9[2,3/3j2x0011p39p9[2,3/3j2x0015p
10p9[2,3/3j2x0031p7p9[2,3/3p2Bp10p9[2,3/3j2x0037p7p9[2,3/3p3Fp10p
9[2,3/3j2x0043p7p9[2,3/3p5p39p9[2,3/3j2x0027p10p9[2,3/3j2x0049p7p
9[2,3/3p21p1Cp9[2,3/3j2x0055p7p9[2,3/3j2x0008p1Cp9[2,3/3j2x0017p1
0p9[2,3/3p4Dp1Cp9[2,3/3j2x0021p10p9[2,3/3j2x0014p1Cp9[2,3/3p46p10
p9[2,3/3pA4p1Cp9}}{SEP_R 15 0}{INP_R 16 0 "> "{TEXT 0 0 ""}}}{END
}
