-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-------------------------- felix.as -----------------------
#if IGNORETHIS
D^3+(4/x+4/(x-1))*D^2+(31/16/x^2+161/16/x/(x
-1)+31/16/(x-1)^2)*D-55/432/x^3+(-613/144+_c)/x^2-(613/144-2*_c)/x-55/432/(x-1
)^3+_c/(x-1)^2+(613/144-2*_c)/(x-1)

Il existe des invariants de degre 6 pour:

 _c = 613/288, _c = RootOf(20736*_Z^2-88272*_Z+94003)
#endif

#include "sumit"

macro {
	Z == Integer;
	R == DenseUnivariatePolynomial(Z, "c");		-- c = parameter
	RX == DenseUnivariatePolynomial R;
	RXD == LinearOrdinaryDifferentialOperator RX;
	Rx == Quotient RX;
	RxD == LinearOrdinaryDifferentialOperator Rx;
	MAT == DenseMatrix R;
}

local mat(n:Z):MAT == {
	import from Z, R, RX, Rx, RxD;
	import from DualFirstIntegral(R, RX, RXD);
	import from UnivariateFreeAlgebraOverQuotient(RX, RXD, Rx, RxD);
	c := monom$R;
	x := monom$RX;
	D := monom$RxD;
	L := D^3+4*(2*x-1)/(x*(x-1))*D^2;
	L := L + (223*x^2-223*x+31::RX)/(16*x^2*(x-1)^2)*D;
	f :=
	  (1674*x-3513*x^2+1729*x^3+55::RX+432*c*x^2-432*c*x)/(432*x^3*(x-1)^3);
	L := L + f::RxD;
	(b, LL) := makeIntegral L;	-- LL = b L, LL has coeffs in Z[c][X]
	determiningMatrix(LL, n);
}

local main():() == {
	import from SingleInteger, Timer, Z, ExpressionTree, MAT;
	t := timer();
	start! t;
	A := mat(6@Z);
	tm := stop! t;
	print << "# Time to compute matrix = " << tm << " millisecs" << newline;
	print << newline << "M := ";
	maple(print, extree A);
	print << ":" << newline << newline;
}

main();

