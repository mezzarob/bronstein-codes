-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
#include "sumit"

#if IGNORETHIS
Timings (idefix/msecs):
                       R     L
0.1.9:               2737   334
quadratic Ore prod:  1450   167
linear term prod:    1317   167
table in right div:  1400   150

#endif

macro {
	Z == Integer;
	ZX == DenseUnivariatePolynomial(Z, "x");
	Zx == Quotient ZX;
	Zxd == LinearOrdinaryDifferentialOperator(Zx, "D");
}

import from Z, ZX, Zx, Zxd, Timer;

local foo(tt:SingleInteger):() == {
	print << newline << "Total time = " << tt << " msecs\\" << newline;
}

D:Zxd := monom;
x:Zx := monom$ZX :: Zx;
A := x^3*D^5 + 3*D^3 + x*D^2 + (1 - x^2)*D + 1;
B := x*D^2 + x*D;
t := timer();
start! t;
L := rightLcm(A, B);
time := stop! t;
print << "degree lcm = " << degree L << newline;
print << "coeff of $D^6$ = $$" << newline << coefficient(L,6) <<newline << "$$";
foo time;
reset! t;
start! t;
L := leftLcm(A, B);
time := stop! t;
print << "degree lcm = " << degree L << newline;
print << "coeff of $D^6$ = $$" << newline << coefficient(L,6) <<newline << "$$";
foo time;

