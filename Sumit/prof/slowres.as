-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
--------------------------- slowalg.as --------------------------
#if IGNORETHIS
Timings on obelix (msecs):

Maple V.4:       495
Axiom 2.0a:     3780
1.1.10a:	 786

#endif

#include "sumit"

macro {
	Z == Integer;
	Za == DenseUnivariatePolynomial(Z, "a");
	ZaX == DenseUnivariatePolynomial(Za, "x");
	NRES == 5;
}

local timeresultant(a:ZaX, b:ZaX):Za == {
	import from SingleInteger, Timer;
	t := timer();
	for i in 1..NRES repeat {
		start! t;
		r := resultant(a, b);
		stop! t;
	}
	print << "time per resultant = " << read(t) quo NRES << newline;
	r;
}

local main():() == {
	import from Z, Za, ZaX;
	x:ZaX := monom;
	A := 27*x^18+81*x^16+2079*x^14+6021*x^12+55278*x^10+149850*x^8
		+553076*x^6+1264956*x^4+1215672*x^2+405224::ZaX;


	a:Za := monom;

	B := -3744*x^16-65759616*x^15+(1320*a-13608::Za)*x^14
	     +(46368960*a-280742976::Za)*x^13+(5400*a-195264::Za)*x^12
	     +(214983360*a-36673632::Za)*x^11+(48280*a-563688::Za)*x^10
	     +(-254326720*a+5540247648::Za)*x^9+(198120*a-3026896::Za)*x^8
	     +(1032763200*a+21627607040::Za)*x^7+(485440*a-4006656::Za)*x^6
	     +(2183556480*a+310065020160::Za)*x^5+(1645760*a-10075840::Za)*x^4
	     +(16116726400*a+102336013696::Za)*x^3+(2409440*a+39909088::Za)*x^2
	     +(15388874240*a+1861284339328::Za)*x+(48626880::Za+1095200*a)::ZaX;

	g := timeresultant(A, B);

	print << "resultant = " << g << newline;
}

main();
