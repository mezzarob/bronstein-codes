-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------------- badroot.as -------------------------------
#if IGNORETHIS
mendel:  maple 1.7s   sumit  198s
#endif

#include "sumit"

macro {
	Z == Integer;
	Zx == DenseUnivariatePolynomial(Z, "x");
}

import from Z, Zx, IntegerRoot, List IntegerRoot, Timer;

t := timer();

x := monom;

p := -(x-4::Zx)*(x-5::Zx)*(x-6::Zx)*(x-7::Zx)*(6*x-13::Zx)*(6*x-19::Zx)_
	*(6*x-25::Zx)*(6*x-31::Zx)*(6*x-37::Zx)*(6*x-43::Zx)*(6*x-49::Zx)_
	*(6*x-55::Zx)*(4*x-17::Zx)*(4*x-21::Zx)*(4*x-25::Zx)*(4*x-29::Zx)_
	*(2*x-9::Zx)*(2*x-11::Zx)*(2*x-13::Zx)*(3*x-5::Zx)*(3*x-8::Zx)_
	*(3*x-11::Zx)*(3*x-14::Zx)*(3*x-17::Zx)*(3*x-20::Zx)*(3*x-23::Zx)_
	*(4*x-15::Zx)*(4*x-19::Zx)*(4*x-23::Zx)*(4*x-27::Zx);

start! t;
l := integerRoots p;
time := stop! t;

for rt in l repeat {
	print << "root = " << root rt << "   with multiplicity ";
	print << multiplicity rt << newline;
}

print << "time = " << time::Z << newline;

