-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------- univdet.as --------------------------
#include "sumit"
#include "sallio"

#assert FIELD

macro {
	I==MachineInteger;
	FX == DenseUnivariatePolynomial F;
	M == DenseMatrix FX;
	-- F == ZechPrimeField 10007;
	-- FF == "F10007";
	-- BOUND == 50;
	-- F == Integer;
	-- FF == "Z";
	-- BOUND == 20;
	F == Fraction Integer;
	FF == "Q";
	BOUND == 5;
	DEGBOUND == 4;
}

import from I;

local go(m:M, det:M -> FX):(FX, I, I) == {
	t:Timer := timer();
	start! t;
	d := det m;
	time := stop! t;
        (d, time, gc t);
}

local bareiss(m:M):(FX, I, I) ==
	go(m, determinant$TwoStepFractionFreeGaussElimination(FX, M));

local crt(m:M):(FX, I, I) == {
	import from Partial FX, UnivariatePolynomialCRTLinearAlgebra(F, FX, M);
	go(m, (a:M):FX +-> retract determinant a);
}

#if FIELD
	local hensel(m:M):(FX, I, I) == {
		import from Partial FX;
		import from UnivariatePolynomialHenselLinearAlgebra(F, FX, M);
		go(m, (a:M):FX +-> retract determinant a);
	}

	local popov(m:M):(FX, I, I) ==
	       go(m,determinant$UnivariatePolynomialPopovLinearAlgebra(F,FX,M));
#endif

local univdet(n:I, degr:I):() == {
	import from Boolean, FX, M;

	degr > DEGBOUND => return;
	stdout << n << "  " << degr << flush;
	deg:Integer := degr::Integer;
	A:M := zero(n,n);
	for i in 1..n repeat for j in 1..n repeat A(i,j) := random deg;

	(d1, t1, gc1) := bareiss A;
	(d2, t2, gc2) := crt A;
	stdout << "  " << t1 << "(" << gc1 << ")";
	stdout << "  " << t2 << "(" << gc2 << ")";
	ok := zero?(d1 - d2);
#if FIELD
	(d3, t3, gc3) := popov A;
	-- (d4, t4, gc4) := hensel A;
	stdout << "  " << t3 << "(" << gc3 << ")";
	-- stdout << "  " << t4 << "(" << gc4 << ")";
	ok := ok and zero?(d3 - d1);
	-- ok := ok and zero?(d4 - d1);
#endif
	stdout << "  " << ok << newline;
}

local univdet(n:I):() == {
	n > BOUND => return;
	univdet(n, 1);
	univdet(n, 2);
	univdet(n, 3);
	univdet(n, 4);
	univdet(n, 5);
	univdet(n, 10);
	univdet(n, 20);
	univdet(n, 30);
	univdet(n, 40);
}

local main():() == {
	stdout << "Determinants of polynomial matrices over " << FF << newline;
	stdout << "size  degree  bareiss  crt";
#if FIELD
	stdout << "  popov";
	stdout << "  hensel";
#endif
	stdout << "  ok" << newline;

#if FIELD
	top:I := 5;
#else
	top:I := 9;
#endif
	for i in 5..top repeat univdet i;
	univdet 10;
	univdet 20;
	univdet 30;
	univdet 40;
	univdet 50;
}

main();

