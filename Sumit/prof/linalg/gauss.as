-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------- gauss.as ---------------------------------
#include "sumit"

#library mylib "TestMatrix.ao"
import from mylib;

macro SI == SingleInteger;
macro Q == Quotient Integer;
macro MQ == DenseMatrix Q;

macro SIZE == 10;

import from SI;

-- matrix will be n x n, entries will have n decimal digits
oge(n:SI):(SI,SI,SI,SI) == {
	macro NTIMES == 2;
	import from Timer,Q,MQ,TestMatrix(Q,MQ);
	a:MQ := testMatrix(n,n,n);
	wgauss := timer(); wmarco := timer();
	wff1 := timer(); wff2 := timer();
	for k in 1..NTIMES repeat {
		start! wgauss;
		(b,c,d,e) := rowEchelon(a)$OrdinaryGaussElimination(Q, MQ);
		stop! wgauss;
		start! wmarco;
		rank(a)$LinearAlgebraOverIntegralDomain(Q,MQ);
		stop! wmarco;
		start! wff1;
		(b,c,d,e) := rowEchelon(a)$FractionFreeGaussElimination(Q,MQ);
		stop! wff1;
		start! wff2;
		(b,c,d,e) := rowEchelon(a)$TwoStepFractionFreeGaussElimination(Q,MQ);
		stop! wff2;
	}
	(read(wgauss) quo NTIMES, read(wmarco) quo NTIMES,_
		read(wff1) quo NTIMES, read(wff2) quo NTIMES);
}

(tg, tm, t1, t2) := oge(SIZE);
print << "Matrix is " << SIZE << " x " << SIZE << newline;
print <<"Time for each OrdinaryGaussElimination = " <<tg<<" millisecs"<<newline;
print<<"Time for each fraction-free rank (marco) = "<<tm<<" millisecs"<<newline;
print<<"Time for each FFGaussElimination = "<<t1<<" millisecs"<<newline;
print<<"Time for each TwoStepFFGaussElimination = "<<t2<<" millisecs"<<newline;
