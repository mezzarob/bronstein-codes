-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
#include "sumit"
#include "sallio"

macro {
	F17 == SmallPrimeField 17;
	Z == Integer;
	int == MachineInteger;
	Pol == DenseUnivariatePolynomial F;
}

import from int;

-- DUP over F17 use FFT or Karatsuba
-- DUP over NaiveF17 use naive multiplication
NaiveF17:Field == F17 add {
	karatsubaCutoff:int == 0;	-- turn off karatsuba
}

#if NAIVE
macro F == NaiveF17;
#else
macro F == F17;
#endif

test(n:int):() == {
	import from Z, Pol, Timer;

	t := timer();
	start!(t);

	x:F := 1;
	p1 :Pol:= 0; 
	for i in n..0 by -1 repeat 
	{
	    p1:= add!(p1,x,i::Z); x := x+x+1;
	    x=0 => x := 1; 
	}	    
	p2 :Pol := 0; 
	for i in n..0 by -1 repeat
	{
	    p2:= add!(p2,x,i::Z); x := x+x+1;
            x=0 => x := 1;
	}

	r := p1 * p2;

	stdout << n << " " << stop! t << newline;
}


import from int;

stdout << "degre   msecs" << newline;
test 1000;
test 2000;
test 4000;
test 8000;
test 10000;
test 20000;
