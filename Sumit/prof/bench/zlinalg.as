-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ zlinalg.as -------------------------------
#if IGNORETHIS
Timings (obelix/msecs):
SIZE 	10   20    30    40    50    100
ALDOR	 6   45   161   440  1066  15539
GMP	 5   35   121   350   822  11705
Ratio  1.2  1.3   1.3   1.3   1.3    1.3

#endif

#include "sumit"
#include "sallio"

#if GMP
macro Z == GMPInteger;
macro ARITH == "GMP";
#else
macro Z == AldorInteger;
macro ARITH == "Aldor";
#endif

macro {
	I == MachineInteger;
	N == 100@I;
	NTIMES == 3@I;
	M == DenseMatrix Z;
}

import from Boolean, I, Z, M, Timer, Vector Z, LinearAlgebra(Z, M);

stdout << "Size = " << N << newline;
stdout << "Arithmetic = " << ARITH << newline;

t := timer();
a := random(N, N);
b := random(N, 1);
start! t;
for j in 1..NTIMES repeat (m, d) := particularSolution(a, b);
tim := stop! t;

~zero?(a * m - b * diagonal d) => error "Not a correct solution!";

ok? := true;
for x in d repeat if zero?(x) then ok? := false;

if ok? then stdout << "Solution found"; else stdout << "No solution";
stdout << newline << "Time per solve = " << tim quo NTIMES << " msecs"<<newline;

