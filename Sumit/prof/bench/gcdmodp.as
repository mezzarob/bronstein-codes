-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------- gcdmodp.as ----------------------------
--
-- THIS BENCH FILE CAN ONLY BE USED IF THE DEFAULT GCD IS gcdmopd
-- SINCE IT USES gcd FOR THE DEFAULT GCD
--

#include "sumit"
#include "sallio"


#if IGNORETHIS
Timings (obelix/msecs/coprime polys):

Degree:       100  200  300  400  500  1000  2000  3000
spf-euclid:     8   31   68  113  176   683  2713  6066
zpf-euclid:     2    8   16   28   45   160   663  1468
gcd:            3   10   21   38   61   238   941  2138

With new 0.1.11e prime fields:
spf-euclid	5   17   36   63   96   396  1551  3465
spf-gcd		3   10   21   38   59   235   936  2121
spf-lazy	1    6   14   25   39   153   613  1394
zpf-euclid      2    9   19   33   51   195   785  1766
zpf-gcd         1    4    9   17   28   113   475  1140

With 0.1.12p4 (12/99)
spf-euclid	8   32   68  123  196   800  2580  5805
spf-lazy	2    8   18   32   50   196   773  1728
zpf-euclid	6   21   44   76  113   451  1705  3966
zpf-gcd 	1    5   12   22   35   143   585  1383
#endif

macro {
	I == MachineInteger;
	Z == Integer;
	N == (3@I);
	DEG == (3000@Z);
	DEGGCD == (0@Z);
	mod == (10007@I);
	Fx == DenseUnivariatePolynomial(F, -"x");
}

#if ZECH
macro F == ZechPrimeField(mod);
macro FF== "ZPF";
#else
macro F == SmallPrimeField(mod);
macro FF== "SPF";
#endif

import from Boolean, String, Symbol, I, Z, Fx, Timer;

stderr << "Degree = " << DEG << newline << "Arithmetic = " << FF << newline;

-- for j in 1@I..10000 repeat {
t := timer();
g := random DEGGCD;
a := g * random(DEG - DEGGCD);
b := g * random(DEG - DEGGCD);
#if ASTRACE
import from ExpressionTree;
stdout << "a := ";
maple(stdout, extree a);
stdout << ":" << newline << newline << "b := ";
maple(stdout, extree b);
stdout << ":" << newline;
#endif
start! t;
for i in 1..N repeat g1 := gcd(a, b);
tim1 := stop! t;
reset! t;
start! t;
for i in 1..N repeat g2 := monic euclid(a, b);
tim2 := stop! t;
-- }

stderr << "Gcd time = " << tim1 quo N << " millisecs per run" << newline;
stderr << "Euclid time = " << tim2 quo N << " millisecs per run" << newline;

err := leadingCoefficient(g2)*g1 ~= leadingCoefficient(g1)*g2;
#if ASTRACE
{
#else
if err then {
#endif
	import from ExpressionTree;
	stdout << "Modgcd := ";
	maple(stdout, extree g1);
	stdout << ":" << newline << newline << "Eucgcd := ";
	maple(stdout, extree g2);
	stdout << ":" << newline;
}

if err then error "Gcd's don't match";

