-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
--
-- Tdup.as
--
-- Laurent Bernardin, March 1999
-- bernardin@inf.ethz.ch
--

#include "sumit.as"

macro {
	SI == SingleInteger;
        Z == Integer;
	F == SmallPrimeField 17;
        P == DenseUnivariatePolynomial(F, "x");
}

import from SI,Z,P;

getx():P == {
	-- this is a separate function so we don't have
	-- to import F at the toplevel.
	import from F;
	monomial(1@F,1@Integer); 
}

x := getx();

-- insert problem size below
n:SI := 4000;

d:Z := (n+1)::Z;

a:F := 1;

-- preallocate space for all monomials up to degree d
-- so we don't have to grow the datastructure for each
-- new monomial
f:P := 0+monomial(1,d+1);

for  i:SI in 0..n repeat {
	f := f+monomial(a,i::Z);
	a := (2@Z)*a+1;
	if a=0 then a := 1;
	}

-- remove dummy monomial
f := f-monomial(1,d+1);

g:P := 0+monomial(1,d+1);

for i:SI in 0..n repeat {
	g := g+monomial(a,i::Z);
	a := (2@Z)*a+1;
	if a=0 then a := 1;
	}

g := g-monomial(1,d+1);

print << "multiplying" << newline;

h := f*g;

print << degree h << newline;
