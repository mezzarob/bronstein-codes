-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ zmodmz.as -------------------------------
#if IGNORETHIS
Timings (obelix/mu-secs):
SIZE          10   20    30    100   200   1000  2000
ALDOR       3.83  4.16  4.83  8.33  13.50   55    107
GMP         2.16  2.66  3.16  6.66  11.83   53    103
Ration      1.77  1.56  1.52  1.25   1.14  1.04  1.03

#endif

#include "salli"
#include "sallio"

#if GMP
macro Z == GMPInteger;
macro ARITH == "GMP";
#else
macro Z == AldorInteger;
macro ARITH == "Aldor";
#endif

macro {
	I == MachineInteger;
	N == 10000@I;
	LIMBS == 2000@I;
}

import from I, Z, Timer;

local rand(l:I):Z == {
	a:Z := 1;
	for i in 1..l repeat a := a * random();
	a;
}

stdout << "Size = " << LIMBS << newline;
stdout << "Arithmetic = " << ARITH << newline;

n := rand LIMBS;
b:I := 0;
t := timer();
start! t;
for j in 1..N repeat b := b + n mod (j + 100000);
tim := stop! t;

stdout << "n = " << n << newline;
stdout << "b = " << b << newline;
stdout << "Time for " << N << " remainders = " << tim << " msecs" << newline;

