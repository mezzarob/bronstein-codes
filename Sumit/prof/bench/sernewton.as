-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------- sernewton.as ----------------------------
#include "algebra"

macro {
	I == MachineInteger;
	Z == Integer;
	RXX == DenseUnivariateTaylorSeries R;
}

FastEuclideanDivision(R:CommutativeRing,
			Rx:UnivariatePolynomialCategory R): with {
	monicQuotientBy: Rx -> Rx -> Rx;
	monicRemainderBy: Rx -> Rx -> Rx;
	monicDivideBy: Rx -> Rx -> (Rx, Rx);
	times: () -> (I, I, I, I, I, I);
} == add {
	import from Timer;
	tstrunc := timer();
	tmult := timer();
	tpost := timer();

	times():(I, I, I, I, I, I) ==
		(read tstrunc, gc tstrunc, read tmult, gc tmult, read tpost, gc tpost);

	monicQuotientBy(b:Rx):Rx -> Rx == {
		import from Boolean, Z, R, Rx, RXX, Partial RXX;
		import from UnivariateTaylorSeriesCategory2Poly(R, RXX, Rx);
		TRACE("monicQuotientBy: b = ", b);
		assert(~zero? b);
		u := reciprocal(expand(0)(revert b));
		assert(~failed? u);
		invrevb := retract u;
		degb := degree b;
		TRACE("monicQuotientBy: degb = ", degb);
		(a:Rx):Rx +-> {
			TRACE("monicQuotientBy: a = ", a);
			zero? a or (dega := degree a) < degb => 0;
			TRACE("monicQuotientBy: dega = ", dega);
			n := next(dega - degb);
			TRACE("monicQuotientBy: n = ", n);
			start! tstrunc;
			b1:Rx := truncate(invrevb, n);
			stop! tstrunc;
			TRACE("monicQuotientBy: b1 = ", b1);
			start! tmult;
			ab1 := truncate!(revert a, n) * b1;
			stop! tmult;
			TRACE("monicQuotientBy: ab1 = ", ab1);
			start! tpost;
			ab1modn := truncate!(ab1, n);
			TRACE("monicQuotientBy: ab1modn = ", ab1modn);
			zero? ab1modn => 0;
			q := revert! ab1modn;
			TRACE("monicQuotientBy: q = ", q);
			m := prev(n) - degree(ab1modn);
			TRACE("monicQuotientBy: m = ", m);
			assert(m >= 0);
			q := shift!(q, m);
			stop! tpost;
			q;
		}
	}

	monicDivideBy(b:Rx):Rx -> (Rx, Rx) == {
		quot := monicQuotientBy b;
		(a:Rx):(Rx, Rx) +-> {
			zero?(q := quot a) => (q, a);
			(q, a - b * q);
		}
	}

	monicRemainderBy(b:Rx):Rx -> Rx == {
		quot := monicQuotientBy b;
		(a:Rx):Rx +-> {
			zero?(q := quot a) => a;
			a - b * q;
		}
	}
}

#include "aldorio"

-- macro I == MachineInteger;

#if ZECH
macro F == ZechPrimeField 10007;
macro N == 200;
#else
macro F == Z;
macro N == 50;
#endif

local quotby(deg:Z, times:I):(I, I, I, I) == {
	macro FX == DenseUnivariatePolynomial(F,-"x");
	import from Symbol, FX, Timer;
	import from FastEuclideanDivision(F, FX);
	tclassic := timer(); tfast := timer();
	b := monomial(deg) + random(deg-1);
	quot := monicQuotientBy b;
	for i in 1..times repeat {
	  	a := random(2*deg);
		start! tclassic;
		(q1, r) := monicDivide(a, b);
		stop! tclassic;
		start! tfast;
		q2 := quot a;
		stop! tfast;
		q1 ~= q2 => {
			stdout << "a = " << a << newline;
			stdout << "b = " << b << newline;
			stdout << "classic quotient = " << q1 << newline;
			stdout << "fast quotient = " << q2 << newline;
			error "quot: quotients differ!";
		}
	}
	(read tclassic, gc tclassic, read tfast, gc tfast);
}

local main():() == {
	import from I, Z;
	stdout << "Coefficients = ";
#if ZECH
	stdout << "F_10007" << newline;
#else
	stdout << "Z" << newline;
#endif
	stdout << newline << "degree   classic  fast" << newline;
	for i in 1@Z..N repeat {
		n := N + 5 - machine i;
		d := 5*i;
		(c, gc, f, gf) := quotby(d, n);
		stdout << d << "  " << c quo n << "  (" << gc quo n << ")   ";
		stdout << f quo n << "  (" << gf quo n << ")" << newline;
	}
	macro FX == DenseUnivariatePolynomial(F,-"x");
	import from Symbol, FastEuclideanDivision(F, FX);
	(t, gt, m, gm, p, gp) := times();
	stdout << newline;
	stdout << "truncation = " << t << "  (" << gt << ")" << newline;
	stdout << "multiplication = " << m << "  (" << gm << ")" << newline;
	stdout << "post-processing = " << p << "  (" << gp << ")" << newline;
}

main()


