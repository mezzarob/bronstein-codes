-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-------------------------------- fftprod.as -----------------------------
--
-- THIS BENCH FILE CAN ONLY BE USED IF THE DEFAULT PRODUCT IS NOT FFT
-- SINCE IT USES * FOR THE DEFAULT PRODUCT
--
#include "sumit"

#if IGNORETHIS
Obelix / msecs:

DEG:  100 100  200 200 200  300 300 300  400 400 400  500 500 500  600 600 600
DENS:  50 100   50 100 200   75 150 300  100 200 400  125 250 500  150 300 600
-------------------------------------------------------------------------------
spf-*:  2   3    5   7   9   10  14  19   19  26  31   26  37  47   36  52  62
zpf-*:  1   1    1   2   3    2   4   6    5   8   9    8  11  15   10  15  20
h-fft:  2   2    5   5   6    9   9  10   11  11  12   12  13  14   21  21  23

DEG:  1000 1000 1000  2000 2000 2000  5000 5000 5000 10000 10000 10000 10000
DENS:  250  500 1000   500 1000 2000  1250 2500 5000  1250  2500  5000 10000
-------------------------------------------------------------------------------
spf-*:  93  119  143   312  388  445  1425 1746 1950  3335  4362  5154  5641
zpf-*:  27   36   45    96  123  147   446  561  648  1106  1469  1791  1971
h-fft:  26   29   30    55   58   62   215  221  230   458   473   483   506

#endif

macro {
	DENSITY == (1@Z);

	-- N == number of different examples (outer loop, large for testing)
	-- M == number of repeat per examples (inner loop)

	-- q == (5@I);
	-- N == (5000@I);
	-- M == (1@I);
	-- DEG == (100@Z);

	-- q == (1193@I);
	-- q == (10007@I);
	-- N == (3@I);
	-- M == (10@I);
	-- DEG == (100@Z);
	-- DEG == (200@Z);
	-- DEG == (300@Z);
	-- DEG == (400@Z);
	-- DEG == (500@Z);
	-- DEG == (600@Z);

	-- q == (547@I);
	-- N == (3@I);
	-- M == (5@I);
	-- DEG == (1000@Z);
	-- DEG == (2000@Z);

	q == (149@I);
	N == (2@I);
	M == (2@I);
	-- DEG == (3000@Z);
	-- DEG == (5000@Z);
	DEG == (10000@Z);

	I == SingleInteger;
	Z == Integer;
	Fx == DenseUnivariatePolynomial(F, "x");
}

#if ZECH
macro F == ZechPrimeField(q);
#else
macro F == SmallPrimeField(q);
#endif

import from Timer, I, Z, F, Fx, Partial Fx, DiscreteFFT F;

print << "Degree = " << DEG << newline;
print << "Number of nonzero terms = " << DEG quo DENSITY << newline;
print << "Modulus = " << q << newline;

t1 := timer(); t2 := timer();
fftprod := fftTimes Fx;
for i in 1..N repeat {
	a := random(DEG, DEG quo DENSITY);
	b := random(DEG, DEG quo DENSITY);
	start! t1;
	for j in 1..M repeat ab := a * b;
	stop! t1;
	start! t2;
	for j in 1..M repeat p := fftprod(a, b);
	stop! t2;
	failed? p => error "FFT failed!";
	retract(p) ~= ab => {
		print << "i = " << i << newline;
		-- print << "a = " << a << newline;
		-- print << "b = " << b << newline;
		-- print << "fftprod = " << retract p << newline;
		error "Mismatch Kara/FFT!";
	}
}

print << "Time for karatsuba = " << read(t1) quo (N*M) << " msecs" << newline;
print << "Time for fft = " << read(t2) quo (N*M) << " msecs" << newline;
