-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------- ffield.as ----------------------------
#include "sumit"

#if IGNORETHIS
Timings (obelix/msecs) (det for SIZE < 1000, gcd for SIZE >= 1000)

SIZE	100	200	300  |	1000	2000	3000	4000
-------------------------------------------------------------
SPF     246    1790    6811  |   453	1863	4106	7013
HWPF    213    1563    5244  |   230     993	2046	3613
-------------------------------------------------------------
ZPF     156    1246    4661  |   130     503	1150	2220
LPF     150    1160    4133  |   123     513	1326	1886
LPF2	143    1190    3876  |	 116	 483	1226	1896
-------------------------------------------------------------
TPF     156    1190    4633  |   110     433	 996	1690
TPF2	143    1190    3926  | 	 120	 483	1216	2146

#endif

macro {
	I == SingleInteger;
	Z == Integer;
	N == (5@I);
	SIZE == (4000@I);
	P == (11@I);
	FS == SmallPrimeField P;
	FH == HalfWordPrimeField P;
	FZ == ZechPrimeField P;
	FL == LogarithmicPrimeField P;
	FT == TablePrimeField P;
	MAT == DenseMatrix;
	UP == DenseUnivariatePolynomial;
}

local gcd(p:UP Z, s:String, F:PrimeFieldCategory):() == {
	import from I, Timer, F, UP F;
	q:UP F := 0;
	for term in p repeat {
		(c, e) := term;
		q := add!(q, c::F, e);
	}
	qq := differentiate q;
	t := timer();
	for i in 1..N repeat {
		start! t;
		g := monic euclid(q, qq);
		stop! t;
	}
	print << s << ": gcd = " << g << "   in time " << read(t) quo N;
	print << " msecs per gcd" << newline;
}

local det(m:MAT Z, s:String, F:PrimeFieldCategory):() == {
	import from I, Timer, F, MAT F;
	import from LinearAlgebra(F, MAT F, OrdinaryGaussElimination(F, MAT F));
	(r, c) := dimensions m;
	mf:MAT F := zero(r, c);
	for i in 1..r repeat for j in 1..c repeat mf(i, j) := m(i, j)::F;
	t := timer();
	for i in 1..N repeat {
		start! t;
		d := determinant mf;
		stop! t;
	}
	print << s << ": determinant = " << d << "   in time " << read(t) quo N;
	print << " msecs per det" << newline;
}

-- macro f == det;
macro f == gcd;

local main():() == {
	import from I, Z, MAT Z, UP Z;
	print << "size = " << SIZE << newline;
	-- m := random(SIZE, SIZE);
	m := random(SIZE::Z);
	-- f(m, "SPF", FS);
	-- f(m, "HWPF", FH);
	-- f(m, "ZPF", FZ);
	f(m, "LPF", FL);
	f(m, "TPF", FT);
}

main();
