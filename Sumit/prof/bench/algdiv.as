-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ algdiv.as -------------------------------
#if IGNORETHIS
Timings (obelix/msecs):

SPF has Karatsuba cutoff at 20, FFT cutoff at 400
ZPF has Karatsuba cutoff at 40, FFT cutoff at 1600
Z   has Karatsuba cutoff at 12, no FFT

* = ERROR

Degree:       30  40  50  100  200  300  400  500  600  700  800  900  1000 2000
SPF-Euclid:  0.7   1 1.7    6   20   51   86  130  191  271  336  436   520 2165
SPF-Fast:      1 1.5 2.5    8   25   48   78   38   48   70   76   80    83  181
ZPF-Euclid:  0.4 0.5 0.7    2    6   12   20   36   51   68   86  108   130  500
ZPF-Fast:    0.5 0.7   1    3    9   17   26   39   53   70   83  100   120  185
Z-Euclid:      6  10  14   77  508  720 1493 5750  9666  9483
Z-Fast:       10  17  21  121  783  811 2019 6516 10550 11688
#endif

#include "sumit"

macro {
	I == SingleInteger;
	-- N == 1000;
	-- N == 100;
	N == 3;
	EXTDEG == 700@Integer;
	Fx == DenseUnivariatePolynomial(F, "x");
}

#if ZECH
macro F  == ZechPrimeField 11;
#elseif USEINT
macro F == Integer;
#else
macro F  == SmallPrimeField 11;
#endif

import from I, SingleFloat, Integer, Fx;

local time(a:Fx, b:Fx):(Fx, I, Fx, I) == {
	macro A == UnivariatePolynomialMod(F, Fx, b, "x");
	import from A, Timer;
	t := timer();
	start! t;
	for i in 1@SingleInteger..N repeat rr := reduce a;
	tim1 := stop! t;
	rr1 := lift rr;
	reset! t;
	start! t;
	for i in 1@SingleInteger..N repeat rr := multred a;
	tim2 := stop! t;
	rr2 := lift rr;
	(rr1, tim1, rr2, tim2);
}

local dv(t:I, n:I):SingleFloat == {
	(nq, nr) := divide(n, 10);
	nr > 0 => {
		ans := (t quo n)::SingleFloat;
		ans;
	}
	t := t quo nq;
	t >= 100 => (t quo 10)::SingleFloat;
	t::SingleFloat / 10.
}

local myerror(a:Fx, b:Fx, q:Fx, r:Fx, s:String):() == {
#if SHOWERROR
	import from ExpressionTree;
	print << "a := ";
	maple(print, extree a);
	print << ";" << newline << "b := ";
	maple(print, extree b);
	print << ";" << newline << "q := ";
	maple(print, extree q);
	print << ";" << newline << "r := ";
	maple(print, extree r);
	print << ";" << newline;
#endif
	error s;
}

x:Fx := monom;
d := x^EXTDEG + random(prev EXTDEG, EXTDEG quo 2);
c := random(prev(EXTDEG + EXTDEG), EXTDEG);

(r1, t1, r2, t2) := time(c, d);

print << "Extension degree = " << EXTDEG << newline;
print << "time per euclidean division = " << dv(t1,N) << " msecs" << newline;
print << "time per fast division = " << dv(t2,N) << " msecs" << newline;

q1 := quotient(c - r1, d);
q2 := quotient(c - r2, d);
(c - q1 * d - r1 ~= 0) or (r1 ~= 0 and degree(r1) >= degree(d)) =>
	myerror(c, d, q1, r1, "algdiv: euclidean division is wrong");
(q1 ~= q2) or (r1 ~= r2) =>
	myerror(c, d, q2, r2, "algdiv: fast division is wrong");

