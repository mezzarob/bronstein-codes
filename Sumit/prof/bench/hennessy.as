-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------- hennessy.as ----------------------------------------
-- 
-- Prototype version!!! This program is still under development
-- Thus, all results without any warranty!
--
-- Author: Niklaus Mannhart
--         ETH Zentrum
--         CH-8092 Zuerich, Switzerland
--         mannhart@inf.ethz.ch
--
-- Date Created: 17 June 1994
-- Date Last Updated:
--
-- Compile sequence to create the executable:
-- 1. axiomxl -Fx -Q3 hennessy.as
--
------------------------------------------------------------------------------------
--
-- Hennessy tests are well known compiler benchmarks written in C, Pascal, Modula
-- and so on. I've translated Hennessy to A# such that we can test other compilers
-- with A# or test different A# compiler versions.
-- But be careful: hennessy only tests a very small part of the A# language.
-- However, they show some overall performace of the compiler and the improvements
-- between different A# versions.
-- 
--   
-- Perm: Permutation
--       heavily recursive, inlining procedures! 
--
-- Towers: solve the Towers of Hanoi
--     NOT IMPLEMENTED YET 
--
-- Queens: the eight Queen problem
--       good example for common subexpression elimination!
--
-- Intmm: Integer matrix multiplication
--       example for integer multiplication and addition
--
-- Floatmm: Floating point matrix multiplication
--       example for floating point multiplication and addition
--
-- Puzzle:
--       NOT IMPLEMENTED YET
--
-- Quick: Quicksort (recursiv)
--       recursive, example for inlining and common subexpression elimination
--
-- Bubble: Bubblesort
--       my favourite one! loop optimization, common subexpression elimination,
--       good general test
--
-- Tree: Treesort
--     NOT IMPLEMENTET YET
--
-- FFT:
--     NOT IMPLEMENTED YET 
--
-- to be done:
--     Test programs which compares A# with other CAS such as Maple, Mathematica
--     and so on. 
-----------------------------------------------------------------------------------

-- In order to get correct result one must check that the C version and
-- A# version of hennessy use the same constants! (otherwise you get wrong
-- result)

#include "axllib"

--
-- macors
--
macro SI          == SingleInteger;
macro BoolVec     == PrimitiveArray (Boolean);
macro SIntVec     == PrimitiveArray (SI);
macro Matrix      == PrimitiveArray (PrimitiveArray (SI));
macro SFloat      == SingleFloat;
macro FloatMatrix == PrimitiveArray (PrimitiveArray (SFloat));

--
-- Timer (taken from sumit)
--
Timer: with {
	read:   %  -> SI;
	reset!: %  -> %;
	start!: %  -> SI;
	stop!:  %  -> SI;
	timer:  () -> %;
} == add {
        -- time     = total accumulated time since created or reset
        -- start    = absolute time of last start
        -- running? = true if currently running, false if currently stopped
	Rep ==> Record(time:SI, start:SI, running?:Boolean);

	import from Rep, SI, Boolean, OperatingSystemInterface;

	timer():% == per [0, 0, false];

	read(t:%):SI == {
		rec := rep t;
		ans := rec.time;
		if rec.running? then ans := ans + cpuTime() - rec.start;
		ans
	}

	stop!(t:%):SI == {
		local ans:SI;
		rec := rep t;
		if rec.running? then {
			ans := cpuTime() - rec.start;
			rec.time := rec.time + ans;
			rec.running? := false
		}
		else ans := 0;
		ans
	}

	start!(t:%):SI == {
		local ans:SI;
		rec := rep t;
		if not(rec.running?) then {
			rec.start := ans := cpuTime();
			rec.running? := true;
		}
		else ans := 0;
		ans
	}

	reset!(t:%):% == {
		rec := rep t;
		rec.time := rec.start := 0;
		rec.running? := false;
		t
	}
}

--
-- imports
--
import from SI;
import from SFloat;

-- constant definitions
nofTestRuns: SI == 10;   -- number of test runs


----
-- Rand (random number generator) is a general routine 
---

-- global variables
seed: SI := 74755;

InitRand(): () == {
	free seed;

	seed := 74755;
}

Rand (): SI == {
	free seed;

	seed := (seed * 1309 + 13849) mod 65535;
	seed;
}

----
-- permutation
----

PermArrayLength: SI == 7;

permArray: PrimitiveArray (SI) := new (PermArrayLength);
nofPerm: SI := 0;

Initialize (): () == {
	
	for i in 1..PermArrayLength repeat {
		permArray.i := i-1;
	} 
}

Swap (i: SI, j: SI): () == {
--!! bug in A# 0.35.6	(permArray.i, permArray.j) := (permArray.j, permArray.i);
	local x: SI;

	x := permArray.i;
	permArray.i := permArray.j;
	permArray.j := x;
}

Permute (n: SI): () == {
	free nofPerm;

	nofPerm := nofPerm + 1;
	if n ~= 1 then {
		Permute (n-1);
		for k in n-1..1 by -1 repeat {
			Swap (n, k);
			Permute (n-1);
			Swap (n, k);
		}
	}
}	

Perm (): () == {
	free nofPerm;
	
	nofPerm := 0;
	for i in 1..5 repeat {
		Initialize();
		Permute (PermArrayLength);
	}
	if nofPerm ~= 43300 then
		print << "Error in Perm " << nofPerm << newline;
}

PermTest (): () == {
	local t: Timer := timer ();
	local ts: SI;

	start! t;
	for i in 1..nofTestRuns repeat {
		Perm();
	}
	ts := stop! (t);
	print << "Perm: " << ts quo nofTestRuns << newline;
}


----
--  Routines for Quicksort and Bubblesort
----

nofQSElems: SI == 5000;        -- number of elements for Quicksort

sortList: PrimitiveArray (SI) := new (nofQSElems); 
biggest:  SI := 0;  -- biggest value in sortList 
littlest: SI := 0;  -- littlest value in sortList

-- initialize sortList with n elements (1..n)
InitArray (n: SI): () == {
	free biggest, littlest, sortList;
	local temp: SI;

	InitRand();
	biggest := 0;
	littlest := 0;
	for i in 1..n repeat {
		temp:= Rand();
		sortList.i := temp - (temp quo 100000) * 100000 - 50000;
		if sortList.i > biggest then
			biggest := sortList.i;     -- for check on correctness
		else if sortList.i < littlest then 
			littlest := sortList.i;    -- for check on correctness
	}
}

----
-- Quicksort
----

Quicksort (a: PrimitiveArray(SI), l: SI, r: SI): () == {
	local i: SI;
	local j: SI;
	local x: SI;
	local w: SI;

	i := l; j := r;
	x := a.((l+r) quo 2);
	repeat {
		while a.i < x repeat {
			i := i+1;
		}
		while x < a.j repeat {
			j := j-1;
		}
		if i <= j then {
		--!! bug in A# 0.35.6 (a.i, a.j) := (a.j, a.i);
			w := a.i;
			a.i := a.j;
			a.j := w;
			i := i + 1;
			j := j - 1;
		}
		if i > j then break;	
	}

	if l < j then {
		Quicksort (a, l, j);
	}
	if i < r then {
		Quicksort (a, i, r);
	}
}

Quick (): () == {
	InitArray (nofQSElems);
	Quicksort (sortList, 1, nofQSElems);
	if (sortList.1 ~= littlest) or (sortList.nofQSElems ~= biggest) then
		print << "Error in Quicksort" << newline;
}

QuicksortTest(): () == {
        local t: Timer := timer();
        local ts: SI;

        start! t;
        for i in 1..nofTestRuns repeat {
                Quick();
        }
        ts := stop! t;
        print << "Quick: " << (ts quo nofTestRuns) << newline;
}


----
-- Bubblesort
----
nofBSElems: SI == 500;  -- number of elements to be sorted

Bubble (): () == {
	local i: SI;
	local j: SI;
	local top: SI;
	
	InitArray (nofBSElems);
	top := nofBSElems;
	while top > 1 repeat {
		i := 1;
		while i < top repeat {
			if sortList.i > sortList.(i+1) then {
				j := sortList.i;
				sortList.i := sortList.(i+1);
				sortList.(i+1) := j;
			}
			i := i + 1;
		}
		top := top - 1;
	}
	if (sortList.1 ~= littlest) or (sortList.nofBSElems ~= biggest) then
		print << "Error in Bubblesort  " << littlest << "  " << biggest
			<< newline;
}
				
BubbleTest (): () == {
        local t: Timer := timer();
        local ts: SI;

	start! t;
	for i in 1..nofTestRuns repeat {
		Bubble();
	}
	ts := stop! t;
	print << "Bubble: " << (ts quo nofTestRuns) << newline;

}


----
-- Queens
----

Try (i: SI, a: BoolVec, b: BoolVec, c: BoolVec, x: SIntVec): Boolean == {
	local j: SI := 0;
	local q: Boolean := false;

	while (~q) and (j ~= 8) repeat {
		j := j + 1;
		q := false;
		if b.j and a.(i+j) and c.(i-j+7) then {
	        	x.i := j;
			b.j := false;
			a.(i+j) := false;
			c.(i-j+7) := false;
			if i < 8 then {
				q := Try (i+1, a, b, c, x);
				if ~q then {
					b.j := true;
					a.(i+j) := true;
					c.(i-j+7) := true;
				}
			}
			else q := true;
		}
	}
	return q;
}

Doit (): () == {
	local i: SI;
	local a: BoolVec := new (9);
	local b: BoolVec := new (17);
	local c: BoolVec := new (15);
	local x: SIntVec := new (9);

	i := 0 - 7;
	while i <= 16 repeat {
		if (i >= 1) and (i <= 8) then a.i := true;
		if i >= 2 then b.i := true;
		if i <= 7 then c.(i+7) := true;
		i := i + 1;
	}
	
	if  ~Try (1, b, a, c, x) then
		print << "Error in Queens" << newline;
}

Queens (): () == {

	for i in 1..50 repeat {
		Doit ();
	}
}

QueensTest(): () == {
	local t: Timer := timer ();
	local ts: SI;

	start! t;
	for i in 1..nofTestRuns repeat {
		Queens();
	}
	ts := stop! t;
	print << "Queens: " << (ts quo nofTestRuns) << newline;
}


----
-- integer matrix multiplication
----

RowSize: SI == 40;             -- for integer and floating point matrices

ima: Matrix := new (RowSize);  -- integer matrix a
imb: Matrix := new (RowSize);  -- integer matrix b
imr: Matrix := new (RowSize);  -- integer matrix r (result) r := a*b

InitMatrix (m: Matrix): () == {

	for i in 1..RowSize repeat {
		for j in 1..RowSize repeat {
			temp := Rand ();
			(m.i).j := temp - (temp quo 120) * 120 - 60;
		}
	}
}

Innerproduct (a: Matrix, b: Matrix, row: SI, column: SI): SI == {
	local result: SI := 0;

	for i in 1..RowSize repeat {
		result := result + (a.row).i * (b.i).column;
	}
	return result;
}


Intmm (): () == {
	free ima, imb, imr;

	InitRand ();
	InitMatrix (ima);
	InitMatrix (imb);
	for i in 1..RowSize repeat {
		for j in 1..RowSize repeat {
			(imr.i).j := Innerproduct (ima, imb, i, j);
		}
	}
}

IntmmTest(): () == {
	free ima, imb, imr;
 
	local t: Timer := timer();
	local ts: SI;

	for i in 1..RowSize repeat {
		ima.i := new (RowSize);
		imb.i := new (RowSize);
		imr.i := new (RowSize);
	}
			
	start! t;
	for i in 1..nofTestRuns repeat {
		Intmm();
	}
	ts := stop! (t);
	print << "Intmm: " << (ts quo nofTestRuns) << newline;
}

----
-- floating point multiplication
----

fma: FloatMatrix := new (RowSize);   -- floating point matrix a
fmb: FloatMatrix := new (RowSize);   -- floating point matrix b
fmr: FloatMatrix := new (RowSize);   -- floating point matrix r (result) r := a*b

FloatInitMatrix (m: FloatMatrix): () == {
	local temp: SI;

	for i in 1..RowSize repeat {
		for j in 1..RowSize repeat {
			temp := Rand ();
			(m.i).j := (temp - (temp quo 120) * 120 - 60)::SFloat;
		}
	}
}

FloatInnerproduct (a: FloatMatrix, b: FloatMatrix, row: SI, column: SI): SFloat == {
	local result: SFloat := 0.0;

	for i in 1..RowSize repeat {
		result := result + (a.row).i * (b.i).column;
	}
	return result;
}


Floatmm (): () == {
	free fma, fmb, fmr;

	InitRand ();
	FloatInitMatrix (fma);
	FloatInitMatrix (fmb);
	for i in 1..RowSize repeat {
		for j in 1..RowSize repeat {
			(fmr.i).j := FloatInnerproduct (fma, fmb, i, j);
		}
	}
}

FloatmmTest(): () == {
	free fma, fmb, fmr;
 
	local t: Timer := timer();
	local ts: SI;

	for i in 1..RowSize repeat {
		fma.i := new (RowSize);
		fmb.i := new (RowSize);
		fmr.i := new (RowSize);
	}
			
	start! t;
	for i in 1..nofTestRuns repeat {
		Floatmm();
	}
	ts := stop! (t);
	print << "Mm: " << (ts quo nofTestRuns) << newline;
}


----
-- main part
----

print << "Hennessy tests ...." << newline << newline;

PermTest();
QueensTest();
IntmmTest();
FloatmmTest();
QuicksortTest();
BubbleTest();

print << newline;
	
		  	


