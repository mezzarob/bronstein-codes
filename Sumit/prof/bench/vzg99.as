-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
#include "sumit"
#include "sallio"

macro {
	V == Vector;
	M == DenseMatrix;
	I == MachineInteger;
	Z == Integer;
	ZX == DenseUnivariatePolynomial(Z, -"x");
	Zx == Fraction ZX;
	ZXE == LinearOrdinaryRecurrence(Z, ZX, -"E");
	Q == Fraction Z;
	QX == DenseUnivariatePolynomial(Q, -"x");
	FX == UnivariateFactorialPolynomial(Q, QX);
}

Z2Q(n:Z):Q == n::Q;

test(N:Z):() == {
	import from Timer, MachineInteger, Symbol,ZX,ZXE,FX, V FX, Partial FX;
	import from LODOPolynomialSolutions(Z, Q, Z2Q, ZX, ZXE, FX);
	import from UnivariatePolynomialCategory2(Z, ZX, Q, QX);

	x:ZX := monom;
	E:ZXE := monom;

	stdout << newline << "N = " << N << newline;
	L := x*E - (x + N::ZX)::ZXE;
	t := timer();
	start! t;
	c := factorial(x + 1, 1, machine prev N);
	time := stop! t;
	stdout << "time for computing rhs = " << time::Z << " msecs" << newline;
	reset! t;
	start! t;
	cc := map(Z2Q)(c)::FX;
	time := stop! t;
	stdout << "time for converting rhs to factorial polynomial = ";
	stdout << time::Z << " msecs" << newline;
	stdout << "L__N = " << L << newline;
	if N < 10 then stdout << "c = " << c << newline;
	reset! t;
	start! t;
	-- (k, p) := solve(L, cc);
	p := particularSolution(L, cc);
	time := stop! t;
	if N < 10 then {
		-- stdout << "hom = " << k << newline;
		stdout << "part = " << p << newline;
	}
	else {
		-- stdout << "dim(hom) = " << #k << newline;
		-- for i in 1..#k repeat
			-- stdout << "  degree = " << degree(k.i) << newline;
		stdout << "degree(part) = " << degree retract p << newline;
	}
	stdout << "time for solving equation = " << time::Z << " msecs" << newline;
}

import from Z;
test 5;
test 64;
test 128;
test 256;
test 512;
