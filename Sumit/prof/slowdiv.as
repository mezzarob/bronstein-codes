-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------ slowdiv.as -------------------------------
#if IGNORETHIS
Timings:

Before in-place division, sparse:
     ru7      mendel
Q0:  1032      963
Q2:   741      735

With in-place division, sparse (vinci)
          spf	    	zpf
Q3:        96		 75	(1.1.6)

With in-place division, dense:
          spf			    zpf
     ru7      mendel		ru7	mendel
Q3:   63        56		 39	  36      (1.1.1)
     ru7      vinci		ru7	vinci
Q3:  381	330		210	 161	  (1.1.4)
Q3:  149 	124		 76	  63	  (1.1.5)
Q3:   46	 34		 28	  20	  (1.1.6)


Axiom/SUP/SPF:  120 (ru7)
Maple/modp1:     17 (ru7)

#endif

#include "sumit.as"

macro ND == 10;

#if ZECH
macro F  == ZechPrimeField 11;
#else
macro F  == SmallPrimeField 11;
#endif

#if POLYDENSE
macro Px == DenseUnivariatePolynomial(F, "x");
#else
macro Px == SparseUnivariatePolynomial(F, "x");
#endif

import from SingleInteger, Integer, Px, Timer;

x:Px := monom;

c := _
5*x^140+7*x^139+7*x^138+4*x^137+2*x^136+8*x^135+10*x^134+9*x^132+8*x^131+9*x^ _
130+4*x^129+6*x^128+8*x^127+9*x^126+6*x^125+7*x^124+4*x^123+5*x^122+10*x^121+6*_
x^118+7*x^117+9*x^115+5*x^114+7*x^113+7*x^112+6*x^111+7*x^110+4*x^109+6*x^108+7_
*x^107+4*x^106+x^105+6*x^104+x^103+10*x^102+10*x^101+9*x^100+6*x^99+2*x^98+10*x_
^97+2*x^96+6*x^94+4*x^93+10*x^92+x^91+3*x^90+2*x^88+3*x^87+8*x^86+3*x^84+10*x^_
83+9*x^82+3*x^79+9*x^78+5*x^77+5*x^76+9*x^75+9*x^74+2*x^73+10*x^72+9*x^70+10*x^_
69+6*x^68+7*x^67+9*x^65+5*x^64+7*x^63+8*x^62+8*x^61+2*x^60+6*x^59+8*x^57+7*x^56_
+2*x^55+6*x^54+2*x^53+6*x^51+10*x^50+9*x^49+7*x^48+3*x^47+4*x^46+4*x^45+3*x^44+_
4*x^43+9*x^42+2*x^41+5*x^40+8*x^39+x^38+3*x^37+9*x^36+9*x^35+2*x^34+3*x^33+7*x^_
32+x^31+9*x^30+3*x^29+7*x^28+7*x^27+x^26+9*x^25+5*x^24+6*x^23+6*x^21+3*x^20+6*x_
^19+2*x^18+9*x^17+4*x^16+6*x^15+6*x^14+x^13+9*x^12+3*x^11+2*x^10+3*x^9+7*x^8+6*_
x^7+7*x^6+x^5+4*x^4+8*x^3+9*x^2+3*x+4@Integer::Px;

d := _
x^71+7*x^70+5*x^69+7*x^68+9*x^67+9*x^66+3*x^65+x^64+x^63+x^62+x^61+3*x^60+2*x^_
59+5*x^58+7*x^57+9*x^55+8*x^54+6*x^53+3*x^50+x^48+4*x^47+x^46+3*x^45+9*x^44+4*x_
^43+8*x^42+5*x^41+8*x^40+9*x^38+8*x^37+4*x^36+4*x^35+3*x^34+6*x^33+7*x^32+6*x^_
31+4*x^30+8*x^29+10*x^28+10*x^27+x^26+8*x^25+x^24+7*x^23+3*x^22+3*x^21+4*x^20+2_
*x^19+3*x^17+9*x^16+8*x^14+4*x^13+2*x^12+3*x^11+9*x^10+3*x^9+4*x^8+3*x^7+6*x^6+_
4*x^5+5*x^4+x^3+x^2+2*x+7@Integer::Px;

t := timer();
start! t;
for i in 1@SingleInteger..ND repeat (q, r) := divide(c, d);
stop! t;

print << "quotient = $$" << q << "$$" << newline;
print << "remainder = $$" << r << "$$" << newline;
print << "time per division = " << read(t) quo ND << " millisecs" << newline;

