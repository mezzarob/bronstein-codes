-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-------------------------- ffcomp.as -----------------------------
-- Adjust the following parameter before compiling to select how long
-- you want to wait...

macro EXPONENT == 6;	-- 5 or 6

#include "sumit"

macro {
	I == SingleInteger;
	Z  == Integer;
	ZX == DenseUnivariatePolynomial(Z, "x");
	ZXd == LinearOrdinaryDifferentialOperator(ZX, "D");
	V == Vector ZX;
	M == DenseMatrix ZX;
	LA1 == LinearAlgebra(ZX, M, FractionFreeGaussElimination(ZX, M));
	LA2 == LinearAlgebra(ZX, M, TwoStepFractionFreeGaussElimination(ZX, M));
}

local hurwitz():ZXd == {
	import from Z, ZX, ZXd;
	x:ZX := monom;
	dx:ZXd := monom;
	op := 24696*x^2*(x-1)^2 * dx^3 + 24696*x*(7*x-4::ZX)*(x-1) * dx^2;
	op := op + 98*(2592*x^2-2963*x+560::ZX)*dx + (57024*x-40805::ZX)::ZXd;
	print << "Hurwitz operator = " << op << newline;
	op;
}

local rowcontent(mat:M, i:I, c:I):ZX == {
	g:ZX := mat(i,1);
	for j in 2..c repeat g := gcd(g, mat(i,j));
	g;
}

local colcontent(mat:M, j:I, r:I):ZX == {
	g:ZX := mat(1,j);
	for i in 2..r repeat g := gcd(g, mat(i,j));
	g;
}

-- p = lcoeff(L); D = degree(L)
local sympow(L:ZXd, N:I, D:Z, p:ZX):() == {
	import from Timer, M;
	import from FactorFreeSymmetricPower(ZX, ZXd, N, retract D, p);
	mat := symPowMatrix L;
	(r, c) := dimensions mat;
	print << "Optimized matrix has " << r << " rows and " << c;
	print << " columns" << newline << "row contents:" << newline;
	for i in 1..r repeat print << rowcontent(mat, i, c) << " ";
	print << newline << "col contents:" << newline;
	for i in 1..c repeat print << colcontent(mat, i, r) << newline;
	t := timer();
	start! t;
	v := nullspace(mat)$LA1;
	time := stop! t;
	print << "time for 1-step nullspace = " << time << newline;
	reset! t;
	start! t;
	v := nullspace(mat)$LA2;
	time := stop! t;
	print << "time for 2-step nullspace = " << time << newline;
	g := cyclicGen(mat, r);
	reset! t;
	start! t;
	dep := firstDependence(g, r)$LA1;
	time := stop! t;
	print << "time for 1-step dependence = " << time << newline;
	g := cyclicGen(mat, r);
	reset! t;
	start! t;
	dep := firstDependence(g, r)$LA2;
	time := stop! t;
	print << "time for 2-step dependence = " << time << newline;
}

local cyclicGen(mat:M, r:I):Generator V == generate {
	import from V;
	w:V := zero r;
	for j in 1..next r repeat yield vectorize!(w, mat, r, j);
}

local vectorize!(w:V, mat:M, r:I, j:I):V == {
	for i in 1..r repeat w.i := mat(i, j);
	w;
}
main():() == {
	import from I, ZXd;
	L := hurwitz();
	sympow(L, EXPONENT, degree L, leadingCoefficient L);
}

main();
