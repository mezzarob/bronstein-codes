-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-------------------------- slowspow.as -----------------------------
#include "sumit"

#if IGNORETHIS
Timings (msecs/ru8/1.1.7a):

Linear algebra			Q[x]				Q(x)
ord/pow/deg	gen	la	content	post	total	gen	la	total
 2/ 4/9		  4730	  460	 1910	  820	  8660	 1893	 1216	  3393
 2/12/9		120370	81050	21060	12980	235480			108600
 2/ 6/4		  1420	  150	  240	  100	  1956	 1503	  789	  2326
 2/12/4		 13440	 1740	  770	  420	 16380			 17220
 2/ 8/5		  8200	 1130  	 1590	  870	 12385	 9625	 6210	 15885
 2/12/5		 32670	 4450	 4800	 2430	 44360			 53600
 2/12/1		  1360	 1000	   50	   20	  2470	 1653	 4470	  6283
 7  2/0		   480	 4240	    0	    0	  4840	 3920	22590	 26680

JAW iteration			Q[x]				Q(x)
		init	iter	   	content	total			total
2/ 4/9		   140	 1290		   63	  1506			  2516
2/12/9		   120	28690		  570	 29390			 67280
2/ 6/4		    40	  350		   36	   433			  1206
2/12/4		    40	 3000		  100	  3150			  7290
2/ 8/5		    50	 2235		   85	  2380			 10120
2/12/5		    50	 7360		  160	  7570			 32430
2/12/1		    10	  633		   50	   700			  3006

#endif

macro {
	I  == SingleInteger;
	Z  == Integer;
	Q  == Quotient Z;
	QX == DenseUnivariatePolynomial(Q, "x");
	Qx == Quotient QX;
	QXd == LinearOrdinaryDifferentialOperator(QX, "D");
	Qxd == LinearOrdinaryDifferentialOperator(Qx, "D");
}

time(R:IntegralDomain,L:LinearOrdinaryDifferentialOperatorCategory R,op:L,n:Z,N:I):I== {
	import from SingleInteger, Z, Timer;
	t := timer();
	for i in 1@I..N repeat {
		start! t;
		opn:L := symmetricPower(op, n);
		stop! t;
	}
	return read(t) quo N;
}

f1(R:IntegralDomain,x:R,L:LinearOrdinaryDifferentialOperatorCategory R,n:Z,N:I):I == {
	import from Z;
	D:L := monom;
        op := 16*x^2*(x-1)^2*(x^2-x-1)^2*(2*x-1) * D^2;
	op := op - 32*x^2*(x-1)^2*(x^2-x-1)^2 * D;
	op := op + (3*(2*x-1)^3 *(x^4-2*x^3+x+1))::L;
	time(R, L, op, n, N);
}

f2(R:IntegralDomain,x:R,L:LinearOrdinaryDifferentialOperatorCategory R,n:Z,N:I):I == {
	import from Z;
	D:L := monom;
        op := 144*x^2*(x-1)^2*D^2 + (32*x^2-27*x+27@Z::R)::L;
	time(R, L, op, n, N);
}

f3(R:IntegralDomain,x:R,L:LinearOrdinaryDifferentialOperatorCategory R,n:Z,N:I):I == {
	import from Z;
	D:L := monom;
        op := x^2*(6*x+1)*(x-1)^2*D^2 + 3*x*(x-1)*(6*x^2-12*x-1)*D+(48*x+1)::L;
	time(R, L, op, n, N);
}

f4(R:IntegralDomain,x:R,L:LinearOrdinaryDifferentialOperatorCategory R):I == {
	import from Z;
	D:L := monom;
        op := x * D^2 + (2*x + 1)::L;
	time(R, L, op, 12, 3);
}

f5(R:IntegralDomain,x:R,L:LinearOrdinaryDifferentialOperatorCategory R):I == {
	import from Z;
	D:L := monom;
        op := 2 * D^7 - 2 * x * D + 1;
	time(R, L, op, 2, 1);
}

demo():() == {
	import from I, Z, QX, Qx;
	x:QX := monom;

	fft := f1(QX, x, QXd, 4, 3);
	t := f1(Qx, x::Qx, Qxd, 4, 3);
	print << "Time for 4th power over Q[X] = " << fft << newline;
	print << "Time for 4th power over Q(X) = " << t << newline;

	fft := f1(QX, x, QXd, 12, 2);
	t := f1(Qx, x::Qx, Qxd, 12, 1);
	print << "Time for 12th power over Q[X] = " << fft << newline;
	print << "Time for 12th power over Q(X) = " << t << newline;

	fft := f2(QX, x, QXd, 6, 3);
	t := f2(Qx, x::Qx, Qxd, 6, 3);
	print << "Time for 6th power over Q[X] = " << fft << newline;
	print << "Time for 6th power over Q(X) = " << t << newline;

	fft := f2(QX, x, QXd, 12, 3);
	t := f2(Qx, x::Qx, Qxd, 12, 1);
	print << "Time for 12th power over Q[X] = " << fft << newline;
	print << "Time for 12th power over Q(X) = " << t << newline;

	fft := f3(QX, x, QXd, 8, 2);
	t := f3(Qx, x::Qx, Qxd, 8, 2);
	print << "Time for 8th power over Q[X] = " << fft << newline;
	print << "Time for 8th power over Q(X) = " << t << newline;

	fft := f3(QX, x, QXd, 12, 2);
	t := f3(Qx, x::Qx, Qxd, 12, 1);
	print << "Time for 12th power over Q[X] = " << fft << newline;
	print << "Time for 12th power over Q(X) = " << t << newline;

	fft := f4(QX, x, QXd);
	t := f4(Qx, x::Qx, Qxd);
	print << "Time for 12th power over Q[X] = " << fft << newline;
	print << "Time for 12th power over Q(X) = " << t << newline;

	fft := f5(QX, x, QXd);
	t := f5(Qx, x::Qx, Qxd);
	print << "Time for 2nd power over Q[X] = " << fft << newline;
	print << "Time for 2nd power over Q(X) = " << t << newline;
}

demo();

