-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------ Demo of rational solutions of lodo's ---------------
--
-- compile with -q3 -fx -lsumit
--
-- Produces output ready for LaTeX
--

#include "sumit"
#include "aldorio"

macro {
	Z   == Integer;
	ZX  == DenseUnivariatePolynomial(Z, "x");
	Zx  == Quotient ZX;
	ZXD == LinearOrdinaryDifferentialOperator(ZX, "D");
}

import from Symbol;

solve(op:ZXD):TextWriter == {
	import from SingleInteger, ZXD, LODORationalSolutions(Z, ZX, ZXD);
	import from Vector Zx, Zx;
	print << "Solving $" << op << "$\\" << newline;
	sols := rationalKernel op;
	if zero? #sols then print << "No rational solutions" << newline;
	else {
		print << #sols;
		print << "-dimensional space of rational solutions found:\\";
		print << newline;
		for i in 1..#sols repeat
			print << "$$" << sols.i << "$$" << newline;
	}
	print << newline;
}

demo():TextWriter == {
	import from SingleInteger, Z, ZX, ZXD;
	print << "\documentstyle[12pt]{article}\begin{document}";
	print << "\begin{center}{\bf Rational solutions of differential";
	print << " operators with $\Sigma^{it}$}";
	print << "\end{center}\par\noindent" << newline;

	x:ZX := monom;
	dx:ZXD := monom;
	deq := x^3 * dx^3 + x^2 * dx^2 - 2*x*dx + (2@Z)::ZXD;
	solve deq;
	print << "\par\bigskip\par\noindent" << newline;
	deq := (x^9+x^3)*dx^3 + 18*x^8*dx^2-90*x*dx-30*(11*x^6-(3@Z)::ZX)::ZXD;
	solve deq;

	print << "\end{document}" << newline;
}

demo();
