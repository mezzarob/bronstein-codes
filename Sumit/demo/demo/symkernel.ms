{VERSION 1 0 "X11/Motif" "1.0"}{GLOBALS 3 1}{FONT 0 "-adobe-helve
tica-bold-r-normal--*-120-*-*-*-*-*-*" "helvetica" "Helvetica-Bol
d" 8 12 0 "Helvetica-Bold" 12}{FONT 1 "-adobe-times-medium-r-norm
al--*-140-*" "times" "Times-Roman" 4 14 64 "Times-Roman" 12}{FONT
 2 "-adobe-courier-medium-r-normal--*-140-*" "courier" "Courier" 
4 14 192 "Courier" 12}{SCP_R 1 0 24{INP_R 2 0 "> "{TEXT 0 22 "sym
Kernel := proc(L,n)"}}{INP_R 3 0 "> "{TEXT 0 34 "   readlib(write
); open(`bernin`);"}}{INP_R 4 0 "> "{TEXT 0 54 "   writeln(`L := \+
`); writeln(lprint(L)); writeln(`;`);"}}{INP_R 5 0 "> "{TEXT 0 82
 "   writeln(`maple(kernel(symmetricPower(L,`); writeln(lprint(n)
); writeln(`)));`);"}}{INP_R 6 0 "> "{TEXT 0 63 "   close(); open
(`bernout`); write(`sumitresult := `); close();"}}{INP_R 7 0 "> "
{TEXT 0 44 "   system(`bernina -q < bernin >> bernout`);"}}{INP_R
 8 0 "> "{TEXT 0 34 "   read bernout; sumitresult; end;"}}{SEP_R 
9 0}{INP_R 10 0 "> "{TEXT 0 34 "v := symKernel(D^2 + 3/16/x^2, 2)
;"}}{SEP_R 11 0}{INP_R 12 0 "> "{TEXT 0 9 "print(v);"}}{SEP_R 13 
0}{INP_R 14 0 "> "{TEXT 0 102 "w:=symKernel(D^2-2/(2*x-1)*D+(27*x
^4-54*x^3+5*x^2+22*x+27)*(2*x-1)^2/(144*x^2*(x-1)^2*(x^2-x-1)^2),
4);"}}{SEP_R 15 0}{INP_R 16 0 "> "{TEXT 0 9 "print(w);"}}{SEP_R 
17 0}{INP_R 18 0 "> "{TEXT 0 92 "w := symKernel(D^2-2/(2*x-1)*D+3
*(2*x-1)^2*(x^4-2*x^3+x+1)/(16*x^2*(x-1)^2*(x^2-x-1)^2), 4);"}}
{SEP_R 19 0}{INP_R 20 0 "> "{TEXT 0 9 "print(w);"}}{SEP_R 21 0}
{INP_R 22 0 "> "{TEXT 0 68 "z := symKernel(D^2 - (-3/16/x^2 - 2/9
/(x-1)^2 + 3/(16*x*(x-1))), 6);"}}{SEP_R 23 0}{INP_R 24 0 "> "
{TEXT 0 9 "print(z);"}}{SEP_R 25 0}}{END}
