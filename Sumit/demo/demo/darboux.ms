{VERSION 1 0 "X11/Motif" "1.0"}{GLOBALS 3 1}{FONT 0 "-adobe-helve
tica-bold-r-normal--*-120-*-*-*-*-*-*" "helvetica" "Helvetica-Bol
d" 8 12 0 "Helvetica-Bold" 12}{FONT 1 "-adobe-times-medium-r-norm
al--*-140-*" "times" "Times-Roman" 4 14 64 "Times-Roman" 12}{FONT
 2 "-adobe-courier-medium-r-normal--*-140-*" "courier" "Courier" 
4 14 192 "Courier" 12}{SCP_R 1 0 34{INP_R 2 0 "> "{TEXT 0 23 "Dar
boux := proc(L,D,x);"}}{INP_R 3 0 "> "{TEXT 0 34 "   readlib(writ
e); open(`bernin`);"}}{INP_R 4 0 "> "{TEXT 0 54 "   writeln(`L :=
 `); writeln(lprint(L)); writeln(`;`);"}}{INP_R 5 0 "> "{TEXT 0 
42 "   writeln(`maple(Darboux(L));`); close();"}}{INP_R 6 0 "> "
{TEXT 0 54 "   open(`bernout`); write(`sumitresult := `); close()
;"}}{INP_R 7 0 "> "{TEXT 0 69 "   system(cat(`bernina -q -d`, D, \+
` -v`, x, ` < bernin >> bernout`));"}}{INP_R 8 0 "> "{TEXT 0 34 "
   read bernout; sumitresult; end;"}}{SEP_R 9 0}{INP_R 10 0 "> "
{TEXT 0 35 "p := Darboux(D^2 + 3/16/x^2, D, x);"}}{INP_R 11 0 "> \+
"{TEXT 0 9 "print(p);"}}{INP_R 12 0 "> "{TEXT 0 13 "factor(p[1]);
"}}{INP_R 13 0 "> "{TEXT 0 14 "solve(p[1],Y);"}}{SEP_R 14 0}
{INP_R 15 0 "> "{TEXT 0 91 "L := D^2-2/(2*x-1)*D+(27*x^4-54*x^3+5
*x^2+22*x+27)*(2*x-1)^2/(144*x^2*(x-1)^2*(x^2-x-1)^2);"}}{INP_R 
16 0 "> "{TEXT 0 20 "p := Darboux(L,D,x);"}}{INP_R 17 0 "> "{TEXT
 0 9 "print(p);"}}{INP_R 18 0 "> "{TEXT 0 19 "fp := factor(p[1]);
"}}{INP_R 19 0 "> "{TEXT 0 27 "collect(op(1,op(2,fp)), Y);"}}
{SEP_R 20 0}{INP_R 21 0 "> "{TEXT 0 93 "p := Darboux(D^2-2/(2*x-1
)*D+3*(2*x-1)^2*(x^4-2*x^3+x+1)/(16*x^2*(x-1)^2*(x^2-x-1)^2), D, \+
x);"}}{INP_R 22 0 "> "{TEXT 0 5 "p[1];"}}{INP_R 23 0 "> "{TEXT 0 
5 "p[2];"}}{SEP_R 24 0}{INP_R 25 0 "> "{TEXT 0 69 "p := Darboux(D
^2 - (-3/16/x^2 - 2/9/(x-1)^2 + 3/(16*x*(x-1))), D, x);"}}{INP_R 
26 0 "> "{TEXT 0 5 "p[1];"}}{SEP_R 27 0}{INP_R 28 0 "> "{TEXT 0 
51 "p := Darboux(D^2 - x/(x^2-2)*D -1/4/(x^2-2), D, x);"}}{INP_R 
29 0 "> "{TEXT 0 9 "print(p);"}}{SEP_R 30 0}{INP_R 31 0 "> "{TEXT
 0 2 "D;"}}{SEP_R 32 0}{INP_R 33 0 "> "{TEXT 0 2 "x;"}}{SEP_R 34 
0}{INP_R 35 0 "> "{TEXT 0 0 ""}}}{END}
