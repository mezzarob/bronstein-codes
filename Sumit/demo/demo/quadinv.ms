{VERSION 1 0 "X11/Motif" "1.0"}{GLOBALS 3 1}{FONT 0 "-adobe-helve
tica-bold-r-normal--*-140-*" "helvetica" "Helvetica-Bold" 8 14 0 
"Helvetica-Bold" 12}{FONT 1 "-adobe-times-medium-r-normal--*-140-
*" "times" "Times-Roman" 4 14 64 "Times-Roman" 12}{FONT 2 "-adobe
-courier-medium-r-normal--*-140-*" "courier" "Courier" 4 14 192 "
Courier" 12}{SCP_R 1 0 15{INP_R 2 0 "> "{TEXT 0 30 "quadraticInva
riants := proc(L)"}}{INP_R 3 0 "> "{TEXT 0 34 "   readlib(write);
 open(`bernin`);"}}{INP_R 4 0 "> "{TEXT 0 54 "   writeln(`L := `)
; writeln(lprint(L)); writeln(`;`);"}}{INP_R 5 0 "> "{TEXT 0 50 "
   writeln(`maple(kernel(symmetricPower(L,2)));`);"}}{INP_R 6 0 "
> "{TEXT 0 63 "   close(); open(`bernout`); write(`sumitresult :=
 `); close();"}}{INP_R 7 0 "> "{TEXT 0 44 "   system(`bernina -q \+
< bernin >> bernout`);"}}{INP_R 8 0 "> "{TEXT 0 34 "   read berno
ut; sumitresult; end;"}}{OUT_R 9 0 8{DAG :3n7\`quadraticInvariant
s`@8,2n3\`L`,1pBpB;E(3n4\`readlib`,2n4\`write`(3n4\`open`,2n4\`be
rnin`(3n4\`writeln`,2n4\`L~`:=~``(3p20,2(3n4\`lprint`,2a2x0001(3p
20,2n3\`;`(3p20,2nB\`maple(kernel(symmetricPower(L,2)));`(3n4\`cl
ose`pB(3p18,2n4\`bernout`(3p14,2n6\`sumitresult~`:=~``p43(3n4\`sy
stem`,2nA\`bernina~`-q~`<~`bernin~`>>~`bernout`r2p4Bn5\`sumitresu
lt`pBpB}{TEXT 2 667 "\012quadraticInvariants := proc(L)\012      \+
                     readlib(write);\012                         \+
  open(bernin);\012                           writeln(`L := `);\0
12                           writeln(lprint(L));\012             \+
              writeln(`;`);\012                           writeln
(`maple(kernel(symmetricPower(L,2)));`);\012                     \+
      close();\012                           open(bernout);\012  \+
                         write(`sumitresult := `);\012           \+
                close();\012                           system(`be
rnina -q < bernin >> bernout`);\012                           rea
d bernout;\012                           sumitresult\012         \+
              end\012"}}{SEP_R 10 0}{INP_R 11 0 "> "{TEXT 0 42 "v
 := quadraticInvariants(D^7 - x*D + 1/2);"}}{SEP_R 12 0}{INP_R 13
 0 "> "{TEXT 0 9 "print(v);"}}{OUT_R 14 0 13{DAG (3n4\`VECTOR`,2[
2,2n3\`x`}}{SEP_R 15 0}{INP_R 16 0 "> "{TEXT 0 0 ""}}}{END}
