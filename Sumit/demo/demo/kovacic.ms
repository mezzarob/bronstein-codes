{VERSION 1 0 "X11/Motif" "1.0"}{GLOBALS 3 1}{FONT 0 "-adobe-helve
tica-bold-r-normal--*-120-*-*-*-*-*-*" "helvetica" "Helvetica-Bol
d" 8 12 0 "Helvetica-Bold" 12}{FONT 1 "-adobe-times-medium-r-norm
al--*-140-*" "times" "Times-Roman" 4 14 64 "Times-Roman" 12}{FONT
 2 "-adobe-courier-medium-r-normal--*-140-*" "courier" "Courier" 
4 14 192 "Courier" 12}{SCP_R 1 0 43{INP_R 2 0 "> "{TEXT 0 23 "Dar
boux := proc(L,D,x);"}}{INP_R 3 0 "> "{TEXT 0 34 "   readlib(writ
e); open(`bernin`);"}}{INP_R 4 0 "> "{TEXT 0 54 "   writeln(`L :=
 `); writeln(lprint(L)); writeln(`;`);"}}{INP_R 5 0 "> "{TEXT 0 
56 "   writeln(`maple(makeIntegral(Darboux(L)));`); close();"}}
{INP_R 6 0 "> "{TEXT 0 54 "   open(`bernout`); write(`sumitresult
 := `); close();"}}{INP_R 7 0 "> "{TEXT 0 69 "   system(cat(`bern
ina -q -d`, D, ` -v`, x, ` < bernin >> bernout`));"}}{INP_R 8 0 "
> "{TEXT 0 34 "   read bernout; sumitresult; end;"}}{SEP_R 9 0}
{INP_R 10 0 "> "{TEXT 0 43 "bdsolve := proc(L, D, x) local dbx, n
, deg;"}}{INP_R 11 0 "> "{TEXT 0 82 "dbx := Darboux(L, D, x); n :
= linalg[vectdim](dbx); if n = 0 then RETURN(dbx); fi;"}}{INP_R 
12 0 "> "{TEXT 0 64 "deg := degree(dbx[1], Y); if deg = 1 then de
gree1(dbx, n, Y, x);"}}{INP_R 13 0 "> "{TEXT 0 81 "elif deg = 2 t
hen degree2(dbx[1], Y, x); elif deg = 4 then degree4(dbx, n, Y, x
);"}}{INP_R 14 0 "> "{TEXT 0 36 "else algcase(dbx[1], Y, x); fi; \+
end;"}}{SEP_R 15 0}{INP_R 16 0 "> "{TEXT 0 97 "degree1 := proc(cu
rves, n, y, x) local i; [seq(exp(int(solve(curves[i], y), x)), i \+
= 1..n)]; end;"}}{SEP_R 17 0}{INP_R 18 0 "> "{TEXT 0 111 "degree2
 := proc(curve, y, x) local s, i; s := [solve(curve, y)]; [seq(ex
p(int(s[i], x)), i = 1..nops(s))]; end;"}}{SEP_R 19 0}{INP_R 20 0
 "> "{TEXT 0 32 "degree4 := proc(curves, n, y, x)"}}{INP_R 21 0 "
> "{TEXT 0 92 " if n = 1 then degree41(curves[1], y, x) else degr
ee42(curves[1], curves[2], y, x); fi; end;"}}{SEP_R 22 0}{INP_R 
23 0 "> "{TEXT 0 62 "degree41 := proc(curve, y, x) local s; s := \+
sqrfree(curve)[2];"}}{INP_R 24 0 "> "{TEXT 0 80 " if nops(s) = 1 \+
and type(s[1][2], even) then degree2(s[1][1]^(s[1][2]/2), y, x);"
}}{INP_R 25 0 "> "{TEXT 0 35 "else algcase(curve, y, x); fi; end;
"}}{SEP_R 26 0}{INP_R 27 0 "> "{TEXT 0 91 "degree42 := proc(curve
1, curve2, y, x) [algcase(curve1, y, x), algcase(curve2, y, x)]; \+
end;"}}{SEP_R 28 0}{INP_R 29 0 "> "{TEXT 0 64 "algcase := proc(cu
rve, y, x) exp(Int(RootOf(curve, y), x)); end;"}}{SEP_R 30 0}
{SEP_R 31 0}{INP_R 32 0 "> "{TEXT 0 21 "L1 := D^2 + 3/16/x^2;"}}
{INP_R 33 0 "> "{TEXT 0 30 "sol := bdsolve(L1, D, x); sol;"}}
{SEP_R 34 0}{INP_R 35 0 "> "{TEXT 0 92 "L2 := D^2-2/(2*x-1)*D+(27
*x^4-54*x^3+5*x^2+22*x+27)*(2*x-1)^2/(144*x^2*(x-1)^2*(x^2-x-1)^2
);"}}{INP_R 36 0 "> "{TEXT 0 30 "sol := bdsolve(L2, D, x); sol;"}
}{SEP_R 37 0}{INP_R 38 0 "> "{TEXT 0 79 "L3 := D^2-2/(2*x-1)*D+3*
(2*x-1)^2*(x^4-2*x^3+x+1)/(16*x^2*(x-1)^2*(x^2-x-1)^2);"}}{INP_R 
39 0 "> "{TEXT 0 30 "sol := bdsolve(L3, D, x); sol;"}}{SEP_R 40 0
}{INP_R 41 0 "> "{TEXT 0 55 "L4 := D^2 - (-3/16/x^2 - 2/9/(x-1)^2
 + 3/(16*x*(x-1)));"}}{INP_R 42 0 "> "{TEXT 0 30 "sol := bdsolve(
L4, D, x); sol;"}}{SEP_R 43 0}{INP_R 44 0 "> "{TEXT 0 0 ""}}}{END
}
