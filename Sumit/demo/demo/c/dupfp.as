-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
----------------------------- dupfp.as ---------------------------
-- C interface to dense Fp[X] factorization
#include "sumit"
macro {
    Z      == SingleInteger;     DUP == DenseUnivariatePolynomial;
    POLY   == Record(F:PrimeFieldCategory, Fx:DUP F);
    FACTOR == Record(factor:POLY, exponent:Z);
    FPOLY  == Record(lc:Z, size:Z, factors:PrimitiveArray FACTOR);
}
export {
    FpXfactor: POLY -> FPOLY;
    FpXmake:   (Z, PrimitiveArray Z, Z) -> POLY;
    FpXprint:  (POLY, String) -> ();
} to Foreign C;

FpXprint(p:POLY,x:String):() == print0(explode p)(x);
print0(Fp:PrimeFieldCategory, p:DUP Fp)(x:String):() == apply(print,p,x);

FpXmake(deg:Z, c:PrimitiveArray Z, charac:Z):POLY == {
    macro Fp == SmallPrimeField charac;
    import from Integer, Fp, DUP Fp;
    p:DUP Fp := 0;
    for i in 0..deg repeat p:=add!(p,monomial(c(i+1)::Fp,i::Integer));
    [Fp, p];
}

FpXfactor(p:POLY):FPOLY == factor0 explode p;
factor0(Fp:PrimeFieldCategory, p:DUP Fp):FPOLY == {
    import from Z, Product DUP Fp, FACTOR, PrimitiveArray FACTOR;
    import from PrimeFieldUnivariateFactorizer(Fp, DUP Fp);
    import from Integer, Record(lcoeff:Fp, factors:Product DUP Fp);
    r := factor p;
    v:PrimitiveArray FACTOR := new #(r.factors);
    for term in r.factors for i in 1@Z.. repeat {
        (f, n) := term;
        v.i := [[Fp, f], retract n];
    }   
    [lift(r.lcoeff), #(r.factors), v];
}

