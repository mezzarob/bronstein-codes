/*
 * ======================================================================
 * This code was written all or part by Dr. Manuel Bronstein from
 * Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
 * decided to publish this code under the CeCILL open source license in
 * memory of Dr. Manuel Bronstein.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and Inria at the following URL :
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ======================================================================
 * 
 */
/* demo the C interface to Fp[X] */

#include <stdio.h>
#include "sumit_dupfp.h"

main()
{
	int i,coeff[8]={6,1,4,3,9,7,5,1};/* x^7+5x^6+7x^5+9x^4+3x^3+4x^2+x+6 */
	FPX p = FpXmake(7, coeff, 11);	  /* F_11[x] */
	FACTORED *fp = FpXfactor(p);

	printf("The factorization of $");
	FpXprint(p, "x");
	printf("$ in $F_{11}[x]$ is: $$\n %d ", fp->leadingCoefficient);
	for (i = 0; i < fp->numberOfFactors; i++) {
		printf("(");
		FpXprint(fp->factors[i]->factor, "x");
		printf(")^%d ", fp->factors[i]->exponent);
	}
	printf("\n$$\n");
}

