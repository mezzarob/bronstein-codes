{VERSION 1 0 "X11/Motif" "1.0"}{GLOBALS 3 1}{FONT 0 "-adobe-helve
tica-bold-r-normal--*-120-*-*-*-*-*-*" "helvetica" "Helvetica-Bol
d" 8 12 0 "Helvetica-Bold" 12}{FONT 1 "-adobe-times-medium-r-norm
al--*-140-*" "times" "Times-Roman" 4 14 64 "Times-Roman" 12}{FONT
 2 "-adobe-courier-medium-r-normal--*-140-*" "courier" "Courier" 
4 14 192 "Courier" 12}{SCP_R 1 0 40{INP_R 2 0 "> "{TEXT 0 22 "sym
Kernel := proc(L,n)"}}{INP_R 3 0 "> "{TEXT 0 34 "   readlib(write
); open(`bernin`);"}}{INP_R 4 0 "> "{TEXT 0 54 "   writeln(`L := \+
`); writeln(lprint(L)); writeln(`;`);"}}{INP_R 5 0 "> "{TEXT 0 82
 "   writeln(`maple(kernel(symmetricPower(L,`); writeln(lprint(n)
); writeln(`)));`);"}}{INP_R 6 0 "> "{TEXT 0 63 "   close(); open
(`bernout`); write(`sumitresult := `); close();"}}{INP_R 7 0 "> "
{TEXT 0 44 "   system(`bernina -q < bernin >> bernout`);"}}{INP_R
 8 0 "> "{TEXT 0 34 "   read bernout; sumitresult; end;"}}{SEP_R 
9 0}{INP_R 10 0 "> "{TEXT 0 65 "special := proc(L, n, D, x, y, v)
 local p, bn1, b, a1, a0, i, bi;"}}{INP_R 11 0 "> "{TEXT 0 58 "bn
1 := normal(- diff(v, x) / v); p := y^n + bn1 * y^(n-1);"}}{INP_R
 12 0 "> "{TEXT 0 78 "a1 := coeff(L, D, 1) / coeff(L, D, 2); a0 :
= coeff(L, D, 0) / coeff(L, D, 2); "}}{INP_R 13 0 "> "{TEXT 0 50 
"for i from n-1 to 1 by -1 do bi := coeff(p, y, i);"}}{INP_R 14 0
 "> "{TEXT 0 87 "  b:= normal((-diff(bi,x) + bn1 * bi + a1*(i-n)*
bi +a0*(i+1)*coeff(p,y,i+1))/(n-i+1)); "}}{INP_R 15 0 "> "{TEXT 0
 28 "  p := p + b * y^(i-1); od; "}}{INP_R 16 0 "> "{TEXT 0 8 "p;
 end; "}}{SEP_R 17 0}{INP_R 18 0 "> "{TEXT 0 42 "Darboux := proc(
L, n, D, x, y) local v, i;"}}{INP_R 19 0 "> "{TEXT 0 88 "v := sym
Kernel(L, n); [seq(special(L, n, D, x, y, v[i]), i=1..linalg[vect
dim](v))]; end;"}}{SEP_R 20 0}{INP_R 21 0 "> "{TEXT 0 41 "p := Da
rboux(D^2 + 3/16/x^2, 2, D, x, y);"}}{INP_R 22 0 "> "{TEXT 0 2 "p
;"}}{INP_R 23 0 "> "{TEXT 0 19 "fp := factor(p[1]);"}}{INP_R 24 0
 "> "{TEXT 0 12 "solve(fp,y);"}}{SEP_R 25 0}{INP_R 26 0 "> "{TEXT
 0 91 "L := D^2-2/(2*x-1)*D+(27*x^4-54*x^3+5*x^2+22*x+27)*(2*x-1)
^2/(144*x^2*(x-1)^2*(x^2-x-1)^2);"}}{INP_R 27 0 "> "{TEXT 0 24 "p
 := Darboux(L,4,D,x,y);"}}{INP_R 28 0 "> "{TEXT 0 2 "p;"}}{INP_R 
29 0 "> "{TEXT 0 19 "fp := factor(p[1]);"}}{INP_R 30 0 "> "{TEXT 
0 27 "collect(op(1,op(2,fp)), y);"}}{SEP_R 31 0}{INP_R 32 0 "> "
{TEXT 0 99 "p := Darboux(D^2-2/(2*x-1)*D+3*(2*x-1)^2*(x^4-2*x^3+x
+1)/(16*x^2*(x-1)^2*(x^2-x-1)^2), 4, D, x, y);"}}{INP_R 33 0 "> "
{TEXT 0 2 "p;"}}{INP_R 34 0 "> "{TEXT 0 13 "factor(p[1]);"}}
{INP_R 35 0 "> "{TEXT 0 13 "factor(p[2]);"}}{SEP_R 36 0}{INP_R 37
 0 "> "{TEXT 0 75 "p := Darboux(D^2 - (-3/16/x^2 - 2/9/(x-1)^2 + \+
3/(16*x*(x-1))), 6, D, x, y);"}}{SEP_R 38 0}{INP_R 39 0 "> "{TEXT
 0 2 "p;"}}{INP_R 40 0 "> "{TEXT 0 13 "factor(p[1]);"}}{SEP_R 41 
0}}{END}
