{VERSION 1 0 "X11/Motif" "1.0"}{GLOBALS 3 1}{FONT 0 "-adobe-helve
tica-bold-r-normal--*-180-*-*-*-*-*-*" "helvetica" "Helvetica-Bol
d" 8 18 0 "Helvetica-Bold" 18}{FONT 1 "-adobe-times-medium-r-norm
al--*-140-*" "times" "Times-Roman" 4 14 64 "Times-Roman" 12}{FONT
 2 "-adobe-courier-medium-r-normal--*-180-*-*-*-*-*-*" "courier" 
"Courier" 4 18 192 "Courier" 18}{SCP_R 1 0 24{INP_R 2 0 "> "{TEXT
 0 15 "readlib(write):"}}{SEP_R 3 0}{INP_R 4 0 "> "{TEXT 0 58 "be
rnina := proc() open(`bernout`):write(`inv :=`):close():"}}{INP_R
 5 0 "> "{TEXT 0 62 "system(`bernina -q < bernin >> bernout`): re
ad(`bernout`):end;"}}{SEP_R 6 0}{INP_R 7 0 "> "{TEXT 0 79 "quadra
ticInvariants:= proc(L) open(`bernin`):writeln(`L:=`):writeln(lpr
int(L)):"}}{INP_R 8 0 "> "{TEXT 0 79 "writeln(`;`): writeln(`mapl
e(kernel(symmetricPower(L,2)));`): writeln(`quit;`):"}}{INP_R 9 0
 "> "{TEXT 0 23 "close(): bernina():end;"}}{SEP_R 10 0}{INP_R 11 
0 "> "{TEXT 0 65 "DarbouxCurve := proc(L,y) local a1,a0,b1: quadr
aticInvariants(L):"}}{INP_R 12 0 "> "{TEXT 0 38 "b1 := - normal(d
iff(inv[1],x)/inv[1]):"}}{INP_R 13 0 "> "{TEXT 0 85 "a1 := normal
(coeff(L,D,1) / coeff(L,D,2)): a0 := normal(coeff(L,D,0) / coeff(
L,D,2)):"}}{INP_R 14 0 "> "{TEXT 0 69 "y^2 + b1 * y - normal(diff
(b1,x) - b1^2 + a1 * b1 - 2 * a0) / 2: end;"}}{SEP_R 15 0}{INP_R 
16 0 "> "{TEXT 0 66 "LLCMFactor := proc(L) local y,s: s := solve(
DarbouxCurve(L,y), y):"}}{INP_R 17 0 "> "{TEXT 0 45 "coeff(L,D,2)
 * LLCM(D - s[1], D - s[2]): end;"}}{SEP_R 18 0}{INP_R 19 0 "> "
{TEXT 0 18 "L := D^2+3/16/x^2;"}}{SEP_R 20 0}{INP_R 21 0 "> "
{TEXT 0 14 "LLCMFactor(L);"}}{SEP_R 22 0}{INP_R 23 0 "> "{TEXT 0 
2 "\";"}}{SEP_R 24 0}{INP_R 25 0 "> "{TEXT 0 0 ""}}}{END}
