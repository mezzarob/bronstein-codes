-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------- Drawing a newton polygon ------------------------
--
-- compile with -q3 -fx -lsumit
--
-- Produces output ready for LaTeX and for gnuplot
--

#include "sumit"

macro {
	Z  == Integer;
	Q  == Quotient Z;
	Qx == DenseUnivariatePolynomial(Q, "x");
	Fd == LinearOrdinaryDifferentialOperator(Qx, "D");
	NP == LinearOrdinaryDifferentialNewtonPolygon(Qx, Q, P, Fd);
}

draw(P:DifferentialPlace(Qx, Q), np:NP):() == {
		import from Z, List Cross(Z, Z), OutFile, TextWriter, FileName;
		import from OperatingSystemInterface, StandardUtilities;

		points := envelope np;  -- rightmost point first
		ptsname := filename uniqueName "/tmp/points";
		fp := open ptsname;
		wp := writer fp;
		-- always add a vertical segment at the right
		(x, y) := first points;
		maxy := y + 1; miny := y;
		wp << x << "  " << y + 1 << newline;
		local lasty:Z;
		for pt in points repeat {
			lasty := y;
			(x, y) := pt;
			if y > maxy then maxy := y;
			else if y < miny then miny := y;
			wp << x << "  " << y << newline;
		}
		-- add an horizontal segment at the left if needed
		if empty? rest points or y ~= lasty then
			wp << -1@Z << "  " << y << newline;
		close fp;
		cmdname := filename uniqueName "/tmp/commands";

		-- generate the gnuplot commands
		fp := open cmdname;
		wp := writer fp;
		wp << "set data style linespoints" << newline;
		wp << "set nogrid" << newline;
		wp << "set noborder" << newline;
		wp << "set nokey" << newline;
		wp << "set zeroaxis" << newline;
		wp << "set terminal postscript" << newline;
		wp << "set output _"drawnp.eps_"" << newline;
		wp << "plot [ ] [" << miny << "-0.5:" << maxy << "] ";
		wp  << ptsname << newline << "quit" << newline;
		close fp;

		run concat("gnuplot < ", unparse cmdname);
		fileRemove unparse ptsname;
		fileRemove unparse cmdname;
}

analyze(L:Fd, P:DifferentialPlace(Qx, Q)):() == {
	import from NP;
	np := newtonPolygon L;
	draw(P, newtonPolygon L);
}

demo():TextWriter == {
	import from Z, Q, Qx, Fd;
	print << "\documentstyle[epsfig,12pt]{article}\begin{document}";
	print << "\begin{center}{\bf A Newton Polygon with $\Sigma^{it}$}";
	print << "\end{center}\par\noindent" << newline;

	x:Qx := monom;		-- create 'x'
	D:Fd := monom;		-- create 'D'
	L := (8 * x^6 + 18 * x^8 -16 * x^9 + 3 * x^10) * D^3 +
			(16 *x^3 + 12 * x^9) * D^2 + (16 * x+6 * x^8) * D
			 + (12 * x^5 - 16::Qx)::Fd;
	print << "Newton polygon of $$" << L << "$$ at $x = 0$" << newline;
	{
		import from NP;
		draw(P, newtonPolygon L);
	} where P == RationalPlace(Q, Qx, 0);

	print << "\begin{figure}[h] \begin{center} \leavevmode" << newline;
	print << "\epsfig{file=drawnp.eps,angle=270,scale=0.6} \end{center}";
	print << "\end{figure}" << newline << "\end{document}" << newline;
}

demo();
