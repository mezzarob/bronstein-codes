-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------- Demo of polynomial factorisation modulo p -------------------
--
-- compile with -q3 -fx -lsalli -lsumit [-D SPF]
--
-- Produces output ready for LaTeX
--

#include "sumit"
#include "sallio"

macro Z == Integer;

#if SPF
macro F == SmallPrimeField 11;
#else
macro F == ZechPrimeField 11;
#endif

macro {
	Z == Integer;
	Fx == DenseUnivariatePolynomial(F, -"x");
}

import from Symbol, MachineInteger;

local Factor11(a:Fx):TextWriter == {
	import from F, Product Fx;

	stdout << "\smallskip$$" << a << " = ";
	(c, prod) := factor a;
	n := #prod;
	if n > 3 then stdout << "$$" << newline << "$$";
	if c ~= 1 then stdout << c << "\;";
	stdout << prod << "$$" << newline;
}

local demo():TextWriter == {
	import from Z, Fx;

	stdout << "\documentstyle[12pt]{article}\begin{document}";
        stdout << "\begin{center}";
	stdout << "{\bf Factorisations modulo 11 with $\Sigma^{it}$}";
        stdout << "\end{center}\par\noindent" << newline;

	x := monom;

	Factor11(x^5 + x^4 + x^3 + x^2 + x + 1);
	Factor11(x^7 + x^2 + 1);
	Factor11(x^3 + 4 * x^2 + x + 3@Z::Fx);
	Factor11(x^13 + x^2 + 1);
	Factor11(x^7 + x^3 + 6@Z::Fx);
	Factor11(x^7 + 5*x^6 + 7*x^5 + 9 * x^4 + 3*x^3 + 4 * x^2 + x + 6@Z::Fx);
	Factor11(x^79 + 3*x^65 + 6*x^59 + 4*x^58 + 10 * x^53 + 4 * x^15 + 1);

	stdout << "\end{document}" << newline;
}

demo();
