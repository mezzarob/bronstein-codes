-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
---------------------- Demo of indicial equations ------------------------
--
-- compile with -q3 -fx -lsumit
--
-- Produces output ready for LaTeX
--

#include "sumit"

macro {
	Z  == Integer;
	Q  == Quotient Z;
	Qx == DenseUnivariatePolynomial(Q, "x");
	Fd == LinearOrdinaryDifferentialOperator(Qx, "D");
	GP == GeneralizedPochammerUnivariatePolynomial Q;
}

analyze(L:Fd, P:DifferentialPlace(Qx, Q)):TextWriter == {
	macro NP == LinearOrdinaryDifferentialNewtonPolygon(Qx, Q, P, Fd);
	import from NP, GP;
	np := newtonPolygon L;
	eq := indicialEquation np;
	print << "Indicial equation: $" << eq << " = 0 $\hfill" << newline;
	if ordinary? np then print << "Not a singularity\\" << newline;
	else if regular? np then print << "Regular singularity\\" << newline;
	else print << "Irregular singularity\\" << newline;
	print << "\\ " << newline;
}

affine(L:Fd, pt:Q):TextWriter == {
	print << "Studying operator$$" << L << "$$ at $x = " << pt << ".\quad$";
	print << newline;
	analyze(L, RationalPlace(Q, Qx, pt));
}

infinity(L:Fd):TextWriter == {
	print << "Studying operator$$" << L << "$$ at $x = \infty.\quad$";
	print << newline;
	analyze(L, RationalInfinity(Q, Qx));
}

demo():TextWriter == {
	import from Z, Q, Qx, Fd;
	print << "\documentstyle[12pt]{article}\begin{document}";
	print << "\begin{center}{\bf Indicial equations with $\Sigma^{it}$}";
	print << "\end{center}\par\noindent" << newline;
	print << "\newcommand{\gpoch}[4]{{z}^{\overline{#2,#3,#4}}}" <<newline;
	print << "$$\gpoch{}{a}{n}{e}=(z+a e)(z+(a+1) e) \dots (z+(a+n-1) e)$$";
	print << newline;
	x:Qx := monom;		-- create 'x'
	D:Fd := monom;		-- create 'D'
	airy := D^2 - x::Fd;
	euler := x^3 * D^2 + (x + x^2) * D - 1;
	legendre := (1 - x^2) * D^2 - 2::Q*x*D + 30::Fd;
	ramisSibuya := (8 * x^6 + 18 * x^8 -16 * x^9 + 3 * x^10) * D^3 +
			(16 *x^3 + 12 * x^9) * D^2 + (16 * x+6 * x^8) * D
			 + (12 * x^5 - 16::Qx)::Fd;
	infinity airy;
	affine(euler, 0);
	infinity euler;
	affine(legendre, 0);
	affine(legendre, 1);
	infinity legendre;
	affine(ramisSibuya, 0);
	infinity ramisSibuya;

	print << "\end{document}" << newline;
}

demo();
