# ======================================================================
# This code was written all or part by Dr. Manuel Bronstein from
# Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
# decided to publish this code under the CeCILL open source license in
# memory of Dr. Manuel Bronstein.
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and Inria at the following URL :
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
# 
# As a counterpart to the access to the source code and rights to copy,
# modify and redistribute granted by the license, users are provided
# only with a limited warranty and the software's author, the holder of
# the economic rights, and the successive licensors have only limited
# liability.
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading, using, modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate, and that also
# therefore means that it is reserved for developers and experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards
# their requirements in conditions enabling the security of their
# systems and/or data to be ensured and, more generally, to use and
# operate it in the same conditions as regards security.
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# ======================================================================
# 
objects   = matcat.o matdense.o matsparse.o linearmap.o zlaring.o
asobjects = matcat.ao matdense.ao matsparse.ao linearmap.ao zlaring.ao
options   = -M2 -Fao -Fo -Q3 -Csmax=0 -Mno-mactext
# options   = -M2 -Fao -Fo -Q1 -Csmax=0 -Mno-mactext -DASASSERT -DASTRACE
tex	  = matcat.tex matdense.tex matsparse.tex linearmap.tex
test      = Tmatsparse.as Tmatdense.as
testdir   = ../../test
libsumit  = ../../lib/libsumit.a
sumitasl  = ../../lib/libsumit.al

all: datestamp

datestamp: $(objects)
	ar rv $(libsumit) $(objects)
	touch datestamp

matdense.o: matdense.as matcat.o ../extree/operators/OPmatrix.o
	axiomxl $(options) matdense.as
	ar rv $(sumitasl) matdense.ao

matsparse.o: matsparse.as matcat.o ../extree/operators/OPmatrix.o ../univpoly/sup.o
	axiomxl $(options) matsparse.as
	ar rv $(sumitasl) matsparse.ao

linearmap.o: linearmap.as matcat.o
	axiomxl $(options) linearmap.as
	ar rv $(sumitasl) linearmap.ao

matcat.o: matcat.as gauss/OGE.o gauss/FF2GE.o categories/laring.o
	axiomxl $(options) matcat.as
	ar rv $(sumitasl) matcat.ao

zlaring.o: zlaring.as gauss/LA.o gauss/FF2GE.o categories/laring.o ../univpoly/gcd/gcdint.o
	axiomxl $(options) zlaring.as
	ar rv $(sumitasl) zlaring.ao

.PHONY: doc
doc: $(tex)

linearmap.tex: linearmap.as
	as2doc linearmap

matcat.tex: matcat.as
	as2doc matcat

matdense.tex: matdense.as
	as2doc matdense

matsparse.tex: matsparse.as
	as2doc matsparse

.PHONY: test
test: $(test)

Tmatsparse.as: matsparse.as
	as2test matsparse
	cp Tmatsparse.as $(testdir)

Tmatdense.as: matdense.as
	as2test matdense
	cp Tmatdense.as $(testdir)

.PHONY: clean
clean:
	-rm $(objects) $(asobjects) $(tex) $(test)

