# ======================================================================
# This code was written all or part by Dr. Manuel Bronstein from
# Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
# decided to publish this code under the CeCILL open source license in
# memory of Dr. Manuel Bronstein.
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and Inria at the following URL :
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
# 
# As a counterpart to the access to the source code and rights to copy,
# modify and redistribute granted by the license, users are provided
# only with a limited warranty and the software's author, the holder of
# the economic rights, and the successive licensors have only limited
# liability.
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading, using, modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate, and that also
# therefore means that it is reserved for developers and experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards
# their requirements in conditions enabling the security of their
# systems and/or data to be ensured and, more generally, to use and
# operate it in the same conditions as regards security.
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# ======================================================================
# 
objects   = condalg.o condcase.o condcat.o condif.o parpol.o
asobjects = condalg.ao condcase.ao condcat.ao condif.ao parpol.ao
options   = -M2 -Fao -Fo -Q3
tex       = condalg.tex condcase.tex condcat.tex condif.tex parpol.tex
test	  = Tcondalg.as
testdir	  = ../../test
libsumit  = ../../lib/libsumit.a
sumitasl  = ../../lib/libsumit.al

all: datestamp

datestamp: $(objects)
	ar rv $(libsumit) $(objects)
	touch datestamp

condalg.o: condalg.as condcat.o ../univpoly/dup.o
	axiomxl $(options) condalg.as
	ar rv $(sumitasl) condalg.ao

condcase.o: condcase.as condif.o
	axiomxl $(options) condcase.as
	ar rv $(sumitasl) condcase.ao

condcat.o: condcat.as ../categories/CATbasic.o
	axiomxl $(options) condcat.as
	ar rv $(sumitasl) condcat.ao

condif.o: condif.as condcat.o
	axiomxl $(options) condif.as
	ar rv $(sumitasl) condif.ao

parpol.o: parpol.as condalg.o condcase.o
	axiomxl $(options) parpol.as
	ar rv $(sumitasl) parpol.ao

.PHONY: doc
doc: $(tex)

condalg.tex: condalg.as
	as2doc condalg

condcase.tex: condcase.as
	as2doc condcase

parpol.tex: parpol.as
	as2doc parpol

condcat.tex: condcat.as
	as2doc condcat

condif.tex: condif.as
	as2doc condif

.PHONY: test
test: $(test)

Tcondalg.as: condalg.as
	as2test condalg
	cp Tcondalg.as $(testdir)

.PHONY: clean
clean:
	-rm $(objects) $(asobjects) $(tex) $(test)

