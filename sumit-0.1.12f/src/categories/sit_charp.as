-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
------------------------------- sit_charp.as ----------------------------------
-- Copyright (c) Manuel Bronstein 1994
-- Copyright (c) INRIA 1999, Version 0.1.12
-- Logiciel Sum^it �INRIA 1999, dans sa version 0.1.12
-- Copyright (c) Swiss Federal Polytechnic Institute Zurich, 1994-97
-------------------------------------------------------------------------------

#include "sumit"

macro {
	MZ == MachineInteger;
	Z  == Integer;
}

#include "sit_power"

#if ALDOC
\thistype{FiniteCharacteristic}
\History{Manuel Bronstein}{22/11/94}{created}
\Usage{\this: Category}
\Descr{\this~is the category of finite characteristic rings.}
\begin{exports}
\category{\astype{Ring}}\\
\asexp{pthPower}: & \% $\to$ \% & Exponentiation to the characteristic\\
\asexp{pthPower!}:
& \% $\to$ \% & In--place exponentiation to the characteristic\\
\end{exports}
#endif

define FiniteCharacteristic: Category == Ring with {
	pthPower: % -> %;
	pthPower!: % -> %;
#if ALDOC
\aspage{pthPower}
\astarget{\name!}
\Usage{\name~x\\ \name!~x}
\Signature{\%}{\%}
\Params{ {\em x} & \% & An element of the ring\\ }
\Retval{Return $x^p$ where $p$ is the characteristic of the ring.}
\Remarks{\name!~does not make a copy of $x$, which is therefore
modified after the call. It is unsafe to use the variable $x$
after the call, unless it has been assigned to the result
of the call, as in {\tt x := pthPower!~x}.}
#endif
	default {
		pthPower!(a:%):% == pthPower a;

		(a:%)^(n:Integer):% ==
			pExponentiation(a,
				n)$PthPowering(% pretend FiniteCharacteristic);
	}
}
