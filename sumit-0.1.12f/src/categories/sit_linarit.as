-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-------------------------- sit_linarith.as ----------------------------------
-- Copyright (c) Manuel Bronstein 2000
-- Copyright (c) INRIA 1999, Version 0.1.12
-- Logiciel Sum^it �INRIA 1999, dans sa version 0.1.12
-----------------------------------------------------------------------------

#include "sumit"

#if ALDOC
\thistype{LinearArithmeticType}
\History{Manuel Bronstein}{14/2/2000}{created}
\Usage{\this~R:Category}
\Params{
{\em R} & \astype{SumitType} & The coefficient domain\\
        & \astype{AdditiveType} &\\
}
\Descr{\this~R is the category of arithmetic types containing linear
combinations of their elements with coefficients in R.}
\Remarks{Use \astype{Algebra} instead
if R is always meant to be a \astype{Ring}.}
\begin{exports}
\category{\astype{ArithmeticType}}\\
\category{\astype{LinearCombinationType} R}\\
\asexp{coerce}: & R $\to$ \% & Natural embedding\\
\end{exports}
#endif

define LinearArithmeticType(R:Join(AdditiveType, SumitType)):
	Category == Join(ArithmeticType, LinearCombinationType R) with {
		coerce: R -> %;
#if ALDOC
\aspage{coerce}
\Usage{\name~r}
\Signature{R}{\%}
\Params{ {\em r} & R & An element of the base type\\ }
\Retval{Returns $r \cdot 1$.}
#endif

	default { coerce(r:R):% == r * 1 }
}
