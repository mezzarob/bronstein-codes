# ======================================================================
# This code was written all or part by Dr. Manuel Bronstein from
# Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
# decided to publish this code under the CeCILL open source license in
# memory of Dr. Manuel Bronstein.
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and Inria at the following URL :
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
# 
# As a counterpart to the access to the source code and rights to copy,
# modify and redistribute granted by the license, users are provided
# only with a limited warranty and the software's author, the holder of
# the economic rights, and the successive licensors have only limited
# liability.
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading, using, modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate, and that also
# therefore means that it is reserved for developers and experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards
# their requirements in conditions enabling the security of their
# systems and/or data to be ensured and, more generally, to use and
# operate it in the same conditions as regards security.
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# ======================================================================
# 
objects   = dhpowprod.o dhextprod.o frmodcat.o dfreemod.o dhpoly.o dhextpow.o
asobjects = dhpowprod.ao dhextprod.ao frmodcat.ao dfreemod.ao dhpoly.ao dhextpow.ao
options	  = -M2 -Fao -Fo -Q3 -Csmax=0 -Mno-mactext
tex       = dhpowprod.tex dhextprod.tex frmodcat.tex dfreemod.tex dhpoly.tex dhextpow.tex
test      =
testdir   = ../../test
libsumit  = ../../lib/libsumit.a
sumitasl  = ../../lib/libsumit.al

all: datestamp

datestamp: $(objects)
	ar rv $(libsumit) $(objects)
	touch datestamp

dhpowprod.o: dhpowprod.as ../univpoly/gcd/gcdint.o ../categories/CATfset.o ../util/binsearch.o
	axiomxl $(options) dhpowprod.as
	ar rv $(sumitasl) dhpowprod.ao

dhextprod.o: dhextprod.as ../univpoly/gcd/gcdint.o ../categories/CATfset.o ../util/binsearch.o
	axiomxl $(options) dhextprod.as
	ar rv $(sumitasl) dhextprod.ao

frmodcat.o: frmodcat.as ../univpoly/gcd/gcdint.o ../categories/CATfset.o
	axiomxl $(options) frmodcat.as
	ar rv $(sumitasl) frmodcat.ao

dfreemod.o: dfreemod.as frmodcat.o
	axiomxl $(options) dfreemod.as
	ar rv $(sumitasl) dfreemod.ao

dhpoly.o: dhpoly.as dhpowprod.o dfreemod.o
	axiomxl $(options) dhpoly.as
	ar rv $(sumitasl) dhpoly.ao

dhextpow.o: dhextpow.as dhextprod.o dfreemod.o
	axiomxl $(options) dhextpow.as
	ar rv $(sumitasl) dhextpow.ao

.PHONY: doc
doc: $(tex)

dhpowprod.tex: dhpowprod.as
	as2doc dhpowprod

dhextprod.tex: dhextprod.as
	as2doc dhextprod

frmodcat.tex: frmodcat.as
	as2doc frmodcat

dfreemod.tex: dfreemod.as
	as2doc dfreemod

dhpoly.tex: dhpoly.as
	as2doc dhpoly

dhextpow.tex: dhextpow.as
	as2doc dhextpow

.PHONY: test
test: $(test)


.PHONY: clean
clean:
	-rm $(objects) $(asobjects) $(tex) $(test)

