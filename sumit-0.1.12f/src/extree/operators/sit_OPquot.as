-- ======================================================================
-- This code was written all or part by Dr. Manuel Bronstein from
-- Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
-- decided to publish this code under the CeCILL open source license in
-- memory of Dr. Manuel Bronstein.
-- 
-- This software is governed by the CeCILL license under French law and
-- abiding by the rules of distribution of free software. You can use,
-- modify and/or redistribute the software under the terms of the CeCILL
-- license as circulated by CEA, CNRS and Inria at the following URL :
-- http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
-- 
-- As a counterpart to the access to the source code and rights to copy,
-- modify and redistribute granted by the license, users are provided
-- only with a limited warranty and the software's author, the holder of
-- the economic rights, and the successive licensors have only limited
-- liability.
-- 
-- In this respect, the user's attention is drawn to the risks associated
-- with loading, using, modifying and/or developing or reproducing the
-- software by the user in light of its specific status of free software,
-- that may mean that it is complicated to manipulate, and that also
-- therefore means that it is reserved for developers and experienced
-- professionals having in-depth computer knowledge. Users are therefore
-- encouraged to load and test the software's suitability as regards
-- their requirements in conditions enabling the security of their
-- systems and/or data to be ensured and, more generally, to use and
-- operate it in the same conditions as regards security.
-- 
-- The fact that you are presently reading this means that you have had
-- knowledge of the CeCILL license and that you accept its terms.
-- ======================================================================
-- 
-------------------------- sit_OPquot.as --------------------------
-- Copyright (c) Manuel Bronstein 1995
-- Copyright (c) INRIA 1999, Version 0.1.12
-- Logiciel Sum^it �INRIA 1999, dans sa version 0.1.12
-- Copyright (c) Swiss Federal Polytechnic Institute Zurich, 1995-97
-----------------------------------------------------------------------------

#include "sumit"
#include "uid"

#if ALDOC
\thistype{ExpressionTreeQuotient}
\History{Manuel Bronstein}{21/11/94}{created}
\Usage{import from \this}
\Descr{\this~is the division operator for expression trees.}
\begin{exports}
\category{\astype{ExpressionTreeOperator}}\\
\end{exports}
#endif

macro {
	Z	== MachineInteger;
	TEXT	== TextWriter;
	TREE 	== ExpressionTree;
}

ExpressionTreeQuotient:ExpressionTreeOperator == add {
	import from String, TREE, ExpressionTreeOperatorTools;

	name:Symbol			== -"/";
	arity:Z				== 2;
	uniqueId:Z			== UID__DIVIDE;
	texParen?(p:Z):Boolean		== p > TEXPREC__DIVIDE;
	axiom(p:TEXT, l:List TREE):TEXT	== trav(p, l, axiom);
	aldor(p:TEXT,l:List TREE):TEXT	== trav(p, l, aldor);
	maple(p:TEXT, l:List TREE):TEXT	== trav(p, l, maple);
	fortran(p:TEXT,l:List TREE):TEXT== trav(p, l, fortran);
	C(p:TEXT,l:List TREE):TEXT	== trav(p, l, C);

	tex(p:TEXT, l:List TREE):TEXT == {
		--tex(tex(p << "\frac{", first l) << "}{", first rest l) << "}";
		tex(tex(p << "{{",first l) << "} \over {",first rest l) << "}}";
	}

	local trav(p:TEXT, l:List TREE, f:(TEXT, TREE) -> TEXT):TEXT ==
		infix(p, l, (t:TREE):Boolean +-> true, f, "/");

}
