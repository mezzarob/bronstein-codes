# ======================================================================
# This code was written all or part by Dr. Manuel Bronstein from
# Inria-CAFE project team. After his sudden death on June 6, 2005, Inria
# decided to publish this code under the CeCILL open source license in
# memory of Dr. Manuel Bronstein.
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and Inria at the following URL :
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
# 
# As a counterpart to the access to the source code and rights to copy,
# modify and redistribute granted by the license, users are provided
# only with a limited warranty and the software's author, the holder of
# the economic rights, and the successive licensors have only limited
# liability.
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading, using, modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate, and that also
# therefore means that it is reserved for developers and experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards
# their requirements in conditions enabling the security of their
# systems and/or data to be ensured and, more generally, to use and
# operate it in the same conditions as regards security.
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# ======================================================================
# 
objects   = spf.o spf231.o zpf.o bpf.o mpf.o pfcat.o spfcat.o modla.o henselsolve.o
asobjects = spf.ao spf231.ao zpf.ao  bpf.ao mpf.ao pfcat.ao spfcat.ao modla.ao henselsolve.ao
options	  = -M2 -Fao -Fo -Q3 -Csmax=0 -Mno-mactext
tex       = spf.tex spf231.tex zpf.tex bpf.tex mpf.tex pfcat.tex spfcat.tex modla.tex
test      = Tspf.as Tspf231.as Tzpf.as Tbpf.as Tmpf.as
testdir	  = ../../test
libsumit  = ../../lib/libsumit.a
sumitasl  = ../../lib/libsumit.al

all: datestamp

datestamp: $(objects)
	ar rv $(libsumit) $(objects)
	touch datestamp

pfcat.o: pfcat.as ../polyfactorp/pffactor.o ../univpoly/categories/fring.o
	axiomxl $(options) pfcat.as
	ar rv $(sumitasl) pfcat.ao

spfcat.o: spfcat.as pfcat.o ../univpoly/gcd/modpgcd.o ../univpoly/categories/ugring.o ../univpoly/fft.o ../categories/CATspf0.o
	axiomxl $(options) spfcat.as
	ar rv $(sumitasl) spfcat.ao

spf.o: spf.as spfcat.o
	axiomxl $(options) spf.as
	ar rv $(sumitasl) spf.ao

zpf.o: zpf.as spfcat.o ../numbers/primroot.o
	axiomxl $(options) zpf.as
	ar rv $(sumitasl) zpf.ao

spf231.o: spf231.as spfcat.o ../util/stdutil.o
	axiomxl $(options) spf231.as
	ar rv $(sumitasl) spf231.ao

bpf.o: bpf.as pfcat.o
	axiomxl $(options) bpf.as
	ar rv $(sumitasl) bpf.ao

mpf.o: mpf.as pfcat.o
	axiomxl $(options) mpf.as
	ar rv $(sumitasl) mpf.ao

henselsolve.o: henselsolve.as ../categories/CATprime0.o ../mat/categories/matcat0.o
	axiomxl $(options) henselsolve.as
	ar rv $(sumitasl) henselsolve.ao

modla.o: modla.as henselsolve.o ../mat/matdense.o ../mat/gauss/modpge.o ../categories/chrem.o ../numbers/lwordprimes.o spf.o
	axiomxl $(options) modla.as
	ar rv $(sumitasl) modla.ao

.PHONY: doc
doc: $(tex)

pfcat.tex: pfcat.as
	as2doc pfcat

spfcat.tex: spfcat.as
	as2doc spfcat

bpf.tex: bpf.as
	as2doc bpf

mpf.tex: mpf.as
	as2doc mpf

spf.tex: spf.as
	as2doc spf

zpf.tex: zpf.as
	as2doc zpf

spf231.tex: spf231.as
	as2doc spf231

modla.tex: modla.as
	as2doc modla

.PHONY: test
test: $(test)

Tbpf.as: bpf.as
	as2test bpf
	cp Tbpf.as $(testdir)

Tmpf.as: mpf.as
	as2test mpf
	cp Tmpf.as $(testdir)

Tspf.as: spf.as
	as2test spf
	cp Tspf.as $(testdir)

Tzpf.as: zpf.as
	as2test zpf
	cp Tzpf.as $(testdir)

Tspf231.as: spf231.as
	as2test spf231
	cp Tspf231.as $(testdir)

.PHONY: clean
clean:
	-rm $(objects) $(asobjects) $(tex) $(test)

